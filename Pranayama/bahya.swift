//
//  bahya.swift
//  Pranayama
//
//  Created by Manish Kumar on 9/13/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

import UIKit

class bahya: GAITrackedViewController {

    @IBOutlet var inhalePicker: UIPickerView!
    @IBOutlet var exhalepkr: UIPickerView!
    @IBOutlet var holdpkr: UIPickerView!
    @IBOutlet var roundPicker: UIPickerView!
    var InhaleRow : Int = 0
    var ExhaleRow : Int = 0
    var holdRow : Int = 0
    var RoundRow : Int = 0
    var in_roundData : NSMutableArray? = NSMutableArray()
    var exhaleOdata : NSMutableArray? = NSMutableArray()
    var holddata : NSMutableArray?=NSMutableArray()

    @IBOutlet var topview: UIView!
    @IBOutlet var img: UIImageView!
    @IBOutlet var inhalelbl: UILabel!
    @IBOutlet var exhalelbl: UILabel!
    @IBOutlet var holdlbl: UILabel!
    @IBOutlet var roundlbl: UILabel!
    @IBOutlet var btnLeftInhel: UIButton!
    @IBOutlet var btnRightExhale: UIButton!
    @IBOutlet var StartButton: UIButton!
    @IBOutlet var reportButton: UIButton!
    @IBOutlet var lblHours: UILabel!
    @IBOutlet var lblminutes: UILabel!
    @IBOutlet var lblseconds: UILabel!
    var totaltime = 0
    var seconds = 0
    var minutes = 0
    var hours = 0
    var button1: UIButton!
    var button2: UIButton!
    var button3: UIButton!
    var isMenuVisible = false
    @IBOutlet var Floatingbutton: UIButton!
    var selectCondition = false
    var buttonCondition = false
    var timer1: Timer!
    var TodayFitness = ""
    var Daystr = ""
    var session = ""
    var Your_leve = ""
    var Fitness_regime_21 = ""
    var Share_experience = ""
    
    ///////
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: (#selector(self.appDidBecomeActive(_:))), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        self.screenName="Bahya"
        Floatingbutton?.layer.cornerRadius=(Floatingbutton?.bounds.size.width)!/2.0
        var fontSize : CGFloat?
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            fontSize=24
        }
        else{
            fontSize=18
        }
        UINavigationBar.appearance().titleTextAttributes=[NSFontAttributeName: Ralewayfont(fontSize!),NSForegroundColorAttributeName:UIColor.white]
        
        for i in 1...50 {
            in_roundData?.add("\(i)")
        }
        for i in 1...100 {
            exhaleOdata?.add("\(i)")
        }
        for i in 1...150 {
            holddata?.add("\(i)")
        }
        self.introduction()
        self.setlblheight()
        self.setupbutton()
        self.getvalue()
        self.setFont()
        
        if UserDefaults.standard.bool(forKey: DincharyaMainCon) && UserDefaults.standard.bool(forKey: DincharyaDelegate) && UserDefaults.standard.bool(forKey: commonClassCond) {
            self.perform(#selector(self.navigationMove), with: nil, afterDelay: 0.5)
        }
        else{
            if UserDefaults.standard.bool(forKey: DincharyaMainCon) {
                let name = UserDefaults.standard.object(forKey: dincharyaName) as! String
                let ar = (UIApplication.shared.delegate! as! AppDelegate).fetchDincharyaFromDatabaseAcording(toName: name) as NSArray
                if ar.count>0 {
                    let DIn = ar.object(at: 0) as! Dincharya
                    if DIn.autometicTime != nil {
                        self.perform(#selector(self.autometicMove), with: nil, afterDelay: 0.5)
                    }
                }
            }
        }
    }
    
    func navigationMove() -> Void {
        CommonSounds.sharedInstance().checkStart(self.navigationController, identifier: "bahya")
    }
    
    func autometicMove() -> Void {
        self.performSegue(withIdentifier: "bahyaAction", sender: self)
    }
    
    func appDidBecomeActive(_ notification:Notification) -> Void {
        CommonSounds.sharedInstance().appDidBecomeActive(notification, navigation: self.navigationController)
    }
    
    ////////////// introduction /////////
    
    func introduction() -> Void {
        let intro = UserDefaults.standard.bool(forKey: Introduction)
        if intro {
            CommonSounds.sharedInstance().instructions(self.view)
        }
        else{
            Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: (#selector(Udgeeth.yourMethodName)), userInfo: nil, repeats: false)
        }
    }
    func yourMethodName() -> Void {
        StartButton?.isUserInteractionEnabled=true
    }
    //////////////
    
    func setlblheight() -> Void {
        let device : Int32 = Int32(UIScreen.main.bounds.size.height)
        switch device {
        case iPhone4:
            img.frame = CGRect(x: 68, y: 186, width: 186, height: 180)
            break
        case iPhone5:
            img.frame = CGRect(x: 68, y: 205, width: 186, height: 230)
            break
        case iPhone6:
            img.frame = CGRect(x: 63, y: 225, width: 250, height: 290)
            break
        case iPhone6P:
            img.frame = CGRect(x: 63, y: 240, width: 290, height: 330)
            break
        default:
            break
        }
        if !(UI_USER_INTERFACE_IDIOM() == .pad) {
            topview.frame = CGRect(x: 8, y:74 , width: self.view.frame.size.width-16, height: 32)
        }
    }
    
    func setfont() -> Void {
        var fontSize : CGFloat?
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            fontSize=25
        }
        else{
            fontSize=12
        }
        inhalelbl!.font = Ralewayfont(fontSize!)
        exhalelbl!.font = Ralewayfont(fontSize!)
        holdlbl!.font = Ralewayfont(fontSize!)
        roundlbl!.font = Ralewayfont(fontSize!)
        
    }
    
    func ChangeText() {
        
        self.navigationItem.title = MCLocalization.string(forKey: "bahya")!
        inhalelbl!.text = MCLocalization.string(forKey: "Inhale")!
        exhalelbl!.text = MCLocalization.string(forKey: "Exhale")!
        holdlbl!.text = MCLocalization.string(forKey: "Hold")!
        roundlbl!.text = MCLocalization.string(forKey: "Rounds")!
        TodayFitness = MCLocalization.string(forKey: "Today_Fitness")!
        Daystr = MCLocalization.string(forKey: "day")!
        Your_leve = MCLocalization.string(forKey: "Your_leve")!
        Fitness_regime_21 = MCLocalization.string(forKey: "Fitness_regime_21")!
        Share_experience = MCLocalization.string(forKey: "Share_experience")!
        session = MCLocalization.string(forKey: "session")!
    }
    
    @IBAction func backbutton(_ sender: AnyObject) {
        self.navigationController!.popViewController(animated: true)
    }
    
    func setFont() {
        var fontSize: CGFloat
        if UI_USER_INTERFACE_IDIOM() == .pad {
            fontSize = 25
        }
        else {
            fontSize = 12
        }
        inhalelbl!.font = Ralewayfont(fontSize)
        exhalelbl!.font = Ralewayfont(fontSize)
        holdlbl!.font = Ralewayfont(fontSize)
        roundlbl!.font = Ralewayfont(fontSize)
    }
    
    ////Play finish sound
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.ChangeText()
        self.disableSlidePanGestureForRightMenu()
        if UserDefaults.standard.bool(forKey: "sound") {
            UserDefaults.standard.set(false, forKey: "sound")
            CommonSounds.sharedInstance().playFinishSound()
        }
        
        if UserDefaults.standard.bool(forKey: HabbitMainCondition) && UserDefaults.standard.bool(forKey: viewwillCondition) {
            UserDefaults.standard.set(false, forKey: HabbitMainCondition)
            let f = DateFormatter()
            f.dateFormat = "yyyy-MM-dd"
            let start = f.string(from: UserDefaults.standard.object(forKey: HabbitStartDate)! as! Date)
            let end = f.string(from: Date())
            let startDate = f.date(from: start)!
            let endDate = f.date(from: end)!
            let gregorianCalendar = Calendar(identifier: Calendar.Identifier.gregorian)
            let components = (gregorianCalendar as NSCalendar).components(.day, from: startDate, to: endDate, options: [])
            let a = components.day
            let level = (a! / 21)
            var day = (a! % 21)
            if day == 0 {
                day = 21
            }
            let title = "\(Daystr) \(day)"
        
            if day == 21 {
                let messagestr = "\(Your_leve) \(Int(level)) \(Fitness_regime_21) \(Int(level) + 1) \(Share_experience)"
                if #available(iOS 8.0, *) {
                    let alert = UIAlertController(title: title, message: messagestr, preferredStyle:UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style:UIAlertActionStyle.default, handler:nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
                let titile = "\(Daystr) \(day) "
                if #available(iOS 8.0, *) {
                    let alert = UIAlertController(title: titile, message: TodayFitness, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            UserDefaults.standard.set(false, forKey: viewwillCondition)
        }
        if UserDefaults.standard.bool(forKey: DincharyaMainCon) && UserDefaults.standard.bool(forKey: viewwillCondition) {
            self.perform(#selector(self.Dinchariya_NextMethod), with: nil, afterDelay: 0.2)
            UserDefaults.standard.set(false, forKey: viewwillCondition)
        }
    }
    


    func Dinchariya_NextMethod() -> Void {
        let PranaAr = UserDefaults.standard.object(forKey: dincharyaPranaAr) as! NSArray
        var arr : NSMutableArray! = NSMutableArray()
        arr = NSMutableArray(array: PranaAr)
        arr.remove("bahya")
        var Yarray : NSArray = NSArray()
        Yarray = arr.copy() as! NSArray
        
        if Yarray.count != 0 {
            UserDefaults.standard.set(Yarray, forKey: dincharyaPranaAr)
            UserDefaults.standard.synchronize()
            
            let home = self.storyboard?.instantiateViewController(withIdentifier:"home") as! HomeTabelVC
            let dincharyavc = self.storyboard?.instantiateViewController(withIdentifier: "dincharya") as! DincharyaVC
            let dinName = self.storyboard?.instantiateViewController(withIdentifier: "dincharya_name") as! DincharyaNameVC
            let pranavc = self.storyboard?.instantiateViewController(withIdentifier: "dincharya_prana") as! Dincharya_pranaVC
            let prana = (self.storyboard?.instantiateViewController(withIdentifier: Yarray.object(at: 0) as! String))! as UIViewController
            self.navigationController?.setViewControllers([home,dincharyavc,dinName,pranavc,prana], animated: true)
        }
        else{
            UserDefaults.standard.set(nil, forKey: dincharyaPranaAr)

            if #available(iOS 8.0, *) {
                let alert = UIAlertController(title: session, message: TodayFitness , preferredStyle:UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style:UIAlertActionStyle.default, handler:{ action in
                    CommonSounds.sharedInstance().commonAlertView(self.navigationController)
                    }))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    @IBAction func reset(_ sender: AnyObject) {
        if InhaleRow == 1 && ExhaleRow == 2 && holdRow == 3 && RoundRow == 1 {
            selectCondition = false
        }
        else {
            selectCondition = true
        }
        InhaleRow = 1
        ExhaleRow = 2
        holdRow = 3
        RoundRow = 1
        inhalePicker!.selectRow(InhaleRow - 1, inComponent: 0, animated: true)
        exhalepkr!.selectRow(ExhaleRow - 1, inComponent: 0, animated: true)
        holdpkr!.selectRow(holdRow - 1, inComponent: 0, animated: true)
        roundPicker!.selectRow(RoundRow - 1, inComponent: 0, animated: true)
        self.setvalueInkey()
        self.getTotalTime()
        Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.yourMethodName), userInfo: nil, repeats: false)
    }
    
    func getvalue() {
        if UserDefaults.standard.integer(forKey: bahyaInhale)==0 {
            UserDefaults.standard.set(1, forKey: bahyaInhale)
            UserDefaults.standard.set(2, forKey: bahyaExhale)
            UserDefaults.standard.set(3, forKey: bahyahold)
            UserDefaults.standard.set(1, forKey: bahyaRound)
        }
        InhaleRow = UserDefaults.standard.integer(forKey: bahyaInhale)
        ExhaleRow = UserDefaults.standard.integer(forKey: bahyaExhale)
        holdRow = UserDefaults.standard.integer(forKey: bahyahold)
        RoundRow = UserDefaults.standard.integer(forKey: bahyaRound)
        inhalePicker!.selectRow(InhaleRow - 1, inComponent: 0, animated: true)
        exhalepkr!.selectRow(ExhaleRow - 1, inComponent: 0, animated: true)
        holdpkr!.selectRow(holdRow - 1, inComponent: 0, animated: true)
        roundPicker!.selectRow(RoundRow - 1, inComponent: 0, animated: true)
        self.getTotalTime()
    }
    
    func setvalueInkey() {
        UserDefaults.standard.set(InhaleRow, forKey: bahyaInhale)
        UserDefaults.standard.set(ExhaleRow, forKey: bahyaExhale)
        UserDefaults.standard.set(holdRow, forKey: bahyahold)
        UserDefaults.standard.set(RoundRow, forKey: bahyaRound)
    }
    
    @IBAction func inhaleButtonPressed(_ sender: UIButton) {
        buttonCondition = true
        if sender.tag == 1 {
            if InhaleRow == 1 {
                InhaleRow = 1
                ExhaleRow = InhaleRow * 2
                holdRow = InhaleRow * 3
                inhalePicker!.selectRow(InhaleRow - 1, inComponent: 0, animated: true)
                exhalepkr!.selectRow(ExhaleRow - 1, inComponent: 0, animated: true)
                holdpkr!.selectRow(holdRow - 1, inComponent: 0, animated: true)
            }
            else {
                InhaleRow = InhaleRow - 1
                ExhaleRow = InhaleRow * 2
                holdRow = InhaleRow * 3
                inhalePicker!.selectRow(InhaleRow - 1, inComponent: 0, animated: true)
                exhalepkr!.selectRow(ExhaleRow - 1, inComponent: 0, animated: true)
                holdpkr!.selectRow(holdRow - 1, inComponent: 0, animated: true)
            }
        }
        else if sender.tag == 2 {
            if InhaleRow == 50 {
                InhaleRow = 50
                ExhaleRow = InhaleRow * 2
                holdRow = InhaleRow * 3
                inhalePicker!.selectRow(InhaleRow - 1, inComponent: 0, animated: true)
                exhalepkr!.selectRow(ExhaleRow - 1, inComponent: 0, animated: true)
                holdpkr!.selectRow(holdRow - 1, inComponent: 0, animated: true)
            }
            else {
                InhaleRow = InhaleRow + 1
                ExhaleRow = InhaleRow * 2
                holdRow = InhaleRow * 3
                inhalePicker!.selectRow(InhaleRow - 1, inComponent: 0, animated: true)
                exhalepkr!.selectRow(ExhaleRow - 1, inComponent: 0, animated: true)
                holdpkr!.selectRow(holdRow - 1, inComponent: 0, animated: true)
            }
        }
        
        self.getTotalTime()
        self.setvalueInkey()
        timer1?.invalidate()
        timer1 = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(self.yourMethodName1), userInfo: nil, repeats: false)
    }
    
    func yourMethodName1() {
        buttonCondition = false
        StartButton!.isUserInteractionEnabled = true
    }
    
    func getTotalTime() {
        totaltime = ((InhaleRow + ExhaleRow + holdRow) * RoundRow)
        lblseconds!.text = "\(totaltime)"
        seconds = totaltime % 60
        minutes = (totaltime / 60) % 60
        hours = totaltime / 3600
        lblseconds!.text = "\(seconds)"
        lblminutes!.text = "\(minutes)"
        lblHours!.text = "\(hours)"
    }
    
    ////////////Delegate methods///////
    
    func numberOfComponentsInPickerView(_ thePickerView: UIPickerView) -> NSInteger {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        var height: CGFloat?
        if UI_USER_INTERFACE_IDIOM() == .pad {
            height = 40
        }
        else {
            height = 30
        }
        return height!
    }
    
    func pickerView(_ thePickerView: UIPickerView, numberOfRowsInComponent component: Int) -> NSInteger {
        var i = 0
        if thePickerView.isEqual(inhalePicker) || thePickerView.isEqual(roundPicker) {
            i = 50
        }
        else if thePickerView.isEqual(exhalepkr) {
            i = 100
        }
        else if thePickerView.isEqual(holdpkr) {
            i = 150
        }
        return i
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.isEqual(inhalePicker) {
            InhaleRow = Int((in_roundData![row] as? String)!)!
        }
        else if pickerView.isEqual(exhalepkr) {
            ExhaleRow = Int((exhaleOdata![row] as? String)!)!
        }
        else if pickerView.isEqual(holdpkr) {
            holdRow = Int((holddata![row] as? String)!)!
        }
        else {
            RoundRow = Int((in_roundData![row] as? String)!)!
        }
        
        StartButton!.isUserInteractionEnabled = true
        selectCondition = false
        self.getTotalTime()
        self.setvalueInkey()
    }
    
    func pickerView1(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.isEqual(inhalePicker) {
            InhaleRow = Int((in_roundData![row] as? String)!)!
        }
        else if pickerView.isEqual(exhalepkr) {
            ExhaleRow = Int((exhaleOdata![row] as? String)!)!
        }
        else if pickerView.isEqual(holdpkr) {
            holdRow = Int((holddata![row] as? String)!)!
        }
        else {
            RoundRow = Int((in_roundData![row] as? String)!)!
        }
        
        timer1?.invalidate()
        timer1 = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(self.yourMethodName), userInfo: nil, repeats: false)
        self.getTotalTime()
        self.setvalueInkey()
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView) -> UIView? {
        var tView = (view as! UILabel)
        //        if !tView {
        tView = UILabel()
        if UI_USER_INTERFACE_IDIOM() == .pad {
            tView.font = UIFont.systemFont(ofSize: 40)
        }
        else {
            tView.font = UIFont.systemFont(ofSize: 22)
        }
        tView.textAlignment = .center
        if pickerView.isEqual(inhalePicker) || pickerView.isEqual(roundPicker) {
            tView.text = in_roundData![row] as? String
        }
        else if pickerView.isEqual(exhalepkr) {
            tView.text = exhaleOdata![row] as? String
        }
        else if pickerView.isEqual(holdpkr) {
            tView.text = holddata![row] as? String
        }
        //        }
        if buttonCondition == true {
            
        }
        else if selectCondition == false {
            self.pickerView1(pickerView, didSelectRow: row, inComponent: component)
        }
        else {
            Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.MethodName), userInfo: nil, repeats: false)
        }
        
        StartButton!.isUserInteractionEnabled = false
        pickerView.subviews[1].isHidden = true
        pickerView.subviews[2].isHidden = true
        return tView
    }
    
    func MethodName() {
        selectCondition = false
    }
    
    
    func setupbutton() -> Void {
        //////// floating button width ////
        var a : CGFloat?
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            if (self.view.frame.size.height == 1366) {
                a=80
            }
            else{
                a=60
            }
        }
        else{
            a=35
        }
        
        let floatFrame = CGRect(x: (Floatingbutton!.frame.origin.x + 5), y: (Floatingbutton!.frame.origin.y + 5), width: a!, height: a!)
        button1 = UIButton(frame: floatFrame)
        let color = UIColor(red: 255/255, green: 154/255, blue: 0/255, alpha: 1.0)
        
        button1!.backgroundColor = color
        button1!.layer.cornerRadius = (button1!.bounds.size.width - 2) / 2
        button1!.tag = 11
        button1!.addTarget(self, action: #selector(self.buttonmethod), for: .touchDown)
        button1!.imageEdgeInsets = UIEdgeInsetsMake(7, 7, 7, 7)
        button1!.setImage(UIImage(named: "help")!, for: UIControlState())
        button2 = UIButton(frame: floatFrame)
        button2!.backgroundColor = color
        button2!.layer.cornerRadius = (button2!.bounds.size.width - 2) / 2
        button2!.tag = 12
        button2!.addTarget(self, action: #selector(self.buttonmethod), for: .touchDown)
        button2!.imageEdgeInsets = UIEdgeInsetsMake(7, 7, 7, 7)
        button2!.setImage(UIImage(named: "benefits_ic")!, for: UIControlState())
        button3 = UIButton(frame: floatFrame)
        button3!.backgroundColor = color
        button3.layer.cornerRadius = (button3.bounds.size.width - 2) / 2
        button3.tag = 13
        button3.addTarget(self, action: #selector(self.buttonmethod), for: .touchDown)
        button3.imageEdgeInsets = UIEdgeInsetsMake(7, 7, 7, 7)
        button3.setImage(UIImage(named: "about_ii")!, for: UIControlState())
        
    }
    
    @IBAction func startbutton(_ sender: AnyObject) {
        self.dismissMenu()
    }
    
    @IBAction func reportbutton(_ sender: AnyObject) {
        self.dismissMenu()
    }
    
    @IBAction func floatingbutton(_ sender: AnyObject) {
        if isMenuVisible == true {
            self.dismissMenu()
        }
        else {
            self.showMenu()
        }
    }
    
    func buttonmethod(_ sender: UIButton) {
        if sender.tag == 11 {
            let help = self.storyboard!.instantiateViewController(withIdentifier: "help") as! HelpViewController
            help.type="Bahya"
            self.navigationController?.pushViewController(help, animated: true)
        }
        else if sender.tag == 12 {
            let benefit = self.storyboard!.instantiateViewController(withIdentifier: "benefit") as! BenefitViewController
            benefit.type="Bahya"
            self.navigationController?.pushViewController(benefit, animated: true)
        }
        else {
            let about = self.storyboard!.instantiateViewController(withIdentifier: "about") as! AboutViewController
            about.type="Bahya"
            self.navigationController?.pushViewController(about, animated: true)
        }
        
        button1!.alpha = 0
        button2!.alpha = 0
        button3!.alpha = 0
        isMenuVisible = false
    }
    
    func showMenu() {
        var Point1: CGPoint
        var Point2: CGPoint
        var Point3: CGPoint
        if UI_USER_INTERFACE_IDIOM() == .pad {
            Point1 = CGPoint(x: -8, y: -172)
            Point2 = CGPoint(x: -95, y: -115)
            Point3 = CGPoint(x: -150, y: -20)
        }
        else {
            Point1 = CGPoint(x: -10, y: -103)
            Point2 = CGPoint(x: -62, y: -73)
            Point3 = CGPoint(x: -90, y: -20)
        }
        Floatingbutton!.isUserInteractionEnabled = false
        UIButton.animate(withDuration: 0.5, animations: {() -> Void in
            UIButton.animate(withDuration: 2.0, delay: 0.0, options: [.repeat, .curveLinear], animations: {() -> Void in
                let transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
                self.button1!.transform = transform
                self.button2!.transform = transform
                self.button3!.transform = transform
                }, completion: { _ in })
            self.button1!.transform = CGAffineTransform(translationX: Point1.x, y: Point1.y)
            self.button2!.transform = CGAffineTransform(translationX: Point2.x, y: Point2.y)
            self.button3!.transform = CGAffineTransform(translationX: Point3.x, y: Point3.y)
            self.view.addSubview(self.button1!)
            self.view.addSubview(self.button2!)
            self.view.addSubview(self.button3!)
            self.button1!.alpha = 0.7
            self.button2!.alpha = 0.7
            self.button3!.alpha = 0.7
            }, completion: {(finished: Bool) -> Void in
                self.button1!.alpha = 1
                self.button2!.alpha = 1
                self.button3!.alpha = 1
                self.Floatingbutton!.isUserInteractionEnabled = true
                self.isMenuVisible = true
        })
    }
    
    
    func dismissMenu() {
        Floatingbutton!.isUserInteractionEnabled = false
        UIButton.animate(withDuration: 0.5, animations: {() -> Void in
            UIButton.animate(withDuration: 2.0, delay: 0.0, options: [.beginFromCurrentState, .curveLinear], animations: {() -> Void in
                let transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
                self.button1!.transform = transform
                self.button2!.transform = transform
                self.button3!.transform = transform
                }, completion: { _ in })
            self.button1!.transform = CGAffineTransform(translationX: 0, y: 0)
            self.button2!.transform = CGAffineTransform(translationX: 0, y: 0)
            self.button3!.transform = CGAffineTransform(translationX: 0, y: 0)
            self.button1!.alpha = 0.3
            self.button2!.alpha = 0.3
            self.button3!.alpha = 0.3
            }, completion: {(finished: Bool) -> Void in
                self.button1!.alpha = 0
                self.button2!.alpha = 0
                self.button3!.alpha = 0
                self.Floatingbutton!.isUserInteractionEnabled = true
                self.isMenuVisible = false
        })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if (isMenuVisible == true) {
            self.dismissMenu()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "bahyaAction" {
            let detailVC = segue.destination as! bahyaAction;
            detailVC.totalTime = totaltime
            detailVC.lblinhale = InhaleRow
            detailVC.lblexhale = ExhaleRow
            detailVC.lblhold = holdRow
            detailVC.rounds = RoundRow
        }
    }
}


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */



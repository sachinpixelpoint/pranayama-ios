//
//  YogasanViewController.m
//  Pranayama
//
//  Created by Manish Kumar on 6/14/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "YogasanListVC.h"

@interface YogasanListVC ()

@end

@implementation YogasanListVC
@synthesize table;

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    self.screenName=@"Yogasana Table";
     self.navigationController.navigationBar.tintColor =RGB(230, 123, 27);
    if (DEVICE==IPAD) {
        fontSize=24;
        fontSize2=16;
    }
    else{
        fontSize=18;
        fontSize2=10;
    }

    self.automaticallyAdjustsScrollViewInsets=NO;
    table.scrollEnabled = NO;
    self.navigationItem.hidesBackButton=YES;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"McLaren-Regular" size:fontSize]}];
    int a=(self.view.frame.size.height-64)/7;
    table.rowHeight=a;
   
}


- (void)HomeMethod:(NSNotification *)notification {
    [[CommonSounds sharedInstance] appDidBecomeActive:notification navigation:self.navigationController];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //If this vc can be poped , then
    if (self.navigationController.viewControllers.count > 1)
    {
        // Disabling pan gesture for left menu
        //        [self disableSlidePanGestureForLeftMenu];
    }
    
    AMSlideMenuMainViewController *mainVC = [AMSlideMenuMainViewController getInstanceForVC:self];
    if (mainVC.rightMenu)
    {
        // Adding right menu button to navigation bar
        [self addRightMenuButton];
    }
    [[NSUserDefaults standardUserDefaults] setObject:@"YogasanaList" forKey:controllerType];
    [self ChangeText];
//    self.bannerView.adUnitID = @"ca-app-pub-0501979451055365/6725248513";
//    self.bannerView.rootViewController = self;
//    [self.bannerView loadRequest:[GADRequest request]];
}


-(void)ChangeText{
    NSString *habbitType=[[NSUserDefaults standardUserDefaults] objectForKey:HabbitType];
    NSString *lan=[[NSUserDefaults standardUserDefaults] objectForKey:Languagevalue];
    if ([lan isEqualToString:Hindi]) {
        self.navigationItem.title = [MCLocalization stringForKey:@"yogasan"];
        if ([habbitType isEqualToString:@"Thyroid"]) {
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Sarvangasana"],[MCLocalization stringForKey:@"Halasana"],  nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"Sarvangasana",@"Halasana", nil];
        }
        else if ([habbitType isEqualToString:@"Insomnia"]){
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Vipritkarani"],[MCLocalization stringForKey:@"Paschimottanasana"],  nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"Vipritkarani",@"Paschimottanasana", nil];
        }
        else if ([habbitType isEqualToString:@"Diabetes"]){
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Dhanurasana"],[MCLocalization stringForKey:@"Balasana"],  nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"Dhanurasana",@"Balasana", nil];
        }
        else if ([habbitType isEqualToString:@"Migraine"])
        {
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Hastapadasana"],[MCLocalization stringForKey:@"Marjariasana"],  nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"Hastapadasana",@"Marjariasana", nil];
        }
        else if ([habbitType isEqualToString:@"Weight Loss"])
        {
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Sarvangasana"],[MCLocalization stringForKey:@"Setu Bandhasana"],  nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"Sarvangasana",@"Setu Bandhasana", nil];
        }

        else if ([habbitType isEqualToString:@"Asthma"])
        {
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Dhanurasana"],[MCLocalization stringForKey:@"Uttanasana"],  nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"Dhanurasana",@"Uttanasana", nil];
        }

        else 
        {
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Balasana"],[MCLocalization stringForKey:@"Virabhadrasana"],  nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"Balasana",@"Virabhadrasana", nil];
        }

    }
    
    else if([lan isEqualToString:English]){
        self.navigationItem.title= @"Yogasana";
        if ([habbitType isEqualToString:@"Thyroid"]) {
            NSString *Sarvangasanastr=[@"Sarvangasana " stringByAppendingString:@"(Shoulder Stand)"];
            NSMutableAttributedString *Sarvangasana = [[NSMutableAttributedString alloc] initWithString:Sarvangasanastr];
            [Sarvangasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 12 )];
            [Sarvangasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(12, 17 )];
            
            NSString *Halasanastr=[@"Halasana " stringByAppendingString:@"(Plow Pose)"];
            NSMutableAttributedString *Halasana = [[NSMutableAttributedString alloc] initWithString:Halasanastr];
            [Halasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 8 )];
            [Halasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(8, 12 )];
            
            homelist=[[NSMutableArray alloc] initWithObjects:Sarvangasana,Halasana, nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"Sarvangasana",@"Halasana", nil];
        }
        else if ([habbitType isEqualToString:@"Insomnia"]){
            NSString *Vipritkaranistr=[@"Vipritkarani " stringByAppendingString:@"(Legs up the Wall)"];
            NSMutableAttributedString *Vipritkarani = [[NSMutableAttributedString alloc] initWithString:Vipritkaranistr];
            [Vipritkarani addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 12)];
            [Vipritkarani addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(12, 19)];
            
            NSString *Pashimothanasanastr=[@"Paschimottanasana " stringByAppendingString:@"(Seated Forward Bend)"];
            NSMutableAttributedString *Pashimothanasana = [[NSMutableAttributedString alloc] initWithString:Pashimothanasanastr];
            [Pashimothanasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 17 )];
            [Pashimothanasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(17, 22 )];
        
            homelist=[[NSMutableArray alloc] initWithObjects:Vipritkarani,Pashimothanasana, nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"Vipritkarani",@"Paschimottanasana", nil];
        }
        else if ([habbitType isEqualToString:@"Diabetes"]){
            NSString *Dhanurasanastr=[@"Dhanurasana " stringByAppendingString:@"(Bow Pose)"];
            NSMutableAttributedString *Dhanurasana = [[NSMutableAttributedString alloc] initWithString:Dhanurasanastr];
            [Dhanurasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 11 )];
            [Dhanurasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(11, 11 )];
            
            NSString *Balasanastr=[@"Balasana " stringByAppendingString:@"(Child Pose)"];
            NSMutableAttributedString *Balasana = [[NSMutableAttributedString alloc] initWithString:Balasanastr];
            [Balasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 8 )];
            [Balasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(8, 13 )];
            
            homelist=[[NSMutableArray alloc] initWithObjects:Dhanurasana,Balasana, nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"Dhanurasana",@"Balasana", nil];
        }
        else if ([habbitType isEqualToString:@"Migraine"])
        {
            NSString *Hastapadasanastr=[@"Hastapadasana " stringByAppendingString:@"(Standing Forward Bend)"];
            NSMutableAttributedString *Hastapadasana = [[NSMutableAttributedString alloc] initWithString:Hastapadasanastr];
            [Hastapadasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 13 )];
            [Hastapadasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(13, 24 )];
            
            NSString *Marjariasanastr=[@"Marjariasana " stringByAppendingString:@"(Cat Stretch)"];
            NSMutableAttributedString *Marjariasana = [[NSMutableAttributedString alloc] initWithString:Marjariasanastr];
            [Marjariasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 12 )];
            [Marjariasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(12, 14 )];
            homelist=[[NSMutableArray alloc] initWithObjects:Hastapadasana,Marjariasana, nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"Hastapadasana",@"Marjariasana", nil];
        }
        else if ([habbitType isEqualToString:@"Weight Loss"])
        {
            NSString *Sarvangasanastr=[@"Sarvangasana " stringByAppendingString:@"(Shoulder Stand)"];
            NSMutableAttributedString *Sarvangasana = [[NSMutableAttributedString alloc] initWithString:Sarvangasanastr];
            [Sarvangasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 12 )];
            [Sarvangasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(12, 17 )];
            
            NSString *setustr=[@"Setu Bandhasana " stringByAppendingString:@"(Bridge Pose)"];
            NSMutableAttributedString *setu = [[NSMutableAttributedString alloc] initWithString:setustr];
            [setu addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 14 )];
            [setu addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(15, 14 )];
            homelist=[[NSMutableArray alloc] initWithObjects:Sarvangasana,setu, nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"Sarvangasana",@"Setu Bandhasana", nil];
        }

        else if ([habbitType isEqualToString:@"Asthma"])
        {
            NSString *Dhanurasanastr=[@"Dhanurasana " stringByAppendingString:@"(Bow Pose)"];
            NSMutableAttributedString *Dhanurasana = [[NSMutableAttributedString alloc] initWithString:Dhanurasanastr];
            [Dhanurasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 11 )];
            [Dhanurasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(11, 11 )];
            
            NSString *uttanasanastr=[@"Uttanasana " stringByAppendingString:@"(Standing Forward Fold)"];
            NSMutableAttributedString *uttanasana = [[NSMutableAttributedString alloc] initWithString:uttanasanastr];
            [uttanasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 10 )];
            [uttanasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(11, 23 )];
            homelist=[[NSMutableArray alloc] initWithObjects:Dhanurasana,uttanasana, nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"Dhanurasana",@"Uttanasana", nil];
        }
        else
        {
            NSString *Balasanastr=[@"Balasana " stringByAppendingString:@"(Child Pose)"];
            NSMutableAttributedString *Balasana = [[NSMutableAttributedString alloc] initWithString:Balasanastr];
            [Balasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 8 )];
            [Balasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(8, 13 )];
            
            NSString *virabhadrasanastr=[@"Virabhadrasana " stringByAppendingString:@"(Warrior Pose)"];
            NSMutableAttributedString *virabhadrasana = [[NSMutableAttributedString alloc] initWithString:virabhadrasanastr];
            [virabhadrasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 14 )];
            [virabhadrasana addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(14, 15 )];
            
            homelist=[[NSMutableArray alloc] initWithObjects:Balasana,virabhadrasana, nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"Balasana",@"Virabhadrasana", nil];
        }
    }
    else{
        ////////////////////
        /////Russion
        //////////////////
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)backbutton:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [homelist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell detail";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.backgroundColor=[UIColor clearColor];
    NSString *lan=[[NSUserDefaults standardUserDefaults] objectForKey:Languagevalue];
    if ([lan isEqualToString:Hindi]) {
        cell.textLabel.text=[homelist objectAtIndex:indexPath.row];
        cell.textLabel.font= Ralewayfont(fontSize);
    }
    else{
        cell.textLabel.attributedText=[homelist objectAtIndex:indexPath.row];
    }
    cell.textLabel.textColor=[UIColor whiteColor];
    
    cell.textLabel.numberOfLines=0;

    int colors = indexPath.row%2;
    switch (colors) {
        case 0:
            cell.backgroundColor=[UIColor colorWithRed:187/255.0 green:20/255.0 blue:37/255.0 alpha:0.5];
            break;
        case 1:
            cell.backgroundColor=[UIColor colorWithRed:176/255.0 green:12/255.0 blue:28/255.0 alpha:0.5];
            break;
        default:
            break;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index=indexPath.row;
    switch (index) {
        case 0:
            [[NSUserDefaults standardUserDefaults] setObject:[conditionArr objectAtIndex:0] forKey:YogasanCondition];
            break;
        case 1:
            [[NSUserDefaults standardUserDefaults] setObject:[conditionArr objectAtIndex:1] forKey:YogasanCondition];
            break;
               default:
            break;
    }
     [self performSegueWithIdentifier:@"pushstart" sender:self];
}



//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    if ([segue.identifier isEqualToString:@"pushstart"]) {
//        startViewController *controller = (startViewController *)segue.destinationViewController;
//        NSIndexPath *indexPath = (NSIndexPath *)sender;
//        controller.index=indexPath.row;
//    }
//
//}


@end

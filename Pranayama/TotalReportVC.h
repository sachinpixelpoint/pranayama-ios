//
//  TotalReportVC.h
//  Pranayama
//
//  Created by Manish Kumar on 04/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TotalReportVC : GAITrackedViewController<UIGestureRecognizerDelegate>
{
    IBOutlet UIView *topview;
    
    IBOutlet UILabel *datelbl;
    IBOutlet UILabel *durationlbl;
    IBOutlet UILabel *typelbl;
    
    NSMutableArray *reportArray;
    IBOutlet UITableView *tblReport;
    UITableViewCell *cell;
    CGPoint p;
    NSIndexPath *indexpath1;
    UILongPressGestureRecognizer *lpgr;
    UITapGestureRecognizer*tap;
    int cl;
    NSMutableArray *selectunselectarray;
    int entityFontSize;
}
@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
@property(nonatomic, readwrite, retain) UIView *backgroundView;
@end

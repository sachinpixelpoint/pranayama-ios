//
//  CommonSounds.h
//  Pranayama
//
//  Created by Manish Kumar on 08/06/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonSounds : NSObject
{
    float volumeValue;
    UIView *introview;
}
@property (nonatomic, strong) AVAudioPlayer *mySoundPlayer;
+(instancetype)sharedInstance;
-(void)playEverySound;
-(void)playChangeSound;
-(void)playKapalExhlaleSound;
-(void)playFinishSound;
-(void)playInhaleSound;
-(void)playHoldSound;
-(void)playExhaleSound;
-(void)instructions:(UIView*)view;
-(void)tableintro:(UITableView*)view;
-(void)NavigationMethod:(UINavigationController *)navigationController;
-(void)becomeActiveMethod:(UINavigationController *)navigationController;
-(void)becomeActivewithWater:(UINavigationController *)navigationController;
-(void)insertintoProgressReport:(BOOL)value durationValue:(int)durationCount isudgeeth:(BOOL)IsUdgeeth;
-(void)becomeActiveWithDincharya:(UINavigationController *)navigatioonController;
- (void)appDidBecomeActive:(NSNotification *)notification navigation:(UINavigationController*)navigation;
-(void)Timecompare:(BOOL)value managedObject:(NSManagedObjectContext*)managedObjectContext pranayama:(NSString*)Prana;
-(void)UpdateDincharyaIntoDatabase:(BOOL)value mangedObject:(NSManagedObjectContext*)managedObjectContext Prana:(NSString*)Prana;
-(void)CommonAlertView:(UINavigationController *)navigatioonController;
-(void)CheckStartController:(UINavigationController *)navigarionController Identifier:(NSString *)identifier;
@end

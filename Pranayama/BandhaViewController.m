//
//  BandhaViewController.m
//  Pranayama
//
//  Created by Manish Kumar on 13/06/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "BandhaViewController.h"

@interface BandhaViewController ()

@end

@implementation BandhaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    // Do any additional setup after loading the view.
    self.screenName=@"Bandha";
    // Do any additional setup after loading the view.
    self.navigationItem.hidesBackButton=YES;
    int fontSize;
    if (DEVICE==IPAD) {
        fontSize=24;
    }
    else{
        fontSize=18;
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self content];
}


- (void)HomeMethod:(NSNotification *)notification {
    [[CommonSounds sharedInstance] appDidBecomeActive:notification navigation:self.navigationController];
    
}


-(void)content{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label8 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label9 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label10 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label11 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label12 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label13 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label14 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label15 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label16 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label17 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label18 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label19 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label20 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label21 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label22 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label23 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label24 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label25 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label26 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label27 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(18);
    label7.font=Ralewayfont(14);
    label8.font=Ralewayfont(18);
    label9.font=Ralewayfont(14);
    label10.font=Ralewayfont(18);
    label11.font=Ralewayfont(14);
    label12.font=Ralewayfont(18);
    label13.font=Ralewayfont(14);
    label14.font=Ralewayfont(14);
    label15.font=Ralewayfont(14);
    label16.font=Ralewayfont(18);
    label17.font=Ralewayfont(14);
    label18.font=Ralewayfont(18);
    label19.font=Ralewayfont(14);
    label20.font=Ralewayfont(18);
    label21.font=Ralewayfont(14);
    label22.font=Ralewayfont(18);
    label23.font=Ralewayfont(14);
    label24.font=Ralewayfont(18);
    label25.font=Ralewayfont(14);
    label26.font=Ralewayfont(18);
    label27.font=Ralewayfont(14);
    
    self.navigationItem.title = [MCLocalization stringForKey:@"bandh"];
    label1.text=[MCLocalization stringForKey:@"bandh_content"];
    label2.text=[MCLocalization stringForKey:@"bandh_content2"];
    label3.text=[MCLocalization stringForKey:@"Steps"];
    label4.text=[MCLocalization stringForKey:@"bandh_content4"];
    label5.text=[MCLocalization stringForKey:@"bandh_content5"];
    label6.text=[MCLocalization stringForKey:@"benefit"];
    label7.text=[MCLocalization stringForKey:@"bandh_content7"];
    label8.text=[MCLocalization stringForKey:@"Precaution"];
    label9.text=[MCLocalization stringForKey:@"bandh_content9"];
    label10.text=[MCLocalization stringForKey:@"bandh_content10"];
    label11.text=[MCLocalization stringForKey:@"bandh_content11"];
    label12.text=[MCLocalization stringForKey:@"Steps"];
    label13.text=[MCLocalization stringForKey:@"bandh_content13"];
    label14.text=[MCLocalization stringForKey:@"bandh_content14"];
    label15.text=[MCLocalization stringForKey:@"bandh_content15"];
    label16.text=[MCLocalization stringForKey:@"benefit"];
    label17.text=[MCLocalization stringForKey:@"bandh_content17"];
    label18.text=[MCLocalization stringForKey:@"Precaution"];
    label19.text=[MCLocalization stringForKey:@"bandh_content19"];
    label20.text=[MCLocalization stringForKey:@"bandh_content20"];
    label21.text=[MCLocalization stringForKey:@"bandh_content21"];
    label22.text=[MCLocalization stringForKey:@"Steps"];
    label23.text=[MCLocalization stringForKey:@"bandh_content23"];
    label24.text=[MCLocalization stringForKey:@"benefit"];
    label25.text=[MCLocalization stringForKey:@"bandh_content25"];
    label26.text=[MCLocalization stringForKey:@"Precaution"];
    label27.text=[MCLocalization stringForKey:@"bandh_content27"];
    
    NSString *string=[MCLocalization stringForKey:@"bandh_link"];
    
    label1.numberOfLines=0;
    label1.textColor=[UIColor redColor];
    [label1 sizeToFit];
    [ScrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [ScrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    label3.textColor=[UIColor redColor];
    [label3 sizeToFit];
    [ScrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [ScrollView addSubview:label4];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((ScrollView.frame.size.width/2)-30, label4.frame.origin.y+label4.frame.size.height+10, 50, 80)];
    imageView1.image = [UIImage imageNamed:@"jalandra_step1"];
    [ScrollView addSubview:imageView1];
    
    label5.frame=CGRectMake(10, imageView1.frame.origin.y+imageView1.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [ScrollView addSubview:label5];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((ScrollView.frame.size.width/2)-20, label5.frame.origin.y+label5.frame.size.height+10, 40, 70)];
    imageView2.image = [UIImage imageNamed:@"jalandra_step2"];
    [ScrollView addSubview:imageView2];
    
    label6.frame=CGRectMake(10, imageView2.frame.origin.y+imageView2.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label6.textColor=[UIColor redColor];
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [ScrollView addSubview:label6];
    
    label7.frame=CGRectMake(10, label6.frame.origin.y+label6.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [ScrollView addSubview:label7];
    
    label8.frame=CGRectMake(10, label7.frame.origin.y+label7.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label8.numberOfLines=0;
    label8.textColor=[UIColor redColor];
    [label8 sizeToFit];
    [ScrollView addSubview:label8];
    
    label9.frame=CGRectMake(10, label8.frame.origin.y+label8.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label9.numberOfLines=0;
    [label9 sizeToFit];
    [ScrollView addSubview:label9];
    
    label10.frame=CGRectMake(10, label9.frame.origin.y+label9.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label10.numberOfLines=0;
    label10.textColor=[UIColor redColor];
    [label10 sizeToFit];
    [ScrollView addSubview:label10];
    
    label11.frame=CGRectMake(10, label10.frame.origin.y+label10.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label11.numberOfLines=0;
    [label11 sizeToFit];
    [ScrollView addSubview:label11];
    
    label12.frame=CGRectMake(10, label11.frame.origin.y+label11.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label12.numberOfLines=0;
    label12.textColor=[UIColor redColor];
    [label12 sizeToFit];
    [ScrollView addSubview:label12];
    
    label13.frame=CGRectMake(10, label12.frame.origin.y+label12.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label13.numberOfLines=0;
    [label13 sizeToFit];
    [ScrollView addSubview:label13];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((ScrollView.frame.size.width/2)-40, label13.frame.origin.y+label13.frame.size.height+10, 70, 140)];
    imageView3.image = [UIImage imageNamed:@"uddiyan_step1"];
    [ScrollView addSubview:imageView3];
    
    label14.frame=CGRectMake(10, imageView3.frame.origin.y+imageView3.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label14.numberOfLines=0;
    [label14 sizeToFit];
    [ScrollView addSubview:label14];
    
    UIImageView *imageView4 = [[UIImageView alloc] initWithFrame:CGRectMake((ScrollView.frame.size.width/2)-40, label14.frame.origin.y+label14.frame.size.height+10, 70, 140)];
    imageView4.image = [UIImage imageNamed:@"uddiyan_step2"];
    [ScrollView addSubview:imageView4];
    
    label15.frame=CGRectMake(10, imageView4.frame.origin.y+imageView4.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label15.numberOfLines=0;
    [label15 sizeToFit];
    [ScrollView addSubview:label15];
    
    label16.frame=CGRectMake(10, label15.frame.origin.y+label15.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label16.numberOfLines=0;
    label16.textColor=[UIColor redColor];
    [label16 sizeToFit];
    [ScrollView addSubview:label16];
    
    label17.frame=CGRectMake(10, label16.frame.origin.y+label16.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label17.numberOfLines=0;
    [label17 sizeToFit];
    [ScrollView addSubview:label17];
    
    label18.frame=CGRectMake(10, label17.frame.origin.y+label17.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label18.numberOfLines=0;
    label18.textColor=[UIColor redColor];
    [label18 sizeToFit];
    [ScrollView addSubview:label18];
    
    label19.frame=CGRectMake(10, label18.frame.origin.y+label18.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label19.numberOfLines=0;
    [label19 sizeToFit];
    [ScrollView addSubview:label19];
    
    label20.frame=CGRectMake(10, label19.frame.origin.y+label19.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label20.numberOfLines=0;
    label20.textColor=[UIColor redColor];
    [label20 sizeToFit];
    [ScrollView addSubview:label20];
    
    label21.frame=CGRectMake(10, label20.frame.origin.y+label20.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label21.numberOfLines=0;
    [label21 sizeToFit];
    [ScrollView addSubview:label21];
    
    label22.frame=CGRectMake(10, label21.frame.origin.y+label21.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label21.numberOfLines=0;
    label22.textColor=[UIColor redColor];
    [label22 sizeToFit];
    [ScrollView addSubview:label22];
    
    label23.frame=CGRectMake(10, label22.frame.origin.y+label22.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label23.numberOfLines=0;
    [label23 sizeToFit];
    [ScrollView addSubview:label23];
    
    UIImageView *imageView5 = [[UIImageView alloc] initWithFrame:CGRectMake((ScrollView.frame.size.width/2)-20, label23.frame.origin.y+label23.frame.size.height+10, 40, 80)];
    imageView5.image = [UIImage imageNamed:@"mool_bandh"];
    [ScrollView addSubview:imageView5];
    
    label24.frame=CGRectMake(10, imageView5.frame.origin.y+imageView5.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label24.numberOfLines=0;
    label24.textColor=[UIColor redColor];
    [label24 sizeToFit];
    [ScrollView addSubview:label24];
    
    label25.frame=CGRectMake(10, label24.frame.origin.y+label24.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label25.numberOfLines=0;
    [label25 sizeToFit];
    [ScrollView addSubview:label25];
    
    label26.frame=CGRectMake(10, label25.frame.origin.y+label25.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label26.numberOfLines=0;
    label26.textColor=[UIColor redColor];
    [label26 sizeToFit];
    [ScrollView addSubview:label26];
    
    label27.frame=CGRectMake(10, label26.frame.origin.y+label26.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label27.numberOfLines=0;
    [label27 sizeToFit];
    [ScrollView addSubview:label27];
    
    
    FRHyperLabel *label = [FRHyperLabel new];
    label.frame=CGRectMake(10,label27.frame.origin.y+label27.frame.size.height , ScrollView.frame.size.width-20, 200);
    label.numberOfLines=0;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]};
    
    
    label.attributedText = [[NSAttributedString alloc]initWithString:string attributes:attributes];
    label.font=[UIFont systemFontOfSize:16];
    
    [label setLinkForSubstring:@"www.7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.7pranayama.com/"]];
    }];
    [label setLinkForSubstring:@"info@7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://info@7pranayama.com/"]];
    }];
    
    [ScrollView addSubview:label];
    
    ScrollView.contentSize=CGSizeMake(ScrollView.frame.size.width,label27.frame.origin.y+label27.frame.size.height+label.frame.size.height+100);
    
    int scrollHeight=label27.frame.origin.y+label27.frame.size.height+label.frame.size.height;
    int a=self.view.frame.size.width/4;
    int b=a/2;
    UIButton*btn1=[[UIButton alloc] initWithFrame:CGRectMake(a/2-25, scrollHeight+20, 50, 50)];
    btn1.tag=1;
    btn1.layer.cornerRadius = btn1.bounds.size.width / 2.0;
    UIImage *btnImage1 = [UIImage imageNamed:@"facebook"];
    [btn1 setImage:btnImage1 forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(goToLink:) forControlEvents:UIControlEventTouchUpInside];
    [ScrollView addSubview:btn1];
    
    UIButton*btn2=[[UIButton alloc] initWithFrame:CGRectMake(((2*a)-b)-25, scrollHeight+20, 50, 50)];
    btn2.tag=2;
    btn2.layer.cornerRadius = btn1.bounds.size.width / 2.0;
    UIImage *btnImage2 = [UIImage imageNamed:@"twitter"];
    [btn2 setImage:btnImage2 forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(goToLink:) forControlEvents:UIControlEventTouchUpInside];
    [ScrollView addSubview:btn2];
    
    UIButton*btn3=[[UIButton alloc] initWithFrame:CGRectMake(((3*a)-b)-25, scrollHeight+20, 50, 50)];
    btn3.tag=3;
    btn3.layer.cornerRadius = btn1.bounds.size.width / 2.0;
    UIImage *btnImage3 = [UIImage imageNamed:@"pinterest"];
    [btn3 setImage:btnImage3 forState:UIControlStateNormal];
    [btn3 addTarget:self action:@selector(goToLink:) forControlEvents:UIControlEventTouchUpInside];
    [ScrollView addSubview:btn3];
    
    UIButton*btn4=[[UIButton alloc] initWithFrame:CGRectMake((self.view.frame.size.width-b)-25,scrollHeight+20, 50, 50)];
    btn4.tag=4;
    btn4.layer.cornerRadius = btn1.bounds.size.width / 2.0;
    UIImage *btnImage4 = [UIImage imageNamed:@"google"];
    [btn4 setImage:btnImage4 forState:UIControlStateNormal];
    [btn4 addTarget:self action:@selector(goToLink:) forControlEvents:UIControlEventTouchUpInside];
    [ScrollView addSubview:btn4];
    
}

-(void)goToLink:(UIButton*)sender{
    if (sender.tag==1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.facebook.com/7pranayama-1742577192620397/"]];
    }
    else if (sender.tag==2){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://twitter.com/7pranayama"]];
        
    }
    else if (sender.tag==3){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://in.pinterest.com/7pranayama/"]];
        
    }
    else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://plus.google.com/111008006040750995181/posts"]];
    }
}

-(IBAction)backbutton:(id)sender
{
    [[CommonSounds sharedInstance] NavigationMethod:self.navigationController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  DincharyaVC.swift
//  Pranayama
//
//  Created by Manish Kumar on 16/09/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

import UIKit

class DincharyaVC: GAITrackedViewController,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,UITextFieldDelegate {
    
    
    let kAppDel = (UIApplication.shared.delegate! as! AppDelegate)
    
    
    @IBOutlet var myTable: UITableView!
    var arrayList : NSMutableArray! = NSMutableArray()
    var yogasanArray : NSArray! = NSArray()
    var pranayamaArray : NSArray! = NSArray()
    ////array for english///
    var yogaArray : NSArray! = NSArray()
    var pranaArray : NSArray! = NSArray()
    var yogaArray1 : NSMutableArray! = NSMutableArray()
    var pranaArray1 : NSMutableArray! = NSMutableArray()
    
    
    @IBOutlet var FlotingButton : UIButton!
    var popUpview: UIView!
    var PranalistView : UIView!
    var YogsanlistView : UIView!
    var DincharyaTypeView : UIView!
    //var timePickerview : UIView!
    var view1: UIView!
    var blackOverlay : UIView!
    var window : UIWindow!
    var nextBtn : UIButton!
    var SchTextFeild : UITextField!
    
    var autometicBtn : UIButton!
    var manualBtn : UIButton!
    
    var DelayTimeView : UIView!
    var delayTextF : UITextField!
    var is_autometic : Bool!
    
    var TimePickerView : UIView!
    var TimePicker : UIDatePicker!
    var timelbl : UILabel!
    
    var managedObjectContext: NSManagedObjectContext!
    
    var cell: UITableViewCell!
    var p = CGPoint.zero
    var indexpath1: IndexPath!
    var lpgr: UILongPressGestureRecognizer!
    var tap: UITapGestureRecognizer!
    var cl = 0
    var selectunselectarray : NSMutableArray! = NSMutableArray()
    
    var add_schedule : String!
    var set_scheduleName : String!
    var nextstr : String!
    var select_yoga : String!
    var select_prana : String!
    var din_type : String!
    var autometicstr : String!
    var menualstr : String!
    var delaytimestr : String!
    var delayTimemainstr : String!
    var in_sec : String!
    var set  : String!
    var cancel : String!
    var minselectionstr : String!
    
    var titleFont : CGFloat!
    var smallFont : CGFloat!
    var mediumFont : CGFloat!
    
    var fontSize : CGFloat!
    
    var frame : CGRect!
    var frame1 : CGRect!

    var height : Int = Int(0.0)
    var count : Int = Int(0.0)
    
     override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: (#selector(self.appDidBecomeActive(_:))), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        // Do any additional setup after loading the view.
        self.screenName="Dincharya"
        is_autometic=true
        self.managedObjectContext = kAppDel.managedObjectContext!
        myTable.allowsSelection = false
        myTable.allowsMultipleSelection = true
        self.automaticallyAdjustsScrollViewInsets=false
        
        
        FlotingButton.layer.cornerRadius=FlotingButton.bounds.size.width/2
        FlotingButton.layer.shadowColor=UIColor.black.cgColor
        FlotingButton.layer.shadowOffset=CGSize(width: 3, height: 3)
        FlotingButton.layer.shadowOpacity=1
        FlotingButton.layer.shadowRadius=2
        FlotingButton.addTarget(self, action: #selector(self.CreateScheduleMethod), for: .touchUpInside)
        
        arrayList = NSMutableArray(array: kAppDel.fetchDincharyaFromDatabase())
        myTable.delegate=self
        myTable.dataSource=self
        window = UIApplication.shared.keyWindow
//        let singleFingerTap = UITapGestureRecognizer(target: self, action: #selector(self.handleSingleTap))
//        singleFingerTap.delegate=self
//        window.addGestureRecognizer(singleFingerTap)
        self.setText()
        self.setSelectedarr()
        self.gesture()
       }
    
    func appDidBecomeActive(_ notification:Notification) -> Void {
        
        CommonSounds.sharedInstance().appDidBecomeActive(notification, navigation: self.navigationController)
    }
    
    
    func setSelectedarr() -> Void {
        selectunselectarray = NSMutableArray()
        for _ in 0..<arrayList.count {
            
            selectunselectarray.add("NO")
        }
        print(selectunselectarray)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        frame = FlotingButton.frame
        self.floattingButton()
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            fontSize=24
            titleFont=30
            mediumFont=20
            smallFont=18
            
        }
        else{
            fontSize=18
            titleFont=24
            mediumFont=14
            smallFont=12
        }
        
        UINavigationBar.appearance().titleTextAttributes=[NSFontAttributeName: Ralewayfont(fontSize),NSForegroundColorAttributeName:UIColor.white]
        
        myTable.reloadData()
        UserDefaults.standard.set(false, forKey: DincharyaMainCon)
        
        UserDefaults.standard.set("Dincharya", forKey: controllerType)
    }
    


    func setText() -> Void {
            self.navigationItem.title = MCLocalization.string(forKey: "dincharya")
            yogasanArray=[MCLocalization.string(forKey: "Sarvangasana"),MCLocalization.string(forKey: "Halasana"),MCLocalization.string(forKey: "Vipritkarani"),MCLocalization.string(forKey: "Paschimottanasana"),MCLocalization.string(forKey: "Dhanurasana"),MCLocalization.string(forKey: "Balasana"),MCLocalization.string(forKey: "Hastapadasana"),MCLocalization.string(forKey: "Marjariasana"),MCLocalization.string(forKey: "Uttanasana"),MCLocalization.string(forKey: "Setu Bandhasana"),MCLocalization.string(forKey: "Virabhadrasana")]
            
            pranayamaArray=[MCLocalization.string(forKey: "Anulom_Vilom"),MCLocalization.string(forKey: "kapalbhati"),MCLocalization.string(forKey: "bhramari"),MCLocalization.string(forKey: "surya_bhedana"),MCLocalization.string(forKey: "chandra_bhedana"),MCLocalization.string(forKey: "bhastrika"),MCLocalization.string(forKey: "sheetali"),MCLocalization.string(forKey: "ujjayi"),MCLocalization.string(forKey: "meditative"),MCLocalization.string(forKey: "udgeeth"),MCLocalization.string(forKey: "bahya")]
            
            yogaArray = ["Sarvangasana","Halasana","Vipritkarani","Paschimottanasana","Dhanurasana","Balasana","Hastapadasana","Marjariasana","Uttanasana","Setu Bandhasana","Virabhadrasana"]
            pranaArray = ["AnulomVilom","Kapalbhati","Bhramari","Surya Bhedana","Chandra Bhedana","Bhastrika","Sheetali","Ujjayi","Meditative Breathing","Udgeeth","Bahya"]

            add_schedule = MCLocalization.string(forKey: "add_dincharya")
            set_scheduleName = MCLocalization.string(forKey: "set_scheduleName")
            nextstr = MCLocalization.string(forKey: "nexstr")
            select_yoga = MCLocalization.string(forKey: "select_yoga")
            select_prana = MCLocalization.string(forKey: "select_prana")
            din_type = MCLocalization.string(forKey: "din_type")
            autometicstr = MCLocalization.string(forKey: "autometic")
            menualstr = MCLocalization.string(forKey: "menual")
            delaytimestr = MCLocalization.string(forKey: "setDelayTime")
            delayTimemainstr = MCLocalization.string(forKey: "delayTime")
            in_sec = MCLocalization.string(forKey: "in_sec")
            set = MCLocalization.string(forKey: "set")
            cancel = MCLocalization.string(forKey: "Cancel")
            minselectionstr = MCLocalization.string(forKey: "min_selection")

    }
    
    func TitleView(_ popupview: UIView,Height:CGFloat,tag:Int) -> Void {
        
        let exitButton=UIButton(frame: CGRect(x: 0, y: 0, width: popupview.frame.size.width/6, height: popupview.frame.size.height/Height))
        exitButton.setImage(UIImage(named: "cancel_icon"), for: UIControlState())
        exitButton.tag=tag
        exitButton.addTarget(self, action: #selector(self.Exitbutton), for: .touchUpInside)
        popupview.addSubview(exitButton)
        
        ////////////Title of popup/////////
        let AddTitle = UILabel(frame: CGRect(x:exitButton.frame.origin.x+exitButton.frame.size.width , y: 0, width: (width: popupview.frame.size.width/6)*4, height: exitButton.frame.size.height))
        AddTitle.text = add_schedule
        AddTitle.numberOfLines = 2
        AddTitle.font=Ralewayfont(fontSize)
        AddTitle.textAlignment=NSTextAlignment.center
        popupview.addSubview(AddTitle)
        
        ///////// Next Button /////
        nextBtn = UIButton(frame: CGRect(x: AddTitle.frame.origin.x+AddTitle.frame.size.width, y: 0, width: exitButton.frame.size.width, height: exitButton.frame.size.height))
        
        nextBtn.setTitle(nextstr, for: UIControlState())
        if(tag==1){
            nextBtn.setTitleColor(UIColor.lightGray, for: UIControlState())
        }
        else{
            nextBtn.setTitleColor(UIColor.black, for: UIControlState())
        }
        nextBtn.titleLabel?.font=Ralewayfont(fontSize)
        nextBtn.addTarget(self, action: #selector(self.NextBtnMethod), for: .touchUpInside)
        nextBtn.tag=tag
        popupview.addSubview(nextBtn)
        
        /////// add line //
        let line = UILabel(frame: CGRect(x: 0, y: exitButton.frame.origin.y+exitButton.frame.size.height, width: popupview.frame.size.width, height: 1))
        line.backgroundColor=UIColor.lightGray
        popupview.addSubview(line)
        }
    /////////////////////////////////////
    ///////floattingbutton up down///////
    func floattingButton()
         {
         if count==0{
            height=0;
            FlotingButton.frame = CGRect(x: frame.origin.x, y: 0, width: frame.size.width, height: frame.size.height)
            UIView.animate(withDuration: 1, animations: {
                self.FlotingButton.frame=self.frame
                }, completion: {(Bool) in
                    self.floattingButton()
              })
        }   else if(count<4){
            height=height+Int(frame.origin.y)/Int(count*2)
            UIView.animate(withDuration: 1, animations: {
                self.FlotingButton.frame = CGRect(x: self.frame.origin.x, y: CGFloat(self.height), width: self.frame.size.width, height: self.frame.size.height)
                self.height=Int(self.FlotingButton.frame.origin.y)
                }, completion: {(Bool) in
                    UIView.animate(withDuration: 1, animations: {
                        self.FlotingButton.frame=self.frame
                        
                        }, completion: {(Bool) in
                         self.floattingButton()
                            })
                      })
 }
 count=count+1;
      }
        func view1Method() -> Void {
        view1=UIView(frame: CGRect(x: 0,y: 0,width: self.view.frame.size.width, height: self.view.frame.size.height))
        blackOverlay=UIView(frame: view1.frame)
        blackOverlay.layer.backgroundColor = UIColor.black.cgColor
        blackOverlay.layer.opacity = 0
        view1.addSubview(blackOverlay)
        window.addSubview(view1)
    }
    
    ///////////////// Create schedule name method//////
          func CreateScheduleMethod() -> Void {
          self.view1Method()
        popUpview = UIView(frame: CGRect(x: self.view.frame.size.width, y: 80, width: self.view.frame.size.width-20, height: self.view.frame.size.height/3))
        popUpview.backgroundColor=UIColor.white
        popUpview.layer.cornerRadius=5
        popUpview.layer.borderColor=UIColor.red.cgColor
        popUpview.layer.borderWidth=2
        view1.addSubview(popUpview)

        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.blackOverlay.layer.opacity=0.4
            self.popUpview.frame.origin.x=10
            },completion: { (finished: Bool) -> Void in
        })
        ///////////////////////////////////////////
       
self.TitleView(popUpview, Height: 5 , tag: 1)
        
        let schdLbl = UILabel(frame: CGRect(x: 10, y: (popUpview.frame.size.height/5)+5, width: popUpview.frame.size.width-20, height: popUpview.frame.size.height/5))
        schdLbl.text=set_scheduleName
        schdLbl.font=Ralewayfont(fontSize)
        schdLbl.textColor=UIColor.gray
        popUpview.addSubview(schdLbl)
        
        SchTextFeild = UITextField(frame: CGRect(x: 10, y: schdLbl.frame.origin.y+schdLbl.frame.size.height+5, width: popUpview.frame.size.width-20, height: popUpview.frame.size.height/5))
        SchTextFeild.delegate=self
        SchTextFeild.font=Ralewayfont(titleFont)
        SchTextFeild.autocorrectionType=UITextAutocorrectionType.no
        SchTextFeild.becomeFirstResponder()
        SchTextFeild.addTarget(self, action: #selector(DincharyaVC.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        popUpview .addSubview(SchTextFeild)
    }
    
    //////////////////////// Add Yogasana method//////////////
    func addYogasanaMethod() -> Void {
        
 ////////////////        self.view1Method() //////////
        
        YogsanlistView = UIView(frame: CGRect(x: self.view.frame.size.width, y: self.view.frame.size.height/5, width: self.view.frame.size.width-20, height: (self.view.frame.size.height/5)*3))
        YogsanlistView.backgroundColor=UIColor.white
        YogsanlistView.layer.cornerRadius=5
        YogsanlistView.layer.borderColor=UIColor.red.cgColor
        YogsanlistView.layer.borderWidth=2
        view1.addSubview(YogsanlistView)
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.blackOverlay.layer.opacity=0.4
            self.YogsanlistView.frame.origin.x=10
            },completion: { (finished: Bool) -> Void in
        })
        
        self.TitleView(YogsanlistView, Height: 10, tag: 2)
        
        let addPrana = UILabel(frame: CGRect(x: 10, y: (YogsanlistView.frame.size.height/10)+5, width: YogsanlistView.frame.size.width-20, height: YogsanlistView.frame.size.height/10))
        addPrana.text=select_yoga
        addPrana.textColor=UIColor.gray
        addPrana.font=Ralewayfont(fontSize)
        YogsanlistView.addSubview(addPrana)
        
        let line2 = UILabel(frame: CGRect(x: 0, y: addPrana.frame.origin.y+addPrana.frame.size.height, width: YogsanlistView.frame.size.width, height: 1))
        line2.backgroundColor=UIColor.lightGray
        YogsanlistView.addSubview(line2)
        
        let Listview = UIView(frame: CGRect(x: 10, y: line2.frame.origin.y+1, width: YogsanlistView.frame.size.width-20, height: YogsanlistView.frame.size.height-(line2.frame.origin.y+3)))
        YogsanlistView.addSubview(Listview)
        
        let a = CGFloat((yogasanArray?.count)!)
        let h = (Listview.frame.size.height/a)
        let b = Int(a)
        
        for i in 0..<b {
            let button : UIButton = UIButton(frame: CGRect(x: 0, y: CGFloat(i)*h, width: Listview.frame.size.width, height: h))
            button.tag=i;
            button.setTitle(yogasanArray![i] as? String, for: UIControlState())
            let star = yogasanArray.object(at: i)
            print(star)
            if yogaArray1.contains(star) {
                button.setTitleColor(UIColor.orange, for: UIControlState())
            }
            else{
                button.setTitleColor(UIColor.gray, for: UIControlState())
            }
            button.setTitleColor(UIColor.gray, for: UIControlState())
            button.contentHorizontalAlignment=UIControlContentHorizontalAlignment.left
            button.titleLabel?.font=Ralewayfont(smallFont)
            button.addTarget(self, action: #selector(self.YogasanabuttonMethod), for: .touchUpInside)
            Listview.addSubview(button)
        }
    }
    
   ////////// Yogasana buttons method for changing the color and  add it//////
       func YogasanabuttonMethod(_ sender:UIButton) -> Void {
        if sender.currentTitleColor.isEqual(UIColor.gray){
            sender.setTitleColor(UIColor.orange, for: UIControlState())
            yogaArray1?.add((yogaArray?.object(at: sender.tag))!)
            print(yogaArray1)
        }
        else{
            sender.setTitleColor(UIColor.gray, for: UIControlState())
            yogaArray1!.remove((yogaArray?.object(at: sender.tag))!)
            print(yogaArray1)
        }
    }
    //////////////////////// Add Pranayama method/////////
    func addPranayamaMethod() -> Void {
        
    //  self.view1Method()
        
        PranalistView = UIView(frame: CGRect(x: self.view.frame.size.width, y: self.view.frame.size.height/5, width: self.view.frame.size.width-20, height: (self.view.frame.size.height/5)*3))
        PranalistView.backgroundColor=UIColor.white
        PranalistView.layer.cornerRadius=5
        PranalistView.layer.borderColor=UIColor.red.cgColor
        PranalistView.layer.borderWidth=2
        view1.addSubview(PranalistView)
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.blackOverlay.layer.opacity=0.4
            self.PranalistView.frame.origin.x=10
            },completion: { (finished: Bool) -> Void in
        })

        self.TitleView(PranalistView, Height: 10, tag: 3)
        
        let addPrana = UILabel(frame: CGRect(x: 10, y: (PranalistView.frame.size.height/10)+5, width: PranalistView.frame.size.width-20, height: PranalistView.frame.size.height/10))
        addPrana.text=select_prana
        addPrana.font=Ralewayfont(fontSize)
        addPrana.textColor=UIColor.gray
        PranalistView.addSubview(addPrana)
        
        let line2 = UILabel(frame: CGRect(x: 0, y: addPrana.frame.origin.y+addPrana.frame.size.height, width: PranalistView.frame.size.width, height: 1))
        line2.backgroundColor=UIColor.lightGray
        PranalistView.addSubview(line2)

        
        let Listview = UIView(frame: CGRect(x: 10, y: line2.frame.origin.y+1, width: PranalistView.frame.size.width-20, height: YogsanlistView.frame.size.height-(line2.frame.origin.y+3)))
        PranalistView.addSubview(Listview)
        
        let a = CGFloat((pranayamaArray?.count)!)
        let h = (Listview.frame.size.height/a)
        let b = Int(a)
        for i in 0..<b {
            let button : UIButton = UIButton(frame: CGRect(x: 0, y: CGFloat(i)*h, width: Listview.frame.size.width, height: h))
            button.tag=i;
            button.setTitle(pranayamaArray![i] as? String, for: UIControlState())
            let star = pranayamaArray.object(at: i)
            print(star)
            if pranaArray1.contains(star) {
                button.setTitleColor(UIColor.orange, for: UIControlState())
            }
            else{
                button.setTitleColor(UIColor.gray, for: UIControlState())
            }
            button.contentHorizontalAlignment=UIControlContentHorizontalAlignment.left
            button.titleLabel?.font=Ralewayfont(smallFont)
            button.addTarget(self, action: #selector(self.PranayamabuttonMethod), for: .touchUpInside)
            Listview.addSubview(button)
        }
    }
    
    ////////////////////////////////////////////////////////////////////////
    ///////////////// Dincharya type Method////////////////
    
    func DincharyaType() -> Void {
        
//        self.view1Method()
        DincharyaTypeView = UIView(frame: CGRect(x: self.view.frame.size.width, y: self.view.frame.size.height/4, width: self.view.frame.size.width-20, height: self.view.frame.size.height/2))
        DincharyaTypeView.backgroundColor=UIColor.white
        DincharyaTypeView.layer.cornerRadius=5
        DincharyaTypeView.layer.borderColor=UIColor.red.cgColor
        DincharyaTypeView.layer.borderWidth=2
        view1.addSubview(DincharyaTypeView)
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.blackOverlay.layer.opacity=0.4
            self.DincharyaTypeView.frame.origin.x=10
            },completion: { (finished: Bool) -> Void in
        })
        
        self.TitleView(DincharyaTypeView, Height: 8, tag: 4)
        
        let selectType = UILabel(frame: CGRect(x: 10, y: (DincharyaTypeView.frame.size.height/8)+5, width: DincharyaTypeView.frame.size.width-20, height: DincharyaTypeView.frame.size.height/8))
        selectType.text=din_type
        selectType.font=Ralewayfont(fontSize)
        selectType.textColor=UIColor.gray
        DincharyaTypeView.addSubview(selectType)
        
        let line2 = UILabel(frame: CGRect(x: 0, y: selectType.frame.origin.y+selectType.frame.size.height, width: DincharyaTypeView.frame.size.width, height: 1))
        line2.backgroundColor=UIColor.lightGray
        DincharyaTypeView.addSubview(line2)
        
        let autometicView = UIView(frame: CGRect(x: 15, y: line2.frame.origin.y+10, width:DincharyaTypeView.frame.size.width-20 , height: DincharyaTypeView.frame.size.height/8))
        DincharyaTypeView.addSubview(autometicView)
        
        autometicBtn = UIButton(frame: CGRect(x: 0, y: autometicView.frame.size.height/2-10, width: 20, height: 20))
        autometicBtn.backgroundColor=UIColor.lightGray
        autometicBtn.layer.cornerRadius=5
        autometicBtn.setImage(UIImage.init(named: "selectedCheck"), for: UIControlState.normal)
                autometicView.addSubview(autometicBtn)
        
        let autometicLbl = UILabel(frame: CGRect(x: 25, y: 0, width: autometicView.frame.size.width-autometicBtn.frame.size.width, height: autometicView.frame.size.height))
        autometicLbl.text=autometicstr
        autometicLbl.font=Ralewayfont(mediumFont)
        autometicView.addSubview(autometicLbl)
        
        let autoBtn = UIButton(frame: CGRect(x: 0, y: 0, width: autometicView.frame.size.width, height: autometicView.frame.size.height))
        autoBtn.addTarget(self, action: #selector(self.typeButtonMethod), for: .touchUpInside)
        autoBtn.tag=1;
        autometicView.addSubview(autoBtn)
        
        
        let menualView = UIView(frame: CGRect(x: 15, y: autometicView.frame.origin.y+autometicView.frame.size.height, width:DincharyaTypeView.frame.size.width-20 , height: DincharyaTypeView.frame.size.height/8))
        DincharyaTypeView.addSubview(menualView)
        
        manualBtn = UIButton(frame: CGRect(x: 0, y: menualView.frame.size.height/2-10, width: 20, height: 20))
        manualBtn.backgroundColor=UIColor.lightGray
        manualBtn.layer.cornerRadius=5
        menualView.addSubview(manualBtn)
        
        let manualLbl = UILabel(frame: CGRect(x: 25, y: 0, width: menualView.frame.size.width-manualBtn.frame.size.width, height: menualView.frame.size.height))
        manualLbl.text=menualstr
        manualLbl.font=Ralewayfont(mediumFont)
        menualView.addSubview(manualLbl)
        
        let manviewBtn = UIButton(frame: CGRect(x: 0, y: 0, width: menualView.frame.size.width, height: menualView.frame.size.height))
        manviewBtn.addTarget(self, action: #selector(self.typeButtonMethod), for: .touchUpInside)
        manviewBtn.tag=2;
        menualView.addSubview(manviewBtn)
        
              ///////////////////
             /// Manula view for time dealay setting ///
        
        DelayTimeView = UIView(frame: CGRect(x:0, y:menualView.frame.origin.y+menualView.frame.size.height+10 , width: DincharyaTypeView.frame.size.width, height: DincharyaTypeView.frame.size.height-(menualView.frame.origin.y+menualView.frame.size.height)))
        
        DincharyaTypeView.addSubview(DelayTimeView)
        
        let  linelbl = UILabel(frame: CGRect(x: 0, y: 0, width: DelayTimeView.frame.size.width, height: 1))
        linelbl.backgroundColor=UIColor.lightGray
        DelayTimeView.addSubview(linelbl)
        
        let delayTitellbl = UILabel(frame: CGRect(x: 10, y: 1, width: DelayTimeView.frame.size.width-20, height: DincharyaTypeView.frame.size.height/8))
        delayTitellbl.text=delaytimestr
        delayTitellbl.font=Ralewayfont(fontSize)
        delayTitellbl.textColor=UIColor.gray
        DelayTimeView.addSubview(delayTitellbl)
        
        let  linelbl2 = UILabel(frame: CGRect(x: 0, y: delayTitellbl.frame.origin.y+delayTitellbl.frame.size.height, width: DelayTimeView.frame.size.width, height: 1))
        linelbl2.backgroundColor=UIColor.lightGray
        DelayTimeView.addSubview(linelbl2)
        
        let h = DelayTimeView.frame.size.height-linelbl2.frame.origin.y+2
        
        
        let desLbl = UILabel(frame: CGRect(x: 10, y: linelbl2.frame.origin.y+1, width: DelayTimeView.frame.size.width-15, height:h/2 ))
        desLbl.text=delayTimemainstr
        desLbl.numberOfLines=0
        desLbl.font=Ralewayfont(smallFont)
        DelayTimeView.addSubview(desLbl)
        
        var W : CGFloat!
        
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            W=60
        }
        else{
            W=40
        }
        delayTextF = UITextField(frame: CGRect(x:DincharyaTypeView.frame.size.width/2-20 , y: desLbl.frame.origin.y+desLbl.frame.size.height, width: W, height: 25))
        delayTextF.delegate=self
        delayTextF.placeholder=in_sec
        delayTextF.keyboardType = UIKeyboardType.phonePad
        delayTextF.font=Ralewayfont(mediumFont)
        DelayTimeView.addSubview(delayTextF)
        
        let textLine = UILabel(frame: CGRect(x:DincharyaTypeView.frame.size.width/2-20 , y: delayTextF.frame.origin.y+25, width: W, height: 1))
        textLine.backgroundColor=UIColor.orange
        DelayTimeView.addSubview(textLine)
    }
     func typeButtonMethod(_ sender: UIButton) -> Void {
        if sender.tag==1 && is_autometic==false{
            is_autometic=true
            autometicBtn.setImage(UIImage.init(named: "selectedCheck"), for: UIControlState.normal)
            manualBtn.setImage(nil , for: UIControlState.normal)
            DelayTimeView.isHidden=false
            DincharyaTypeView.frame=CGRect(x: 10, y: self.view1.frame.size.height/4, width: self.view.frame.size.width-20, height:DincharyaTypeView.frame.size.height+DelayTimeView.frame.size.height-10)
            
        }
        else if sender.tag==2 && is_autometic==true{
            is_autometic=false
            self.view1.endEditing(true)
            manualBtn.setImage(UIImage.init(named: "selectedCheck"), for: UIControlState.normal)
            autometicBtn.setImage(nil , for: UIControlState.normal)
            DincharyaTypeView.frame=CGRect(x: 10, y: self.view1.frame.size.height/4, width: self.view.frame.size.width-20, height:DincharyaTypeView.frame.size.height-DelayTimeView.frame.size.height+10)
            DelayTimeView.isHidden=true
        }
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.DincharyaTypeView.frame.origin.y=self.view.frame.size.height/2-self.DincharyaTypeView.frame.size.height/2;
            },completion: { (finished: Bool) -> Void in
        })
    }
    
      ////////// Pranayama buttons method for changing the color and  add it//////
    
    func PranayamabuttonMethod(_ sender:UIButton) -> Void {
        if sender.currentTitleColor.isEqual(UIColor.gray){
            sender.setTitleColor(UIColor.orange, for: UIControlState())
            pranaArray1?.add((pranaArray?.object(at: sender.tag))!)
        }
        else{
            sender.setTitleColor(UIColor.gray, for: UIControlState())
            pranaArray1!.remove((pranaArray?.object(at: sender.tag))!)
        }
    }
          ////////////////////  Time Picker method///////////
                func Timepickermethod() -> Void {
        TimePickerView = UIView(frame: CGRect(x: self.view.frame.size.width, y: self.view.frame.size.height/4, width: self.view.frame.size.width-40, height: self.view.frame.size.height/2))
        TimePickerView.backgroundColor=UIColor.white
        TimePickerView.layer.cornerRadius=5
        TimePickerView.layer.borderColor=UIColor.red.cgColor
        TimePickerView.layer.borderWidth=2
        view1.addSubview(TimePickerView)
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.blackOverlay.layer.opacity=0.4
            self.TimePickerView.frame.origin.x=20
            },completion: { (finished: Bool) -> Void in
        })
        
        let pikerview = UIView(frame: CGRect(x: 10, y: 10, width: TimePickerView.frame.size.width-20, height: TimePickerView.frame.size.height-20))
        pikerview.backgroundColor=UIColor.white
        pikerview.layer.shadowColor=UIColor.darkGray.cgColor
        pikerview.layer.shadowOffset=CGSize(width: 0, height: 0)
        pikerview.layer.shadowOpacity=0.7
        pikerview.layer.shadowRadius=5
        TimePickerView.addSubview(pikerview)
        
        let height = pikerview.frame.size.height/5
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let datestr = dateFormatter.string(from:NSDate() as Date)
        
        timelbl = UILabel(frame: CGRect(x: 15, y: 0, width: pikerview.frame.size.width-20, height: height))
        timelbl.text=datestr
        timelbl.font=Ralewayfont(fontSize)
        pikerview.addSubview(timelbl)
        
        let line = UILabel(frame: CGRect(x: 0, y: height, width: pikerview.frame.size.width, height: 1))
        line.backgroundColor=UIColor.red
        pikerview.addSubview(line)
        
        TimePicker = UIDatePicker(frame: CGRect(x: 20, y: height+20, width: pikerview.frame.size.width-40, height: height*3-40))
        TimePicker.datePickerMode=UIDatePickerMode.time
        TimePicker.date=NSDate() as Date
        TimePicker.addTarget(self, action: #selector(self.dateChange), for: .valueChanged)
        pikerview.addSubview(TimePicker)
        
        let color = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1.0)
        let line2 = UILabel(frame: CGRect(x: 0, y: height*4, width: pikerview.frame.size.width, height: 1))
        line2.backgroundColor=color
        pikerview.addSubview(line2)
        
        let line3 = UILabel(frame: CGRect(x: pikerview.frame.size.width/2, y: height*4, width: 1, height: height))
        line3.backgroundColor=color
        pikerview.addSubview(line3)
        
        let cancelBtn = UIButton(frame: CGRect(x: 0, y: height*4+1, width: pikerview.frame.size.width/2, height: height))
        cancelBtn.setTitle(cancel, for: .normal)
        cancelBtn.setTitleColor(UIColor.orange, for: .normal)
        cancelBtn.titleLabel?.font=Ralewayfont(fontSize)
        cancelBtn.tag=1
        cancelBtn.addTarget(self, action: #selector(self.pickerBtnMethod(_:)), for: .touchUpInside)
        pikerview.addSubview(cancelBtn)
        
        let Okbtn = UIButton(frame: CGRect(x: pikerview.frame.size.width/2+1, y: height*4, width: pikerview.frame.size.width/2, height: height))
        Okbtn.setTitle(set, for: .normal)
        Okbtn.titleLabel?.font=Ralewayfont(fontSize)
        Okbtn.setTitleColor(UIColor.orange, for: .normal)
        Okbtn.tag=2
        Okbtn.addTarget(self, action: #selector(self.pickerBtnMethod(_:)), for: .touchUpInside)
        pikerview.addSubview(Okbtn)
        
    }
    ////////////////////    Date change by date picker /////////
      func dateChange() -> Void {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let datestr = dateFormatter.string(from:TimePicker.date)
        timelbl.text=datestr
    }
    
    /////////////////  Ok cancel button method///////
    func pickerBtnMethod(_ sender: UIButton) -> Void {
        
        if sender.tag==2{
            self.saveIntoDatabase()
        }
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.blackOverlay.layer.opacity=0
            self.TimePickerView.frame.origin.x=(-self.TimePickerView.frame.size.width)
            self.view1.endEditing(true)
            },completion: { (finished: Bool) -> Void in
                self.view1.isHidden=true
        })
        self.yogaArray1?.removeAllObjects()
        self.pranaArray1?.removeAllObjects()
        
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////  Save Dincharya into Database //////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    
    func saveIntoDatabase() -> Void {
        let yogaar = yogaArray1?.copy() as! NSArray
        let pranaar = pranaArray1?.copy() as! NSArray
        
        let pranaData = NSKeyedArchiver.archivedData(withRootObject: pranaar)
        let yogaData = NSKeyedArchiver.archivedData(withRootObject: yogaar)
        
        let reportEntity = NSEntityDescription.insertNewObject(forEntityName: "Dincharya", into: self.managedObjectContext) as! Dincharya
        
        reportEntity.name = SchTextFeild.text
        reportEntity.notification_time = TimePicker.date
        reportEntity.pranayama_arr = pranaData
        reportEntity.yogasana_arr = yogaData
        if is_autometic ==  true {
            reportEntity.autometicTime = delayTextF.text
            print(delayTextF.text)
        }
        else{
            reportEntity.autometicTime = nil
        }
        
        do {
            try self.managedObjectContext?.save()
        } catch {
            print(error)
        }
        
        arrayList = NSMutableArray(array: kAppDel.fetchDincharyaFromDatabase())
        print(arrayList)
        myTable.reloadData()
        self.setSelectedarr()
    }
    
    
    
     /////////// Exit button method/////////////////
    
        func Exitbutton(_ sender: UIButton) -> Void {
        if sender.tag==1 {
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.blackOverlay.layer.opacity=0
                self.popUpview.frame.origin.x=(-self.popUpview.frame.size.width)
                self.view1.endEditing(true)
                self.yogaArray1?.removeAllObjects()
                self.pranaArray1?.removeAllObjects()
                },completion: { (finished: Bool) -> Void in
                    self.view1.isHidden=true
            })
        }
                else if sender.tag==2{
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.popUpview.frame.origin.x=10
                self.YogsanlistView.frame.origin.x=(-self.view.frame.size.width)
                self.view1.endEditing(true)
                },completion: { (finished: Bool) -> Void in
                    self.YogsanlistView.frame.origin.x=(self.view.frame.size.width)
//                    self.view1.isHidden=true
            })
        }
        else if sender.tag==3{
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.YogsanlistView.frame.origin.x=10
                self.PranalistView.frame.origin.x=(-self.view.frame.size.width)
                },completion: { (finished: Bool) -> Void in
                    self.PranalistView.frame.origin.x=(self.view.frame.size.width)
//                    self.view1.isHidden=true
            })
        }
        else if sender.tag==4{
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.PranalistView.frame.origin.x=10
                self.DincharyaTypeView.frame.origin.x=(-self.view.frame.size.width)
                self.view1.endEditing(true)
                },completion: { (finished: Bool) -> Void in
                    self.DincharyaTypeView.frame.origin.x=(self.view.frame.size.width)
                    //                    self.view1.isHidden=true
            })
        }
    }
    
    ///////////////////// Next button method///////////////////
    //////////////////////////////////////////////////////////
    
    func NextBtnMethod(_ sender: UIButton) -> Void {
        if sender.tag==1 {
            var con = false
            
            if (arrayList.count) >= 0 {
                for i in 0..<arrayList.count {
                    let dincharya = arrayList[i] as! Dincharya
                    if dincharya.name == SchTextFeild.text{
                        con=true
                    }
                }
            }
            if con==true{
                if #available(iOS 8.0, *) {
                    let alert = UIAlertController(title: "", message: "\(SchTextFeild.text) is already exist", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
            else{
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    //                self.blackOverlay.layer.opacity=0
                    self.popUpview.frame.origin.x=(-self.view.frame.size.width)
                    self.view1.endEditing(true)
                    self.addYogasanaMethod()
                    },completion: { (finished: Bool) -> Void in
                        self.popUpview.frame.origin.x=(self.view.frame.size.width)
                })
                
            }
            
        }
        else if sender.tag==2  {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
//                self.blackOverlay.layer.opacity=0
                self.YogsanlistView.frame.origin.x=(-self.view.frame.size.width)
                self.addPranayamaMethod()
                },completion: { (finished: Bool) -> Void in
                    self.YogsanlistView.frame.origin.x=(self.view.frame.size.width)
            })
        }
        else if sender.tag==3{
            if yogaArray1.count == 0 && pranaArray1.count == 0 {
                if #available(iOS 8.0, *) {
                    let alert = UIAlertController(title: "", message: minselectionstr, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    // Fallback on earlier versions
                }

            }
            else{
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    //                self.blackOverlay.layer.opacity=0
                    self.PranalistView.frame.origin.x=(-self.view.frame.size.width)
                    self.DincharyaType()
                    self.is_autometic=true
                    },completion: { (finished: Bool) -> Void in
                        self.PranalistView.frame.origin.x=(self.view.frame.size.width)
                })
                
            }
        }
        else if sender.tag==4{
            
            if is_autometic==true {
                if (delayTextF.text?.isEmpty)! {
                    if #available(iOS 8.0, *) {
                        let alert = UIAlertController(title: "", message: "Enter Time", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        // Fallback on earlier versions
                    }
                }
                else{
                    UIView.animate(withDuration: 0.3, animations: { () -> Void in
                        self.DincharyaTypeView.frame.origin.x=(-self.view.frame.size.width)
                        self.Timepickermethod()
                        self.view1.endEditing(true)
                        },completion: { (finished: Bool) -> Void in
                            self.DincharyaTypeView.frame.origin.x=(self.view.frame.size.width)
                    })
                }
                
            }
            else{
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    self.DincharyaTypeView.frame.origin.x=(-self.view.frame.size.width)
                    self.Timepickermethod()
                    },completion: { (finished: Bool) -> Void in
                        self.DincharyaTypeView.frame.origin.x=(self.view.frame.size.width)
                })
            }
        }
    }
    
    
    ///////////// text feild delegate method///////////
    func textFieldDidChange(_ textField: UITextField) {
        if textField .isEqual(SchTextFeild) {
            if SchTextFeild.text == "" {
                nextBtn.setTitleColor(UIColor.lightGray, for: UIControlState())
            }
            else{
                nextBtn.setTitleColor(UIColor.black, for: UIControlState())
            }
            
        }
    }
    
    //////////////Tesxt feild delegate////
    
    ///////////// text feild delegate ///////////
    
    func textField(textField: UITextField,
                   shouldChangeCharactersInRange range: NSRange,
                   replacementString string: String) -> Bool {
        
        if textField .isEqual(delayTextF){
            let inverseSet = NSCharacterSet(charactersIn:"0123456789").inverted
            
            
            let components = string.components(separatedBy: inverseSet)
            
            let filtered = components.joined(separator: "")
            return string == filtered
        }
        return true
    }

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        
        if textField .isEqual(delayTextF) {
            let inverseSet = NSCharacterSet(charactersIn:"0123456789").inverted
            
            
            let components = string.components(separatedBy: inverseSet)
            
            let filtered = components.joined(separator: "")
            return string == filtered && newLength<=1
        }
        return newLength <= 10
    }
    
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        
        if textField .isEqual(delayTextF) {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.DincharyaTypeView.frame.origin.y=20
                },completion: { (finished: Bool) -> Void in
            })
        }
        else{
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.popUpview.frame.origin.y=80
                },completion: { (finished: Bool) -> Void in
            })
        }
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool{
        
        if textField .isEqual(delayTextF) {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.DincharyaTypeView.frame.origin.y=self.view.frame.size.height/4
                },completion: { (finished: Bool) -> Void in
            })
        }
        else{
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.popUpview.frame.origin.y=(self.view.frame.size.height/3)
                },completion: { (finished: Bool) -> Void in
            })
        }
        return true
    }
    
    /////////////////////////////////////////////////////////////////
    ///////////// Tabel view delegate methods////////
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return arrayList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cellidentifier")
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        cell.accessoryType = .disclosureIndicator
        let dincharya = arrayList[(indexPath as NSIndexPath).row] as! Dincharya
        cell.textLabel?.text=dincharya.name
        cell.textLabel?.textColor=UIColor.white
        cell.textLabel?.font = Ralewayfont(fontSize!)

        let colors = (indexPath as NSIndexPath).row%2
        switch (colors) {
        case 0:
            cell.backgroundColor=UIColor(red:187/255.0 , green: 20/255.0, blue: 37/255.0, alpha: 0.5)
            break
        case 1:
            cell.backgroundColor=UIColor(red:176/255.0 , green: 12/255.0, blue: 28/255.0, alpha: 0.5)
            break
        default:
            break
        }
        
        return cell
    }
    
    /////////////  long gesture for delete ///
    func gesture() {
        lpgr = UILongPressGestureRecognizer()
        lpgr = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        lpgr.minimumPressDuration = 0.5
        lpgr.delegate = self
        myTable.addGestureRecognizer(lpgr)
    }
    
    func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer) {
        p = gestureRecognizer.location(in: myTable)
        indexpath1 = myTable.indexPathForRow(at: p)
        if indexpath1 == nil {
            print("long press on table view but not on a row")
        }
        else{
            let theCell = myTable.cellForRow(at: indexpath1)
            if gestureRecognizer.state == .began {
                myTable.allowsSelection=false;
                FlotingButton.isUserInteractionEnabled=false
                print("long press on table view at row \(Int(indexpath1.row))")
                theCell?.backgroundColor = UIColor.init(red: 210/255, green: 210/255, blue: 210/255, alpha: 1)
                cl += 1
                selectunselectarray[indexpath1.row] = "YES" as String
                tap = UITapGestureRecognizer(target: self, action: #selector(self.cellTapped))
                tap.numberOfTapsRequired = 1
                tap.numberOfTouchesRequired = 1
                tap.delegate = self
                myTable.addGestureRecognizer(tap)
            }
            else {
                print("gestureRecognizer.state = \(gestureRecognizer.state)")
            }
            
        }
    }
    
    
    func cellTapped(_ tap1: UITapGestureRecognizer) {
        let q = tap1.location(in: myTable)
        indexpath1 = myTable.indexPathForRow(at: q)
        if indexpath1 != nil {
            print(indexpath1)
            print(indexpath1.row)
            print(arrayList.count)
            
            let theCell = myTable.cellForRow(at: indexpath1)
            if selectunselectarray[indexpath1.row] as! String == "NO" {
                theCell?.backgroundColor = UIColor.init(red: 210/255, green: 210/255, blue: 210/255, alpha: 1)
                cl += 1
                selectunselectarray[indexpath1.row] = "YES" as String
            }
            else {
                let colors = (indexpath1 as NSIndexPath).row%2
                switch (colors) {
                case 0:
                    theCell?.backgroundColor=UIColor(red:187/255.0 , green: 20/255.0, blue: 37/255.0, alpha: 0.5)
                    break
                case 1:
                    theCell?.backgroundColor=UIColor(red:176/255.0 , green: 12/255.0, blue: 28/255.0, alpha: 0.5)
                    break
                default:
                    break
                }
                cl -= 1
                selectunselectarray[indexpath1.row] = "NO" as String
            }
            if cl == 0 {
                myTable.allowsSelection=true
                FlotingButton.isUserInteractionEnabled=true
                tap1.isEnabled = false
                self.gesture()
            }
        }
    }
    
    @IBAction func deletebutton(_ sendar: AnyObject) {
        var j = 0
        for var i in 0..<selectunselectarray.count {
            i = i-j
            let a = IndexPath(row: i, section: 0)
            if (selectunselectarray[i] as! String == "YES") {
                let repoEntity = arrayList[(a as NSIndexPath).row] as! Dincharya
                self.DeleteFromDincharyaReport(name: repoEntity.name!)
                self.managedObjectContext.delete(repoEntity as NSManagedObject)
                do {
                    try self.managedObjectContext?.save()
                } catch {
                    print(error)
                }
                
                arrayList.removeObject(at: (a as NSIndexPath).row)
                selectunselectarray.removeObject(at: (a as NSIndexPath).row)
                myTable.deleteRows(at: [a], with: .automatic)
                j = j+1
            }
        }
        myTable.allowsSelection=true
        FlotingButton.isUserInteractionEnabled=true
        tap?.isEnabled = false
        cl = 0
        self.gesture()
        myTable.reloadData()
        self.setSelectedarr()
    }
    
    func DeleteFromDincharyaReport(name : String) -> Void {
        let  arr = kAppDel.fetchDincharyaReportFromDatabaseAcording(toName: name as String!) as NSArray
        if arr.count>0 {
            for i in 0..<arr.count {
                let report = arr.object(at: i) as! DincharyaReport
                self.managedObjectContext.delete(report as NSManagedObject)
                do {
                    try self.managedObjectContext?.save()
                } catch {
                    print(error)
                }
            }
        }
    }
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        // Return YES if you want the specified item to be editable.
//        if cl > 0 {
//            return false
//        }
//        return true
//    }
//    // Override to support editing the table view.
//    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//            let repoEntity = arrayList[(indexPath as NSIndexPath).row]
//            self.managedObjectContext.delete(repoEntity as! NSManagedObject)
//            do {
//                try self.managedObjectContext?.save()
//            } catch {
//                print(error)
//            }
//            arrayList.removeObject(at: (indexPath as NSIndexPath).row)
//            tableView.deleteRows(at: [indexPath], with: .automatic)
//            myTable.reloadData()
//            //add code here for when you hit delete
//        }
//    }
    
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dinch = arrayList[(indexPath as NSIndexPath).row] as! Dincharya
        
        let dinchNmae = self.storyboard?.instantiateViewController(withIdentifier: "dincharya_name") as! DincharyaNameVC
        dinchNmae.din_name = dinch.name as NSString!
        UserDefaults.standard.set(dinch.name, forKey: dincharyaName)
        self.navigationController?.pushViewController(dinchNmae, animated: true)
        }
    

    
    @IBAction func backbutton(_ sender: AnyObject) {
        self.navigationController!.popViewController(animated: true)
                        }
        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
            }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}


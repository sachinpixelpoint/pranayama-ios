//
//  Dincharya_pranaVC.swift
//  Pranayama
//
//  Created by Manish Kumar on 20/10/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

import UIKit

class Dincharya_pranaVC: GAITrackedViewController,UITableViewDataSource,UITableViewDelegate {
    
    let kAppDel = (UIApplication.shared.delegate! as! AppDelegate)
    
    var PranaList : NSMutableArray!  = NSMutableArray()
    var ListArray : NSMutableArray! = NSMutableArray()
    var ListArray2 : NSMutableArray! = NSMutableArray()
    
    
    var PranaIdArr : NSMutableArray! = NSMutableArray()
    
    
    var Fontsize : CGFloat!
    var Fontsize2 : CGFloat!
    
    
    @IBOutlet var tabelview : UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: (#selector(self.appDidBecomeActive(_:))), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        self.screenName="Dincharya Pranayama list"
        // Do any additional setup after loading the view.
        
        
        self.automaticallyAdjustsScrollViewInsets=false
        
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            Fontsize=24
            Fontsize2=16
        }
        else{
            Fontsize=18
            Fontsize2=10
        }
        UINavigationBar.appearance().titleTextAttributes=[NSFontAttributeName: Ralewayfont(Fontsize!),NSForegroundColorAttributeName:UIColor.white]

        tabelview.delegate=self
        tabelview.dataSource=self

    }
    
    func appDidBecomeActive(_ notification:Notification) -> Void {
        
        CommonSounds.sharedInstance().appDidBecomeActive(notification, navigation: self.navigationController)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let mainVC = AMSlideMenuMainViewController.getInstanceForVC(self) as AMSlideMenuMainViewController
        if (mainVC.rightMenu != nil) {
            self.addRightMenuButton()
        }
        UserDefaults.standard.set("Dincharya Prana", forKey: controllerType)
        
        if PranaList.count==0 {
            let str = UserDefaults.standard.object(forKey: dincharyaName) as! String
            let ar = kAppDel.fetchDincharyaFromDatabaseAcording(toName: str) as NSArray
            if ar.count>0{
                let dincharya = ar.object(at: 0) as! Dincharya
                let pranaAr = NSKeyedUnarchiver.unarchiveObject(with: dincharya.pranayama_arr!) as! NSArray
                PranaList = NSMutableArray(array: pranaAr)
            }
        }
        self.getList()
    }
    
    
    func getList() -> Void {
        
        self.navigationItem.title=MCLocalization.string(forKey: "Pranayam")
        for i in 0..<PranaList.count {
            if PranaList.object(at: i) as! String == "AnulomVilom" {
                ListArray.add(MCLocalization.string(forKey: "Anulom_Vilom"))
                ListArray2.add(MCLocalization.string(forKey: "Alternate_Nostrils"))
                PranaIdArr?.add("anulom")
            }
            if PranaList.object(at: i) as! String == "Kapalbhati" {
                ListArray.add(MCLocalization.string(forKey: "kapalbhati"))
                ListArray2.add(MCLocalization.string(forKey: "Skull_Purification"))
                PranaIdArr?.add("kapal")
            }
            if PranaList.object(at: i) as! String == "Bhramari" {
                ListArray.add(MCLocalization.string(forKey: "bhramari"))
                ListArray2.add(MCLocalization.string(forKey: "Humming_Bee_Breath"))
                PranaIdArr?.add("bhramari")
            }
            if PranaList.object(at: i) as! String == "Surya Bhedana" {
                ListArray.add(MCLocalization.string(forKey: "surya_bhedana"))
                ListArray2.add(MCLocalization.string(forKey: "Right_Nostril"))
                PranaIdArr?.add("surya")
            }
            if PranaList.object(at: i) as! String == "Chandra Bhedana" {
                ListArray.add(MCLocalization.string(forKey: "chandra_bhedana"))
                ListArray2.add(MCLocalization.string(forKey: "Left_Nostril"))
                PranaIdArr?.add("chandra")
            }
            if PranaList.object(at: i) as! String == "Bhastrika" {
                ListArray.add(MCLocalization.string(forKey: "bhastrika"))
                ListArray2.add(MCLocalization.string(forKey: "Bellows"))
                PranaIdArr?.add("bhastrika")
            }
            if PranaList.object(at: i) as! String == "Sheetali" {
                ListArray.add(MCLocalization.string(forKey: "sheetali"))
                ListArray2.add(MCLocalization.string(forKey: "Cooling"))
                PranaIdArr?.add("sheetali")
            }
            if PranaList.object(at: i) as! String == "Ujjayi" {
                ListArray.add(MCLocalization.string(forKey: "ujjayi"))
                ListArray2.add(MCLocalization.string(forKey: "Ocean"))
                PranaIdArr?.add("ujjayi")
            }
            if PranaList.object(at: i) as! String == "Meditative Breathing" {
                ListArray.add(MCLocalization.string(forKey: "meditative"))
                ListArray2.add(MCLocalization.string(forKey: "Meditative_Breathing"))
                PranaIdArr?.add("meditative")
            }
            if PranaList.object(at: i) as! String == "Udgeeth" {
                ListArray.add(MCLocalization.string(forKey: "udgeeth"))
                ListArray2.add(MCLocalization.string(forKey: "Chanting_Breath"))
                PranaIdArr?.add("udgeeth")
            }
            if PranaList.object(at: i) as! String == "Bahya" {
                ListArray.add(MCLocalization.string(forKey: "bahya"))
                ListArray2.add(MCLocalization.string(forKey: "External_Kumbhak"))
                PranaIdArr?.add("bahya")
            }
        }
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return (ListArray?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellidentifier") as UITableViewCell!
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        cell.accessoryType = .disclosureIndicator
        let lan = UserDefaults.standard.object(forKey: Languagevalue) as! NSString
        if lan.isEqual(to: Hindi) {
            cell.textLabel?.text=ListArray?[(indexPath as NSIndexPath).row] as! String?
            cell.textLabel?.font = Ralewayfont(Fontsize)
        }
        else if lan.isEqual(to: English){
            cell.textLabel?.attributedText=ListArray?[(indexPath as NSIndexPath).row] as! NSAttributedString?
        }
        else{
            let str1 = ListArray.object(at: indexPath.row) as! String
            let str2 = ListArray2.object(at: indexPath.row) as! String
            let string = "\(str1) \(str2)"
            let attributedStr = NSMutableAttributedString(string: string)
            attributedStr.addAttribute(NSFontAttributeName, value: Ralewayfont(Fontsize), range: NSRange(location: 0, length: str1.characters.count))
            attributedStr.addAttribute(NSFontAttributeName, value: Ralewayfont(Fontsize2), range: NSRange(location: str1.characters.count + 1, length: str2.characters.count))
            cell.textLabel?.attributedText = attributedStr
        }
        cell.textLabel?.textColor=UIColor.white
        cell.textLabel?.numberOfLines=0
        let colors = (indexPath as NSIndexPath).row%2
        switch (colors) {
        case 0:
            cell.backgroundColor=UIColor(red:187/255.0 , green: 20/255.0, blue: 37/255.0, alpha: 0.5)
            break
        case 1:
            cell.backgroundColor=UIColor(red:176/255.0 , green: 12/255.0, blue: 28/255.0, alpha: 0.5)
            break
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let VC = (self.storyboard?.instantiateViewController(withIdentifier: (PranaIdArr?[(indexPath as NSIndexPath).row] as! String?)!))! as UIViewController
        self.navigationController?.pushViewController(VC, animated: true)
        
    }

    
    
    @IBAction func backbutton(_ sender: AnyObject) {
        self.navigationController!.popViewController(animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

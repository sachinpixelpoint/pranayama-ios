//
//  SuryaBhednaActionVC.h
//  Pranayama
//
//  Created by Manish Kumar on 02/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol suryadelegate <NSObject>

-(void)playsound;

@end


@interface SuryaBhednaActionVC : GAITrackedViewController
{
    IBOutlet UIView *topview;
    
    IBOutlet UILabel *totalTimelbl;
    IBOutlet UILabel *actionlbl;
    
    IBOutlet UIButton *btnStop;
    IBOutlet UIButton *btnpuse;
    
    IBOutlet UILabel *lblHours;
    IBOutlet UILabel *lblminutes;
    IBOutlet UILabel *lblseconds;
    IBOutlet UILabel *lblTimer;
    IBOutlet UILabel *lblmode;
    IBOutlet UIImageView *imageview;
    
    NSTimer *startTimer;
    BOOL isPause;
    BOOL PlayStop;
    int lblhold1;
    int lblinhaleRight;
    int lblexhaleleft;
    int seconds;
    int minutes;
    int hours;
    int roundforReport;
    int suryaCount;
    int prepare;
}
@property (nonatomic,assign)id <suryadelegate>delegate;
@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic) int rounds;
@property (nonatomic) int lblinhale;
@property (nonatomic) int lblexhale;
@property (nonatomic) int lblhold;
@property (nonatomic) int totalTime;
@end

//
//  SettingsViewController.h
//  AnulomaViloma
//
//  Created by Hirendra Sharma on 1/14/16.
//  Copyright © 2016 Hirendra Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : GAITrackedViewController<UITextFieldDelegate>
{
    IBOutlet UILabel *lblSound;
    IBOutlet UILabel *lblSoundEverSec;
    IBOutlet UILabel *lblVibrator;
    IBOutlet UILabel *lblKeepDisplay;
    IBOutlet UILabel *lblLanguage;
    IBOutlet UILabel *reminder;
    IBOutlet UILabel*setPreprationTime;
    IBOutlet UIView *languageView;
    IBOutlet UIButton *englishButton;
    IBOutlet UIButton *hindiButton;
    IBOutlet UIButton *russionButton;
    IBOutlet UIButton *language;
    IBOutlet UISwitch *btnSound;
    IBOutlet UISwitch *btnSoundEverSec;
    IBOutlet UISwitch *btnVibrator;
    IBOutlet UISwitch *btnKeepDisplay;
    IBOutlet UILabel *lblVolume;
    IBOutlet UISlider *volumeSlider;
    BOOL bvalue;
    IBOutlet UIDatePicker*picker;
    IBOutlet UIView* dateview;
    IBOutlet UIButton*btn;
    IBOutlet UITextField *preprationTime;
    IBOutlet UIButton *dateButton;
    IBOutlet UIView*viewvoice;
    IBOutlet UILabel *soundTypelbl;
    IBOutlet UILabel *topsetSoundLbl;
    IBOutlet UILabel *topOthersettongLbl;
    IBOutlet UIButton *soundTypeBtn;
    IBOutlet  UIButton *voiceBtn;
    IBOutlet UIButton *tuneBtn;
    IBOutlet UIView *TopView,*bottomView;
    IBOutlet UIButton *frenchButton;
}

@end

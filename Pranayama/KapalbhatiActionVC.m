//
//  KapalbhatiActionVC.m
//  Pranayama
//
//  Created by Manish Kumar on 02/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "KapalbhatiActionVC.h"

@interface KapalbhatiActionVC ()

@end

@implementation KapalbhatiActionVC
@synthesize totalTime,rounds;
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //google analytic
    NSString *name=@"KapalBhati ";
    NSString *ratio=[NSString stringWithFormat:@"%d:%d",_minutes,rounds];
    
    NSString *AppendStr=[name stringByAppendingString:ratio];
    self.screenName=AppendStr;
    //
    // Do any additional setup after loading the view.
    //    [self setlblheight];
    if (DEVICE == Iphone) {
        topview.frame = CGRectMake(8, 74, self.view.frame.size.width-16, 32);
    }
    prepare = [[NSUserDefaults standardUserDefaults] floatForKey:preparationtime];
    prepare=prepare+1;
    NSInteger value = [[NSUserDefaults standardUserDefaults]integerForKey:dincharyaDelayTime];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaMainCon] && value>0) {
        prepare = (int) value ;
    }
    [self ChangeText];
    self.navigationItem.hidesBackButton=YES;
    int fontSize;
    int totaltimeFont;
    int lblmodeFont;
    if (DEVICE==IPAD) {
        fontSize=24;
        totaltimeFont=25;
        lblmodeFont=36;
    }
    else{
        fontSize=18;
        totaltimeFont=12;
        lblmodeFont=22;
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback
                                     withOptions:AVAudioSessionCategoryOptionMixWithOthers
                                           error:nil];
    self.managedObjectContext=[kAppDele managedObjectContext];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:isPause1];
    PlayStop=NO;
    report_round=rounds;
    
    totaltimelbl.font=Ralewayfont(totaltimeFont);
    roundlbl.font=Ralewayfont(totaltimeFont);
    actionlbl.font=Ralewayfont(lblmodeFont);
    lblmode.font=Ralewayfont(lblmodeFont);
    LblPrepare.font=Ralewayfont(lblmodeFont);
    lblTimer.text=[NSString stringWithFormat:@"%d",rounds];
    timetrail=((float)totalTime/((float)rounds*2));
    [self setValueoflbl];
    [self startButtonPreshed];
    lblprepareTime.hidden=YES;
    LblPrepare.hidden=YES;
}


- (void)HomeMethod:(NSNotification *)notification {
    HomeTabelVC *homeObject = [[HomeTabelVC alloc]init];
    [homeObject appDidBecomeActive:notification];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self disableSlidePanGestureForRightMenu];
}

//-(void)setlblheight{
//    if (Device_Type==iPhone4)
//        topview.frame=CGRectMake(8, 75, 305, 32);
//    else if(Device_Type==iPhone5)
//        topview.frame=CGRectMake(8, 74, 305, 32);
//    else if(Device_Type==iPhone6)
//        topview.frame=CGRectMake(8, 74, 359, 32);
//    else if(Device_Type==iPhone6P)
//        topview.frame=CGRectMake(8, 74, 397, 32);
//}

-(void)ChangeText{
    self.navigationItem.title = [MCLocalization stringForKey:@"kapalbhati"];
    roundlbl.text=[MCLocalization stringForKey:@"Rounds"];
    totaltimelbl.text=[MCLocalization stringForKey:@"Total_Time"];
    actionlbl.text=[MCLocalization stringForKey:@"Action"];
    if (prepare>0) {
        lblmode.hidden=YES;
    }
    else{
        lblmode.text=[MCLocalization stringForKey: @"inhale_normally" ];
    }
    
}

-(void)setValueoflbl
{
    lblTimer.text=[NSString stringWithFormat:@"%d",rounds];
    seconds1 = totalTime % 60;
    minutes1 = (totalTime / 60) % 60;
    hours1 = totalTime / 3600;
    lblseconds.text=[NSString stringWithFormat:@"%d",seconds1];
    lblminutes.text=[NSString stringWithFormat:@"%d",minutes1];
    lblHours.text=[NSString stringWithFormat:@"%d",hours1];
}
-(IBAction)pausebutton:(id)sender{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:isPause1];
    pauseincrease++;
    PlayStop=YES;
    [btnStop setImage:[UIImage imageNamed:@"start_icon"] forState:UIControlStateNormal];
    [startTimer invalidate];
    [SecTimer invalidate];
}

-(IBAction)stopbutton:(id)sender{
    if(pauseincrease==3)
    {
        rounds++;
        pauseincrease=0;
    }
    if (PlayStop==YES) {
        startTimer = [NSTimer scheduledTimerWithTimeInterval: timetrail
                                                      target: self
                                                    selector:@selector(onTick:)
                                                    userInfo: nil repeats:YES];
        SecTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                    target: self
                                                  selector:@selector(onsec:)
                                                  userInfo: nil repeats:YES];
        [btnStop setImage:[UIImage imageNamed:@"stop_icon"] forState:UIControlStateNormal];
        PlayStop=NO;
    }
    else{
        [SecTimer invalidate];
        [startTimer invalidate];
        if (kapalCount>0) {
            [self insertReportIntoDatabase];
            [self insertintoProgressReport:YES];
            [self saveInDincharya:NO];
        }
        [[self navigationController] popViewControllerAnimated:NO];
        
    }
}

-(void)startButtonPreshed
{
    startTimer = [NSTimer scheduledTimerWithTimeInterval: timetrail
                                                  target: self
                                                selector:@selector(onTick:)
                                                userInfo: nil repeats:YES];
    SecTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                target: self
                                              selector:@selector(onsec:)
                                              userInfo: nil repeats:YES];
}
-(void)onsec:(NSTimer *)stimer{
    isPause=[[NSUserDefaults standardUserDefaults] boolForKey:isPause1];
    if (isPause) {
        [self pausebutton:isPause1];
    }
    else{
        if (prepare>0) {
            lblmode.hidden=YES;
            lblprepareTime.hidden=NO;
            LblPrepare.hidden=NO;
            prepare--;
            LblPrepare.text=[MCLocalization stringForKey:@"preparation_time"];
            lblprepareTime.text=[NSString stringWithFormat:@"%d",prepare];
            [[CommonSounds sharedInstance] playEverySound];
            if (prepare==0) {
                lblprepareTime.hidden=YES;
                LblPrepare.hidden=YES;
                lblmode.hidden=NO;
                lblprepareTime.text=@"";
                imageview.image=[UIImage imageNamed:@"kapal_postion2"];
                lblmode.text=[MCLocalization stringForKey: @"inhale_normally" ];
                
            }
        }
        else{
            if (isPause) {
                return;
            }
            if (totalTime>0) {
                totalTime=totalTime-1;
                seconds1 = totalTime % 60;
                minutes1 = (totalTime / 60) % 60;
                hours1 = totalTime / 3600;
                lblseconds.text=[NSString stringWithFormat:@"%d",seconds1];
                lblminutes.text=[NSString stringWithFormat:@"%d",minutes1];
                lblHours.text=[NSString stringWithFormat:@"%d",hours1];
            }
            else{
                
                [stimer invalidate];
                stimer = nil;
                if ([self.delegate respondsToSelector:@selector(playsound)]) {
                    [self.delegate playsound];
                    [self insertReportIntoDatabase];
                    [self insertintoProgressReport:NO];
                    [self saveInDincharya:YES];
                }
                [[self navigationController] popViewControllerAnimated:NO];
            }
            kapalCount++;
        }
    }
}
-(void)onTick:(NSTimer *)timer {
    isPause=[[NSUserDefaults standardUserDefaults] boolForKey:isPause1];
    if (isPause) {
        [self pausebutton:isPause1];
    }
    else{
        if (prepare>0) {
            
        }
        else{
            if (isPause) {
                return;
            }
            if (rounds>0) {
                lblmode.text=[MCLocalization stringForKey: @"inhale_normally" ];
                if(kapal_position==NO)
                {
                    imageview.image=[UIImage imageNamed:@"kapal_postion"];
                    lblmode.text=[MCLocalization stringForKey: @"exhale_force" ];
                    lblTimer.text=[NSString stringWithFormat:@"%d",rounds-1];
                    [[CommonSounds sharedInstance] playKapalExhlaleSound];
                    kapal_position=YES;
                }
                else{
                    imageview.image=[UIImage imageNamed:@"kapal_postion2"];
                    lblmode.text=[MCLocalization stringForKey: @"inhale_normally" ];
                    rounds=rounds-1;
                    if (rounds==0) {
                        lblseconds.text=[NSString stringWithFormat:@"%d",1];
                    }
                    else{
                        imageview.image=[UIImage imageNamed:@"kapal_postion2"];
                        kapal_position=NO;
                    }}
            }
            else{
                
                [timer invalidate];
                timer = nil;
            }
        }
    }
}
-(void)insertReportIntoDatabase{
    NSArray *array=[kAppDele fetchReportFromDatabseAccordingToType:@"Kapalbhati"];
    if (array.count>0) {
        Record *recordEntity=[array objectAtIndex:0];
        NSDate *str=recordEntity.anulomDate;
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        NSString *datestr=[dateFormatter stringFromDate:str];
        NSString *date=[dateFormatter stringFromDate:[NSDate date]];
        if ([datestr isEqualToString:date]) {
            NSString *duration=[NSString stringWithFormat:@"%d",[recordEntity.anulomduration intValue]+kapalCount];
            recordEntity.anulomduration=duration;
            NSError *error;
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"%@",error);
            }
        }
        else{
            
            Record *reportEntity = [NSEntityDescription insertNewObjectForEntityForName:@"Record" inManagedObjectContext:self.managedObjectContext];
            reportEntity.anulomLevel=[NSString stringWithFormat:@"%d",_minutes];
            reportEntity.anulomRounds=[NSString stringWithFormat:@"%d",report_round];
            reportEntity.anulomDate=[NSDate date];
            reportEntity.anulomduration=[NSString stringWithFormat:@"%d",kapalCount];
            reportEntity.anulomtype=@"Kapalbhati";
            NSError *error;
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"%@",error);
            }
        }
    }
    else{
        Record *reportEntity = [NSEntityDescription insertNewObjectForEntityForName:@"Record" inManagedObjectContext:self.managedObjectContext];
        reportEntity.anulomLevel=[NSString stringWithFormat:@"%d",_minutes];
        reportEntity.anulomRounds=[NSString stringWithFormat:@"%d",report_round];
        reportEntity.anulomDate=[NSDate date];
        reportEntity.anulomduration=[NSString stringWithFormat:@"%d",kapalCount];
        reportEntity.anulomtype=@"Kapalbhati";
        NSError *error;
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"%@",error);
        }
    }
    Report *reportEntity = [NSEntityDescription insertNewObjectForEntityForName:@"Report" inManagedObjectContext:self.managedObjectContext];
    reportEntity.reportLevel=[NSString stringWithFormat:@"%d",_minutes];
    reportEntity.reportRounds=[NSString stringWithFormat:@"%d",report_round];
    reportEntity.reportDate=[NSDate date];
    reportEntity.reportduration=[NSString stringWithFormat:@"%d",kapalCount];
    reportEntity.reporttype=@"Kapalbhati";
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"%@",error);
    }
}

-(void)insertintoProgressReport:(BOOL)value{
    ////progress Report ///////////
    NSString *habbitconditon=[[NSUserDefaults standardUserDefaults]objectForKey:HabbitCondition];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:HabbitMainCondition]) {
        NSArray *Progressarray=[kAppDele fetchReportFromProgressDatabseAccordingToType:habbitconditon];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"Progress" inManagedObjectContext:self.managedObjectContext]];
        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"progressType==%@ AND days==%d",habbitconditon,Progressarray.count];
        [request setPredicate:predicate];
        
        NSError *error1=nil;
        NSArray *results=[self.managedObjectContext executeFetchRequest:request error:&error1];
        Progress *entity=[results objectAtIndex:0];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:viewwillCondition];

        if (value==YES) {
            NSNumber *duration=[NSNumber numberWithInt:[entity.duration intValue]+kapalCount];
            entity.duration=duration;
        }
        else{
            entity.kapalbhati=[NSNumber numberWithBool:YES];
            NSNumber *duration=[NSNumber numberWithInt:[entity.duration intValue]+kapalCount];
            entity.duration=duration;
        }
        [self.managedObjectContext save:nil];
    }
}

-(void)saveInDincharya:(BOOL)Value{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaMainCon]) {
        
        NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaName];
        NSArray *ar = [kAppDele FetchDincharyaFromDatabaseAcordingToName:str];
        Dincharya *dincharya = [ar objectAtIndex:0];
        NSArray *yogaAr = [NSKeyedUnarchiver unarchiveObjectWithData:dincharya.yogasana_arr];
        NSArray *pranaMainList = [NSKeyedUnarchiver unarchiveObjectWithData:dincharya.pranayama_arr];
        NSArray *pranaList = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaPranaAr];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:viewwillCondition];
        
        if (pranaMainList.count == pranaList.count  && yogaAr.count == 0) {
            [[CommonSounds sharedInstance]Timecompare:Value managedObject:self.managedObjectContext pranayama:@"Kapalbhati"];
        }
        else{
            [[CommonSounds sharedInstance]UpdateDincharyaIntoDatabase:Value mangedObject:self.managedObjectContext Prana:@"Kapalbhati"];
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

//
//  StepsViewController.h
//  Pranayama
//
//  Created by Manish Kumar on 01/07/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface StepsViewController : GAITrackedViewController<UIScrollViewDelegate>
{
    IBOutlet UIScrollView *myScrollView;
}
@end

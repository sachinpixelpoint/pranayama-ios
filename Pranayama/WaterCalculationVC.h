//
//  WaterCalculationVC.h
//  Pranayama
//
//  Created by Manish Kumar on 24/08/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WaterCalculationVC : GAITrackedViewController<UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    CGFloat fontSize;
    IBOutlet UIView *Topview;
    IBOutlet UIView*BottomView;
    IBOutlet UIView*WeightView;
    IBOutlet UIView*fixView;
    IBOutlet UIView *submitView;
    IBOutlet UIView* datePkrView;
    UIView *windowView;
    IBOutlet UIButton *submitButton;
    IBOutlet UIButton*stopbtn;
    BOOL IsReadyToPlay,IspoundCon,IsfixwaterCon;
    float waterQuentity;
    
    IBOutlet UILabel *ml;
    IBOutlet UITextField*weight;
    IBOutlet UITextField*exercise;
    IBOutlet UITextField* water;
    IBOutlet UIButton*kgbutton;
    IBOutlet UIButton*interval;
    IBOutlet UIButton* RiseTimebtn;
    IBOutlet UIButton* bedTimebtn;
    IBOutlet UIButton *fixTimebtn;
    IBOutlet UIButton *fixWaterbtn;
    IBOutlet UIButton *viewKgbtn;
    IBOutlet UIButton *viewpoundbtn;

    IBOutlet UILabel *DeviderLine;
    
    IBOutlet UILabel *dateLbl;
    IBOutlet UIButton *okBtn;
    IBOutlet UIButton *cancelBtn;
    
    IBOutlet UIDatePicker*datepicker;
    UITapGestureRecognizer*tapper;
    UIWindow *window;
    
    IBOutlet UILabel*waterlabel;
    IBOutlet UILabel*volumelabel;
    IBOutlet UILabel* reminderlabel;
    
    ///labels
    IBOutlet UILabel *weightlbl;
    IBOutlet UILabel *RiseTimelbl;
    IBOutlet UILabel *BedTimelbl;
    IBOutlet UILabel *Excerciselbl;
    IBOutlet UILabel *Selectonelbl;
    IBOutlet UILabel *volumeAndinterval;
    IBOutlet UILabel *TotalTimeDaylbl;
    IBOutlet UILabel *EveryIntervalWaterlbl;
    IBOutlet UILabel *ReminderIntervallbl;
    IBOutlet UILabel *Minlbl;
    IBOutlet UILabel *Detaillbl;
    IBOutlet UILabel *Resultlbl;
    
    NSString *Enter_waterVolume,*Enter_interval,*Enter_weight,*Enter_excerciseTime,*NotificationAleartstr,*NotificationCancelStr;
    NSString *str1,*str2,*str3,*str4,*str5,*str6;
    UIView *aleartView;
}
@end

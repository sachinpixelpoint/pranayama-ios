//
//  Report+CoreDataProperties.h
//  Pranayama
//
//  Created by Manish Kumar on 02/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Report.h"

NS_ASSUME_NONNULL_BEGIN

@interface Report (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *reportDate;
@property (nullable, nonatomic, retain) NSString *reportduration;
@property (nullable, nonatomic, retain) NSString *reportLevel;
@property (nullable, nonatomic, retain) NSString *reportRounds;
@property (nullable, nonatomic, retain) NSString *reportSerialNo;
@property (nullable, nonatomic, retain) NSString *reporttype;

@end

NS_ASSUME_NONNULL_END

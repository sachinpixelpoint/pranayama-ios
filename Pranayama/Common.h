//
//  Common.h
//  Pranayama
//
//  Created by Manish Kumar on 02/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//


#import<UIKit/UIKit.h>


#ifndef EngNet_Common_h
#define EngNet_Common_h

#define DEVICE    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad
#define Iphone UIUserInterfaceIdiomPhone

#define RGB(r, g, b) \
[UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]

#define Device_Type ([[UIScreen mainScreen] bounds].size.height)
#define iPhone4 480
#define iPhone5 568
#define iPhone6 667
#define iPhone6P 736
#define iPadAir 1024
#define iPadPro 1366

#define kAppDele ((AppDelegate *)[[UIApplication sharedApplication] delegate])
//// problem moduel macro///

#define Hindi @"HindiLanguage"
#define English @"EnglishLanguage"
#define Russion @"RussionLanguage"

#define conditionalDate @"Cdate"
#define  HabbitStartDate @"habbitDate"
#define  HabbitType @"TypeOfHbbit"
#define YogasanCondition @"conditionForYogasana"
#define HabbitCondition @"ratio"
#define HabbitMainCondition @"condition"

#define ProbUjjayiInhale @"ProbUjjayiI"
#define ProbUjjayiHold @"ProbUjjayiH"
#define ProbUjjayiExhale @"ProbUjjayiE"
#define ProbUjjayiRound @"ProbUjjayiR"

#define viewwillCondition @"viewwill"
#define controllerType @"Controller"

#define delegateCondition @"delegateC"
#define commonClassCond @"common"

#define PranayamaList @"pranaList"
#define YogasanList @"yogaList"

#define tableIntro @"tableIntroduction"
//water intake//
#define waterSated @"IswaterModulSed"
#define isfixWaterVolume @"fixTimeinterval"
#define isPounds @"poundselect"
#define riseTime @"wakeuptime"
#define bedTime @"sleepTime"
#define Userweight @"Weight"
#define excerciseTime @"excercise"
#define volumeAndTime @"VolumeAndTimeinHour"
#define notificationWater @"notificationWaterInmL"
#define notificationTimes @"noOfnotificationTime"
#define notificationTimeInterval @"NotificationTimeinterval"
#define waterdelegate @"waterdelegateCondition"
#define waterMainCondition @"waterMainCond"
/////

//////// Dincharya///////

#define DincharyaMainCon @"DincharyaMainCondition"
#define DincharyaDelegate @"DincharyadelegateCondition"
#define dincharyaName @"Dincharya_Name"
#define dincharyaYogaAr @"DincharyaYogaAr"
#define dincharyaPranaAr @"DincharyaPranaAr"
#define dincharyaDelayTime @"DincharyaDelaytime"

#define Is_Perchaged @"AlreadyPerchaged"


#define isPause1 @"pause"
#define Introduction @"Intro"
#define Languagevalue @"language"
#define klReminder @ "Reminder"
#define kldate @"date"
#define kISSound @"isSound"
#define kISSoundEver @"isSoundEver"
#define kISVibrator @"isVibrator"
#define kISKeepDisplay @"isKeepDisplayOn"
#define kVolume @"volume"
#define preparationtime @"preprationTime"
#define soundType @"Soundtype"
#define anulomInhale @"lblInhaleanulom"
#define anulomExhale @"lblExhaleanulom"
#define anulomHold @"lblHoldanulom"
#define anulomRound @"lblRoundsanulom"
#define anulomAfHold @"lblAfHoldanulom"

#define kapalRound @"roundkapal"
#define kapaltime @"timekapal"

#define bhramariInhale @"inhalebhramari"
#define bhramariExhale @"exhalebhramari"
#define bhramariRound @"roundsbhramari"

#define suryaInhale @"lblInhalesurya"
#define suryaExhale @"lblExhalesurya"
#define suryaHold @"lblHoldsurya"
#define suryaRound @"lblRoundssurya"

#define chandraInhale @"lblInhalechandra"
#define chandraExhale @"lblExhalechandra"
#define chandraHold @"lblHoldchandra"
#define chandraRound @"lblRoundschandra"

#define bhastrikaInhale @"inhalebhastrika"
#define bhastrikaExhale @"exhalebhastrika"
#define bhastrikaRound @"roundsbhastrika"

#define sheetaliInhale @"lblInhalesheetali"
#define sheetaliExhale @"lblExhalesheetali"
#define sheetaliHold @"lblHoldsheetali"
#define sheetaliRound @"lblRoundssheetali"

#define meditativeRound @"meditativeRound"

#define UdgeethInhale @"udgeethInhale"
#define UdgeethExhale_O @"udgeethExhaleO"
#define UdgeethExhale_M @"udgeethExhaleM"
#define UdgeethRound @"udgeethRound"

#define bahyaInhale @"BahyaInhale"
#define bahyaExhale @"BahyaExhale"
#define bahyahold @"Bhayahold"
#define bahyaRound @"BahyaRound"

#endif

//static inline UIFont *LightRobotoFont(CGFloat size) {
//    return [UIFont fontWithName:@"HelveticaNeueCE-Light" size:size];
//}
//
//static inline UIFont *RegularRobotoFont(CGFloat size) {
//    return [UIFont fontWithName:@"McLaren-Regular" size:size];
//}
//
//static inline UIFont *BoldRobotoFont(CGFloat size) {
//    return [UIFont fontWithName:@"Helvetica-Condensed" size:size];
//}
static inline UIFont *Ralewayfont(CGFloat size) {
    return [UIFont fontWithName:@"Raleway-Medium" size:size];
}

//
//  KapalbhatiActionVC.h
//  Pranayama
//
//  Created by Manish Kumar on 02/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol kapaldelegate <NSObject>

-(void)playsound;

@end

@interface KapalbhatiActionVC : GAITrackedViewController
{
    IBOutlet UIView *topview;
    
    IBOutlet UILabel *totaltimelbl;
    IBOutlet UILabel *roundlbl;
    IBOutlet UILabel *actionlbl;
    IBOutlet UILabel *lblprepareTime;
    IBOutlet UILabel *LblPrepare;
    
    
    IBOutlet UIButton *btnStop;
    IBOutlet UIButton *btnpuse;
    
    IBOutlet UILabel *lblHours;
    IBOutlet UILabel *lblminutes;
    IBOutlet UILabel *lblseconds;
    IBOutlet UILabel *lblTimer;
    IBOutlet UILabel *lblmode;
    IBOutlet UIImageView *imageview;
    
    NSTimer *startTimer;
    NSTimer *SecTimer;
    BOOL isPause;
    BOOL PlayStop;
    int seconds1;
    int minutes1;
    int hours1;
    BOOL kapal_position;
    int report_round;
    float timetrail;
    int kapalCount;
    int prepare;
    int pauseincrease;
    
}
@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic) int rounds;
@property (nonatomic) int totalTime;
@property (nonatomic) int minutes;

@property (nonatomic,assign)id <kapaldelegate>delegate;
@end

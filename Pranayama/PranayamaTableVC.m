//
//  PranayamaTableVC.m
//  Pranayama
//
//  Created by Manish Kumar on 17/06/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "PranayamaTableVC.h"

@interface PranayamaTableVC ()

@end

@implementation PranayamaTableVC

@synthesize table;
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    self.screenName = @"PranayamaTable";
     self.navigationController.navigationBar.tintColor =RGB(230, 123, 27);
    if (DEVICE==IPAD) {
        fontSize=24;
        fontSize2=16;
    }
    else{
        fontSize=18;
        fontSize2=10;
    }
    self.automaticallyAdjustsScrollViewInsets=NO;
    table.scrollEnabled = NO;
    self.navigationItem.hidesBackButton=YES;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    int a=(self.view.frame.size.height-64)/7;
    table.rowHeight=a;
}


- (void)HomeMethod:(NSNotification *)notification {
    [[CommonSounds sharedInstance] appDidBecomeActive:notification navigation:self.navigationController];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //If this vc can be poped , then
    if (self.navigationController.viewControllers.count > 1)
    {
        // Disabling pan gesture for left menu
        //        [self disableSlidePanGestureForLeftMenu];
    }
    
    AMSlideMenuMainViewController *mainVC = [AMSlideMenuMainViewController getInstanceForVC:self];
    if (mainVC.rightMenu)
    {
        // Adding right menu button to navigation bar
        [self addRightMenuButton];
    }
    [[NSUserDefaults standardUserDefaults] setObject:@"PranayamaList" forKey:controllerType];
    [self ChangeText];
 
}


-(void)ChangeText{
    NSString *habbitType=[[NSUserDefaults standardUserDefaults] objectForKey:HabbitType];
    NSString *lan=[[NSUserDefaults standardUserDefaults] objectForKey:Languagevalue];
    if ([lan isEqualToString:Hindi]) {
        self.navigationItem.title = [MCLocalization stringForKey:@"Pranayam"];
        if ([habbitType isEqualToString:@"Thyroid"]) {
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Anulom_Vilom"],[MCLocalization stringForKey:@"kapalbhati"],[MCLocalization stringForKey:@"ujjayi"], nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"anulomvilom",@"kapalbhati",@"ujjayi", nil];
        }
        else if ([habbitType isEqualToString:@"Insomnia"]){
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"bhramari"],[MCLocalization stringForKey:@"surya_bhedana"],[MCLocalization stringForKey:@"meditative"],  nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"bhramari",@"suryabhedana",@"meditativebreathing", nil];
        }
        else if ([habbitType isEqualToString:@"Diabetes"]){
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Anulom_Vilom"],[MCLocalization stringForKey:@"kapalbhati"],[MCLocalization stringForKey:@"bhastrika"],  nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"anulomvilom",@"kapalbhati",@"bhastrika", nil];
        }
        else if ([habbitType isEqualToString:@"Migraine"])
        {
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Anulom_Vilom"],[MCLocalization stringForKey:@"bhramari"],[MCLocalization stringForKey:@"bhastrika"],  nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"anulomvilom",@"bhramari",@"bhastrika", nil];
        }
        
        else if ([habbitType isEqualToString:@"Weight Loss"])
        {
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Anulom_Vilom"],[MCLocalization stringForKey:@"kapalbhati"],[MCLocalization stringForKey:@"bhastrika"],  nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"anulomvilom",@"kapalbhati",@"bhastrika", nil];
        }
        
        else if ([habbitType isEqualToString:@"Asthma"])
 
        {
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Anulom_Vilom"],[MCLocalization stringForKey:@"bhastrika"],[MCLocalization stringForKey:@"ujjayi"],  nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"anulomvilom",@"bhastrika",@"ujjayi", nil];
        }
        
        else
        {
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Anulom_Vilom"],[MCLocalization stringForKey:@"udgeeth"],[MCLocalization stringForKey:@"bahya"],  nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"anulomvilom",@"udgeethseuge",@"bahyaseuge", nil];
        }

    }
    else if([lan isEqualToString:English]){
        self.navigationItem.title= @"Pranayam";
        NSString *anulomstr=[@"AnulomVilom " stringByAppendingString:@"(Alternate Nostrils)"];
        NSMutableAttributedString *anulom = [[NSMutableAttributedString alloc] initWithString:anulomstr];
        [anulom addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 11 )];
        [anulom addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(12, 20 )];
        
        NSString *kapalstr=[@"Kapalbhati " stringByAppendingString:@"(Skull Purification)"];
        NSMutableAttributedString *kapal = [[NSMutableAttributedString alloc] initWithString:kapalstr];
        [kapal addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 10 )];
        [kapal addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(11, 20 )];
        
        NSString *ujjayistr=[@"Ujjayi " stringByAppendingString:@"(Ocean)"];
        NSMutableAttributedString *ujjayi = [[NSMutableAttributedString alloc] initWithString:ujjayistr];
        [ujjayi addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0,6 )];
        [ujjayi addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(7, 7 )];
        
        NSString *bhramaristr=[@"Bhramari " stringByAppendingString:@"(Humming Bee Breath)"];
        NSMutableAttributedString *bhramari = [[NSMutableAttributedString alloc] initWithString:bhramaristr];
        [bhramari addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 8 )];
        [bhramari addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(8, 21 )];
        
        NSString *suryastr=[@"Surya Bhedana " stringByAppendingString:@"(Right Nostril)"];
        NSMutableAttributedString *surya = [[NSMutableAttributedString alloc] initWithString:suryastr];
        [surya addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 13 )];
        [surya addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(14, 15 )];
        
        NSString *meditativestr=[@"Meditative Breathing " stringByAppendingString:@"(4:7:8)"];
        NSMutableAttributedString *meditative = [[NSMutableAttributedString alloc] initWithString:meditativestr];
        [meditative addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 20 )];
        [meditative addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(21, 7 )];
        
        NSString *bhastrikastr=[@"Bhastrika " stringByAppendingString:@"(Bellows)"];
        NSMutableAttributedString *bhastrika = [[NSMutableAttributedString alloc] initWithString:bhastrikastr];
        [bhastrika addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 9 )];
        [bhastrika addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(9, 10 )];
        
        NSString *udgeethstr=[@"Udgeeth" stringByAppendingString:@" (Chanting Breath)"];
        NSMutableAttributedString *udgeeth = [[NSMutableAttributedString alloc] initWithString:udgeethstr];
        [udgeeth addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 6 )];
        [udgeeth addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(7, 18 )];
        
        NSString *bahyastr=[@"Bahya" stringByAppendingString:@" (External Kumbhak)"];
        NSMutableAttributedString *bahya = [[NSMutableAttributedString alloc] initWithString:bahyastr];
        [bahya addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, 5 )];
        [bahya addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(5, 19 )];

        
        if ([habbitType isEqualToString:@"Thyroid"]) {
            
            homelist=[[NSMutableArray alloc] initWithObjects:anulom,kapal,ujjayi, nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"anulomvilom",@"kapalbhati",@"ujjayi", nil];
        }
        else if ([habbitType isEqualToString:@"Insomnia"]){
            
            homelist=[[NSMutableArray alloc] initWithObjects:bhramari,surya,meditative, nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"bhramari",@"suryabhedana",@"meditativebreathing", nil];
        }
        else if ([habbitType isEqualToString:@"Diabetes"]){
            
            homelist=[[NSMutableArray alloc] initWithObjects:anulom,kapal,bhastrika, nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"anulomvilom",@"kapalbhati",@"bhastrika", nil];
        }
       else if ([habbitType isEqualToString:@"Migraine"])
       {
            homelist=[[NSMutableArray alloc] initWithObjects:anulom,bhramari,bhastrika, nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"anulomvilom",@"bhramari",@"bhastrika", nil];
        }
        
       else if ([habbitType isEqualToString:@"Weight Loss"])
       {
           homelist=[[NSMutableArray alloc] initWithObjects:anulom,kapal,bhastrika, nil];
           conditionArr=[[NSMutableArray alloc] initWithObjects:@"anulomvilom",@"kapalbhati",@"bhastrika", nil];
       }

       else if ([habbitType isEqualToString:@"Asthma"])
       {
           homelist=[[NSMutableArray alloc] initWithObjects:anulom,bhastrika,ujjayi, nil];
           conditionArr=[[NSMutableArray alloc] initWithObjects:@"anulomvilom",@"bhastrika",@"ujjayi", nil];
       }

       else
       {
           homelist=[[NSMutableArray alloc] initWithObjects:anulom,udgeeth,bahya, nil];
           conditionArr=[[NSMutableArray alloc] initWithObjects:@"anulomvilom",@"udgeethseuge",@"bahyaseuge", nil];
       }
    }
    else{
        self.navigationItem.title = [MCLocalization stringForKey:@"Pranayam"];
        if ([habbitType isEqualToString:@"Thyroid"]) {
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Anulom_Vilom"],[MCLocalization stringForKey:@"kapalbhati"],[MCLocalization stringForKey:@"ujjayi"], nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"anulomvilom",@"kapalbhati",@"ujjayi", nil];
        }
        else if ([habbitType isEqualToString:@"Insomnia"]){
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"bhramari"],[MCLocalization stringForKey:@"surya_bhedana"],[MCLocalization stringForKey:@"meditative"],  nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"bhramari",@"suryabhedana",@"meditativebreathing", nil];
        }
        else if ([habbitType isEqualToString:@"Diabetes"]){
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Anulom_Vilom"],[MCLocalization stringForKey:@"kapalbhati"],[MCLocalization stringForKey:@"bhastrika"],  nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"anulomvilom",@"kapalbhati",@"bhastrika", nil];
        }
        else if ([habbitType isEqualToString:@"Migraine"])
        {
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Anulom_Vilom"],[MCLocalization stringForKey:@"bhramari"],[MCLocalization stringForKey:@"bhastrika"],  nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"anulomvilom",@"bhramari",@"bhastrika", nil];
        }
        
        else if ([habbitType isEqualToString:@"Weight Loss"])
        {
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Anulom_Vilom"],[MCLocalization stringForKey:@"kapalbhati"],[MCLocalization stringForKey:@"bhastrika"],  nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"anulomvilom",@"kapalbhati",@"bhastrika", nil];
        }
        
        else if ([habbitType isEqualToString:@"Asthma"])
            
        {
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Anulom_Vilom"],[MCLocalization stringForKey:@"bhastrika"],[MCLocalization stringForKey:@"ujjayi"],  nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"anulomvilom",@"bhastrika",@"ujjayi", nil];
        }
        
        else
        {
            homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Anulom_Vilom"],[MCLocalization stringForKey:@"udgeeth"],[MCLocalization stringForKey:@"bahya"],  nil];
            conditionArr=[[NSMutableArray alloc] initWithObjects:@"anulomvilom",@"udgeethseuge",@"bahyaseuge", nil];
        }

    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)backbutton:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [homelist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cellidentifier";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.backgroundColor=[UIColor clearColor];
    NSString *lan=[[NSUserDefaults standardUserDefaults] objectForKey:Languagevalue];
    if ([lan isEqualToString:Hindi]) {
        cell.textLabel.text=[homelist objectAtIndex:indexPath.row];
        cell.textLabel.font= Ralewayfont(fontSize);
    }
    else{
        cell.textLabel.attributedText=[homelist objectAtIndex:indexPath.row];
    }
    cell.textLabel.textColor=[UIColor whiteColor];
    int colors = indexPath.row%2;
    switch (colors) {
        case 0:
            cell.backgroundColor=[UIColor colorWithRed:187/255.0 green:20/255.0 blue:37/255.0 alpha:0.5];
            break;
        case 1:
            cell.backgroundColor=[UIColor colorWithRed:176/255.0 green:12/255.0 blue:28/255.0 alpha:0.5];
            break;
        default:
            break;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index=indexPath.row;
    switch (index) {
        case 0:
            [self performSegueWithIdentifier:[conditionArr objectAtIndex:0] sender:self];
            break;
        case 1:
            [self performSegueWithIdentifier:[conditionArr objectAtIndex:1] sender:self];
            break;
        case 2:
            [self performSegueWithIdentifier:[conditionArr objectAtIndex:2] sender:self];
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

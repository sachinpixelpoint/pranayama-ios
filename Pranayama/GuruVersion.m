//
//  GuruVersion.m
//  Pranayama
//
//  Created by Manish Kumar on 16/09/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "GuruVersion.h"
#define kRemoveAdsProductIdentifier @"com.pixelpranayama.app.dincharya"
@interface GuruVersion ()

@end

@implementation GuruVersion

- (void)viewDidLoad {
    [super viewDidLoad];
    if (DEVICE==IPAD) {
        fontSize=24;
    }
    else{
        fontSize=18;
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
//    [self customwork];
//     [self changetext];
}

-(IBAction)PerchageBtn_Method:(id)sender{
    SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:kRemoveAdsProductIdentifier]];
    productsRequest.delegate = self;
    [productsRequest start];
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    SKProduct *validProduct = nil;
    int count = [response.products count];
    if(count > 0){
        validProduct = [response.products objectAtIndex:0];
        NSLog(@"Products Available!");
        [self purchase:validProduct];
    }
    else if(!validProduct){
        NSLog(@"No products available");
        //this is called if your product id is not valid, this shouldn't be called unless that happens.
    }
}

- (void)purchase:(SKProduct *)product{
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    NSLog(@"received restored transactions: %lu", (unsigned long)queue.transactions.count);
    for(SKPaymentTransaction *transaction in queue.transactions){
        if(transaction.transactionState == SKPaymentTransactionStateRestored){
            //called when the user successfully restores a purchase
            NSLog(@"Transaction state -> Restored");
            
            //if you have more than one in-app purchase product,
            //you restore the correct product for the identifier.
            //For example, you could use
            //if(productID == kRemoveAdsProductIdentifier)
            //to get the product identifier for the
            //restored purchases, you can use
            //
            //NSString *productID = transaction.payment.productIdentifier;
//            [self doRemoveAds];
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            break;
        }
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
    for(SKPaymentTransaction *transaction in transactions){
        switch(transaction.transactionState){
            case SKPaymentTransactionStatePurchasing: NSLog(@"Transaction state -> Purchasing");
                //called when the user is in the process of purchasing, do not add any of your own code here.
                break;
            case SKPaymentTransactionStatePurchased:
                //this is called when the user has successfully purchased the package (Cha-Ching!)
//                [self doRemoveAds]; //you can add your code for what you want to happen when the user buys the purchase here, for this tutorial we use removing ads
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                NSLog(@"Transaction state -> Purchased");
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"Transaction state -> Restored");
                //add the same code as you did from SKPaymentTransactionStatePurchased here
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                //called when the transaction does not finish
                if(transaction.error.code == SKErrorPaymentCancelled){
                    NSLog(@"Transaction state -> Cancelled");
                    //the user cancelled the payment ;(
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
        }
    }
}

    -(void) customwork
    {
        image=[[UIImageView alloc] initWithFrame:CGRectMake(scroller.frame.size.width/2-40,scroller.frame.origin.y-85, 100, 100)];
        image.image=[UIImage imageNamed:@"app_icon"];
        [scroller addSubview:image];
        gurulbl=[[UILabel alloc ]initWithFrame:CGRectMake(self.view.frame.size.width/2-45, scroller.frame.origin.y+30,image.frame.size.width+10 , 20)];
        [gurulbl setText:@"Guru Version"];
        gurulbl.font=Ralewayfont(fontSize);
        [scroller addSubview:gurulbl];
        textlabel=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-145 , scroller.frame.origin.y+60, scroller.frame.size.width-20, 80)];
        // [textlabel setText:@"Unlock advanced features and hlp us to improve the app by supporting us."];
        textlabel.font=Ralewayfont(fontSize);
        textlabel.numberOfLines=0;
        [scroller addSubview:textlabel];
        ///////
        UIImageView *image0 =[[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-140 ,scroller.frame.origin.y+140,20,20)];
        image0.image=[UIImage imageNamed:@"remove"];
        [scroller addSubview:image0];
        
        //////////
        
        UIImageView *image1 =[[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-140 ,scroller.frame.origin.y+180,20,20)];
        image1.image=[UIImage imageNamed:@"pranay"];
        [scroller addSubview:image1];
        ////////////
        UIImageView *image2 =[[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-140 ,scroller.frame.origin.y+220,20,20)];
        image2.image=[UIImage imageNamed:@"yoga"];
        [scroller addSubview:image2];
        ///////////
        UIImageView *image3 =[[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-140 ,scroller.frame.origin.y+275,20,20)];
        image3.image=[UIImage imageNamed:@"calender"];
        [scroller addSubview:image3];
        
        /////////
        UIImageView *image4 =[[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-140 ,scroller.frame.origin.y+349,20,20)];
        image4.image=[UIImage imageNamed:@"ok"];
        [scroller addSubview:image4];
        
        ///////
        
        fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-105, scroller.frame.origin.y+135, scroller.frame.size.width-20, 30)];
        fromLabel.numberOfLines = 1;
        fromLabel.textColor = [UIColor blackColor];
        fromLabel.font=Ralewayfont(fontSize);
        fromLabel.textAlignment = NSTextAlignmentLeft;
        [fromLabel setText:@"Remove annoying ads"];
        [scroller addSubview:fromLabel];
        
        ////////////
        from = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-105, scroller.frame.origin.y+175, scroller.frame.size.width-20, 30)];
        from.numberOfLines = 1;
        from.textColor = [UIColor blackColor];
        from.font=Ralewayfont(fontSize);
        from.textAlignment = NSTextAlignmentLeft;
        //  [from setText:@"Get access to more pranayama"];
        [scroller addSubview:from];
        //////////////////////////
        from1 = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-105, scroller.frame.origin.y+215, scroller.frame.size.width-20, 30)];
        from1.numberOfLines = 1;
        from1.textColor = [UIColor blackColor];
        from1.font=Ralewayfont(fontSize);
        from1.textAlignment = NSTextAlignmentLeft;
        // [from1 setText:@"Get access to more yogasanas"];
        [scroller addSubview:from1];
        ////////////////////////
        from2 = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-105, scroller.frame.origin.y+250, scroller.frame.size.width-50, 80)];
        from2.numberOfLines = 0;
        from2.textColor = [UIColor blackColor];
        from2.font=Ralewayfont(fontSize);
        from2.textAlignment = NSTextAlignmentLeft;
        //  [from2 setText:@"Get access to more habbit : Asthma joint pain and weight Loss"];
        [scroller addSubview:from2];
        
        ////////////
        from3 = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-105, scroller.frame.origin.y+330, scroller.frame.size.width-60, 90)];
        from3.numberOfLines = 0;
        from3.textColor = [UIColor blackColor];
        from3.font=Ralewayfont(fontSize);
        from3.textAlignment = NSTextAlignmentLeft;
        // [from3 setText:@"You can make your own package of your schedule by selecting paranayamas and asanas you wish."];
        [scroller addSubview:from3];
        
        scroller.contentSize=CGSizeMake(scroller.frame.size.width,scroller.frame.size.height+40);
 
    }

-(IBAction)backbutton:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}


-(IBAction)purchasebutton:(id)sender
{
  
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //If this vc can be poped , then
    if (self.navigationController.viewControllers.count > 1)
    {
        // Disabling pan gesture for left menu
        //        [self disableSlidePanGestureForLeftMenu];
    }
    
    AMSlideMenuMainViewController *mainVC = [AMSlideMenuMainViewController getInstanceForVC:self];
    if (mainVC.rightMenu)
    {
        // Adding right menu button to navigation bar
        [self addRightMenuButton];
    }
}

    -(void)changetext{
        NSString *lan=[[NSUserDefaults standardUserDefaults] objectForKey:Languagevalue];
        if ([lan isEqualToString:Hindi]) {
            textlabel.text= [MCLocalization stringForKey:@"gurutext"];
            fromLabel.text=[MCLocalization stringForKey:@"annoying_ads"];
            from.text=[MCLocalization stringForKey:@"add_more"];
            from1.text=[MCLocalization stringForKey:@"addyogasanas"];
            from2.text=[MCLocalization stringForKey:@"addhabits"];
            from3.text=[MCLocalization stringForKey:@"dincharya_details"];
            
        }
        else if([lan isEqualToString:English]){
            self.navigationItem.title= @"GuruVersion";
            textlabel.text=@"Unlock advanced features and hlp us to improve the app by supporting us.";
            fromLabel.text=@"Remove annoying ads";
            from.text=@"Get access to more pranayama";
            from1.text=@"Get access to more yogasanas";
            from2.text=@"Get access to more habbit : Asthma joint pain and weight Loss";
            from3.text=@"You can make your own package of your schedule by selecting paranayamas and asanas you wish.";
        }
        else{
            textlabel.text= [MCLocalization stringForKey:@"gurutext"];
            fromLabel.text=[MCLocalization stringForKey:@"annoying_ads"];
            from.text=[MCLocalization stringForKey:@"add_more"];
            from1.text=[MCLocalization stringForKey:@"addyogasanas"];
            from2.text=[MCLocalization stringForKey:@"addhabits"];
            from3.text=[MCLocalization stringForKey:@"dincharya_details"];
        }
    }





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  UjjayiActionVC.m
//  Pranayama
//
//  Created by Manish Kumar on 17/06/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "UjjayiActionVC.h"

@interface UjjayiActionVC ()

@end

@implementation UjjayiActionVC

@synthesize totalTime,lblhold,lblexhale,lblinhale,rounds;
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //    [self setlblheight];
    if (DEVICE == Iphone) {
        topview.frame = CGRectMake(8, 74, self.view.frame.size.width-16, 32);
    }
    //google analytic
    NSString *name=@"Ujjayi ";
    NSString *ratio=[NSString stringWithFormat:@"%d:%d:%d:%d",lblinhale,lblhold,lblexhale,rounds];
    
    NSString *AppendStr=[name stringByAppendingString:ratio];
    self.screenName=AppendStr;
    //
    // Do any additional setup after loading the view.
    self.navigationItem.hidesBackButton=YES;
    int fontSize;
    int totaltimeFont;
    int lblmodeFont;
    if (DEVICE==IPAD) {
        fontSize=24;
        totaltimeFont=24;
        lblmodeFont=36;
    }
    else{
        fontSize=18;
        totaltimeFont=12;
        lblmodeFont=22;
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback
                                     withOptions:AVAudioSessionCategoryOptionMixWithOthers
                                           error:nil];
    self.managedObjectContext=[kAppDele managedObjectContext];
    prepare = [[NSUserDefaults standardUserDefaults] floatForKey:preparationtime];
    NSInteger value = [[NSUserDefaults standardUserDefaults]integerForKey:dincharyaDelayTime];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaMainCon] && value>0) {
        prepare = (int) value ;
    }
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:isPause1];
    PlayStop=NO;
    roundForReport=rounds;
    totaltimelbl.font=Ralewayfont(totaltimeFont);
    actionlbl.font=Ralewayfont(lblmodeFont);
    lblmode.font=Ralewayfont(lblmodeFont);
    [self ChangeText];
    [self startButtonPreshed];
    [self setValueoflbl];
}


- (void)HomeMethod:(NSNotification *)notification {
    [[CommonSounds sharedInstance] appDidBecomeActive:notification navigation:self.navigationController];
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self disableSlidePanGestureForRightMenu];
}


-(void)ChangeText{
    
    self.navigationItem.title = [MCLocalization stringForKey:@"ujjayi"];
    totaltimelbl.text=[MCLocalization stringForKey:@"Total_Time"];
    actionlbl.text=[MCLocalization stringForKey:@"Action"];
}

-(void)setValueoflbl
{
    seconds = totalTime % 60;
    minutes = (totalTime / 60) % 60;
    hours = totalTime / 3600;
    lblseconds.text=[NSString stringWithFormat:@"%d",seconds];
    lblminutes.text=[NSString stringWithFormat:@"%d",minutes];
    lblHours.text=[NSString stringWithFormat:@"%d",hours];
    lblinhale1=lblinhale;
    lblhold1=lblhold;
    lblexhale1=lblexhale;
}

-(IBAction)pausebutton:(id)sender{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:isPause1];
    PlayStop=YES;
    [btnStop setImage:[UIImage imageNamed:@"start_icon"] forState:UIControlStateNormal];
    [startTimer invalidate];
}

-(IBAction)stopbutton:(id)sender{
    if (PlayStop==YES) {
        startTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                      target: self
                                                    selector:@selector(onTick:)
                                                    userInfo: nil repeats:YES];
        [btnStop setImage:[UIImage imageNamed:@"stop_icon"] forState:UIControlStateNormal];
        PlayStop=NO;
    }
    else{
        
        [startTimer invalidate];
        if (ujjayiCount>0) {
            [self insertReportIntoDatabase];
            [self insertintoProgressReport:YES];
            [self saveInDincharya:NO];
            
        }
        [[self navigationController] popViewControllerAnimated:NO];
        ujjayiCount=0;
    }
}

-(void)startButtonPreshed
{
    startTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                  target: self
                                                selector:@selector(onTick:)
                                                userInfo: nil repeats:YES];
}
-(void)onTick:(NSTimer *)timer {
    isPause=[[NSUserDefaults standardUserDefaults] boolForKey:isPause1];
    if (isPause) {
        [self pausebutton:isPause1];
    }
    else{
        if (prepare>0) {
            lblmode.text=[MCLocalization stringForKey:@"preparation_time"];
            lblTimer.text=[NSString stringWithFormat:@"%d",prepare];
            [[CommonSounds sharedInstance] playEverySound];
            prepare--;
        }
        else{
            
            if (totalTime>0) {
                seconds = totalTime % 60;
                minutes = (totalTime / 60) % 60;
                hours = totalTime / 3600;
                lblseconds.text=[NSString stringWithFormat:@"%d",seconds];
                lblminutes.text=[NSString stringWithFormat:@"%d",minutes];
                lblHours.text=[NSString stringWithFormat:@"%d",hours];
                if (rounds>0) {
                    if (lblinhale1>0) {
                        if (lblinhale1==lblinhale) {
                            [[CommonSounds sharedInstance] playInhaleSound];
                        }
                        else{
                            [[CommonSounds sharedInstance] playEverySound];
                        }
                        imageview.image = [UIImage imageNamed:@"action_inhale_both"];
                        lblmode.text=[MCLocalization stringForKey:@"Inhale_left"];
                        lblTimer.text=[NSString stringWithFormat:@"%d",lblinhale1];
                        lblinhale1=lblinhale1-1;
                    }
                    else if (lblhold1>0){
                        if (lblhold1==lblhold) {
                            [[CommonSounds sharedInstance] playHoldSound];
                        }
                        else{
                            [[CommonSounds sharedInstance] playEverySound];
                        }
                        imageview.image=[UIImage imageNamed:@"jalandrabandh"];
                        lblmode.text=[MCLocalization stringForKey:@"hold_hold"];
                        lblTimer.text=[NSString stringWithFormat:@"%d",lblhold1];
                        lblhold1=lblhold1-1;
                        
                    }
                    else if (lblexhale1>0) {
                        if (lblexhale1==lblexhale) {
                            [[CommonSounds sharedInstance] playExhaleSound];
                        }
                        else{
                            [[CommonSounds sharedInstance] playEverySound];
                        }
                        imageview.image=[UIImage imageNamed:@"Action_exhale_left"];
                        lblmode.text=[MCLocalization stringForKey:@"Exhale_right"];
                        lblTimer.text=[NSString stringWithFormat:@"%d",lblexhale1];
                        lblexhale1=lblexhale1-1;
                        if (lblexhale1==0) {
                            rounds=rounds-1;
                            [self setValueoflbl];
                        }
                        
                    }
                }
            }
            else{
                [timer invalidate];
                timer = nil;
                if ([self.delegate respondsToSelector:@selector(playsound)]) {
                    [self.delegate playsound];
                    [self insertReportIntoDatabase];
                    [self insertintoProgressReport:NO];
                    [self saveInDincharya:YES];
                }
                [[self navigationController] popViewControllerAnimated:NO];
            }
            totalTime=totalTime-1;
            ujjayiCount++;
        }
    }
}
-(void)insertReportIntoDatabase{
    NSArray *array=[kAppDele fetchReportFromDatabseAccordingToType:@"Ujjayi"];
    if (array.count>0) {
        Record *recordEntity=[array objectAtIndex:0];
        NSDate *str=recordEntity.anulomDate;
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        NSString *datestr=[dateFormatter stringFromDate:str];
        NSString *date=[dateFormatter stringFromDate:[NSDate date]];
        if ([datestr isEqualToString:date]) {
            NSString *duration=[NSString stringWithFormat:@"%d",[recordEntity.anulomduration intValue]+ujjayiCount];
            recordEntity.anulomduration=duration;
            NSError *error;
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"%@",error);
            }
        }
        
        else{
            Record *reportEntity = [NSEntityDescription insertNewObjectForEntityForName:@"Record" inManagedObjectContext:self.managedObjectContext];
            reportEntity.anulomLevel=[NSString stringWithFormat:@"%d:%d:%d",lblinhale,lblhold,lblexhale];
            reportEntity.anulomRounds=[NSString stringWithFormat:@"%d",roundForReport];
            reportEntity.anulomDate=[NSDate date];
            reportEntity.anulomduration=[NSString stringWithFormat:@"%d",ujjayiCount];
            reportEntity.anulomtype=@"Ujjayi";
            NSError *error;
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"%@",error);
            }
        }
    }
    else{
        Record *reportEntity = [NSEntityDescription insertNewObjectForEntityForName:@"Record" inManagedObjectContext:self.managedObjectContext];
        reportEntity.anulomLevel=[NSString stringWithFormat:@"%d:%d:%d",lblinhale,lblhold,lblexhale];
        reportEntity.anulomRounds=[NSString stringWithFormat:@"%d",roundForReport];
        reportEntity.anulomDate=[NSDate date];
        reportEntity.anulomduration=[NSString stringWithFormat:@"%d",ujjayiCount];
        reportEntity.anulomtype=@"Ujjayi";
        NSError *error;
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"%@",error);
        }
    }
    Report *reportEntity = [NSEntityDescription insertNewObjectForEntityForName:@"Report" inManagedObjectContext:self.managedObjectContext];
    reportEntity.reportLevel=[NSString stringWithFormat:@"%d:%d:%d",lblinhale,lblhold,lblexhale];
    reportEntity.reportRounds=[NSString stringWithFormat:@"%d",roundForReport];
    reportEntity.reportDate=[NSDate date];
    reportEntity.reportduration=[NSString stringWithFormat:@"%d",ujjayiCount];
    reportEntity.reporttype=@"Ujjayi";
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"%@",error);
    }
}

-(void)insertintoProgressReport:(BOOL)value{
    ////progress Report ///////////
    
    NSString *habbitconditon=[[NSUserDefaults standardUserDefaults]objectForKey:HabbitCondition];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:HabbitMainCondition]) {
        NSArray *Progressarray=[kAppDele fetchReportFromProgressDatabseAccordingToType:habbitconditon];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"Progress" inManagedObjectContext:self.managedObjectContext]];
        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"progressType==%@ AND days==%d",habbitconditon,Progressarray.count];
        [request setPredicate:predicate];
        
        NSError *error1=nil;
        NSArray *results=[self.managedObjectContext executeFetchRequest:request error:&error1];
        Progress *entity=[results objectAtIndex:0];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:viewwillCondition];
        if (value==YES) {
            NSNumber *duration=[NSNumber numberWithInt:[entity.duration intValue]+ujjayiCount];
            entity.duration=duration;
        }
        else{
            entity.ujjayi=[NSNumber numberWithBool:YES];
            NSNumber *duration=[NSNumber numberWithInt:[entity.duration intValue]+ujjayiCount];
            entity.duration=duration;
        }
        [self.managedObjectContext save:nil];
    }
}

-(void)saveInDincharya:(BOOL)Value{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaMainCon]) {
        
        NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaName];
        NSArray *ar = [kAppDele FetchDincharyaFromDatabaseAcordingToName:str];
        Dincharya *dincharya = [ar objectAtIndex:0];
        NSArray *yogaAr = [NSKeyedUnarchiver unarchiveObjectWithData:dincharya.yogasana_arr];
        NSArray *pranaMainList = [NSKeyedUnarchiver unarchiveObjectWithData:dincharya.pranayama_arr];
        NSArray *pranaList = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaPranaAr];
        
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:viewwillCondition];
        
        if (pranaMainList.count == pranaList.count  && yogaAr.count == 0) {
            [[CommonSounds sharedInstance]Timecompare:Value managedObject:self.managedObjectContext pranayama:@"Ujjayi"];
        }
        else{
            [[CommonSounds sharedInstance]UpdateDincharyaIntoDatabase:Value mangedObject:self.managedObjectContext Prana:@"Ujjayi"];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
@end

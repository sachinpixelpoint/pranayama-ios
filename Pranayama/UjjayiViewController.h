//
//  UjjayiViewController.h
//  Pranayama
//
//  Created by Manish Kumar on 17/06/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UjjayiActionVC.h"
@interface UjjayiViewController : GAITrackedViewController<Ujjayidelegate>
{
    IBOutlet UIPickerView *inhalePicker;
    IBOutlet UIPickerView *holdPicker;
    IBOutlet UIPickerView *exhalePicker;
    IBOutlet UIPickerView *roundPicker;
    int InhaleRow;
    int HoldRow;
    int ExhaleRow;
    int RoundRow;
    NSMutableArray *In_roundData;
    NSMutableArray *holdData;
    NSMutableArray *exhaleData;
    
    IBOutlet UIView *topview;
    IBOutlet UIImageView *img;
    
    IBOutlet UILabel *inhalelbl;
    IBOutlet UILabel *holdlbl;
    IBOutlet UILabel *exhalelbl;
    IBOutlet UILabel *roundlbl;
    
    IBOutlet UIButton *btnLeftInhel;
    IBOutlet UIButton *btnRightExhale;
    IBOutlet UIButton *StartButton;
    IBOutlet UIButton *reportButton;
    
    IBOutlet UILabel *lblHours;
    IBOutlet UILabel *lblminutes;
    IBOutlet UILabel *lblseconds;
    int totaltime;
    int seconds;
    int minutes;
    int hours;
    UIButton *button1,*button2,*button3;
    BOOL isMenuVisible;
    IBOutlet UIButton *Floatingbutton;
    BOOL selectCondition;
    BOOL buttonCondition;
    NSTimer *timer1;
    NSString *TodayFitness,*Daystr,*Your_leve,*Fitness_regime_21,*Share_experience,*session;
}

@end

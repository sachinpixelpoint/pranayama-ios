//
//  AnulomVilomActionVC.m
//  Pranayama
//
//  Created by Manish Kumar on 31/03/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "AnulomVilomActionVC.h"
#import "AnulomVilom.h"

@interface AnulomVilomActionVC ()

@end

@implementation AnulomVilomActionVC
@synthesize totalTime,lblhold,lblexhale,lblinhale,rounds,HoldAfter;
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //    [self setlblheight];
    if (DEVICE == Iphone) {
        topview.frame = CGRectMake(8, 74, self.view.frame.size.width-16, 32);
    }
    
    //google analytic
    NSString *name=@"AnulomVilom ";
    NSString *ratio=[NSString stringWithFormat:@"%d:%d:%d:%d:%d",lblinhale,lblhold,lblexhale,HoldAfter,rounds];
    
    NSString *AppendStr=[name stringByAppendingString:ratio];
    self.screenName=AppendStr;
    //
    // Do any additional setup after loading the view.
    self.navigationItem.hidesBackButton=YES;
    int fontSize;
    int totaltimeFont;
    int lblmodeFont;
    if (DEVICE==IPAD) {
        fontSize=24;
        totaltimeFont=24;
        lblmodeFont=36;
    }
    else{
        fontSize=18;
        totaltimeFont=12;
        lblmodeFont=22;
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback
                                     withOptions:AVAudioSessionCategoryOptionMixWithOthers
                                           error:nil];
    self.managedObjectContext=[kAppDele managedObjectContext];
    prepare = [[NSUserDefaults standardUserDefaults] floatForKey:preparationtime];
    
    NSInteger value = [[NSUserDefaults standardUserDefaults]integerForKey:dincharyaDelayTime];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaMainCon] && value>0) {
        prepare = (int) value ;
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:isPause1];
    PlayStop=NO;
    roundForReport=rounds;
    totaltimelbl.font=Ralewayfont(totaltimeFont);
    actionlbl.font=Ralewayfont(lblmodeFont);
    lblmode.font=Ralewayfont(lblmodeFont);
    [self ChangeText];
    [self startButtonPreshed];
    [self setValueoflbl];
    
}


- (void)HomeMethod:(NSNotification *)notification {
    HomeTabelVC *homeObject = [[HomeTabelVC alloc]init];
    [homeObject appDidBecomeActive:notification];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self disableSlidePanGestureForRightMenu];
}

//-(void)setlblheight{
//    if (Device_Type==iPhone4)
//        topview.frame=CGRectMake(8, 75, 305, 32);
//    else if(Device_Type==iPhone5)
//        topview.frame=CGRectMake(8, 74, 305, 32);
//    else if(Device_Type==iPhone6)
//        topview.frame=CGRectMake(8, 74, 359, 32);
//    else if(Device_Type==iPhone6P)
//        topview.frame=CGRectMake(8, 74, 397, 32);
//}
-(void)ChangeText{

        self.navigationItem.title = [MCLocalization stringForKey:@"Anulom_Vilom"];
        totaltimelbl.text=[MCLocalization stringForKey:@"Total_Time"];
        actionlbl.text=[MCLocalization stringForKey:@"Action"];

}

-(void)setValueoflbl
{
    seconds = totalTime % 60;
    minutes = (totalTime / 60) % 60;
    hours = totalTime / 3600;
    lblseconds.text=[NSString stringWithFormat:@"%d",seconds];
    lblminutes.text=[NSString stringWithFormat:@"%d",minutes];
    lblHours.text=[NSString stringWithFormat:@"%d",hours];
    lblinhaleleft=lblinhale;
    lblinhaleRight=lblinhale;
    lblholdleft=lblhold;
    lblholdRight=lblhold;
    lblexhaleleft=lblexhale;
    lblexhaleright=lblexhale;
    HoldAfter_left=HoldAfter;
    HoldAfter_right=HoldAfter;
}
-(IBAction)pausebutton:(id)sender{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:isPause1];
    PlayStop=YES;
    [btnStop setImage:[UIImage imageNamed:@"start_icon"] forState:UIControlStateNormal];
    [startTimer invalidate];
}

-(IBAction)stopbutton:(id)sender{
    if (PlayStop==YES) {
        startTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                      target: self
                                                    selector:@selector(onTick:)
                                                    userInfo: nil repeats:YES];
        [btnStop setImage:[UIImage imageNamed:@"stop_icon"] forState:UIControlStateNormal];
        PlayStop=NO;
    }
    else{
        
        [startTimer invalidate];
        if (anulomCount>0) {
            [self insertReportIntoDatabase];
            [self insertintoProgressReport:YES];
            [self saveInDincharya:NO];
        }
//        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:addcondition];
        [[self navigationController] popViewControllerAnimated:NO];
        anulomCount=0;
    }
}

-(void)startButtonPreshed
{
    startTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                  target: self
                                                selector:@selector(onTick:)
                                                userInfo: nil repeats:YES];
}
-(void)onTick:(NSTimer *)timer {
    isPause=[[NSUserDefaults standardUserDefaults] boolForKey:isPause1];
    if (isPause) {
        [self pausebutton:isPause1];
    }
    else{
        if (prepare>0) {
            lblmode.text=[MCLocalization stringForKey:@"preparation_time"];
            lblTimer.text=[NSString stringWithFormat:@"%d",prepare];
            [[CommonSounds sharedInstance] playEverySound];
            prepare--;
        }
        else{
            
            if (totalTime>0) {
                seconds = totalTime % 60;
                minutes = (totalTime / 60) % 60;
                hours = totalTime / 3600;
                lblseconds.text=[NSString stringWithFormat:@"%d",seconds];
                lblminutes.text=[NSString stringWithFormat:@"%d",minutes];
                lblHours.text=[NSString stringWithFormat:@"%d",hours];
                if (rounds>0) {
                    if (lblinhaleleft>0) {
                        if (lblinhaleleft==lblinhale) {
                            [[CommonSounds sharedInstance] playInhaleSound];
                        }
                        else{
                            [[CommonSounds sharedInstance] playEverySound];
                        }
                        imageview.image = [UIImage imageNamed:@"Action_inhale_left"];
                        lblmode.text=[MCLocalization stringForKey:@"Inhale_left"];
                        lblTimer.text=[NSString stringWithFormat:@"%d",lblinhaleleft];
                        lblinhaleleft=lblinhaleleft-1;
                    }
                    else if (lblholdleft>0){
                        if (lblholdleft==lblhold) {
                            [[CommonSounds sharedInstance] playHoldSound];
                        }
                        else{
                            [[CommonSounds sharedInstance] playEverySound];
                        }
                        imageview.image=[UIImage imageNamed:@"Action_no"];
                        lblmode.text=[MCLocalization stringForKey:@"hold_hold"];
                        lblTimer.text=[NSString stringWithFormat:@"%d",lblholdleft];
                        lblholdleft=lblholdleft-1;
                        
                    }
                    else if (lblexhaleright>0) {
                        if (lblexhaleright==lblexhale) {
                            [[CommonSounds sharedInstance] playExhaleSound];
                        }
                        else{
                            [[CommonSounds sharedInstance] playEverySound];
                        }
                        imageview.image=[UIImage imageNamed:@"Action_exhale_right"];
                        lblmode.text=[MCLocalization stringForKey:@"Exhale_right"];
                        lblTimer.text=[NSString stringWithFormat:@"%d",lblexhaleright];
                        lblexhaleright=lblexhaleright-1;
                    }
                    else if (HoldAfter_right>0){
                        if (HoldAfter_right==HoldAfter) {
                            [[CommonSounds sharedInstance] playHoldSound];
                        }
                        else{
                            [[CommonSounds sharedInstance] playEverySound];
                        }
                        imageview.image=[UIImage imageNamed:@"Action_no"];
                        lblmode.text=[MCLocalization stringForKey:@"hold_hold"];
                        lblTimer.text=[NSString stringWithFormat:@"%d",HoldAfter_right];
                        HoldAfter_right=HoldAfter_right-1;
                        
                    }
                    else if (lblinhaleRight>0){
                        if (lblinhaleRight==lblinhale) {
                            [[CommonSounds sharedInstance] playInhaleSound];
                        }
                        else{
                            [[CommonSounds sharedInstance] playEverySound];
                        }
                        imageview.image=[UIImage imageNamed:@"Action_inhale_right"];
                        lblmode.text=[MCLocalization stringForKey:@"Inhale_right"];
                        lblTimer.text=[NSString stringWithFormat:@"%d",lblinhaleRight];
                        lblinhaleRight=lblinhaleRight-1;
                    }
                    else if (lblholdRight>0){
                        if (lblholdRight==lblhold) {
                            [[CommonSounds sharedInstance] playHoldSound];
                        }
                        else{
                            [[CommonSounds sharedInstance] playEverySound];
                        }
                        imageview.image=[UIImage imageNamed:@"Action_no"];
                        lblmode.text=[MCLocalization stringForKey:@"hold_hold"];
                        lblTimer.text=[NSString stringWithFormat:@"%d",lblholdRight];
                        lblholdRight=lblholdRight-1;
                    }
                    else if (lblexhaleleft>0) {
                        if (lblexhaleleft==lblexhale) {
                            [[CommonSounds sharedInstance] playExhaleSound];
                        }
                        else{
                            [[CommonSounds sharedInstance] playEverySound];
                        }
                        imageview.image=[UIImage imageNamed:@"Action_exhale_left"];
                        lblmode.text=[MCLocalization stringForKey:@"Exhale_left"];
                        lblTimer.text=[NSString stringWithFormat:@"%d",lblexhaleleft];
                        lblexhaleleft=lblexhaleleft-1;
                        if (lblexhaleleft==0 && HoldAfter==0) {
                            rounds=rounds-1;
                            [self setValueoflbl];
                        }
                        
                    }
                    else if (HoldAfter_left>0){
                        if (HoldAfter_left==HoldAfter) {
                            [[CommonSounds sharedInstance] playHoldSound];
                        }
                        else{
                            [[CommonSounds sharedInstance] playEverySound];
                        }
                        imageview.image=[UIImage imageNamed:@"Action_no"];
                        lblmode.text=[MCLocalization stringForKey:@"hold_hold"];
                        lblTimer.text=[NSString stringWithFormat:@"%d",HoldAfter_left];
                        HoldAfter_left=HoldAfter_left-1;
                        
                        if (HoldAfter_left==0) {
                            rounds=rounds-1;
                            [self setValueoflbl];
                        }
                    }
                }
            }
            else{
                [timer invalidate];
                timer = nil;
                if ([self.delegate respondsToSelector:@selector(playsound)]) {
                    [self.delegate playsound];
                    [self insertReportIntoDatabase];
                    [self insertintoProgressReport:NO];
                    [self saveInDincharya:YES];
                }
//                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:addcondition];
                [[self navigationController] popViewControllerAnimated:NO];
            }
            totalTime=totalTime-1;
            anulomCount++;
        }
    }
}
-(void)insertReportIntoDatabase{
    NSArray *array=[kAppDele fetchReportFromDatabseAccordingToType:@"AnulomVilom"];
    if (array.count>0) {
        Record *recordEntity=[array objectAtIndex:0];
        NSDate *str=recordEntity.anulomDate;
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        NSString *datestr=[dateFormatter stringFromDate:str];
        NSString *date=[dateFormatter stringFromDate:[NSDate date]];
        if ([datestr isEqualToString:date]) {
            NSString *duration=[NSString stringWithFormat:@"%d",[recordEntity.anulomduration intValue]+anulomCount];
            recordEntity.anulomduration=duration;
            NSError *error;
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"%@",error);
            }
        }
        else{
            Record *reportEntity = [NSEntityDescription insertNewObjectForEntityForName:@"Record" inManagedObjectContext:self.managedObjectContext];
            reportEntity.anulomLevel=[NSString stringWithFormat:@"%d:%d:%d",lblinhale,lblhold,lblexhale];
            reportEntity.anulomRounds=[NSString stringWithFormat:@"%d",roundForReport];
            reportEntity.anulomDate=[NSDate date];
            reportEntity.anulomduration=[NSString stringWithFormat:@"%d",anulomCount];
            reportEntity.anulomtype=@"AnulomVilom";
            NSError *error;
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"%@",error);
            }
        }
    }
    else{
        Record *reportEntity = [NSEntityDescription insertNewObjectForEntityForName:@"Record" inManagedObjectContext:self.managedObjectContext];
        reportEntity.anulomLevel=[NSString stringWithFormat:@"%d:%d:%d",lblinhale,lblhold,lblexhale];
        reportEntity.anulomRounds=[NSString stringWithFormat:@"%d",roundForReport];
        reportEntity.anulomDate=[NSDate date];
        reportEntity.anulomduration=[NSString stringWithFormat:@"%d",anulomCount];
        reportEntity.anulomtype=@"AnulomVilom";
        NSError *error;
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"%@",error);
        }
    }
    Report *reportEntity = [NSEntityDescription insertNewObjectForEntityForName:@"Report" inManagedObjectContext:self.managedObjectContext];
    reportEntity.reportLevel=[NSString stringWithFormat:@"%d:%d:%d",lblinhale,lblhold,lblexhale];
    reportEntity.reportRounds=[NSString stringWithFormat:@"%d",roundForReport];
    reportEntity.reportDate=[NSDate date];
    reportEntity.reportduration=[NSString stringWithFormat:@"%d",anulomCount];
    reportEntity.reporttype=@"AnulomVilom";
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"%@",error);
    }
}

-(void)insertintoProgressReport:(BOOL)value{
    ////progress Report ///////////
    
    NSString *habbitconditon=[[NSUserDefaults standardUserDefaults]objectForKey:HabbitCondition];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:HabbitMainCondition]) {
        NSArray *Progressarray=[kAppDele fetchReportFromProgressDatabseAccordingToType:habbitconditon];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"Progress" inManagedObjectContext:self.managedObjectContext]];
        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"progressType==%@ AND days==%d",habbitconditon,Progressarray.count];
        [request setPredicate:predicate];
        
        NSError *error1=nil;
        NSArray *results=[self.managedObjectContext executeFetchRequest:request error:&error1];
        Progress *entity=[results objectAtIndex:0];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:viewwillCondition];

        if (value==YES) {
            NSNumber *duration=[NSNumber numberWithInt:[entity.duration intValue]+anulomCount];
            entity.duration=duration;
        }
        else{
            entity.anulomVilom=[NSNumber numberWithBool:YES];
            NSNumber *duration=[NSNumber numberWithInt:[entity.duration intValue]+anulomCount];
            entity.duration=duration;
        }
        [self.managedObjectContext save:nil];
    }
}

-(void)saveInDincharya:(BOOL)Value{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaMainCon]) {
        
        NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaName];
        NSArray *ar = [kAppDele FetchDincharyaFromDatabaseAcordingToName:str];
        Dincharya *dincharya = [ar objectAtIndex:0];
        NSArray *yogaAr = [NSKeyedUnarchiver unarchiveObjectWithData:dincharya.yogasana_arr];
        NSArray *pranaMainList = [NSKeyedUnarchiver unarchiveObjectWithData:dincharya.pranayama_arr];
        NSArray *pranaList = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaPranaAr];
        
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:viewwillCondition];
        
        if (pranaMainList.count == pranaList.count  && yogaAr.count == 0) {
            [[CommonSounds sharedInstance]Timecompare:Value managedObject:self.managedObjectContext pranayama:@"AnulomVilom"];
        }
        else{
            [[CommonSounds sharedInstance]UpdateDincharyaIntoDatabase:Value mangedObject:self.managedObjectContext Prana:@"AnulomVilom"];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
@end

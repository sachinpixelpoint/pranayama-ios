//
//  AnulomVilomActionVC.h
//  Pranayama
//
//  Created by Manish Kumar on 31/03/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol anulomdelegate <NSObject>

-(void)playsound;

@end

@interface AnulomVilomActionVC : GAITrackedViewController
{
    IBOutlet UIView *topview;
    
    IBOutlet UILabel *totaltimelbl;
    IBOutlet UILabel *actionlbl;
    
    IBOutlet UIButton *btnStop;
    IBOutlet UIButton *btnpuse;
    
    IBOutlet UILabel *lblHours;
    IBOutlet UILabel *lblminutes;
    IBOutlet UILabel *lblseconds;
    IBOutlet UILabel *lblTimer;
    IBOutlet UILabel *lblmode;
    IBOutlet UIImageView *imageview;
    
    NSTimer *startTimer;
    BOOL isPause;
    BOOL PlayStop;
    int lblinhaleleft;
    int lblholdleft;
    int lblexhaleright;
    int lblinhaleRight;
    int lblholdRight;
    int lblexhaleleft;
    int HoldAfter_left;
    int HoldAfter_right;
    int seconds;
    int minutes;
    int hours;
    int roundForReport;
    int anulomCount;
    int prepare;
}
@property (nonatomic,assign)id <anulomdelegate>delegate;
@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic) int rounds;
@property (nonatomic) int lblinhale;
@property (nonatomic) int lblexhale;
@property (nonatomic) int lblhold;
@property (nonatomic) int totalTime;
@property (nonatomic) int HoldAfter;
@end

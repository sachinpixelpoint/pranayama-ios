//
//  HowtoSitVC.m
//  Pranayama
//
//  Created by Manish Kumar on 30/03/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "HowtoSitVC.h"

@interface HowtoSitVC ()

@end

@implementation HowtoSitVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    self.screenName=@"Sitting Pose";
    // Do any additional setup after loading the view.
    self.navigationItem.title=@"Sitting Pose";
    int fontSize;
    if (DEVICE==IPAD) {
        fontSize=24;
    }
    else{
        fontSize=18;
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    self.automaticallyAdjustsScrollViewInsets=NO;
    [self content];
}


- (void)HomeMethod:(NSNotification *)notification {
    [[CommonSounds sharedInstance] appDidBecomeActive:notification navigation:self.navigationController];
}




-(void)content{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    UILabel *label8 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    UILabel *label9 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    UILabel *label10 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(18);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(18);
    label8.font=Ralewayfont(14);
    label9.font=Ralewayfont(18);
    label10.font=Ralewayfont(14);
    self.navigationItem.title = [MCLocalization stringForKey:@"how_to_sit"];
    label1.text=[MCLocalization stringForKey:@"how_to_sit_content"];
    label2.text=[MCLocalization stringForKey:@"how_to_sit_content2"];
    label3.text=[MCLocalization stringForKey:@"how_to_sit_content3"];
    label4.text=[MCLocalization stringForKey:@"how_to_sit_content4"];
    label5.text=[MCLocalization stringForKey:@"how_to_sit_content5"];
    label6.text=[MCLocalization stringForKey:@"how_to_sit_content6"];
    label7.text=[MCLocalization stringForKey:@"how_to_sit_content7"];
    label8.text=[MCLocalization stringForKey:@"how_to_sit_content8"];
    label9.text=[MCLocalization stringForKey:@"how_to_sit_content9"];
    label10.text=[MCLocalization stringForKey:@"how_to_sit_content10"];
    
    NSString *string=[MCLocalization stringForKey:@"how_to_sit_link"];

    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor=[UIColor redColor];
    [myScrollView addSubview:label1];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-80, label1.frame.origin.y+label1.frame.size.height+10, 160, 140)];
    imageView1.image = [UIImage imageNamed:@"sukhasana"];
    [myScrollView addSubview:imageView1];
    
    label2.frame=CGRectMake(10, imageView1.frame.origin.y+imageView1.frame.size.height+10, myScrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [myScrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, myScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    label3.textColor=[UIColor redColor];
    [myScrollView addSubview:label3];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-80, label3.frame.origin.y+label3.frame.size.height+5, 160, 140)];
    imageView2.image = [UIImage imageNamed:@"sidhasana"];
    [myScrollView addSubview:imageView2];
    
    label4.frame=CGRectMake(10, imageView2.frame.origin.y+imageView2.frame.size.height+10, myScrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [myScrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, myScrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    label5.textColor=[UIColor redColor];
    [myScrollView addSubview:label5];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-80, label5.frame.origin.y+label5.frame.size.height+10, 160, 140)];
    imageView3.image = [UIImage imageNamed:@"vajrasana"];
    [myScrollView addSubview:imageView3];
    
    label6.frame=CGRectMake(10, imageView3.frame.origin.y+imageView3.frame.size.height+10, myScrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [myScrollView addSubview:label6];
    
    label7.frame=CGRectMake(10, label6.frame.origin.y+label6.frame.size.height+10, myScrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    label7.textColor=[UIColor redColor];
    [myScrollView addSubview:label7];
    
    UIImageView *imageView4 = [[UIImageView alloc] initWithFrame:CGRectMake(10, label7.frame.origin.y+label7.frame.size.height+10, myScrollView.frame.size.width-20, 140)];
    imageView4.image = [UIImage imageNamed:@"ardha_padmasana"];
    [myScrollView addSubview:imageView4];
    
    label8.frame=CGRectMake(10, imageView4.frame.origin.y+imageView4.frame.size.height+5, myScrollView.frame.size.width-20, 100);
    label8.numberOfLines=0;
    [label8 sizeToFit];
    [myScrollView addSubview:label8];
    
    label9.frame=CGRectMake(10, label8.frame.origin.y+label8.frame.size.height+10, myScrollView.frame.size.width-20, 100);
    label9.numberOfLines=0;
    [label9 sizeToFit];
    label9.textColor=[UIColor redColor];
    [myScrollView addSubview:label9];
    
    
    UIImageView *imageView5 = [[UIImageView alloc] initWithFrame:CGRectMake(10, label9.frame.origin.y+label9.frame.size.height+10, myScrollView.frame.size.width-20, 140)];
    imageView5.image = [UIImage imageNamed:@"padmasan"];
    [myScrollView addSubview:imageView5];
    
    label10.frame=CGRectMake(10, imageView5.frame.origin.y+imageView5.frame.size.height+10, myScrollView.frame.size.width-20, 100);
    label10.numberOfLines=0;
    [label10 sizeToFit];
    [myScrollView addSubview:label10];
    
    
    FRHyperLabel *label = [FRHyperLabel new];
    label.frame=CGRectMake(10,label10.frame.origin.y+label10.frame.size.height , myScrollView.frame.size.width-20, 200);
    label.numberOfLines=100;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]};
    
    label.attributedText = [[NSAttributedString alloc]initWithString:string attributes:attributes];
    label.font=[UIFont systemFontOfSize:16];
    
    [label setLinkForSubstring:@"www.7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.7pranayama.com/"]];
    }];
    [label setLinkForSubstring:@"info@7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://info@7pranayama.com/"]];
    }];
    
    [myScrollView addSubview:label];

    
    myScrollView.contentSize=CGSizeMake(myScrollView.frame.size.width,label10.frame.origin.y+label10.frame.size.height+label.frame.size.height+100);
    
    int scrollHeight=label10.frame.origin.y+label10.frame.size.height+label.frame.size.height;
    int a=self.view.frame.size.width/4;
    int b=a/2;
    
    UIButton*btn1=[[UIButton alloc] initWithFrame:CGRectMake(a/2-25, scrollHeight+20, 50, 50)];
    btn1.tag=1;
    btn1.layer.cornerRadius = btn1.bounds.size.width / 2.0;
    UIImage *btnImage1 = [UIImage imageNamed:@"facebook"];
    [btn1 setImage:btnImage1 forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(goToLink:) forControlEvents:UIControlEventTouchUpInside];
    [myScrollView addSubview:btn1];
    
    UIButton*btn2=[[UIButton alloc] initWithFrame:CGRectMake(((2*a)-b)-25, scrollHeight+20, 50, 50)];
    btn2.tag=2;
    btn2.layer.cornerRadius = btn1.bounds.size.width / 2.0;
    UIImage *btnImage2 = [UIImage imageNamed:@"twitter"];
    [btn2 setImage:btnImage2 forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(goToLink:) forControlEvents:UIControlEventTouchUpInside];
    [myScrollView addSubview:btn2];
    
    UIButton*btn3=[[UIButton alloc] initWithFrame:CGRectMake(((3*a)-b)-25, scrollHeight+20, 50, 50)];
    btn3.tag=3;
    btn3.layer.cornerRadius = btn1.bounds.size.width / 2.0;
    UIImage *btnImage3 = [UIImage imageNamed:@"pinterest"];
    [btn3 setImage:btnImage3 forState:UIControlStateNormal];
    [btn3 addTarget:self action:@selector(goToLink:) forControlEvents:UIControlEventTouchUpInside];
    [myScrollView addSubview:btn3];
    
    UIButton*btn4=[[UIButton alloc] initWithFrame:CGRectMake((self.view.frame.size.width-b)-25,scrollHeight+20, 50, 50)];
    btn4.tag=4;
    btn4.layer.cornerRadius = btn1.bounds.size.width / 2.0;
    UIImage *btnImage4 = [UIImage imageNamed:@"google"];
    [btn4 setImage:btnImage4 forState:UIControlStateNormal];
    [btn4 addTarget:self action:@selector(goToLink:) forControlEvents:UIControlEventTouchUpInside];
    [myScrollView addSubview:btn4];
    
}

-(void)goToLink:(UIButton*)sender{
    if (sender.tag==1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.facebook.com/7pranayama-1742577192620397/"]];
    }
    else if (sender.tag==2){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://twitter.com/7pranayama"]];
    }
    else if (sender.tag==3){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://in.pinterest.com/7pranayama/"]];
    }
    else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://plus.google.com/111008006040750995181/posts"]];
    }
}

-(IBAction)backbutton:(id)sender{
    [[CommonSounds sharedInstance] NavigationMethod:self.navigationController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

//
//  ProblemListViewController.m
//  Pranayama
//
//  Created by Manish Kumar on 13/06/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "ProblemListViewController.h"

@interface ProblemListViewController ()

@end

@implementation ProblemListViewController
@synthesize table;
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    // Do any additional setup after loading the view.
    self.screenName = @"Problem List" ;
    [self ChangeText];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets=NO;
    
    if (DEVICE==IPAD) {
        fontSize=24;
    }
    else{
        fontSize=18;
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
        
    [self SlidingImage];
    
}


- (void)HomeMethod:(NSNotification *)notification {
    [[CommonSounds sharedInstance] appDidBecomeActive:notification navigation:self.navigationController];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [table reloadData];
    //If this vc can be poped , then
    if (self.navigationController.viewControllers.count > 1)
    {
        // Disabling pan gesture for left menu
        //        [self disableSlidePanGestureForLeftMenu];
    }
    
    AMSlideMenuMainViewController *mainVC = [AMSlideMenuMainViewController getInstanceForVC:self];
    if (mainVC.rightMenu)
    {
        // Adding right menu button to navigation bar
        [self addRightMenuButton];
    }
    [[NSUserDefaults standardUserDefaults] setObject:@"Habbitlist" forKey:controllerType];

   }

-(void)SlidingImage{
    
    imageVIew=[[UIView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-table.frame.size.height-64)];
    
    ImageView1=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, imageVIew.frame.size.width, imageVIew.frame.size.height)];
    [ImageView1 setImage:[UIImage imageNamed:@"qu3"]];
    ImageView2=[[UIImageView alloc]initWithFrame:CGRectMake(imageVIew.frame.size.width, 0, imageVIew.frame.size.width, imageVIew.frame.size.height)];
    [ImageView2 setImage:[UIImage imageNamed:@"quot2"]];
    ImageView3=[[UIImageView alloc]initWithFrame:CGRectMake(imageVIew.frame.size.width, 0, imageVIew.frame.size.width, imageVIew.frame.size.height)];
    [ImageView3 setImage:[UIImage imageNamed:@"quot1"]];
    [imageVIew addSubview:ImageView1];
    [self.view addSubview:imageVIew];
    [self performSelector:@selector(changeImage) withObject:nil afterDelay:5];
    conditionValue=1;
}

-(void)changeImage{
    CGRect right=CGRectMake(imageVIew.frame.size.width, 0, imageVIew.frame.size.width, imageVIew.frame.size.height);
    CGRect center=CGRectMake(0, 0, imageVIew.frame.size.width, imageVIew.frame.size.height);
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction=[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    
    switch (conditionValue) {
        case 1:
            ImageView2.hidden=NO;
            ImageView3.hidden=NO;
            ImageView3.frame=right;
            ImageView2.frame=center;
            [imageVIew addSubview:ImageView2];
            [ImageView2.layer addAnimation:transition forKey:nil];
            conditionValue=2;
              ImageView1.hidden=YES;
            [self performSelector:@selector(changeImage) withObject:nil afterDelay:5];
          
            
            break;
        case 2:
            ImageView1.hidden=NO;
            ImageView3.hidden=NO;
            ImageView1.frame=right;
            ImageView3.frame=center;
            [imageVIew addSubview:ImageView3];
            [ImageView3.layer addAnimation:transition forKey:nil];
            conditionValue=3;
            ImageView2.hidden=YES;
            [self performSelector:@selector(changeImage) withObject:nil afterDelay:5];
           
            break;
        case 3:
             ImageView1.hidden=NO;
            ImageView2.hidden=NO;
            ImageView2.frame=right;
            ImageView1.frame=center;
            [imageVIew addSubview:ImageView1];
            [ImageView1.layer addAnimation:transition forKey:nil];
            conditionValue=1;
             ImageView3.hidden=YES;
            [self performSelector:@selector(changeImage) withObject:nil afterDelay:5];
          
            break;
        default:
            break;
    }
}

-(void)ChangeText{
    self.navigationItem.title = [MCLocalization stringForKey:@"habbit"];
    homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"water"],[MCLocalization stringForKey:@"Thyroid"],[MCLocalization stringForKey:@"Insomnia"],[MCLocalization stringForKey:@"Diabetes"],[MCLocalization stringForKey:@"Migraine"],[MCLocalization stringForKey:@"Weight Loss"],[MCLocalization stringForKey:@"Asthma"],[MCLocalization stringForKey:@"Joint Pain"], nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [homelist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    UIImageView *lockImg = (UIImageView *)[cell viewWithTag:1];
    if (indexPath.row == 5 || indexPath.row == 6 || indexPath.row == 7) {
        if ([[NSUserDefaults standardUserDefaults]boolForKey:Is_Perchaged]) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            lockImg.hidden=YES;
        }
        else{
            lockImg.hidden=NO;
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    else{
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        lockImg.hidden=YES;
    }
    cell.textLabel.text=[homelist objectAtIndex:indexPath.row];
    cell.textLabel.textColor=[UIColor whiteColor];
    cell.textLabel.backgroundColor=[UIColor clearColor];
    cell.textLabel.font=Ralewayfont(fontSize);
    int colors = indexPath.row%2;
    switch (colors) {
        case 0:
            cell.backgroundColor=[UIColor colorWithRed:187/255.0 green:20/255.0 blue:37/255.0 alpha:0.90];
            break;
        case 1:
            cell.backgroundColor=[UIColor colorWithRed:176/255.0 green:12/255.0 blue:28/255.0 alpha:0.90];
            break;
        default:
            break;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:{
            if ([[NSUserDefaults standardUserDefaults]boolForKey:waterSated]) {
                [self performSegueWithIdentifier:@"intake" sender:self];
            }
            else{
                HomeTabelVC *home=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
                ProblemListViewController *ProblumList=[self.storyboard instantiateViewControllerWithIdentifier:@"problumlist"];
                WaterIntakeVC *waterIntake=[self.storyboard instantiateViewControllerWithIdentifier:@"waterintake"];
                WaterCalculationVC *waterCalculate=[self.storyboard instantiateViewControllerWithIdentifier:@"watercalculation"];
                [self.navigationController setViewControllers:@[home,ProblumList,waterIntake,waterCalculate] animated:YES];
            }
        }
            break;
        case 1:
             [[NSUserDefaults standardUserDefaults] setObject:@"Thyroid" forKey:HabbitType];
            [self performSegueWithIdentifier:@"Thyroid" sender:self];
            break;
        case 2:
            [[NSUserDefaults standardUserDefaults] setObject:@"Insomnia" forKey:HabbitType];
            [self performSegueWithIdentifier:@"Thyroid" sender:self];
            break;
        case 3:
            [[NSUserDefaults standardUserDefaults] setObject:@"Diabetes" forKey:HabbitType];
            [self performSegueWithIdentifier:@"Thyroid" sender:self];
            break;
        case 4:
            [[NSUserDefaults standardUserDefaults] setObject:@"Migraine" forKey:HabbitType];
            [self performSegueWithIdentifier:@"Thyroid" sender:self];
            break;
         case 5:
            if ([[NSUserDefaults standardUserDefaults]boolForKey:Is_Perchaged]) {
                [[NSUserDefaults standardUserDefaults] setObject:@"Weight Loss" forKey:HabbitType];
                [self performSegueWithIdentifier:@"Thyroid" sender:self];
            }
            else{
//                UIViewController *guru = [self.storyboard instantiateViewControllerWithIdentifier:@"guru"];
//                [self.navigationController pushViewController:guru animated:YES];
                [[NSUserDefaults standardUserDefaults] setObject:@"Weight Loss" forKey:HabbitType];
                [self performSegueWithIdentifier:@"Thyroid" sender:self];
            }
            break;
        case 6:
            if ([[NSUserDefaults standardUserDefaults]boolForKey:Is_Perchaged]) {
            [[NSUserDefaults standardUserDefaults] setObject:@"Asthma" forKey:HabbitType];
            [self performSegueWithIdentifier:@"Thyroid" sender:self];
            }
            else{
//                UIViewController *guru = [self.storyboard instantiateViewControllerWithIdentifier:@"guru"];
//                [self.navigationController pushViewController:guru animated:YES];
                [[NSUserDefaults standardUserDefaults] setObject:@"Asthma" forKey:HabbitType];
                [self performSegueWithIdentifier:@"Thyroid" sender:self];
            }
            break;
        case 7:
            if ([[NSUserDefaults standardUserDefaults]boolForKey:Is_Perchaged]) {
                [[NSUserDefaults standardUserDefaults] setObject:@"Joint Pain" forKey:HabbitType];
                [self performSegueWithIdentifier:@"Thyroid" sender:self];
            }
            else{
//                UIViewController *guru = [self.storyboard instantiateViewControllerWithIdentifier:@"guru"];
//                [self.navigationController pushViewController:guru animated:YES];
                [[NSUserDefaults standardUserDefaults] setObject:@"Joint Pain" forKey:HabbitType];
                [self performSegueWithIdentifier:@"Thyroid" sender:self];
            }
            break;
        default:
            break;
    }
}


-(IBAction)backbutton:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:HabbitType];
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    if ([segue.identifier isEqualToString:@"Thyroid"]) {
//        ProblemCatagoriVC *controller = (ProblemCatagoriVC *)segue.destinationViewController;
//        NSString *indexPath = (NSString *)sender;
//        controller.indexStr=indexPath;
//    }
//
//}


@end

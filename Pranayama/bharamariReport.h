//
//  bharamariReport.h
//  Pranayama
//
//  Created by Manish Kumar on 09/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface bharamariReport : GAITrackedViewController <UIGestureRecognizerDelegate>
{
    IBOutlet UIView *topview;
    
    IBOutlet UILabel *sn;
    IBOutlet UILabel *levellbl;
    IBOutlet UILabel *roundlbl;
    IBOutlet UILabel *datelbl;
    IBOutlet UILabel *timelbl;
    IBOutlet UILabel *InExhal;
    
    NSMutableArray *reportArray;
    IBOutlet UITableView *tblReport;
    UITableViewCell *cell;
    CGPoint p;
    NSIndexPath *indexpath1;
    UILongPressGestureRecognizer *lpgr;
    UITapGestureRecognizer*tap;
    int cl;
    NSMutableArray *selectunselectarray;
    int entityFontSize;
}
@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
@end

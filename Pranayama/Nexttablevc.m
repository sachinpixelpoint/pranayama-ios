//
//  Nexttablevc.m
//  Pranayama
//
//  Created by Manish Kumar on 09/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "Nexttablevc.h"

@interface Nexttablevc ()

@end

@implementation Nexttablevc
@synthesize table;
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    movearray=[[NSArray alloc]init];
    noArray=[[NSMutableArray alloc]init];
    self.navigationController.navigationBar.tintColor =RGB(230, 123, 27);
    self.screenName = @"Pranayama list";
    if (DEVICE==IPAD) {
        fontSize=24;
        fontSize2=16;
    }
    else{
        fontSize=18;
        fontSize2=10;
    }
    
    [self ChangeText];
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.navigationItem.hidesBackButton=YES;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];

   // double a=(self.table.frame.size.height)/9;
    //table.rowHeight=a;
    [self introduction];
}

-(void)introduction{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:tableIntro]) {
        [[CommonSounds sharedInstance]tableintro:table];
    }
}

- (void)HomeMethod:(NSNotification *)notification {
    [[CommonSounds sharedInstance] appDidBecomeActive:notification navigation:self.navigationController];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //If this vc can be poped , then
    if (self.navigationController.viewControllers.count > 1)
    {
        // Disabling pan gesture for left menu
        //        [self disableSlidePanGestureForLeftMenu];
    }
    
    AMSlideMenuMainViewController *mainVC = [AMSlideMenuMainViewController getInstanceForVC:self];
    if (mainVC.rightMenu)
    {
        // Adding right menu button to navigation bar
        [self addRightMenuButton];
    }
    [[NSUserDefaults standardUserDefaults] setObject:@"Nexttable" forKey:controllerType];
}


-(void)ChangeText{
    
    homelist=[[NSMutableArray alloc] init];
    nextList = [[NSMutableArray alloc]init];
    movearray=[[NSUserDefaults standardUserDefaults]objectForKey:PranayamaList];
    self.navigationItem.title = [MCLocalization stringForKey:@"Pranayam"];
    self.navigationItem.title = [MCLocalization stringForKey:@"Pranayam"];
    homeList2=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Anulom_Vilom"],[MCLocalization stringForKey:@"kapalbhati"],[MCLocalization stringForKey:@"bhramari"],[MCLocalization stringForKey:@"surya_bhedana"],[MCLocalization stringForKey:@"chandra_bhedana"],[MCLocalization stringForKey:@"bhastrika"],[MCLocalization stringForKey:@"sheetali"],[MCLocalization stringForKey:@"ujjayi"],[MCLocalization stringForKey:@"meditative"],[MCLocalization stringForKey:@"udgeeth"],[MCLocalization stringForKey:@"bahya"], nil];
    nextList2=[[NSMutableArray alloc]initWithObjects:[MCLocalization stringForKey:@"Alternate_Nostrils"],[MCLocalization stringForKey:@"Skull_Purification"],[MCLocalization stringForKey:@"Humming_Bee_Breath"],[MCLocalization stringForKey:@"Right_Nostril"],[MCLocalization stringForKey:@"Left_Nostril"],[MCLocalization stringForKey:@"Bellows"],[MCLocalization stringForKey:@"Cooling"],[MCLocalization stringForKey:@"Ocean"],[MCLocalization stringForKey:@"Meditative_Breathing"],[MCLocalization stringForKey:@"Chanting_Breath"],[MCLocalization stringForKey:@"External_Kumbhak"], nil];
    
    
    if (movearray.count<=0 || movearray.count<homeList2.count) {
        homelist=homeList2;
        nextList=nextList2;
        noArray=[[NSMutableArray alloc]initWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10", nil];
    }
    else{
        noArray=[movearray mutableCopy];
        for (int i=0; i<movearray.count; i++) {
            NSString *str=[movearray objectAtIndex:i];
            [homelist addObject:[homeList2 objectAtIndex:[str integerValue]]];
            [nextList addObject:[nextList2 objectAtIndex:[str integerValue]]];
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)backbutton:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [homelist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cellidentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    NSString *lan=[[NSUserDefaults standardUserDefaults] objectForKey:Languagevalue];
    if ([lan isEqualToString: Hindi]) {
        cell.textLabel.text=[homelist objectAtIndex:indexPath.row];
        cell.textLabel.font= Ralewayfont(fontSize);
    }
    else{
        NSString *str1 = [homelist objectAtIndex:indexPath.row];
        NSString *str2 = [nextList objectAtIndex:indexPath.row];
        NSString *string=[NSString stringWithFormat:@"%@ %@",str1,str2];
        NSMutableAttributedString *attributedstring = [[NSMutableAttributedString alloc] initWithString:string];
        [attributedstring addAttribute:NSFontAttributeName value:Ralewayfont(fontSize) range:NSMakeRange(0, str1.length )];
        [attributedstring addAttribute:NSFontAttributeName value:Ralewayfont(fontSize2) range:NSMakeRange(str1.length+1, str2.length)];
        cell.textLabel.attributedText=attributedstring;
    }
    
    btn = (UIButton *)[cell viewWithTag:2];
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGestureRecognized:)];
    longPress.minimumPressDuration=0.0;
    [btn addGestureRecognizer:longPress];
    
    cell.textLabel.textColor=[UIColor whiteColor];
    cell.textLabel.backgroundColor=[UIColor clearColor];
    
    int index = [[noArray objectAtIndex:indexPath.row] intValue];
    if (index == 9 || index == 10) {
        if (![[NSUserDefaults standardUserDefaults]boolForKey:Is_Perchaged]) {
            [btn setImage:[UIImage imageNamed:@"lock"] forState:UIControlStateNormal];
        }
    }
    else{
        [btn setImage:[UIImage imageNamed:@"drag_drop"] forState:UIControlStateNormal];
    }
    
    int colors = indexPath.row%2;
    switch (colors) {
        case 0:
            cell.backgroundColor=[UIColor colorWithRed:187/255.0 green:20/255.0 blue:37/255.0 alpha:0.90];
            break;
        case 1:
           cell.backgroundColor=[UIColor colorWithRed:176/255.0 green:12/255.0 blue:28/255.0 alpha:0.90];
            break;
        default:
            break;
    }
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    int index = [[noArray objectAtIndex:indexPath.row] intValue];
    
    switch (index) {
        case 0:
            [self performSegueWithIdentifier:@"anulomvilom" sender:self];
            break;
        case 1:
            [self performSegueWithIdentifier:@"kapalbhati" sender:self];
            break;
        case 2:
            [self performSegueWithIdentifier:@"bhramari" sender:self];
            break;
        case 3:
            [self performSegueWithIdentifier:@"surya" sender:self];
            break;
        case 4:
            [self performSegueWithIdentifier:@"Chandra" sender:self];
            break;
        case 5:
            [self performSegueWithIdentifier:@"bhastrika" sender:self];
            break;
        case 6:
            [self performSegueWithIdentifier:@"sheetali" sender:self];
            break;
        case 7:
            [self performSegueWithIdentifier:@"ujjayi" sender:self];
            break;
        case 8:
            [self performSegueWithIdentifier:@"meditative" sender:self];
            break;
        case 9:
//            if ([[NSUserDefaults standardUserDefaults]boolForKey:Is_Perchaged]) {
//                [self performSegueWithIdentifier:@"udgeet" sender:self];
//            }
//            else{
//                UIViewController *guru = [self.storyboard instantiateViewControllerWithIdentifier:@"guru"];
//                [self.navigationController pushViewController:guru animated:YES];
//            }
            [self performSegueWithIdentifier:@"udgeet" sender:self];
            break;
        case 10:
            [self performSegueWithIdentifier:@"Baya" sender:self];
            break;
        default:
            break;
    }
}



- (void)longPressGestureRecognized:(id)sender {
    
    UILongPressGestureRecognizer *longPress = (UILongPressGestureRecognizer *)sender;
    UIGestureRecognizerState state = longPress.state;
    
    CGPoint location = [longPress locationInView:self.table];
    NSIndexPath *indexPath = [self.table indexPathForRowAtPoint:location];
    
    static UIView       *snapshot = nil;        ///< A snapshot of the row user is moving.
    static NSIndexPath  *sourceIndexPath = nil; ///< Initial index path, where gesture begins.
    
    switch (state) {
        case UIGestureRecognizerStateBegan: {
            if (indexPath) {
                sourceIndexPath = indexPath;
                
                UITableViewCell *cell = [self.table cellForRowAtIndexPath:indexPath];
                
                // Take a snapshot of the selected row using helper method.
                snapshot = [self customSnapshoFromView:cell];
                
                // Add the snapshot as subview, centered at cell's center...
                __block CGPoint center = cell.center;
                snapshot.center = center;
                snapshot.alpha = 0.0;
                [self.table addSubview:snapshot];
                [UIView animateWithDuration:0.25 animations:^{
                    
                    // Offset for gesture location.
                    center.y = location.y;
                    snapshot.center = center;
                    snapshot.transform = CGAffineTransformMakeScale(1.05, 1.05);
                    snapshot.alpha = 0.98;
                    cell.alpha = 0.0;
                    cell.hidden = YES;
                    
                }];
            }
            break;
        }
            
        case UIGestureRecognizerStateChanged: {
            CGPoint center = snapshot.center;
            center.y = location.y;
            snapshot.center = center;
            
            // Is destination valid and is it different from source?
            if (indexPath && ![indexPath isEqual:sourceIndexPath]) {
                
                // ... update data source.
                [homelist exchangeObjectAtIndex:indexPath.row withObjectAtIndex:sourceIndexPath.row];
                [nextList exchangeObjectAtIndex:indexPath.row withObjectAtIndex:sourceIndexPath.row];
                [noArray exchangeObjectAtIndex:indexPath.row withObjectAtIndex:sourceIndexPath.row];
                
                // ... move the rows.
                [self.table moveRowAtIndexPath:sourceIndexPath toIndexPath:indexPath];
                sourceIndexPath = indexPath;
                [table scrollToRowAtIndexPath:sourceIndexPath
                                     atScrollPosition:UITableViewScrollPositionMiddle
                                             animated:YES];
            
            }
            break;
        }
            
        default: {
            // Clean up.
            UITableViewCell *cell = [self.table cellForRowAtIndexPath:sourceIndexPath];
            cell.alpha = 0.0;
            
            [UIView animateWithDuration:0.25 animations:^{
                
                snapshot.center = cell.center;
                snapshot.transform = CGAffineTransformIdentity;
                snapshot.alpha = 0.0;
                cell.alpha = 1.0;
                
            } completion:^(BOOL finished) {
                
                cell.hidden = NO;
                sourceIndexPath = nil;
                [snapshot removeFromSuperview];
                snapshot = nil;
                
            }];
            break;
        }
    }
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval: 0.3 target: self selector:@selector(reloadMethod) userInfo: nil repeats:NO];
}
-(void)reloadMethod{
    [table reloadData];
    movearray=[noArray copy];
    [[NSUserDefaults standardUserDefaults]setObject:movearray forKey:PranayamaList];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

#pragma mark - Helper methods

/** @brief Returns a customized snapshot of a given view. */
- (UIView *)customSnapshoFromView:(UIView *)inputView {
    
    // Make an image from the input view.
    UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, NO, 0);
    [inputView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Create an image view.
    UIView *snapshot = [[UIImageView alloc] initWithImage:image];
    snapshot.layer.masksToBounds = NO;
    snapshot.layer.cornerRadius = 0.0;
    snapshot.layer.shadowOffset = CGSizeMake(-5.0, 0.0);
    snapshot.layer.shadowRadius = 5.0;
    snapshot.layer.shadowOpacity = 0.4;
    
    return snapshot;
}



@end

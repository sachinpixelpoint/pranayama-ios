//
//  AppDelegate.h
//  Pranayama
//
//  Created by Manish Kumar on 30/03/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "GAI.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
UILocalNotification *notifications,*waterNotification,*dincharyaNotification;
}
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController*navcontroller;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property(nonatomic, strong) id<GAITracker> tracker;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
-(NSArray*)fetchReportFromDatabse;
-(NSArray*)fetchReportFromDatabseAccordingToType:(NSString*)type;
-(NSArray*)fetchAllReportFromDatabseAccordingToType:(NSString*)type;
///////////progress////
-(NSArray*)fetchReportFromProgressDatabseAccordingToType:(NSString*)type;
///water
-(NSArray*)fetchwaterReportFromDatabase;
-(NSArray*)FetchDincharyaFromDatabase;
-(NSArray*)FetchDincharyaFromDatabaseAcordingToName:(NSString*)Name;
-(NSArray*)FetchDincharyaReportFromDatabaseAcordingToName:(NSString*)Name;
@end

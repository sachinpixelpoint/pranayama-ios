//
//  ProblemListViewController.h
//  Pranayama
//
//  Created by Manish Kumar on 13/06/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProblemListViewController : GAITrackedViewController
{
    NSMutableArray *homelist;
    int fontSize,conditionValue;
    IBOutlet UIView *imageVIew;
    UIImageView *ImageView1,*ImageView2,*ImageView3;
}
@property(nonatomic,strong) IBOutlet UITableView *table;
@end

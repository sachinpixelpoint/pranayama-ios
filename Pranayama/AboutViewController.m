//
//  AboutViewController.m
//  Pranayama
//
//  Created by Manish Kumar on 27/07/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController
@synthesize Type;
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    // Do any additional setup after loading the view.
    self.screenName=[NSString stringWithFormat:@"%@ About",Type];
    int fontSize;
    if (DEVICE==IPAD) {
        fontSize=24;
    }
    else{
        fontSize=18;
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    self.automaticallyAdjustsScrollViewInsets = NO;

    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    NSString *linkStr;
    
    if ([Type isEqualToString:@"AnulomVilom"]) {
        self.navigationItem.title = [MCLocalization stringForKey:@"Anulom_Vilom"];
        label1.text=[MCLocalization stringForKey:@"anulom_about_content"];
        linkStr=[MCLocalization stringForKey:@"anulom_link"];
    }
    else if ([Type isEqualToString:@"Kapalbhati"]){
        label1.text=[MCLocalization stringForKey:@"kapal_about_content1"];
        self.navigationItem.title = [MCLocalization stringForKey:@"kapalbhati"];
        linkStr=[MCLocalization stringForKey:@"kapal_link"];
    }
    else if ([Type isEqualToString:@"Bhramari"]){
        label1.text=[MCLocalization stringForKey:@"bharamari_about_content"];
        self.navigationItem.title = [MCLocalization stringForKey:@"bhramari"];
        linkStr=[MCLocalization stringForKey:@"bharamari_link"];
    }
    else if ([Type isEqualToString:@"Surya"]){
        label1.text=[MCLocalization stringForKey:@"surya_about_content"];
        self.navigationItem.title = [MCLocalization stringForKey:@"surya_bhedana"];
        linkStr=[MCLocalization stringForKey:@"surya_link"];
    }
    else if ([Type isEqualToString:@"Chandra"]){
        label1.text=[MCLocalization stringForKey:@"chandra_about_content"];
        self.navigationItem.title = [MCLocalization stringForKey:@"chandra_bhedana"];
        linkStr=[MCLocalization stringForKey:@"chandra_link"];
    }
    else if ([Type isEqualToString:@"Bhastrika"]){
        label1.text=[MCLocalization stringForKey:@"bhastrika_about_content"];
        self.navigationItem.title = [MCLocalization stringForKey:@"bhastrika"];
        linkStr=[MCLocalization stringForKey:@"bhastrika_link"];
    }
    else if ([Type isEqualToString:@"Sheetali"]){
        label1.text=[MCLocalization stringForKey:@"sheetali_about_content1"];
        self.navigationItem.title = [MCLocalization stringForKey:@"sheetali"];
        linkStr=[MCLocalization stringForKey:@"sheetali_link"];
    }
    else if ([Type isEqualToString:@"Ujjayi"]){
        label1.text=[MCLocalization stringForKey:@"ujjayi_about1"];
        self.navigationItem.title = [MCLocalization stringForKey:@"ujjayi"];
        linkStr=[MCLocalization stringForKey:@"ujjayi_link"];
    }
    else if ([Type isEqualToString:@"Meditative"])
    {
        label1.text=[MCLocalization stringForKey:@"meditative_about"];
        self.navigationItem.title = [MCLocalization stringForKey:@"meditative"];
        linkStr=[MCLocalization stringForKey:@"meditative_link"];
    }
    else if ([Type isEqualToString:@"Udgeeth"])
    {
        label1.text=[MCLocalization stringForKey:@"udgeet_about"];
        self.navigationItem.title = [MCLocalization stringForKey:@"udgeeth"];
        linkStr=[MCLocalization stringForKey:@"udgeet_link"];
    }
    else 
    {
        label1.text=[MCLocalization stringForKey:@"bhay_about"];
        self.navigationItem.title = [MCLocalization stringForKey:@"bahya"];
        linkStr=[MCLocalization stringForKey:@"udgeet_link"];
    }
    
    label1.font=Ralewayfont(14);
    label1.numberOfLines=0;
    [label1 sizeToFit];
    [scrollView addSubview:label1];
    
    FRHyperLabel *label = [FRHyperLabel new];
    label.frame=CGRectMake(10,label1.frame.origin.y+label1.frame.size.height ,scrollView.frame.size.width-20, 200);
    label.numberOfLines=0;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]};
    
    
    label.attributedText = [[NSAttributedString alloc]initWithString:linkStr attributes:attributes];
    label.font=[UIFont systemFontOfSize:16];
    
    [label setLinkForSubstring:@"www.7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.7pranayama.com/"]];
    }];
    [label setLinkForSubstring:@"info@7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://info@7pranayama.com/"]];
    }];
    
    [scrollView addSubview:label];
    scrollView.contentSize=CGSizeMake(scrollView.frame.size.width,label.frame.origin.y+label.frame.size.height+100);
    int scrollHeight=label.frame.origin.y+label.frame.size.height;
    [self ImageMethod:scrollHeight];
}


- (void)HomeMethod:(NSNotification *)notification {
    HomeTabelVC *homeObject = [[HomeTabelVC alloc]init];
    [homeObject appDidBecomeActive:notification];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self disableSlidePanGestureForRightMenu];
}


-(void)ImageMethod:(int)scrollHeight{
    
    int a=self.view.frame.size.width/4;
    int b=a/2;
    UIButton*btn1=[[UIButton alloc] initWithFrame:CGRectMake(a/2-25, scrollHeight+20, 50, 50)];
    btn1.tag=1;
    btn1.layer.cornerRadius = btn1.bounds.size.width / 2.0;
    UIImage *btnImage1 = [UIImage imageNamed:@"facebook"];
    [btn1 setImage:btnImage1 forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(goToLink:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:btn1];
    
    UIButton*btn2=[[UIButton alloc] initWithFrame:CGRectMake(((2*a)-b)-25, scrollHeight+20, 50, 50)];
    btn2.tag=2;
    btn2.layer.cornerRadius = btn1.bounds.size.width / 2.0;
    UIImage *btnImage2 = [UIImage imageNamed:@"twitter"];
    [btn2 setImage:btnImage2 forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(goToLink:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:btn2];
    
    UIButton*btn3=[[UIButton alloc] initWithFrame:CGRectMake(((3*a)-b)-25, scrollHeight+20, 50, 50)];
    btn3.tag=3;
    btn3.layer.cornerRadius = btn1.bounds.size.width / 2.0;
    UIImage *btnImage3 = [UIImage imageNamed:@"pinterest"];
    [btn3 setImage:btnImage3 forState:UIControlStateNormal];
    [btn3 addTarget:self action:@selector(goToLink:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:btn3];
    
    UIButton*btn4=[[UIButton alloc] initWithFrame:CGRectMake((self.view.frame.size.width-b)-25,scrollHeight+20, 50, 50)];
    btn4.tag=4;
    btn4.layer.cornerRadius = btn1.bounds.size.width / 2.0;
    UIImage *btnImage4 = [UIImage imageNamed:@"google"];
    [btn4 setImage:btnImage4 forState:UIControlStateNormal];
    [btn4 addTarget:self action:@selector(goToLink:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:btn4];
    
}

-(void)goToLink:(UIButton*)sender{
    if (sender.tag==1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.facebook.com/7pranayama-1742577192620397/"]];
    }
    else if (sender.tag==2){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://twitter.com/7pranayama"]];
        
    }
    else if (sender.tag==3){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://in.pinterest.com/7pranayama/"]];
        
    }
    else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://plus.google.com/111008006040750995181/posts"]];
    }
}
-(IBAction)backbutton:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

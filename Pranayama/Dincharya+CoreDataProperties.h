//
//  Progress+CoreDataProperties.h
//  Pranayama
//
//  Created by Manish Kumar on 02/07/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//


#import "Dincharya.h"

NS_ASSUME_NONNULL_BEGIN

@interface Dincharya (CoreDataProperties)
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSDate *notification_time;
@property (nullable, nonatomic, retain) NSData *pranayama_arr;
@property (nullable, nonatomic, retain) NSData *yogasana_arr;
@property (nullable, nonatomic, retain) NSString *autometicTime;
@end

NS_ASSUME_NONNULL_END

//
//  MeditativeVC.h
//  Pranayama
//
//  Created by Manish Kumar on 05/07/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MeditativeActionVC.h"

@interface MeditativeVC : GAITrackedViewController<meditativedelegate>
{
    IBOutlet UIPickerView *roundPicker;
    int RoundRow;
    NSMutableArray *roundData;
    IBOutlet UIView *topview;
    IBOutlet UIImageView *img;
    
    IBOutlet UILabel *inhalelbl;
    IBOutlet UILabel *holdlbl;
    IBOutlet UILabel *exhalelbl;
    IBOutlet UILabel *roundlbl;
    
    IBOutlet UIButton *StartButton;
    IBOutlet UIButton *reportButton;
    
    IBOutlet UILabel *lblHours;
    IBOutlet UILabel *lblminutes;
    IBOutlet UILabel *lblseconds;
    int totaltime;
    int seconds;
    int minutes;
    int hours;
    UIButton *button1,*button2,*button3;
    BOOL isMenuVisible;
    IBOutlet UIButton *Floatingbutton;
    BOOL selectCondition;
     BOOL buttonCondition;
    NSString *TodayFitness,*Daystr,*Your_leve,*Fitness_regime_21,*Share_experience,*session;
}
@end

//
//  DincharyaNameVC.swift
//  Pranayama
//
//  Created by Manish Kumar on 19/10/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

import UIKit

class DincharyaNameVC: GAITrackedViewController,UIGestureRecognizerDelegate {
    
    let kAppDel = (UIApplication.shared.delegate! as! AppDelegate)
    
    var dincharya : Dincharya!
    var din_name : NSString!
    
    @IBOutlet var YogaBtn : UIButton!
    @IBOutlet var yogaLine : UILabel!
    @IBOutlet var pranaBtn : UIButton!
    @IBOutlet var pranaLine : UILabel!
    @IBOutlet var my_ProgressBtn : UIButton!
    @IBOutlet var goToStartBtn :UIButton!
    var yogabtnPosition : CGFloat!
    var pranaBtnPosition : CGFloat!
    ///// new
    
    
    @IBOutlet var AddBtn : UIButton!
    @IBOutlet var EditTimeBtn : UIButton!
    @IBOutlet var CancelNotBtn : UIButton!
    
    var yogasanArray : NSArray! = NSArray()
    var pranayamaArray : NSArray! = NSArray()
    ////array for english///
    var yogaArray : NSArray! = NSArray()
    var pranaArray : NSArray! = NSArray()
    var yogaArray1 : NSMutableArray! = NSMutableArray()
    var pranaArray1 : NSMutableArray! = NSMutableArray()
    var PranalistView : UIView!
    var YogsanlistView : UIView!
    var nextBtn : UIButton!
    
    var view1: UIView!
    var blackOverlay : UIView!
    var window : UIWindow!
    
    var TimePickerView : UIView!
    var TimePicker : UIDatePicker!
    var timelbl : UILabel!
    
    var managedObjectContext: NSManagedObjectContext!
    var nextstr : String!
    var select_yoga : String!
    var select_prana : String!
    var cancle_schedule : String!
    var cancle_Msg : String!
    var add_schedule : String!
    var set : String!
    var cancel : String!
    var minselectionstr : String!
    
    var btnFont : CGFloat!
    var fontSize : CGFloat!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: (#selector(self.appDidBecomeActive(_:))), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        self.screenName="Dincharya"
        //change Button corner to circle
        goToStartBtn.layer.cornerRadius = 10
        goToStartBtn.layer.masksToBounds = true
        goToStartBtn.setTitle("START NOW", for: .normal)
        
        // Do any additional setup after loading the view.
        yogabtnPosition = YogaBtn.frame.origin.y
        pranaBtnPosition = pranaBtn.frame.origin.y
          }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            fontSize=24
            btnFont=18
        }
        else{
            fontSize=18
            btnFont=12
        }
        UINavigationBar.appearance().titleTextAttributes=[NSFontAttributeName: Ralewayfont(fontSize!),NSForegroundColorAttributeName:UIColor.white]
        
        let mainVC = AMSlideMenuMainViewController.getInstanceForVC(self) as AMSlideMenuMainViewController
        if (mainVC.rightMenu != nil) {
            self.addRightMenuButton()
        }
        UserDefaults.standard.set("Dincharya Name", forKey: controllerType)

        
        if din_name == nil{
            din_name = UserDefaults.standard.object(forKey: dincharyaName) as! NSString!
            print(din_name)
        }
        
        self.GetDincharya()
        
        self.managedObjectContext = kAppDel.managedObjectContext!
        self.navigationItem.title = dincharya.name
        
        window = UIApplication.shared.keyWindow
        let singleFingerTap = UITapGestureRecognizer(target: self, action: #selector(self.handleSingleTap))
        singleFingerTap.delegate=self
        window.addGestureRecognizer(singleFingerTap)
        
        if dincharya.notification_time != nil {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            let datestr = dateFormatter.string(from:dincharya.notification_time! as Date)
            
            EditTimeBtn.setTitle(datestr, for: .normal)
        }
        else{
            EditTimeBtn.setTitle(nil, for: .normal)
            CancelNotBtn.setImage(UIImage.init(named: "cancel_round_icon"), for: .normal)
        }
        
        self.setText()
        
    }
    
    func appDidBecomeActive(_ notification:Notification) -> Void {
        
        CommonSounds.sharedInstance().appDidBecomeActive(notification, navigation: self.navigationController)
    }
    
    
    /////////////////  Get dincharya from database///////////
        func GetDincharya() -> Void {
        
        let ar = kAppDel.fetchDincharyaFromDatabaseAcording(toName: din_name as String!) as NSArray
        dincharya = ar.object(at: 0) as! Dincharya
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let datestr = dateFormatter.string(from:dincharya.notification_time! as Date)
        print(datestr)
        
        let yogaAr = NSKeyedUnarchiver.unarchiveObject(with: dincharya.yogasana_arr!) as! NSArray
        let pranaAr = NSKeyedUnarchiver.unarchiveObject(with: dincharya.pranayama_arr!) as! NSArray
        
        yogaArray1 = NSMutableArray(array: yogaAr)
        pranaArray1 = NSMutableArray(array: pranaAr)
        print(yogaArray1)
        print(pranaArray1)
        
        self.setPosition_ifCountzero()
        }
    
        func setPosition_ifCountzero() -> Void {
        if yogaArray1.count == 0 {
            YogaBtn.isHidden=true;
            yogaLine.isHidden=true;
          }
        else{
            YogaBtn.frame.origin.y=yogabtnPosition
            YogaBtn.isHidden=false
            yogaLine.isHidden=false
         }
        if pranaArray1.count == 0 {
            pranaLine.isHidden=true
            pranaBtn.isHidden=true
            YogaBtn.frame.origin.y = pranaBtnPosition
        }
        else{
            pranaLine.isHidden = false
            pranaBtn.isHidden = false
        }
        yogaLine.frame.origin.y = YogaBtn.frame.origin.y+YogaBtn.frame.size.height
    }
    
    //    //    /////////tap gesture/////
    //
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        return (touch.view == blackOverlay)
    }
    
    func handleSingleTap(_ recognizer:UITapGestureRecognizer) -> Void{
        view1.endEditing(true)
    }
    /////////////
    
    
    func setText() -> Void {
        
        yogasanArray=[MCLocalization.string(forKey: "Sarvangasana"),MCLocalization.string(forKey: "Halasana"),MCLocalization.string(forKey: "Vipritkarani"),MCLocalization.string(forKey: "Paschimottanasana"),MCLocalization.string(forKey: "Dhanurasana"),MCLocalization.string(forKey: "Balasana"),MCLocalization.string(forKey: "Hastapadasana"),MCLocalization.string(forKey: "Marjariasana"),MCLocalization.string(forKey: "Uttanasana"),MCLocalization.string(forKey: "Setu Bandhasana"),MCLocalization.string(forKey: "Virabhadrasana")]
        
        pranayamaArray=[MCLocalization.string(forKey: "Anulom_Vilom"),MCLocalization.string(forKey: "kapalbhati"),MCLocalization.string(forKey: "bhramari"),MCLocalization.string(forKey: "surya_bhedana"),MCLocalization.string(forKey: "chandra_bhedana"),MCLocalization.string(forKey: "bhastrika"),MCLocalization.string(forKey: "sheetali"),MCLocalization.string(forKey: "ujjayi"),MCLocalization.string(forKey: "meditative"),MCLocalization.string(forKey: "udgeeth"),MCLocalization.string(forKey: "bahya")]
        
        yogaArray = ["Sarvangasana","Halasana","Vipritkarani","Paschimottanasana","Dhanurasana","Balasana","Hastapadasana","Marjariasana","Uttanasana","Setu Bandhasana","Virabhadrasana"]
        pranaArray = ["AnulomVilom","Kapalbhati","Bhramari","Surya Bhedana","Chandra Bhedana","Bhastrika","Sheetali","Ujjayi","Meditative Breathing","Udgeeth","Bahya"]
        
        YogaBtn.setTitle(MCLocalization.string(forKey: "yogasan"), for: .normal)
        pranaBtn.setTitle(MCLocalization.string(forKey: "Pranayam"), for: .normal)
        my_ProgressBtn.setTitle(MCLocalization.string(forKey: "progress"), for: .normal)
        
        add_schedule = MCLocalization.string(forKey: "add_dincharya")
        nextstr = MCLocalization.string(forKey: "nexstr")
        select_yoga = MCLocalization.string(forKey: "select_yoga")
        select_prana = MCLocalization.string(forKey: "select_prana")
        set = MCLocalization.string(forKey: "set")
        cancel = MCLocalization.string(forKey: "Cancel")
        minselectionstr = MCLocalization.string(forKey: "min_selection")
        
    }
    
    
    @IBAction func AddBtnMethod(_ sender : AnyObject){
          self.addYogasanaMethod()
        
    }
    
    @IBAction func EditNotBtnMethod(_ sender : AnyObject){
        
        self.show_time_picker()
        
    }
    
    @IBAction func CancelNotBtnMethod(_ sender : AnyObject){
        
        if dincharya.notification_time != nil {
            if #available(iOS 8.0, *) {
                let alert = UIAlertController(title: "Cancel Schedule", message: "Are you sure you want to cancel your schedule notifications?", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.default, handler: nil))
                alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: { action in
                    
                    self.CancelNotBtn.setImage(UIImage.init(named: "add_icon"), for: .normal)
                    self.UpdateNotificationTime(is_nil: true)
                    self.EditTimeBtn.setTitle("", for: .normal)
            
                }))
                
                self.present(alert, animated: true, completion: nil)
            } else {
                // Fallback on earlier versions
            }
        }
        else{
            
            self.show_time_picker()
            
        }
        
    }
    
    
    
    func show_time_picker() -> Void {
        
        self.view1Method()
        
        TimePickerView = UIView(frame: CGRect(x: self.view.frame.size.width, y: self.view.frame.size.height/4, width: self.view.frame.size.width-40, height: self.view.frame.size.height/2))
        TimePickerView.backgroundColor=UIColor.white
        TimePickerView.layer.cornerRadius=5
        TimePickerView.layer.borderColor=UIColor.red.cgColor
        TimePickerView.layer.borderWidth=2
        view1.addSubview(TimePickerView)
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.blackOverlay.layer.opacity=0.4
            self.TimePickerView.frame.origin.x=20
            },completion: { (finished: Bool) -> Void in
        })
        
        let pikerview = UIView(frame: CGRect(x: 10, y: 10, width: TimePickerView.frame.size.width-20, height: TimePickerView.frame.size.height-20))
        pikerview.backgroundColor=UIColor.white
        pikerview.layer.shadowColor=UIColor.darkGray.cgColor
        pikerview.layer.shadowOffset=CGSize(width: 0, height: 0)
        pikerview.layer.shadowOpacity=0.7
        pikerview.layer.shadowRadius=5
        TimePickerView.addSubview(pikerview)
        
        let height = pikerview.frame.size.height/5
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let datestr = dateFormatter.string(from:NSDate() as Date)
        
        timelbl = UILabel(frame: CGRect(x: 15, y: 0, width: pikerview.frame.size.width-20, height: height))
        timelbl.text=datestr
        timelbl.font=Ralewayfont(fontSize)
        pikerview.addSubview(timelbl)
        
        let line = UILabel(frame: CGRect(x: 0, y: height, width: pikerview.frame.size.width, height: 1))
        line.backgroundColor=UIColor.red
        pikerview.addSubview(line)
        
        TimePicker = UIDatePicker(frame: CGRect(x: 20, y: height+20, width: pikerview.frame.size.width-40, height: height*3-40))
        TimePicker.datePickerMode=UIDatePickerMode.time
        if dincharya.notification_time != nil {
            TimePicker.date=dincharya.notification_time!
        }
        else{
            TimePicker.date=NSDate() as Date
        }
        TimePicker.addTarget(self, action: #selector(self.dateChange), for: .valueChanged)
        pikerview.addSubview(TimePicker)
        
        let color = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1.0)
        let line2 = UILabel(frame: CGRect(x: 0, y: height*4, width: pikerview.frame.size.width, height: 1))
        line2.backgroundColor=color
        pikerview.addSubview(line2)
        
        let line3 = UILabel(frame: CGRect(x: pikerview.frame.size.width/2, y: height*4, width: 1, height: height))
        line3.backgroundColor=color
        pikerview.addSubview(line3)
        
        let cancelBtn = UIButton(frame: CGRect(x: 0, y: height*4+1, width: pikerview.frame.size.width/2, height: height))
        cancelBtn.setTitle(cancel, for: .normal)
        cancelBtn.titleLabel?.font=Ralewayfont(fontSize)
        cancelBtn.setTitleColor(UIColor.orange, for: .normal)
        cancelBtn.tag=1
        cancelBtn.addTarget(self, action: #selector(self.pickerBtnMethod(_:)), for: .touchUpInside)
        pikerview.addSubview(cancelBtn)
        
        let Okbtn = UIButton(frame: CGRect(x: pikerview.frame.size.width/2+1, y: height*4, width: pikerview.frame.size.width/2, height: height))
        Okbtn.setTitle(set, for: .normal)
        Okbtn.titleLabel?.font=Ralewayfont(fontSize)
        Okbtn.setTitleColor(UIColor.orange, for: .normal)
        Okbtn.tag=2
        Okbtn.addTarget(self, action: #selector(self.pickerBtnMethod(_:)), for: .touchUpInside)
        pikerview.addSubview(Okbtn)
        
        
    }
    
    ////////////////////    Date change by date picker /////////
    func dateChange() -> Void {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let datestr = dateFormatter.string(from:TimePicker.date)
        timelbl.text=datestr
    }
    
    
    /////////////////  Ok cancel button method///////
    func pickerBtnMethod(_ sender: UIButton) -> Void {
        
        if sender.tag==2{
            self.UpdateNotificationTime(is_nil: false)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            let datestr = dateFormatter.string(from:dincharya.notification_time!)
            EditTimeBtn.setTitle(datestr, for: .normal)
            CancelNotBtn.setImage(UIImage.init(named: "cancel_round_icon"), for: .normal)
        }
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.blackOverlay.layer.opacity=0
            self.TimePickerView.frame.origin.x=(-self.TimePickerView.frame.size.width)
            self.view1.endEditing(true)
            },completion: { (finished: Bool) -> Void in
                self.view1.isHidden=true
        })
        
    }
    
    func UpdateNotificationTime(is_nil : Bool) -> Void {
        
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Dincharya" )
        request.entity = NSEntityDescription.entity(forEntityName: "Dincharya", in: self.managedObjectContext!)!
        let predicate = NSPredicate(format: "name==%@", dincharya.name!)
        request.predicate = predicate
        var results = try! self.managedObjectContext!.fetch(request)
        let entity = results[0] as! Dincharya
        if is_nil == true {
            entity.notification_time = nil
        }
        else{
            entity.notification_time=TimePicker.date
        }
        
        do {
            try self.managedObjectContext?.save()
        } catch {
            print(error)
        }

        
        self.GetDincharya()
    }
    
    
    func view1Method() -> Void {
        view1=UIView(frame: CGRect(x: 0,y: 0,width: self.view.frame.size.width, height: self.view.frame.size.height))
        blackOverlay=UIView(frame: view1.frame)
        blackOverlay.layer.backgroundColor = UIColor.black.cgColor
        blackOverlay.layer.opacity = 0
        view1.addSubview(blackOverlay)
        window.addSubview(view1)
    }
    
    
    func TitleView(_ popupview: UIView,Height:CGFloat,tag:Int) -> Void {
        let exitButton=UIButton(frame: CGRect(x: 0, y: 0, width: popupview.frame.size.width/6, height: popupview.frame.size.height/Height))
        exitButton.setImage(UIImage(named: "cancel_icon"), for: UIControlState())
        exitButton.tag=tag
        exitButton.addTarget(self, action: #selector(self.Exitbutton), for: .touchUpInside)
        popupview.addSubview(exitButton)
        
        ////////////Title of popup/////////
        let AddTitle = UILabel(frame: CGRect(x:exitButton.frame.origin.x+exitButton.frame.size.width , y: 0, width: (width: popupview.frame.size.width/6)*4, height: exitButton.frame.size.height))
        AddTitle.text = add_schedule
        AddTitle.font=Ralewayfont(fontSize)
        AddTitle.textAlignment=NSTextAlignment.center
        popupview.addSubview(AddTitle)
        
        ///////// Next Button /////
        nextBtn = UIButton(frame: CGRect(x: AddTitle.frame.origin.x+AddTitle.frame.size.width, y: 0, width: exitButton.frame.size.width, height: exitButton.frame.size.height))
        if(tag==1){
            nextBtn.setTitle(nextstr, for: UIControlState())
        }
        else{
            nextBtn.setTitle("OK", for: UIControlState())
        }
        nextBtn.titleLabel?.font=Ralewayfont(fontSize)
        nextBtn.setTitleColor(UIColor.black, for: UIControlState())
        nextBtn.addTarget(self, action: #selector(self.NextBtnMethod), for: .touchUpInside)
        nextBtn.tag=tag
        popupview.addSubview(nextBtn)
        
        /////// add line //
        let line = UILabel(frame: CGRect(x: 0, y: exitButton.frame.origin.y+exitButton.frame.size.height, width: popupview.frame.size.width, height: 1))
        line.backgroundColor=UIColor.lightGray
        popupview.addSubview(line)
        
    }
    
    //////////////////////  Add yogasana method ////////////
    
    func addYogasanaMethod() -> Void {
        
                self.view1Method()
        
        YogsanlistView = UIView(frame: CGRect(x: self.view.frame.size.width, y: self.view.frame.size.height/5, width: self.view.frame.size.width-20, height: (self.view.frame.size.height/5)*3))
        YogsanlistView.backgroundColor=UIColor.white
        YogsanlistView.layer.cornerRadius=5
        YogsanlistView.layer.borderColor=UIColor.red.cgColor
        YogsanlistView.layer.borderWidth=2
        view1.addSubview(YogsanlistView)
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.blackOverlay.layer.opacity=0.4
            self.YogsanlistView.frame.origin.x=10
            },completion: { (finished: Bool) -> Void in
        })
        
        self.TitleView(YogsanlistView, Height: 10, tag: 1)
        
        let addPrana = UILabel(frame: CGRect(x: 10, y: (YogsanlistView.frame.size.height/10)+5, width: YogsanlistView.frame.size.width-20, height: YogsanlistView.frame.size.height/10))
        addPrana.text=select_yoga
        addPrana.font=Ralewayfont(fontSize)
        addPrana.textColor=UIColor.gray
        YogsanlistView.addSubview(addPrana)
        
        let line2 = UILabel(frame: CGRect(x: 0, y: addPrana.frame.origin.y+addPrana.frame.size.height, width: YogsanlistView.frame.size.width, height: 1))
        line2.backgroundColor=UIColor.lightGray
        YogsanlistView.addSubview(line2)
        
        let Listview = UIView(frame: CGRect(x: 10, y: line2.frame.origin.y+1, width: YogsanlistView.frame.size.width-20, height: YogsanlistView.frame.size.height-(line2.frame.origin.y+3)))
        YogsanlistView.addSubview(Listview)
        
        let a = CGFloat((yogasanArray?.count)!)
        let h = (Listview.frame.size.height/a)
        let b = Int(a)
        
        for i in 0..<b {
            let button : UIButton = UIButton(frame: CGRect(x: 0, y: CGFloat(i)*h, width: Listview.frame.size.width, height: h))
            button.tag=i;
            button.setTitle(yogasanArray![i] as? String, for: UIControlState())
            let star = yogaArray.object(at: i)
             print(star)
            if yogaArray1.contains(star) {
                button.setTitleColor(UIColor.orange, for: UIControlState())
            }
            else{
                button.setTitleColor(UIColor.gray, for: UIControlState())
            }
            button.contentHorizontalAlignment=UIControlContentHorizontalAlignment.left
            button.titleLabel?.font=Ralewayfont(btnFont)
            button.addTarget(self, action: #selector(self.YogasanabuttonMethod), for: .touchUpInside)
            Listview.addSubview(button)
        }
    }
    
    ////////// Yogasana buttons method for changing the color and  add it//////
    func YogasanabuttonMethod(_ sender:UIButton) -> Void {
        if sender.currentTitleColor.isEqual(UIColor.gray){
            sender.setTitleColor(UIColor.orange, for: UIControlState())
            yogaArray1?.add((yogaArray?.object(at: sender.tag))!)
            print(yogaArray1)
        }
        else{
            sender.setTitleColor(UIColor.gray, for: UIControlState())
            yogaArray1!.remove((yogaArray?.object(at: sender.tag))!)
            print(yogaArray1)
        }
    }
    
    
    //////////////////////// Add Pranayama method/////////
    func addPranayamaMethod() -> Void {
        
        //        self.view1Method()
        
        PranalistView = UIView(frame: CGRect(x: self.view.frame.size.width, y: self.view.frame.size.height/5, width: self.view.frame.size.width-20, height: (self.view.frame.size.height/5)*3))
        PranalistView.backgroundColor=UIColor.white
        PranalistView.layer.cornerRadius=5
        PranalistView.layer.borderColor=UIColor.red.cgColor
        PranalistView.layer.borderWidth=2
        view1.addSubview(PranalistView)
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.blackOverlay.layer.opacity=0.4
            self.PranalistView.frame.origin.x=10
            },completion: { (finished: Bool) -> Void in
        })
        
        self.TitleView(PranalistView, Height: 10, tag: 2)
        
        let addPrana = UILabel(frame: CGRect(x: 10, y: (PranalistView.frame.size.height/10)+5, width: PranalistView.frame.size.width-20, height: PranalistView.frame.size.height/10))
        addPrana.text=select_prana
        addPrana.font=Ralewayfont(fontSize)
        addPrana.textColor=UIColor.gray
        PranalistView.addSubview(addPrana)
        
        let line2 = UILabel(frame: CGRect(x: 0, y: addPrana.frame.origin.y+addPrana.frame.size.height, width: PranalistView.frame.size.width, height: 1))
        line2.backgroundColor=UIColor.lightGray
        PranalistView.addSubview(line2)
        
        
        let Listview = UIView(frame: CGRect(x: 10, y: line2.frame.origin.y+1, width: PranalistView.frame.size.width-20, height: YogsanlistView.frame.size.height-(line2.frame.origin.y+3)))
        PranalistView.addSubview(Listview)
        
        let a = CGFloat((pranayamaArray?.count)!)
        let h = (Listview.frame.size.height/a)
        let b = Int(a)
        
        for i in 0..<b {
            let button : UIButton = UIButton(frame: CGRect(x: 0, y: CGFloat(i)*h, width: Listview.frame.size.width, height: h))
            button.tag=i;
            button.setTitle(pranayamaArray![i] as? String, for: UIControlState())
            let star = pranaArray.object(at: i)
            print(star)
            if pranaArray1.contains(star) {
                button.setTitleColor(UIColor.orange, for: UIControlState())
            }
            else{
                button.setTitleColor(UIColor.gray, for: UIControlState())
            }
            button.contentHorizontalAlignment=UIControlContentHorizontalAlignment.left
            button.titleLabel?.font=Ralewayfont(btnFont)
            button.addTarget(self, action: #selector(self.PranayamabuttonMethod), for: .touchUpInside)
            Listview.addSubview(button)
        }
    }
    
    ////////// Pranayama buttons method for changing the color and  add it//////
    
    func PranayamabuttonMethod(_ sender:UIButton) -> Void {
        if sender.currentTitleColor.isEqual(UIColor.gray){
            sender.setTitleColor(UIColor.orange, for: UIControlState())
            pranaArray1?.add((pranaArray?.object(at: sender.tag))!)
            print(pranaArray1)
        }
        else{
            sender.setTitleColor(UIColor.gray, for: UIControlState())
            pranaArray1!.remove((pranaArray?.object(at: sender.tag))!)
            print(pranaArray1)
        }
    }

    
    
    func Exitbutton(_ sender: UIButton) -> Void {
        if sender.tag==1 {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.blackOverlay.layer.opacity=0
                self.YogsanlistView.frame.origin.x=(-self.YogsanlistView.frame.size.width)
                self.view1.endEditing(true)
                self.yogaArray1?.removeAllObjects()
                self.pranaArray1?.removeAllObjects()
                self.GetDincharya()
                },completion: { (finished: Bool) -> Void in
                    self.view1.isHidden=true
            })
        }
        else if sender.tag==2{
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.YogsanlistView.frame.origin.x=10
                self.PranalistView.frame.origin.x=(-self.view.frame.size.width)
                self.view1.endEditing(true)
                },completion: { (finished: Bool) -> Void in
                    self.PranalistView.frame.origin.x=(self.view.frame.size.width)
                    //                    self.view1.isHidden=true
            })
        }
    }
    
    ///////////////// Next button method////////////////
    
    func NextBtnMethod(_ sender: UIButton) -> Void {
        if sender.tag==1 {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                //                self.blackOverlay.layer.opacity=0
                self.YogsanlistView.frame.origin.x=(-self.view.frame.size.width)
                self.view1.endEditing(true)
                self.addPranayamaMethod()
                },completion: { (finished: Bool) -> Void in
                    self.YogsanlistView.frame.origin.x=(self.view.frame.size.width)
            })
        }
        else if sender.tag==2  {
            if pranaArray1.count == 0 && yogaArray1.count == 0{
                if #available(iOS 8.0, *) {
                    let alert = UIAlertController(title: "", message: minselectionstr, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
            else{
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    self.blackOverlay.layer.opacity=0
                    self.PranalistView.frame.origin.x=(-self.view.frame.size.width)
                    self.updatePranaYogaArr()
                    },completion: { (finished: Bool) -> Void in
                        self.view1.isHidden=true
                })
            }
        }
    }
    
    func updatePranaYogaArr() -> Void {
        
        let yogaar = yogaArray1?.copy() as! NSArray
        let pranaar = pranaArray1?.copy() as! NSArray
        
        let pranaData = NSKeyedArchiver.archivedData(withRootObject: pranaar)
        let yogaData = NSKeyedArchiver.archivedData(withRootObject: yogaar)
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Dincharya" )
        request.entity = NSEntityDescription.entity(forEntityName: "Dincharya", in: self.managedObjectContext!)!
        let predicate = NSPredicate(format: "name==%@", dincharya.name!)
        request.predicate = predicate
        var results = try! self.managedObjectContext!.fetch(request)
        let entity = results[0] as! Dincharya
        entity.pranayama_arr = pranaData
        entity.yogasana_arr = yogaData
        
        do {
            try self.managedObjectContext?.save()
        } catch {
            print(error)
        }
        
        self.GetDincharya()
    }
    
    @IBAction func pranaBtnMethod(_ sender : AnyObject){
        
        let dinchNmae = self.storyboard?.instantiateViewController(withIdentifier: "dincharya_prana") as! Dincharya_pranaVC
        dinchNmae.PranaList = pranaArray1
        self.navigationController?.pushViewController(dinchNmae, animated: true)
        
    }
    
    @IBAction func YogaBtnMethod(_ sender : AnyObject){
        
        let dinchNmae = self.storyboard?.instantiateViewController(withIdentifier: "dincharya_yoga") as! Dincharya_yogaVC
        dinchNmae.yogasanList = yogaArray1
        self.navigationController?.pushViewController(dinchNmae, animated: true)
        
    }
    
    
    @IBAction func My_progressBtnMethod(_ sender : AnyObject){
        
        let dinchNmae = self.storyboard?.instantiateViewController(withIdentifier: "dincharya_progress") as! Dincharya_ProgressVC
        dinchNmae.dincharya = dincharya
        self.navigationController?.pushViewController(dinchNmae, animated: true)
        
    }
    
    @IBAction func backbutton(_ sender: AnyObject) {
        self.navigationController!.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//
//  CommonSounds.m
//  Pranayama
//
//  Created by Manish Kumar on 08/06/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "CommonSounds.h"
#import "Pranayama-Swift.h"

@implementation CommonSounds
+(instancetype)sharedInstance{
    static CommonSounds *sharedManager=nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        sharedManager=[[self alloc] init];
    });
    return sharedManager;
}
-(void)playEverySound{
    BOOL isInAppSound=[[NSUserDefaults standardUserDefaults] boolForKey:kISSound];
    BOOL isInAppSoundEverSec=[[NSUserDefaults standardUserDefaults] boolForKey:kISSoundEver];
    if (!isInAppSoundEverSec) {
        return;
    }
    
    if (isInAppSound) {
        NSURL *soundurl   = [[NSBundle mainBundle] URLForResource: @"Clave_Pop" withExtension: @"mp3"];
        [self soundControlMethod:soundurl];
    }
}

-(void)playFinishSound{
    BOOL isInAppSound=[[NSUserDefaults standardUserDefaults] boolForKey:kISSound];    
    if (isInAppSound) {
        NSURL *soundurl   = [[NSBundle mainBundle] URLForResource: @"Finish1" withExtension: @"mp3"];
        [self soundControlMethod:soundurl];
    }
}

-(void)playInhaleSound{
    BOOL isInAppSound=[[NSUserDefaults standardUserDefaults] boolForKey:kISSound];
    BOOL isInAppVibrate=[[NSUserDefaults standardUserDefaults] boolForKey:kISVibrator];
    NSString *lan=[[NSUserDefaults standardUserDefaults]objectForKey:Languagevalue];
    BOOL soundtype=[[NSUserDefaults standardUserDefaults] boolForKey:soundType];
    NSURL *soundurl;
    if (isInAppSound) {
        if (soundtype) {
            if ([lan isEqualToString:Hindi]) {
                soundurl=[[NSBundle mainBundle] URLForResource:@"inhale_hindi" withExtension:@"mp3"];
            }
            else{
                soundurl=[[NSBundle mainBundle] URLForResource:@"inhale" withExtension:@"mp3"];
            }
            [self soundControlMethod:soundurl];
        }
        else{
            [self playChangeSound];
        }
    }
    if (isInAppVibrate) {
        if([[UIDevice currentDevice].model isEqualToString:@"iPhone"])
        {
            AudioServicesPlaySystemSound (kSystemSoundID_Vibrate);
        }
    }
    
}

-(void)playHoldSound{
    BOOL isInAppSound=[[NSUserDefaults standardUserDefaults] boolForKey:kISSound];
    BOOL isInAppVibrate=[[NSUserDefaults standardUserDefaults] boolForKey:kISVibrator];
    NSString *lan=[[NSUserDefaults standardUserDefaults]objectForKey:Languagevalue];
    BOOL soundtype=[[NSUserDefaults standardUserDefaults] boolForKey:soundType];
    NSURL *soundurl;
    if (isInAppSound) {
        if (soundtype) {
            if ([lan isEqualToString:Hindi]) {
                soundurl=[[NSBundle mainBundle] URLForResource:@"hold_hindi" withExtension:@"mp3"];
            }
            else{
                soundurl=[[NSBundle mainBundle] URLForResource:@"hold" withExtension:@"mp3"];
            }
            [self soundControlMethod:soundurl];
        }
        else{
            [self playChangeSound];
        }
    }
    if (isInAppVibrate) {
        if([[UIDevice currentDevice].model isEqualToString:@"iPhone"])
        {
            AudioServicesPlaySystemSound (kSystemSoundID_Vibrate);
        }
    }
}

-(void)playExhaleSound{
    BOOL isInAppSound=[[NSUserDefaults standardUserDefaults] boolForKey:kISSound];
    BOOL isInAppVibrate=[[NSUserDefaults standardUserDefaults] boolForKey:kISVibrator];
    NSString *lan=[[NSUserDefaults standardUserDefaults]objectForKey:Languagevalue];
    BOOL soundtype=[[NSUserDefaults standardUserDefaults] boolForKey:soundType];
    NSURL *soundurl;
    if (isInAppSound) {
        if (soundtype) {
            if ([lan isEqualToString:Hindi]) {
                soundurl=[[NSBundle mainBundle] URLForResource:@"exhale_hindi" withExtension:@"mp3"];
            }
            else{
                soundurl=[[NSBundle mainBundle] URLForResource:@"exhale_normal" withExtension:@"mp3"];
            }
            [self soundControlMethod:soundurl];
        }
        else{
            [self playChangeSound];
        }
    }
    if (isInAppVibrate) {
        if([[UIDevice currentDevice].model isEqualToString:@"iPhone"])
        {
            AudioServicesPlaySystemSound (kSystemSoundID_Vibrate);
        }
    }
}


-(void)playChangeSound{
    BOOL isInAppSound=[[NSUserDefaults standardUserDefaults] boolForKey:kISSound];
    if (isInAppSound) {
        
        NSURL *soundurl   = [[NSBundle mainBundle] URLForResource: @"change" withExtension: @"mp3"];
        [self soundControlMethod:soundurl];
    }
}

-(void)playKapalExhlaleSound{
    BOOL isInAppSound=[[NSUserDefaults standardUserDefaults] boolForKey:kISSound];
    BOOL isInAppVibrate=[[NSUserDefaults standardUserDefaults] boolForKey:kISVibrator];
    
    if (isInAppSound) {
        
        NSURL *soundurl   = [[NSBundle mainBundle] URLForResource: @"exhale" withExtension: @"mp3"];
        [self soundControlMethod:soundurl];
    }
    
    if (isInAppVibrate) {
        if([[UIDevice currentDevice].model isEqualToString:@"iPhone"])
        {
            AudioServicesPlaySystemSound (kSystemSoundID_Vibrate);
        }
    }
}


-(void)soundControlMethod:(NSURL*)soundurl{
    volumeValue = [[NSUserDefaults standardUserDefaults] floatForKey:kVolume];
    self.mySoundPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL:soundurl error:nil];
    self.mySoundPlayer.volume=volumeValue; //between 0 and 1
    [self.mySoundPlayer prepareToPlay];
    self.mySoundPlayer.numberOfLoops=0; //or more if needed
    [self.mySoundPlayer play];
}

-(void)instructions:(UIView*)view{
    int titlesize;
    CGRect rect;
    if (DEVICE==IPAD) {
        titlesize=32;
        rect=CGRectMake((view.frame.size.width/2)-100,view.frame.size.height-200 , 200, 80);
    }
    else{
        titlesize=14;
        rect=CGRectMake((view.frame.size.width/2)-50,view.frame.size.height-200 , 100, 40);
    }
    UIImageView *IntroImgage=[[UIImageView alloc]init];
    introview=[[UIView alloc]initWithFrame:CGRectMake(0, 64, view.frame.size.width, view.frame.size.height)];
    IntroImgage.frame=CGRectMake(0, 0, introview.frame.size.width, introview.frame.size.height);
    IntroImgage.image=[UIImage imageNamed:@"change_ratop_instruction"];
    UIButton *gotItbutton=[[UIButton alloc]initWithFrame:rect];
    gotItbutton.backgroundColor=RGB(230,123,53);
    gotItbutton.layer.cornerRadius=5;
    [gotItbutton setTitle:@"Got it" forState:UIControlStateNormal];
    gotItbutton.titleLabel.font = [UIFont systemFontOfSize:titlesize];
    [gotItbutton addTarget:self
                    action:@selector(myAction)
          forControlEvents:UIControlEventTouchUpInside];
    
    [introview addSubview:IntroImgage];
    [introview addSubview:gotItbutton];
    [view addSubview:introview];
    
    
}
-(void)myAction{
    introview.hidden=YES;
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:Introduction];
}

-(void)tableintro:(UITableView*)view{
    int titlesize;
    CGRect rect;
    if (DEVICE==IPAD) {
        titlesize=32;
        rect=CGRectMake((view.frame.size.width/2)-100,view.frame.size.height-200 , 200, 80);
    }
    else{
        titlesize=14;
        rect=CGRectMake((view.frame.size.width/2)-50,view.frame.size.height-200 , 100, 40);
    }
    UIImageView *IntroImgage=[[UIImageView alloc]init];
    introview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    IntroImgage.frame=CGRectMake(0, 0, introview.frame.size.width, introview.frame.size.height);
    IntroImgage.image=[UIImage imageNamed:@"instruction_cate"];
    UIButton *gotItbutton=[[UIButton alloc]initWithFrame:rect];
    gotItbutton.backgroundColor=RGB(230,123,53);
    gotItbutton.layer.cornerRadius=5;
    [gotItbutton setTitle:@"Got it" forState:UIControlStateNormal];
    gotItbutton.titleLabel.font = [UIFont systemFontOfSize:titlesize];
    [gotItbutton addTarget:self
                    action:@selector(myAction1)
          forControlEvents:UIControlEventTouchUpInside];
    
    [introview addSubview:IntroImgage];
    [introview addSubview:gotItbutton];
    [view addSubview:introview];

}

-(void)myAction1{
    introview.hidden=YES;
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:tableIntro];
}


-(void)NavigationMethod:(UINavigationController *)navigationController{
    
    NSString *ControllerType=[[NSUserDefaults standardUserDefaults]objectForKey:controllerType];
    UIStoryboard *storyBoard=[[UIStoryboard alloc]init];
    if (DEVICE==IPAD) {
        UIStoryboard *Board = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
        storyBoard=Board;
    }
    else{
        UIStoryboard * Board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        storyBoard=Board;
    }
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction=[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [navigationController.view.layer addAnimation:transition forKey:nil];
    HomeTabelVC *home=[storyBoard instantiateViewControllerWithIdentifier:@"home"];
    ProblemListViewController *ProblumList=[storyBoard instantiateViewControllerWithIdentifier:@"problumlist"];
    ProblemCatagoriVC *problumCatagori=[storyBoard instantiateViewControllerWithIdentifier:@"problumcatagori"];
    YogasanListVC *Yogasanlist=[storyBoard instantiateViewControllerWithIdentifier:@"nextstart"];
    PranayamaTableVC *pranayamaList=[storyBoard instantiateViewControllerWithIdentifier:@"pranayamatable"];
    Nexttablevc *pranayamalist=[storyBoard instantiateViewControllerWithIdentifier:@"pranayamatbl"];
    YogasanaViewController *yogasan=[storyBoard instantiateViewControllerWithIdentifier:@"yogasanatbl"];
    DincharyaVC *dincharya = [storyBoard instantiateViewControllerWithIdentifier:@"dincharya"];
    DincharyaNameVC *din_name = [storyBoard instantiateViewControllerWithIdentifier:@"dincharya_name"];
    Dincharya_pranaVC *din_prana = [storyBoard instantiateViewControllerWithIdentifier:@"dincharya_prana"];
    Dincharya_yogaVC *din_yoga = [storyBoard instantiateViewControllerWithIdentifier:@"dincharya_yoga"];
    if ([ControllerType isEqualToString:@"Home"]) {
        [navigationController setViewControllers:@[home] animated:YES];
    }
    else if ([ControllerType isEqualToString:@"Nexttable"]){
        [navigationController setViewControllers:@[home,pranayamalist] animated:YES];
    }
    else if ([ControllerType isEqualToString:@"Yogasanatable"]){
        [navigationController setViewControllers:@[home,yogasan] animated:YES];
    }
    else if ([ControllerType isEqualToString:@"Habbitlist"]){
        [navigationController setViewControllers:@[home,ProblumList] animated:YES];
    }
    else if ([ControllerType isEqualToString:@"Habbitcatagori"]){
        [navigationController setViewControllers:@[home,ProblumList,problumCatagori] animated:YES];
    }
    else if ([ControllerType isEqualToString:@"YogasanaList"]){
        [navigationController setViewControllers:@[home,ProblumList,problumCatagori,Yogasanlist] animated:YES];
    }
    else if ([ControllerType isEqualToString:@"PranayamaList"]){
        [navigationController setViewControllers:@[home,ProblumList,problumCatagori,pranayamaList] animated:YES];
    }
    else if ([ControllerType isEqualToString:@"Dincharya"]){
        [navigationController setViewControllers:@[home,dincharya] animated:YES];
    }
    else if ([ControllerType isEqualToString:@"Dincharya Name"]){
        [navigationController setViewControllers:@[home,dincharya,din_name] animated:YES];
    }
    else if ([ControllerType isEqualToString:@"Dincharya Yoga"]){
        [navigationController setViewControllers:@[home,dincharya,din_name,din_yoga] animated:YES];
    }
    else if ([ControllerType isEqualToString:@"Dincharya Prana"]){
        [navigationController setViewControllers:@[home,dincharya,din_name,din_prana] animated:YES];
    }
}

-(void)becomeActiveMethod:(UINavigationController *)navigationController{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:delegateCondition]==YES) {
        UIStoryboard *storyBoard=[[UIStoryboard alloc]init];
        if (DEVICE==IPAD) {
            UIStoryboard *Board = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
            storyBoard=Board;
        }
        else{
            UIStoryboard * Board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            storyBoard=Board;
        }
        startViewController * startView = [storyBoard instantiateViewControllerWithIdentifier:@"nextcontroller"];
        
        HomeTabelVC *home=[storyBoard instantiateViewControllerWithIdentifier:@"home"];
        ProblemListViewController *ProblumList=[storyBoard instantiateViewControllerWithIdentifier:@"problumlist"];
        ProblemCatagoriVC *problumCatagori=[storyBoard instantiateViewControllerWithIdentifier:@"problumcatagori"];
        YogasanListVC *Yogasanlist=[storyBoard instantiateViewControllerWithIdentifier:@"nextstart"];
        [navigationController setViewControllers:@[home,ProblumList,problumCatagori,Yogasanlist,startView]];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:commonClassCond];
    }
}
-(void)becomeActivewithWater:(UINavigationController *)navigationController{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:waterdelegate]==YES) {
        
        UIStoryboard *storyBoard=[[UIStoryboard alloc]init];
        if (DEVICE==IPAD) {
            UIStoryboard *Board = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
            storyBoard=Board;
        }
        else{
            UIStoryboard * Board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            storyBoard=Board;
        }
        
        HomeTabelVC *home=[storyBoard instantiateViewControllerWithIdentifier:@"home"];
        ProblemListViewController *ProblumList=[storyBoard instantiateViewControllerWithIdentifier:@"problumlist"];
        WaterIntakeVC *waterIntake=[storyBoard instantiateViewControllerWithIdentifier:@"waterintake"];
        [navigationController setViewControllers:@[home,ProblumList,waterIntake]];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:commonClassCond];
    }
}

-(void)becomeActiveWithDincharya:(UINavigationController *)navigatioonController{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaDelegate]==YES) {
        NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaName];
        NSArray *ar = [kAppDele FetchDincharyaFromDatabaseAcordingToName:str];
        Dincharya *dincharya = [ar objectAtIndex:0];
        
        NSArray *yogasanaList = [NSKeyedUnarchiver unarchiveObjectWithData:dincharya.yogasana_arr];
        NSArray *PranaList = [NSKeyedUnarchiver unarchiveObjectWithData:dincharya.pranayama_arr];
        [[NSUserDefaults standardUserDefaults] setObject: yogasanaList forKey:dincharyaYogaAr];
        [[NSUserDefaults standardUserDefaults] setObject:PranaList forKey:dincharyaPranaAr];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSUserDefaults standardUserDefaults] setInteger:[dincharya.autometicTime intValue] forKey:dincharyaDelayTime];
        NSMutableArray *PranaIdAr = [[NSMutableArray alloc]init];
        if (yogasanaList.count>0) {
            NSLog(@"%@",[yogasanaList objectAtIndex:0]);
            [[NSUserDefaults standardUserDefaults]setObject:[yogasanaList objectAtIndex:0] forKey:YogasanCondition];
            [[NSUserDefaults standardUserDefaults] setObject: yogasanaList forKey:dincharyaYogaAr];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
        if (PranaList.count>0) {
            for (int i=0 ; i<PranaList.count; i++) {
                
                if ([[PranaList objectAtIndex:i] isEqualToString:@"AnulomVilom"]) {
                    [PranaIdAr addObject:@"anulom"];
                }
                if ([[PranaList objectAtIndex:i] isEqualToString:@"Kapalbhati"]) {
                    [PranaIdAr addObject:@"kapal"];
                }
                if ([[PranaList objectAtIndex:i] isEqualToString:@"Bhramari" ]){
                    [PranaIdAr addObject:@"bhramari"];
                }
                if ([[PranaList objectAtIndex:i] isEqualToString:@"Surya Bhedana"]) {
                    [PranaIdAr addObject:@"surya"];
                }
                if ([[PranaList objectAtIndex:i] isEqualToString:@"Chandra Bhedana"]) {
                    [PranaIdAr addObject:@"chandra"];
                }
                if ([[PranaList objectAtIndex:i] isEqualToString:@"Bhastrika"]) {
                    [PranaIdAr addObject:@"bhastrika"];
                }
                if ([[PranaList objectAtIndex:i] isEqualToString:@"Sheetali"]) {
                    [PranaIdAr addObject:@"sheetali"];
                }
                if ([[PranaList objectAtIndex:i] isEqualToString:@"Ujjayi"]) {
                    [PranaIdAr addObject:@"ujjayi"];
                }
                if ([[PranaList objectAtIndex:i] isEqualToString:@"Meditative Breathing"]) {
                    [PranaIdAr addObject:@"meditative"];
                }
                if ([[PranaList objectAtIndex:i] isEqualToString:@"Udgeeth"]) {
                    [PranaIdAr addObject:@"udgeeth"];
                }
                if ([[PranaList objectAtIndex:i] isEqualToString:@"Bahya"]) {
                    [PranaIdAr addObject:@"bahya"];
                }
            }
            NSArray *Arr = [[NSArray alloc]init];
            Arr=[PranaIdAr copy];
            [[NSUserDefaults standardUserDefaults]setObject:Arr forKey:dincharyaPranaAr];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        
        ///////////  Move to Pranayama or Yogasana ////
        UIStoryboard *storyBoard=[[UIStoryboard alloc]init];
        if (DEVICE==IPAD) {
            UIStoryboard *Board = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
            storyBoard=Board;
        }
        else{
            UIStoryboard * Board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            storyBoard=Board;
        }
        
        HomeTabelVC *home=[storyBoard instantiateViewControllerWithIdentifier:@"home"];
        DincharyaVC *dincharyavc=[storyBoard instantiateViewControllerWithIdentifier:@"dincharya"];
        DincharyaNameVC *dinName=[storyBoard instantiateViewControllerWithIdentifier:@"dincharya_name"];
        if (yogasanaList.count>0) {
            Dincharya_yogaVC *yoga = [storyBoard instantiateViewControllerWithIdentifier:@"dincharya_yoga"];
            startViewController *start = [storyBoard instantiateViewControllerWithIdentifier:@"nextcontroller"];
            [navigatioonController setViewControllers:@[home,dincharyavc,dinName,yoga,start]];
        }
        else{
            Dincharya_pranaVC *prana = [storyBoard instantiateViewControllerWithIdentifier:@"dincharya_prana"];
            NSArray *ar = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaPranaAr];
            UIViewController *VC = [storyBoard instantiateViewControllerWithIdentifier:[ar objectAtIndex:0]];
            [navigatioonController setViewControllers:@[home,dincharyavc,dinName,prana,VC]];
        }
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:commonClassCond];
    }
}


-(void)insertintoProgressReport:(BOOL)value durationValue:(int)durationCount isudgeeth:(BOOL)IsUdgeeth{
    ////progress Report ///////////
    NSManagedObjectContext* managedObjectContext=[[NSManagedObjectContext alloc]init];
    managedObjectContext=[kAppDele managedObjectContext];
    NSString *habbitconditon=[[NSUserDefaults standardUserDefaults]objectForKey:HabbitCondition];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:HabbitMainCondition]) {
        NSArray *Progressarray=[kAppDele fetchReportFromProgressDatabseAccordingToType:habbitconditon];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"Progress" inManagedObjectContext:managedObjectContext]];
        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"progressType==%@ AND days==%d",habbitconditon,Progressarray.count];
        [request setPredicate:predicate];
        
        NSError *error1=nil;
        NSArray *results=[managedObjectContext executeFetchRequest:request error:&error1];
        Progress *entity=[results objectAtIndex:0];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:viewwillCondition];
        if (value==YES) {
            NSNumber *duration=[NSNumber numberWithInt:([entity.duration intValue]+durationCount)];
            entity.duration=duration;
        }
        else{
            if (IsUdgeeth) {
                entity.udgeeth=[NSNumber numberWithBool:YES];
            }
            else{
                entity.bahya=[NSNumber numberWithBool:YES];
            }
            NSNumber *duration=[NSNumber numberWithInt:[entity.duration intValue]+durationCount];
            entity.duration=duration;
        }
        [managedObjectContext save:nil];
    }
}


- (void)appDidBecomeActive:(NSNotification *)notification navigation:(UINavigationController*)navigation{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:waterdelegate]) {
        [[CommonSounds sharedInstance] becomeActivewithWater:navigation];
    }
    else if([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaDelegate]) {
        [[CommonSounds sharedInstance] becomeActiveWithDincharya:navigation];
    }
    else{
        [[CommonSounds sharedInstance] becomeActiveMethod:navigation];
    }
}


-(void)Timecompare:(BOOL)value managedObject:(NSManagedObjectContext*)managedObjectContext pranayama:(NSString*)Prana{
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    NSString *start;
    NSInteger count = 0;
    NSString *name = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaName];
    NSArray *ar = [kAppDele FetchDincharyaReportFromDatabaseAcordingToName:name];
    if (ar.count>0) {
        count=ar.count;
        DincharyaReport *Dreport = [ar objectAtIndex:0];
        start = [f stringFromDate:Dreport.date];
    }
    else{
        start = [f stringFromDate: [NSDate date]];
    }
    NSString *end=[f stringFromDate:[NSDate date]];
    NSDate *startDate = [f dateFromString:start];
    NSDate *endDate = [f dateFromString:end];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    NSInteger numberOfDays=[components day];
    
    NSString *todayStr = [f stringFromDate:[NSDate date]];
    if ([start isEqualToString:todayStr] && count>0) {
        [self UpdateDincharyaIntoDatabase:value mangedObject:managedObjectContext Prana:Prana];
    }
    else{
        for (NSInteger a=numberOfDays; a>1; a--) {
            [self addDincharayIntoDatabase:NO managedObject:managedObjectContext pranayama:Prana];
        }
        [self addDincharayIntoDatabase:value managedObject:managedObjectContext pranayama:Prana];
    }
}


-(void)addDincharayIntoDatabase:(BOOL)value managedObject:(NSManagedObjectContext*)managedObjectContext pranayama:(NSString*)Prana{
    
    NSString *name = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaName];
    
    NSArray *Darray = [kAppDele FetchDincharyaReportFromDatabaseAcordingToName:name];
    DincharyaReport *Dreport=[NSEntityDescription insertNewObjectForEntityForName:@"DincharyaReport" inManagedObjectContext:managedObjectContext];
    Dreport.dincharya_name=name;
    Dreport = [self CheckPranayama:Dreport isYes:value Pranayama:Prana];
    if (Darray.count>0) {
        DincharyaReport *dinch = [Darray objectAtIndex:0];
        NSDate *date = [dinch.date dateByAddingTimeInterval:60*60*24*1];
        Dreport.date=date;
    }
    else{
        Dreport.date=[NSDate date];
    }
    NSError *error1;
    if (![managedObjectContext save:&error1]) {
        NSLog(@"%@",error1);
    }
}



-(void)UpdateDincharyaIntoDatabase:(BOOL)value mangedObject:(NSManagedObjectContext*)managedObjectContext Prana:(NSString*)Prana{
    
    NSString *name = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaName];
//    NSArray *pranayamaList = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaPranaAr];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"DincharyaReport" inManagedObjectContext:managedObjectContext]];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"dincharya_name==%@",name];
    [request setPredicate:predicate];
    
    NSError *error1=nil;
    NSArray *results=[managedObjectContext executeFetchRequest:request error:&error1];
    
    DincharyaReport *Dreport=[results objectAtIndex:results.count-1];
    
    Dreport = [self CheckPranayama:Dreport isYes:value Pranayama:Prana];
    
    [managedObjectContext save:nil];
    
}

-(DincharyaReport*)CheckPranayama:(DincharyaReport*)Dreport isYes:(BOOL)Value Pranayama:(NSString*)Prana{
    
    if ([Prana isEqualToString:@"AnulomVilom"]) {
        Dreport.anulomvilom = [NSNumber numberWithBool:Value];
    }
    if ([Prana isEqualToString:@"Kapalbhati"]) {
        Dreport.kapalbhati = [NSNumber numberWithBool:Value];
    }
    if ([Prana isEqualToString:@"Bhramari"]) {
        Dreport.bhramari = [NSNumber numberWithBool:Value];
    }
    
    if ([Prana isEqualToString:@"Surya Bhedana"]) {
        Dreport.surya = [NSNumber numberWithBool:Value];
    }
    if ([Prana isEqualToString:@"Chandra Bhedana"]) {
        Dreport.chandra = [NSNumber numberWithBool:Value];
    }
    if ([Prana isEqualToString:@"Bhastrika"]) {
        Dreport.bhastrika = [NSNumber numberWithBool:Value];
    }
    if ([Prana isEqualToString:@"Sheetali"]) {
        Dreport.sheetali = [NSNumber numberWithBool:Value];
    }
    if ([Prana isEqualToString:@"Ujjayi"]) {
        Dreport.ujjayi = [NSNumber numberWithBool:Value];
    }
    if ([Prana isEqualToString:@"Meditative Breathing"]) {
        Dreport.meditative = [NSNumber numberWithBool:Value];
    }
    if ([Prana isEqualToString:@"Udgeeth"]) {
        Dreport.udgeeth = [NSNumber numberWithBool:Value];
    }
    if ([Prana isEqualToString:@"Bahya"]) {
        Dreport.bahya = [NSNumber numberWithBool:Value];
    }
    return Dreport;
}

-(void)CommonAlertView:(UINavigationController *)navigatioonController{

    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:DincharyaMainCon];

    UIStoryboard *storyBoard = [[UIStoryboard alloc]init];
    if (DEVICE==IPAD) {
        UIStoryboard *Board = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
        storyBoard=Board;
    }
    else{
        UIStoryboard * Board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        storyBoard=Board;
    }
    HomeTabelVC *home=[storyBoard instantiateViewControllerWithIdentifier:@"home"];
    DincharyaVC *dincharyavc=[storyBoard instantiateViewControllerWithIdentifier:@"dincharya"];
    DincharyaNameVC *dinName=[storyBoard instantiateViewControllerWithIdentifier:@"dincharya_name"];
    Dincharya_ProgressVC *progress = [storyBoard instantiateViewControllerWithIdentifier:@"dincharya_progress"];
    [navigatioonController setViewControllers:@[home,dincharyavc,dinName,progress]];
}


-(void)CheckStartController:(UINavigationController *)navigarionController Identifier:(NSString *)identifier{
    UIStoryboard *storyBoard = [[UIStoryboard alloc]init];
    if (DEVICE==IPAD) {
        UIStoryboard *Board = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
        storyBoard=Board;
    }
    else{
        UIStoryboard * Board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        storyBoard=Board;
    }

    HomeTabelVC *home=[storyBoard instantiateViewControllerWithIdentifier:@"home"];
    DincharyaVC *dincharyavc=[storyBoard instantiateViewControllerWithIdentifier:@"dincharya"];
    DincharyaNameVC *dinName=[storyBoard instantiateViewControllerWithIdentifier:@"dincharya_name"];
    Dincharya_pranaVC *prana = [storyBoard instantiateViewControllerWithIdentifier:@"dincharya_prana"];
    UIViewController *VC = [storyBoard instantiateViewControllerWithIdentifier:@"anulom"];
    NSArray *ar = @[home,dincharyavc,dinName,prana,VC];
    navigarionController.viewControllers = ar;
    
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:DincharyaDelegate];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:commonClassCond];
}




@end

//
//  benifitViewController.m
//  Pranayama
//
//  Created by Manish Kumar on 6/14/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "PrecautionsVC.h"

@interface PrecautionsVC ()

@end

@implementation PrecautionsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.screenName=@"Precaution";
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView1.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc]init];
    label1.font=Ralewayfont(18);
    label2.font = Ralewayfont(14);
    label1.text=[MCLocalization stringForKey:@"yogasana_precaution"];
    
    NSString *Yogasana=[[NSUserDefaults standardUserDefaults] objectForKey:YogasanCondition];
    if ([Yogasana isEqualToString:@"Sarvangasana"]) {
        
        label2.text=[MCLocalization stringForKey:@"saravagsana_precaution"];
    }
    else if ([Yogasana isEqualToString:@"Halasana"]){
        label2.text=[MCLocalization stringForKey:@"halasana_precaution"];
    }
    else if ([Yogasana isEqualToString:@"Vipritkarani"]){
        label2.text=[MCLocalization stringForKey:@"vipritkarni_precaution"];
    }
    else if ([Yogasana isEqualToString:@"Paschimottanasana"]){
        label2.text=[MCLocalization stringForKey:@"pas_precaution"];
    }
    else if ([Yogasana isEqualToString:@"Dhanurasana"]){
        label2.text=[MCLocalization stringForKey:@"dhanu_precaution"];
    }
    else if ([Yogasana isEqualToString:@"Balasana"]){
       label2.text=[MCLocalization stringForKey:@"bala_precaution"];
    }
    else if ([Yogasana isEqualToString:@"Hastapadasana"]){
        label2.text=[MCLocalization stringForKey:@"hast_precaution"];
    }
    else if ([Yogasana isEqualToString:@"Marjariasana"])
    {
        label2.text=[MCLocalization stringForKey:@"marj_precaution"];
    }
    else if ([Yogasana isEqualToString:@"Uttanasana"]){
        label2.text=[MCLocalization stringForKey:@"Uttanasana_precaution"];
    }
    else if ([Yogasana isEqualToString:@"Setu Bandhasana"])
    {
        label2.text=[MCLocalization stringForKey:@"Setu_precaution"];
    }
    else{
        label2.text=[MCLocalization stringForKey:@"Virabhadrasana_precaution"];
    }
    
    label1.numberOfLines=100;
    [label1 sizeToFit];
    label1.textColor=[UIColor redColor];
    [myScrollView1 addSubview:label1];
    
    label2.frame=CGRectMake(15, label1.frame.origin.y+label1.frame.size.height+10, myScrollView1.frame.size.width-25, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [myScrollView1 addSubview:label2];
    
    myScrollView1.contentSize=CGSizeMake(myScrollView1.frame.size.width,label2.frame.origin.y+label2.frame.size.height+20);

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

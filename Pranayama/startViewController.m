//
//  startViewController.m
//  Pranayama
//
//  Created by Manish Kumar on 6/15/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "startViewController.h"
#import "StepsViewController.h"
#import "BenefitsVC.h"
#import "PrecautionsVC.h"
#import "Pranayama-Swift.h"


@interface startViewController () <YSLContainerViewControllerDelegate>

@end

@implementation startViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:HabbitMainCondition] && [[NSUserDefaults standardUserDefaults]boolForKey:delegateCondition] && ![[NSUserDefaults standardUserDefaults] boolForKey:commonClassCond]) {
        
        startViewController * startView = [self.storyboard instantiateViewControllerWithIdentifier:@"nextcontroller"];
        HomeTabelVC *home=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
        ProblemListViewController *ProblumList=[self.storyboard instantiateViewControllerWithIdentifier:@"problumlist"];
        ProblemCatagoriVC *problumCatagori=[self.storyboard instantiateViewControllerWithIdentifier:@"problumcatagori"];
        YogasanListVC *Yogasanlist=[self.storyboard instantiateViewControllerWithIdentifier:@"nextstart"];
        
        [self.navigationController setViewControllers:@[home,ProblumList,problumCatagori,Yogasanlist,startView] animated:YES];
    }
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaMainCon] && [[NSUserDefaults standardUserDefaults]boolForKey:DincharyaDelegate] && [[NSUserDefaults standardUserDefaults] boolForKey:commonClassCond]) {
        [self performSelector:@selector(navigationMove) withObject:nil afterDelay:0.5];
        }
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:commonClassCond];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:delegateCondition];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:DincharyaDelegate];
    
    if (DEVICE==IPAD) {
        fontSize=24;
        TitleFontsize=18;
        if (self.view.frame.size.height==1366) {
            FbutonSize=80;
        }
        else{
            FbutonSize=60;
        }
    }
    else{
        TitleFontsize=13;
        fontSize=18;
        FbutonSize=50;
    }

    self.managedObjectContext=[kAppDele managedObjectContext];
    [self setTitleAndContent];
}


-(void)navigationMove{
    startViewController * startView = [self.storyboard instantiateViewControllerWithIdentifier:@"nextcontroller"];
    HomeTabelVC *home=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
    DincharyaVC *dincharyavc=[self.storyboard instantiateViewControllerWithIdentifier:@"dincharya"];
    DincharyaNameVC *dinName=[self.storyboard instantiateViewControllerWithIdentifier:@"dincharya_name"];
    Dincharya_yogaVC *yoga = [self.storyboard instantiateViewControllerWithIdentifier:@"dincharya_yoga"];
    
    [self.navigationController setViewControllers:@[home,dincharyavc,dinName,yoga,startView] animated:NO];
}


- (void)HomeMethod:(NSNotification *)notification {
    [[CommonSounds sharedInstance] appDidBecomeActive:notification navigation:self.navigationController];
}



-(void)setTitleAndContent{
    NSString *Yogasana=[[NSUserDefaults standardUserDefaults] objectForKey:YogasanCondition];
    NSString *navigationTitle;
    if ([Yogasana isEqualToString:@"Sarvangasana"]) {
        navigationTitle=@"Sarvangasana";
//        [[NSUserDefaults standardUserDefaults] setObject:@"Thyroid" forKey:HabbitType];
    }
    else if ([Yogasana isEqualToString:@"Halasana"]){
        navigationTitle=@"Halasana";
    }
    else if ([Yogasana isEqualToString:@"Vipritkarani"]){
        navigationTitle=@"Vipritkarani";
        //[[NSUserDefaults standardUserDefaults] setObject:@"Insomnia" forKey:HabbitType];
    }
    else if ([Yogasana isEqualToString:@"Paschimottanasana"]){
        navigationTitle=@"Paschimottanasana";
    }
    else if ([Yogasana isEqualToString:@"Dhanurasana"]){
        navigationTitle=@"Dhanurasana";
        //[[NSUserDefaults standardUserDefaults] setObject:@"Diabetes" forKey:HabbitType];
    }
    else if ([Yogasana isEqualToString:@"Balasana"]){
        navigationTitle=@"Balasana";
    }
    else if ([Yogasana isEqualToString:@"Hastapadasana"]){
        navigationTitle=@"Hastapadasana";
        //[[NSUserDefaults standardUserDefaults] setObject:@"Migraine" forKey:HabbitType];
    }
    else if ([Yogasana isEqualToString:@"Marjariasana"]){
        navigationTitle=@"Marjariasana";
    }
    else if ([Yogasana isEqualToString:@"Uttanasana"]){
        navigationTitle=@"Uttanasana";
    }
    else if ([Yogasana isEqualToString:@"Setu Bandhasana"])
    {
        navigationTitle=@"Setu Bandhasana";
        
    }
    else{
        navigationTitle=@"Virabhadrasana";
    }
  
    
       [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"McLaren-Regular" size:fontSize]}];
    
    // SetUp ViewControllers
    UIStoryboard *storyBoard=[[UIStoryboard alloc]init];
    if (DEVICE==IPAD) {
        UIStoryboard *Board = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
        storyBoard=Board;
    }
    else{
        UIStoryboard * Board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        storyBoard=Board;
    }
    StepsViewController *stepsview = [storyBoard instantiateViewControllerWithIdentifier:@"detailview"];
    
    BenefitsVC *benefitview = [storyBoard instantiateViewControllerWithIdentifier:@"stepview"];
    
    PrecautionsVC *precautionsview = [storyBoard instantiateViewControllerWithIdentifier:@"benifitview"];
    
    NSString *lan=[[NSUserDefaults standardUserDefaults] objectForKey:Languagevalue];
    if ([lan isEqualToString:Hindi]) {
        self.navigationItem.title=[MCLocalization stringForKey:navigationTitle];
        stepsview.title=[MCLocalization stringForKey:@"Steps"];
        benefitview.title=[MCLocalization stringForKey:@"benefit"];
        precautionsview.title=[MCLocalization stringForKey:@"Precaution"];
        yes=[MCLocalization stringForKey:@"ha"];
        no=[MCLocalization stringForKey:@"nahi"];
        taskcompletion=[MCLocalization stringForKey:@"taskcompletion"];
        forday=[MCLocalization stringForKey:@"forday"];
        yogasana=[MCLocalization stringForKey:Yogasana];
    }
    else if([lan isEqualToString:English]){
        self.navigationItem.title=navigationTitle;
        stepsview.title = @"STEPS";
        benefitview.title = @"BENEFITS";
        precautionsview.title= @"PRECAUTIONS";
        yes=@"Yes";
        no=@"No";
        taskcompletion=@"Have you completed your ";
        forday=@" task for the day?";
        yogasana=Yogasana;
    }
    else{
        self.navigationItem.title=[MCLocalization stringForKey:navigationTitle];
        stepsview.title=[MCLocalization stringForKey:@"Steps"];
        benefitview.title=[MCLocalization stringForKey:@"benefit"];
        precautionsview.title=[MCLocalization stringForKey:@"Precaution"];
        yes=[MCLocalization stringForKey:@"ha"];
        no=[MCLocalization stringForKey:@"nahi"];
        taskcompletion=[MCLocalization stringForKey:@"taskcompletion"];
        forday=[MCLocalization stringForKey:@"forday"];
        yogasana=[MCLocalization stringForKey:Yogasana];
    }
    
    // ContainerView
    float statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    float navigationHeight = self.navigationController.navigationBar.frame.size.height;
    
    self.containerVC = [[YSLContainerViewController alloc]initWithControllers:@[stepsview,benefitview,precautionsview]
                                                                 topBarHeight:statusHeight + navigationHeight
                                                         parentViewController:self];
    self. containerVC.delegate = self;
    self .containerVC.menuItemFont = [UIFont fontWithName:@"Futura-Medium" size:TitleFontsize];
    
    [self.view addSubview:self.containerVC.view];
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:HabbitMainCondition] || [[NSUserDefaults standardUserDefaults]boolForKey:DincharyaMainCon]) {
        [self floatingButton];
    }
}

-(void)floatingButton{
    UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width-FbutonSize-20, self.view.frame.size.height-FbutonSize-20, FbutonSize , FbutonSize)];
    button.backgroundColor=RGB(143, 0, 0);
    [button setTitle:@"OK" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button addTarget:nil action:@selector(buttonMethod) forControlEvents:UIControlEventTouchUpInside];
    button.layer.cornerRadius=button.bounds.size.width/2;
    [self.containerVC.view addSubview:button];
}

-(void)buttonMethod{
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaMainCon]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaName];
        NSArray *ar = [kAppDele FetchDincharyaFromDatabaseAcordingToName:str];
        Dincharya *dincharya = [ar objectAtIndex:0];
        NSArray *yogasanaMainList = [NSKeyedUnarchiver unarchiveObjectWithData:dincharya.yogasana_arr];
        NSArray *yogasanList = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaYogaAr];
        
        NSString *yogasan = [[NSUserDefaults standardUserDefaults]objectForKey:YogasanCondition];
        NSString *msg = [NSString stringWithFormat:@"Have you completed your %@ task for the day?",yogasan];
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Task"
                                     message:msg
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:yes
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        if (yogasanaMainList.count == yogasanList.count) {
                                            [self timecompare:YES];
                                            [self MoveToNext_DincharyaList];
                                        }
                                        else{
                                            [self UpdateDincharyaIntoDatabase:YES];
                                            [self MoveToNext_DincharyaList];
                                        }
                                        
        }];
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:no
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       if (yogasanaMainList.count == yogasanList.count) {
                                           [self timecompare:NO];
                                           [self MoveToNext_DincharyaList];
                                       }
                                       else{
                                           [self UpdateDincharyaIntoDatabase:NO];
                                           [self MoveToNext_DincharyaList];
                                       }

                                   }];
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        
        NSString *Yogasana=[[NSUserDefaults standardUserDefaults] objectForKey:YogasanCondition];
        NSString *habbitType=[[NSUserDefaults standardUserDefaults] objectForKey:HabbitType];
        NSString *message=[NSString stringWithFormat:@"%@%@%@",taskcompletion,yogasana,forday];
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:message
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:yes
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        if (
                                            ([Yogasana isEqualToString:@"Sarvangasana"] && (([habbitType isEqualToString:@"Thyroid"]) || [habbitType isEqualToString:@"Weight Loss"])) ||
                                            ([Yogasana isEqualToString:@"Vipritkarani"] && [habbitType isEqualToString:@"Insomnia"]) ||
                                            ([Yogasana isEqualToString:@"Dhanurasana"] && ([habbitType isEqualToString:@"Diabetes"] || [habbitType isEqualToString:@"Asthma"])) ||
                                            ([Yogasana isEqualToString:@"Hastapadasana"] && [habbitType isEqualToString:@"Migraine"]) ||
                                            ([Yogasana isEqualToString:@"Balasana"] && [habbitType isEqualToString:@"Joint Pain"])
                                            )
                                        {
                                            [self timecompare:YES];
                                            [self setYogasan:Yogasana];
                                        }
                                        else {
                                            [self UpdateProgressForYogasana:Yogasana Yes:YES];
                                            [self MovetoPranayama:Yogasana];
                                        }
                                    }];
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:no
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       if (
                                           ([Yogasana isEqualToString:@"Sarvangasana"] && (([habbitType isEqualToString:@"Thyroid"]) || [habbitType isEqualToString:@"Weight Loss"])) ||
                                           ([Yogasana isEqualToString:@"Vipritkarani"] && [habbitType isEqualToString:@"Insomnia"]) ||
                                           ([Yogasana isEqualToString:@"Dhanurasana"] && ([habbitType isEqualToString:@"Diabetes"] || [habbitType isEqualToString:@"Asthma"])) ||
                                           ([Yogasana isEqualToString:@"Hastapadasana"] && [habbitType isEqualToString:@"Migraine"]) ||
                                           ([Yogasana isEqualToString:@"Balasana"] && [habbitType isEqualToString:@"Joint Pain"])
                                           )
                                       {
                                           [self timecompare:NO];
                                           [self setYogasan:Yogasana];
                                       }
                                       else {
                                           [self UpdateProgressForYogasana:Yogasana Yes:NO];
                                           [self MovetoPranayama:Yogasana];
                                       }
                                   }];
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)setYogasan:(NSString *)asan{
    NSString *habbitType=[[NSUserDefaults standardUserDefaults] objectForKey:HabbitType];
    if ([asan isEqualToString:@"Sarvangasana"]) {
        if ([habbitType isEqualToString:@"Thyroid"]) {
            [[NSUserDefaults standardUserDefaults] setObject:@"Halasana" forKey:YogasanCondition];
        }
        else{
            [[NSUserDefaults standardUserDefaults] setObject:@"Setu Bandhasana" forKey:YogasanCondition];
        }
    }
    else if ([asan isEqualToString:@"Vipritkarani"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"Paschimottanasana" forKey:YogasanCondition];
    }
    else if ([asan isEqualToString:@"Dhanurasana"]){
        if ([habbitType isEqualToString:@"Diabetes"]) {
            [[NSUserDefaults standardUserDefaults]setObject:@"Balasana" forKey:YogasanCondition];
        }
        else{
            [[NSUserDefaults standardUserDefaults]setObject:@"Uttanasana" forKey:YogasanCondition];
        }
    }
    else if([asan isEqualToString:@"Hastapadasana"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"Marjariasana" forKey:YogasanCondition];
    }
    else{
        [[NSUserDefaults standardUserDefaults] setObject:@"Virabhadrasana" forKey:YogasanCondition];
    }
    startViewController * startView = [self.storyboard instantiateViewControllerWithIdentifier:@"nextcontroller"];
    
    HomeTabelVC *home=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
    ProblemListViewController *ProblumList=[self.storyboard instantiateViewControllerWithIdentifier:@"problumlist"];
    ProblemCatagoriVC *problumCatagori=[self.storyboard instantiateViewControllerWithIdentifier:@"problumcatagori"];
    YogasanListVC *Yogasanlist=[self.storyboard instantiateViewControllerWithIdentifier:@"nextstart"];
    [self.navigationController setViewControllers:@[home,ProblumList,problumCatagori,Yogasanlist,startView] animated:YES];
}

-(void)MovetoPranayama:(NSString *)asana{
    UIViewController *startView=[[UIViewController alloc]init];
    if ([asana isEqualToString:@"Paschimottanasana"]) {
        BhramariVC *startView1=[self.storyboard instantiateViewControllerWithIdentifier:@"bhramari"];
        startView=startView1;
    }
    else{
        AnulomVilom * startView1 = [self.storyboard instantiateViewControllerWithIdentifier:@"anulom"];
        startView=startView1;
    }
    HomeTabelVC *home=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
    ProblemListViewController *ProblumList=[self.storyboard instantiateViewControllerWithIdentifier:@"problumlist"];
    ProblemCatagoriVC *problumCatagori=[self.storyboard instantiateViewControllerWithIdentifier:@"problumcatagori"];
    PranayamaTableVC *pranayamatbl=[self.storyboard instantiateViewControllerWithIdentifier:@"pranayamatable"];
    [self.navigationController setViewControllers:@[home,ProblumList,problumCatagori,pranayamatbl,startView] animated:YES];
}


-(void)timecompare:(BOOL)value{

    
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    NSString *start;
    NSInteger count=0;
    if ([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaMainCon]) {
        NSString *name = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaName];
        NSArray *ar = [kAppDele FetchDincharyaReportFromDatabaseAcordingToName:name];
        if (ar.count>0) {
            count=ar.count;
            DincharyaReport *Dreport = [ar objectAtIndex:0];
            start = [f stringFromDate:Dreport.date];
        }
        else{
            start = [f stringFromDate: [NSDate date]];
        }
    }
    else{
        start=[f stringFromDate:[[NSUserDefaults standardUserDefaults] objectForKey:conditionalDate]];
    }
    NSString *end=[f stringFromDate:[NSDate date]];
    NSDate *startDate = [f dateFromString:start];
    NSDate *endDate = [f dateFromString:end];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    NSInteger numberOfDays=[components day];
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaMainCon]) {
        NSString *todayStr = [f stringFromDate:[NSDate date]];
        if ([start isEqualToString:todayStr] && count>0) {
            [self UpdateDincharyaIntoDatabase:value];
        }
        else{
            for (NSInteger a=numberOfDays; a>1; a--) {
                [self addDincharayIntoDatabase:NO];
            }
            [self addDincharayIntoDatabase:value];
            NSArray *yogasanList = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaYogaAr];
            NSMutableArray *arr = [[NSMutableArray alloc]init];
            arr = [yogasanList mutableCopy];
            [arr removeObjectAtIndex:0];
            NSArray *Yarray = [arr copy];
            [[NSUserDefaults standardUserDefaults]setObject:Yarray forKey:dincharyaYogaAr];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    else{
        for (NSInteger a=numberOfDays; a>1; a--) {
            [self InsertIntoDatabase:NO];
        }
        [self InsertIntoDatabase:value];
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:conditionalDate];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self disableSlidePanGestureForRightMenu];
}

/////////////For sarvangsana//////////
-(void)InsertIntoDatabase:(BOOL)value{
    Progress *entity=[NSEntityDescription insertNewObjectForEntityForName:@"Progress" inManagedObjectContext:self.managedObjectContext];
    NSString *Problume=[[NSUserDefaults standardUserDefaults] objectForKey:HabbitCondition];
    NSArray *Progressarray=[kAppDele fetchReportFromProgressDatabseAccordingToType:Problume];
    NSNumber *day=[NSNumber numberWithInteger:Progressarray.count+1];
    entity.days=day;
    if ([Problume isEqualToString:@"Thyroid"] || [Problume isEqualToString:@"Weight Loss"]) {
        entity.sarvangasana=[NSNumber numberWithBool:value];
    }
    else if ([Problume isEqualToString:@"Insomnia"]){
        entity.vipritkarani=[NSNumber numberWithBool:value];
    }
    else if ([Problume isEqualToString:@"Diabetes"] || [Problume isEqualToString:@"Asthma"]){
        entity.dhanurasana=[NSNumber numberWithBool:value];
    }
    else if([Problume isEqualToString:@"Migraine"]){
        entity.hastapadasana=[NSNumber numberWithBool:value];
    }
    else{
        entity.balasana=[NSNumber numberWithBool:value];
    }
    entity.progressType=Problume;
    NSError *error1;
    if (![self.managedObjectContext save:&error1]) {
        NSLog(@"%@",error1);
    }
}
////////

//////////
-(void)UpdateProgressForYogasana:(NSString *)asan Yes:(BOOL)value{
    ////progress Report ///////////
     NSString *habbitType=[[NSUserDefaults standardUserDefaults] objectForKey:HabbitType];
    NSString *habbitconditon=[[NSUserDefaults standardUserDefaults]objectForKey:HabbitCondition];
    if (habbitconditon!=nil) {

        NSArray *Progressarray=[kAppDele fetchReportFromProgressDatabseAccordingToType:habbitconditon];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"Progress" inManagedObjectContext:self.managedObjectContext]];
        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"progressType==%@ AND days==%d",habbitconditon,Progressarray.count];
        [request setPredicate:predicate];
        
        NSError *error1=nil;
        NSArray *results=[self.managedObjectContext executeFetchRequest:request error:&error1];
        
        Progress *entity=[results objectAtIndex:0];
        if ([asan isEqualToString:@"Halasana"] && [habbitType isEqualToString:@"Thyroid"]) {
            entity.halasana=[NSNumber numberWithBool:value];
        }
        else if ([asan isEqualToString:@"Paschimottanasana"] && [habbitType isEqualToString:@"Insomnia"]){
            entity.pashimothanasana=[NSNumber numberWithBool:value];
        }
        else if ([asan isEqualToString:@"Balasana"] && [habbitType isEqualToString:@"Diabetes"]){
            entity.balasana=[NSNumber numberWithBool:value];
        }
        else if([asan isEqualToString:@"Marjariasana"] && [habbitType isEqualToString:@"Migraine"]){
            entity.marjariasana=[NSNumber numberWithBool:value];
        }
        else if ([asan isEqualToString:@"Setu Bandhasana"] && [habbitType isEqualToString:@"Weight Loss"]){
            entity.setubandhasana=[NSNumber numberWithBool:value];
        }
        else if ([asan isEqualToString:@"Uttanasana"] && [habbitType isEqualToString:@"Asthma"]){
            entity.uttanasana=[NSNumber numberWithBool:value];
        }
        else if ([asan isEqualToString:@"Virabhadrasana"] && [habbitType isEqualToString:@"Joint Pain"]){
            entity.virabhadrasana=[NSNumber numberWithBool:value];
        }
        [self.managedObjectContext save:nil];
    }
}

////////////////////  Add Dincharya into Database/////////////

-(void)addDincharayIntoDatabase:(BOOL)value{
    NSString *name = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaName];
    NSArray *yogasanList = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaYogaAr];
    NSArray *Darray = [kAppDele FetchDincharyaReportFromDatabaseAcordingToName:name];
    NSString *yoga = [yogasanList objectAtIndex:0];
    DincharyaReport *Dreport=[NSEntityDescription insertNewObjectForEntityForName:@"DincharyaReport" inManagedObjectContext:self.managedObjectContext];
    Dreport.dincharya_name=name;
    Dreport = [self CheckYogasan:Dreport isYes:value yogasan:yoga];
    if (Darray.count>0) {
        DincharyaReport *dinch = [Darray objectAtIndex:0];
        NSDate *date = [dinch.date dateByAddingTimeInterval:60*60*24*1];
        Dreport.date=date;
    }
    else{
        Dreport.date=[NSDate date];
    }
    
    NSError *error1;
    if (![self.managedObjectContext save:&error1]) {
        NSLog(@"%@",error1);
    }
    
}

-(void)UpdateDincharyaIntoDatabase:(BOOL)value{
    
    NSString *name = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaName];
    NSArray *yogasanList = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaYogaAr];
    NSString *yoga = [yogasanList objectAtIndex:0];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"DincharyaReport" inManagedObjectContext:self.managedObjectContext]];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"dincharya_name==%@",name];
    [request setPredicate:predicate];
    
    NSError *error1=nil;
    NSArray *results=[self.managedObjectContext executeFetchRequest:request error:&error1];
    
    DincharyaReport *Dreport=[results objectAtIndex:results.count-1];
    
    Dreport = [self CheckYogasan:Dreport isYes:value yogasan:yoga];
    
    [self.managedObjectContext save:nil];
    
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    arr = [yogasanList mutableCopy];
    [arr removeObjectAtIndex:0];
    NSArray *Yarray = [arr copy];
    [[NSUserDefaults standardUserDefaults]setObject:Yarray forKey:dincharyaYogaAr];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

-(DincharyaReport*)CheckYogasan:(DincharyaReport*)Dreport isYes:(BOOL)value yogasan:(NSString*)yoga{
    
    
    if ([yoga isEqualToString:@"Sarvangasana"]) {
        Dreport.sarvangasana=[NSNumber numberWithBool:value];
    }
    if ([yoga isEqualToString:@"Halasana"]) {
        Dreport.halasana=[NSNumber numberWithBool:value];
    }
    if ([yoga isEqualToString:@"Vipritkarani"]) {
        Dreport.vipritkarani=[NSNumber numberWithBool:value];
    }
    if ([yoga isEqualToString:@"Paschimottanasana"]) {
        Dreport.paschimottanasana=[NSNumber numberWithBool:value];
    }
    if ([yoga isEqualToString:@"Dhanurasana"]) {
        Dreport.dhanurasana=[NSNumber numberWithBool:value];
    }
    if ([yoga isEqualToString:@"Balasana"]) {
        Dreport.balasana=[NSNumber numberWithBool:value];
    }
    if ([yoga isEqualToString:@"Hastapadasana"]) {
        Dreport.hastapadasana=[NSNumber numberWithBool:value];
    }
    if ([yoga isEqualToString:@"Marjariasana"]) {
        Dreport.marjariasana=[NSNumber numberWithBool:value];
    }
    if ([yoga isEqualToString:@"Uttanasana"]) {
        Dreport.uttanasana=[NSNumber numberWithBool:value];
    }
    if ([yoga isEqualToString:@"Setu Bandhasana"]) {
        Dreport.setu=[NSNumber numberWithBool:value];
    }
    if ([yoga isEqualToString:@"Virabhadrasana"]) {
        Dreport.virabhadrasana=[NSNumber numberWithBool:value];
    }

    return Dreport;
}


-(void)MoveToNext_DincharyaList{
    
   
    NSArray *ar = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaYogaAr];

    startViewController * startView = [self.storyboard instantiateViewControllerWithIdentifier:@"nextcontroller"];
    HomeTabelVC *home=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
    DincharyaVC *dincharyavc=[self.storyboard instantiateViewControllerWithIdentifier:@"dincharya"];
    DincharyaNameVC *dinName=[self.storyboard instantiateViewControllerWithIdentifier:@"dincharya_name"];
    
    if (ar.count>0) {
        NSString *yoga = [ar objectAtIndex:0];
        [[NSUserDefaults standardUserDefaults]setObject:yoga forKey:YogasanCondition];
        Dincharya_yogaVC *yogaVC = [self.storyboard instantiateViewControllerWithIdentifier:@"dincharya_yoga"];
        [self.navigationController setViewControllers:@[home,dincharyavc,dinName,yogaVC,startView] animated:YES];
    }
    else{
        NSArray *pranaAr = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaPranaAr];
        if (pranaAr.count>0) {
            Dincharya_pranaVC *pranaVC = [self.storyboard instantiateViewControllerWithIdentifier:@"dincharya_prana"];
            UIViewController *prana = [self.storyboard instantiateViewControllerWithIdentifier:[pranaAr objectAtIndex:0]];
            [self.navigationController setViewControllers:@[home,dincharyavc,dinName,pranaVC,prana] animated:YES];
        }
        else{
            NSString *msg = [NSString stringWithFormat:@"Your today's fitness regima is over. Come back tomorrow.!!"];
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Session Completed"
                                         message:msg
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"Ok"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            [[CommonSounds sharedInstance] CommonAlertView:self.navigationController];
                                        }];
            [alert addAction:yesButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

#pragma mark -- YSLContainerViewControllerDelegate
- (void)containerViewItemIndex:(NSInteger)index currentController:(UIViewController *)controller
{
    //    NSLog(@"current Index : %ld",(long)index);
    //    NSLog(@"current controller : %@",controller);
    [controller viewWillAppear:YES];
}

-(IBAction)backbutton:(id)sender
{
    [[self navigationController] popViewControllerAnimated: YES];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

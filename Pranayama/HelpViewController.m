//
//  HelpViewController.m
//  Pranayama
//
//  Created by Manish Kumar on 27/07/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "HelpViewController.h"

@interface HelpViewController ()

@end

@implementation HelpViewController
@synthesize Type;
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    // tracker
    self.screenName=@"Sheetali_Help";
    // Do any additional setup after loading the view.
    int fontSize;
    if (DEVICE==IPAD) {
        fontSize=24;
    }
    else{
        fontSize=18;
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    self.automaticallyAdjustsScrollViewInsets = NO;
    if ([Type isEqualToString:@"AnulomVilom"]) {
        [self AnulomContent];
    }
    else if ([Type isEqualToString:@"Kapalbhati"]){
        [self KapalBhatiContent];
    }
    else if ([Type isEqualToString:@"Bhramari"]){
        [self BhramariContent];
    }
    else if ([Type isEqualToString:@"Surya"]){
        [self SuryaContent];
    }
    else if ([Type isEqualToString:@"Chandra"]){
        [self ChandraContent];
    }
    else if ([Type isEqualToString:@"Bhastrika"]){
        [self BhastrikaContent];
    }
    else if ([Type isEqualToString:@"Sheetali"]){
        [self SheetaliContent];
    }
    else if ([Type isEqualToString:@"Ujjayi"]){
        [self UjjayiContent];
    }
    else if ([Type isEqualToString:@"Meditative"])
    {
        [self MeditativeContent];
    }
    else if ([Type isEqualToString:@"Udgeeth"])
    {
        [self udgeethContent];
    }
    else 
    {
        [self bahyaContent];
    }
}

- (void)HomeMethod:(NSNotification *)notification {
    HomeTabelVC *homeObject = [[HomeTabelVC alloc]init];
    [homeObject appDidBecomeActive:notification];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self disableSlidePanGestureForRightMenu];
}

-(void)AnulomContent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc] init];
    UILabel *label3 = [[UILabel alloc] init];
    UILabel *label4 = [[UILabel alloc] init];
    UILabel *label5 = [[UILabel alloc] init];
    UILabel *label6 = [[UILabel alloc] init];
    UILabel *label7 = [[UILabel alloc] init];
    UILabel *label8 = [[UILabel alloc] init];
    UILabel *label9 = [[UILabel alloc] init];
    UILabel *label10 = [[UILabel alloc] init];
    UILabel *label11 = [[UILabel alloc] init];
    UILabel *label12 = [[UILabel alloc] init];
    UILabel *label13 = [[UILabel alloc] init];
    
    NSString *string;
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(14);
    label4.font=Ralewayfont(18);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);
    label8.font=Ralewayfont(14);
    label9.font=Ralewayfont(14);
    label10.font=Ralewayfont(14);
    label11.font=Ralewayfont(14);
    label12.font=Ralewayfont(14);
    label13.font=Ralewayfont(14);
    self.navigationItem.title = [MCLocalization stringForKey:@"help"];
    label1.text=[MCLocalization stringForKey:@"anulom_help1"];
    label2.text=[MCLocalization stringForKey:@"anulom_help2"];
    label3.text=[MCLocalization stringForKey:@"anulom_help3"];
    label4.text=[MCLocalization stringForKey:@"anulom_help4"];
    label5.text=[MCLocalization stringForKey:@"anulom_help5"];
    label6.text=[MCLocalization stringForKey:@"anulom_help6"];
    label7.text=[MCLocalization stringForKey:@"anulom_help7"];
    label8.text=[MCLocalization stringForKey:@"anulom_help8"];
    label9.text=[MCLocalization stringForKey:@"anulom_help9"];
    label10.text=[MCLocalization stringForKey:@"anulom_help10"];
    label11.text=[MCLocalization stringForKey:@"anulom_help11"];
    label12.text=[MCLocalization stringForKey:@"anulom_help12"];
    label13.text=[MCLocalization stringForKey:@"anulom_help13"];
    string=[MCLocalization stringForKey:@"anulom_link"];
    
    label1.numberOfLines=0;
    label1.textColor = UIColor.redColor;
    label4.textColor = UIColor.redColor;
    [label1 sizeToFit];
 [scrollView addSubview:label1];
    label2.frame = CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [scrollView addSubview:label2];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label2.frame.origin.y+label2.frame.size.height+5, 50, 60)];
    imageView1.image = [UIImage imageNamed:@"anuloma_vilomahand"];
    [scrollView addSubview:imageView1];
    
    label3.frame=CGRectMake(10, imageView1.frame.origin.y+imageView1.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    [scrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, scrollView.frame.size.width-20, 100);
    
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [scrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [scrollView addSubview:label5];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label5.frame.origin.y+label5.frame.size.height+5, 50, 60)];
    imageView2.image = [UIImage imageNamed:@"Action_inhale_left"];
    [scrollView addSubview:imageView2];
    
    label6.frame=CGRectMake(10, imageView2.frame.origin.y+imageView2.frame.size.height, scrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [scrollView addSubview:label6];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label6.frame.origin.y+label6.frame.size.height+5, 50, 60)];
    imageView3.image = [UIImage imageNamed:@"Action_no"];
    [scrollView addSubview:imageView3];
    
    label7.frame=CGRectMake(10, imageView3.frame.origin.y+imageView3.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [scrollView addSubview:label7];
    
    UIImageView *imageView4 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label7.frame.origin.y+label7.frame.size.height+5, 50, 60)];
    imageView4.image = [UIImage imageNamed:@"Action_exhale_right"];
    [scrollView addSubview:imageView4];
    
    label8.frame=CGRectMake(10, imageView4.frame.origin.y+imageView4.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label8.numberOfLines=0;
    [label8 sizeToFit];
    [scrollView addSubview:label8];
    
    UIImageView *imageView5 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label8.frame.origin.y+label8.frame.size.height+5, 50, 60)];
    imageView5.image = [UIImage imageNamed:@"Action_no"];
    [scrollView addSubview:imageView5];
    
    label9.frame=CGRectMake(10, imageView5.frame.origin.y+imageView5.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label9.numberOfLines=0;
    [label9 sizeToFit];
    [scrollView addSubview:label9];
    
    UIImageView *imageView6 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label9.frame.origin.y+label9.frame.size.height+5, 50, 60)];
    imageView6.image = [UIImage imageNamed:@"Action_inhale_right"];
    [scrollView addSubview:imageView6];
    
    label10.frame=CGRectMake(10, imageView6.frame.origin.y+imageView6.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label10.numberOfLines=0;
    [label10 sizeToFit];
    [scrollView addSubview:label10];
    
    UIImageView *imageView7 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label10.frame.origin.y+label10.frame.size.height+5, 50, 60)];
    imageView7.image = [UIImage imageNamed:@"Action_no"];
    [scrollView addSubview:imageView7];
    
    label11.frame=CGRectMake(10, imageView7.frame.origin.y+imageView7.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label11.numberOfLines=0;
    [label11 sizeToFit];
    [scrollView addSubview:label11];
    
    UIImageView *imageView8 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label11.frame.origin.y+label11.frame.size.height+5, 50, 60)];
    imageView8.image = [UIImage imageNamed:@"Action_exhale_left"];
    [scrollView addSubview:imageView8];
    
    label12.frame=CGRectMake(10, imageView8.frame.origin.y+imageView8.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label12.numberOfLines=0;
    [label12 sizeToFit];
    [scrollView addSubview:label12];
    
    UIImageView *imageView9 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label12.frame.origin.y+label12.frame.size.height+5, 50, 60)];
    imageView9.image = [UIImage imageNamed:@"Action_no"];
    [scrollView addSubview:imageView9];
    
    label13.frame=CGRectMake(10, imageView9.frame.origin.y+imageView9.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label13.numberOfLines=0;
    [label13 sizeToFit];
    [scrollView addSubview:label13];
    FRHyperLabel *label = [FRHyperLabel new];
    label.frame=CGRectMake(10,label13.frame.origin.y+label13.frame.size.height , scrollView.frame.size.width-20, 200);
    label.numberOfLines=100;
    
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]};
    
    
    label.attributedText = [[NSAttributedString alloc]initWithString:string attributes:attributes];
    label.font=[UIFont systemFontOfSize:16];
    
    [label setLinkForSubstring:@"www.7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.7pranayama.com/"]];
    }];
    [label setLinkForSubstring:@"info@7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://info@7pranayama.com/"]];
    }];

    [scrollView addSubview:label];
    
    scrollView.contentSize=CGSizeMake(scrollView.frame.size.width,label.frame.origin.y+label.frame.size.height+100);
    int scrollHeight=label.frame.origin.y+label.frame.size.height;
    [self ImageMethod:scrollHeight];
}

//////////////////////  KapalBhati content////////////////

-(void)KapalBhatiContent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc] init];
    UILabel *label3 = [[UILabel alloc] init];
    UILabel *label4 = [[UILabel alloc] init];
    UILabel *label5 = [[UILabel alloc] init];
    UILabel *label6 = [[UILabel alloc] init];
    UILabel *label7 = [[UILabel alloc] init];
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(14);
    label4.font=Ralewayfont(18);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);

    
    self.navigationItem.title = [MCLocalization stringForKey:@"help"];
    label1.text=[MCLocalization stringForKey:@"kapal_help1"];
    label2.text=[MCLocalization stringForKey:@"Kapal_help2"];
    label3.text=[MCLocalization stringForKey:@"kapal_help3"];
    label4.text=[MCLocalization stringForKey:@"kapal_help4"];
    label5.text=[MCLocalization stringForKey:@"kapal_help5"];
    label6.text=[MCLocalization stringForKey:@"kapal_help6"];
    label7.text=[MCLocalization stringForKey:@"kapal_help7"];
    
    label1.textColor = UIColor.redColor;
    label4.textColor = UIColor.redColor;
    label1.numberOfLines=0;
    [label1 sizeToFit];
    [scrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [scrollView addSubview:label2];
    
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label2.frame.origin.y+label2.frame.size.height+10, 50, 60)];
    imageView1.image = [UIImage imageNamed:@"kapal_postion"];
    [scrollView addSubview:imageView1];
    
    
    label3.frame=CGRectMake(10, imageView1.frame.origin.y+imageView1.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    [scrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [scrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [scrollView addSubview:label5];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-40, label5.frame.origin.y+label5.frame.size.height+10, 70, 40)];
    imageView2.image = [UIImage imageNamed:@"bellyout"];
    [scrollView addSubview:imageView2];
    
    label6.frame=CGRectMake(10, imageView2.frame.origin.y+imageView2.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [scrollView addSubview:label6];

    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label6.frame.origin.y+label6.frame.size.height+5, 70, 40)];
    imageView3.image = [UIImage imageNamed:@"bellyin"];
    [scrollView addSubview:imageView3];
    
    label7.frame=CGRectMake(10, imageView3.frame.origin.y+imageView3.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [scrollView addSubview:label7];
    
    
               //////// link text////
    
    FRHyperLabel *label = [FRHyperLabel new];
    label.frame=CGRectMake(10,label7.frame.origin.y+label7.frame.size.height ,scrollView.frame.size.width-20, 200);
      label.numberOfLines=0;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]};
    
    
    label.attributedText = [[NSAttributedString alloc]initWithString:[MCLocalization stringForKey:@"kapal_link"] attributes:attributes];
    label.font=[UIFont systemFontOfSize:16];
    
    [label setLinkForSubstring:@"www.7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.7pranayama.com/"]];
    }];
    [label setLinkForSubstring:@"info@7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://info@7pranayama.com/"]];
    }];
    
    [scrollView addSubview:label];
    scrollView.contentSize=CGSizeMake(scrollView.frame.size.width,label.frame.origin.y+label.frame.size.height+100);
    int scrollHeight=label.frame.origin.y+label.frame.size.height;
    [self ImageMethod:scrollHeight];

}


-(void)BhramariContent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc]init];
    UILabel *label3 = [[UILabel alloc]init];
    UILabel *label4 = [[UILabel alloc]init];
    UILabel *label5 = [[UILabel alloc]init];
    UILabel *label6 = [[UILabel alloc]init];
    UILabel *label7 = [[UILabel alloc]init];
    NSString *string;
    label1.textColor = UIColor.redColor;
    label4.textColor = UIColor.redColor;
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(14);
    label4.font=Ralewayfont(18);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);
    self.navigationItem.title = [MCLocalization stringForKey:@"help"];
    label1.text=[MCLocalization stringForKey:@"bharamari_help1"];
    label2.text=[MCLocalization stringForKey:@"bharamari_help2"];
    label3.text=[MCLocalization stringForKey:@"bharamari_help3"];
    label4.text=[MCLocalization stringForKey:@"bharamari_help4"];
    label5.text=[MCLocalization stringForKey:@"bharamari_help5"];
    label6.text=[MCLocalization stringForKey:@"bharamari_help6"];
    label7.text=[MCLocalization stringForKey:@"bharamari_help7"];
    string=[MCLocalization stringForKey:@"bharamari_link"];
    label1.numberOfLines=0;
    [label1 sizeToFit];
    [scrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [scrollView addSubview:label2];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label2.frame.origin.y+label2.frame.size.height+5, 60, 70)];
    imageView1.image = [UIImage imageNamed:@"bhramarri_position"];
    [scrollView addSubview:imageView1];
    
    label3.frame=CGRectMake(10, imageView1.frame.origin.y+imageView1.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    [scrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [scrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [scrollView addSubview:label5];
    
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-25, label5.frame.origin.y+label5.frame.size.height+5, 50, 70)];
    imageView2.image = [UIImage imageNamed:@"action_inhale_both"];
    [scrollView addSubview:imageView2];
   
    label6.frame=CGRectMake(10, imageView2.frame.origin.y+imageView2.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label4 sizeToFit];
    [scrollView addSubview:label6];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-40, label6.frame.origin.y+label6.frame.size.height+5, 80, 90)];
    imageView3.image = [UIImage imageNamed:@"exhale_bhramari"];
    [scrollView addSubview:imageView3];
    
    
    label7.frame=CGRectMake(10, imageView3.frame.origin.y+imageView3.frame.size.height, scrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [scrollView addSubview:label7];
    
            ///////link//////
    FRHyperLabel *label = [FRHyperLabel new];
    label.frame=CGRectMake(10,label7.frame.size.height+label7.frame.origin.y, scrollView.frame.size.width-20, 200);
    label.numberOfLines=100;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]};
    
    label.attributedText = [[NSAttributedString alloc]initWithString:string attributes:attributes];
    label.font=[UIFont systemFontOfSize:16];
    
    [label setLinkForSubstring:@"www.7pranayama.com" withLinkHandler:^(FRHyperLabel  *label7, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.7pranayama.com/"]];
    }];
    [label setLinkForSubstring:@"info@7pranayama.com" withLinkHandler:^(FRHyperLabel  *label7, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://info@7pranayama.com/"]];
    }];
    
    [scrollView addSubview:label];
    
    scrollView.contentSize=CGSizeMake(scrollView.frame.size.width,label.frame.origin.y+label.frame.size.height+100);
    int scrollHeight=label.frame.origin.y+label.frame.size.height;
    [self ImageMethod:scrollHeight];

}


-(void)SuryaContent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc]init];
    UILabel *label3 = [[UILabel alloc]init];
    UILabel *label4 = [[UILabel alloc]init];
    UILabel *label5 = [[UILabel alloc]init];
    UILabel *label6 = [[UILabel alloc]init];
    UILabel *label7 = [[UILabel alloc]init];
    UILabel *label8 = [[UILabel alloc]init];
    NSString *string;
    label1.textColor = UIColor.redColor;
    label4.textColor = UIColor.redColor;
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(14);
    label4.font=Ralewayfont(18);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);
    label8.font=Ralewayfont(14);
    self.navigationItem.title = [MCLocalization stringForKey:@"help"];
    label1.text=[MCLocalization stringForKey:@"surya_help1"];
    label2.text=[MCLocalization stringForKey:@"surya_help2"];
    label3.text=[MCLocalization stringForKey:@"surya_help3"];
    label4.text=[MCLocalization stringForKey:@"surya_help4"];
    label5.text=[MCLocalization stringForKey:@"surya_help5"];
    label6.text=[MCLocalization stringForKey:@"surya_help6"];
    label7.text=[MCLocalization stringForKey:@"surya_help7"];
    label8.text=[MCLocalization stringForKey:@"surya_help8"];
    string=[MCLocalization stringForKey:@"surya_link"];
    
    label1.numberOfLines=0;
    [label1 sizeToFit];
    [scrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [scrollView addSubview:label2];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label2.frame.origin.y+label2.frame.size.height+10, 60, 70)];
    imageView1.image = [UIImage imageNamed:@"anuloma_vilomahand"];
    [scrollView addSubview:imageView1];
    
    label3.frame=CGRectMake(10, imageView1.frame.origin.y+imageView1.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    [scrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [scrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [scrollView addSubview:label5];
    
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-25, label5.frame.origin.y+label5.frame.size.height+5, 50, 70)];
    imageView2.image = [UIImage imageNamed:@"Action_inhale_right"];
    [scrollView addSubview:imageView2];
    
    label6.frame=CGRectMake(10, imageView2.frame.origin.y+imageView2.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [scrollView addSubview:label6];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-40, label6.frame.origin.y+label6.frame.size.height+5, 80, 90)];
    imageView3.image = [UIImage imageNamed:@"Action_no"];
    [scrollView addSubview:imageView3];
    
    
    label7.frame=CGRectMake(10, imageView3.frame.origin.y+imageView3.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [scrollView addSubview:label7];
    
    UIImageView *imageView4 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-40, label7.frame.origin.y+label7.frame.size.height+5, 80, 90)];
    imageView4.image = [UIImage imageNamed:@"Action_exhale_left"];
    [scrollView addSubview:imageView4];
    
    label8.frame=CGRectMake(10, imageView4.frame.origin.y+imageView4.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label8.numberOfLines=0;
    [label8 sizeToFit];
    [scrollView addSubview:label8];
    
   
    
    ///////link//////
    FRHyperLabel *label = [FRHyperLabel new];
    label.frame=CGRectMake(10,label8.frame.size.height+label8.frame.origin.y, scrollView.frame.size.width-20, 200);
    label.numberOfLines=100;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]};
    
    label.attributedText = [[NSAttributedString alloc]initWithString:string attributes:attributes];
    label.font=[UIFont systemFontOfSize:16];
    
    [label setLinkForSubstring:@"www.7pranayama.com" withLinkHandler:^(FRHyperLabel  *label7, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.7pranayama.com/"]];
    }];
    [label setLinkForSubstring:@"info@7pranayama.com" withLinkHandler:^(FRHyperLabel  *label7, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://info@7pranayama.com/"]];
    }];
    
    [scrollView addSubview:label];
    
    scrollView.contentSize=CGSizeMake(scrollView.frame.size.width,label.frame.origin.y+label.frame.size.height+100);
    int scrollHeight=label.frame.origin.y+label.frame.size.height;
    [self ImageMethod:scrollHeight];
    

}

//////////////// ChandraCOntent////////
-(void)ChandraContent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc]init];
    UILabel *label3 = [[UILabel alloc]init];
    UILabel *label4 = [[UILabel alloc]init];
    UILabel *label5 = [[UILabel alloc]init];
    UILabel *label6 = [[UILabel alloc]init];
    UILabel *label7 = [[UILabel alloc]init];
    UILabel *label8 = [[UILabel alloc]init];
    NSString *string;
    label1.textColor = UIColor.redColor;
    label4.textColor = UIColor.redColor;
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(14);
    label4.font=Ralewayfont(18);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);
    label8.font=Ralewayfont(14);
    
    label1.text=[MCLocalization stringForKey:@"chandra_help1"];
    label2.text=[MCLocalization stringForKey:@"chandra_help2"];
    label3.text=[MCLocalization stringForKey:@"chandra_help3"];
    label4.text=[MCLocalization stringForKey:@"chandra_help4"];
    label5.text=[MCLocalization stringForKey:@"chandra_help5"];
    label6.text=[MCLocalization stringForKey:@"chandra_help6"];
    label7.text=[MCLocalization stringForKey:@"chandra_help7"];
    label8.text=[MCLocalization stringForKey:@"chandra_help8"];
    string=[MCLocalization stringForKey:@"chandra_link"];
    
    label1.numberOfLines=0;
    [label1 sizeToFit];
    [scrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [scrollView addSubview:label2];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-25, label2.frame.origin.y+label2.frame.size.height+5, 50, 60)];
    imageView1.image = [UIImage imageNamed:@"anuloma_vilomahand"];
    [scrollView addSubview:imageView1];
    
    label3.frame=CGRectMake(10, imageView1.frame.origin.y+imageView1.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    [scrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [scrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [scrollView addSubview:label5];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label5.frame.origin.y+label5.frame.size.height+5, 50, 70)];
    imageView2.image = [UIImage imageNamed:@"Action_inhale_left"];
    [scrollView addSubview:imageView2];
    
    label6.frame=CGRectMake(10, imageView2.frame.origin.y+imageView2.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [scrollView addSubview:label6];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label6.frame.origin.y+label6.frame.size.height+5, 50, 70)];
    imageView3.image = [UIImage imageNamed:@"Action_no"];
    [scrollView addSubview:imageView3];
    
    label7.frame=CGRectMake(10, imageView3.frame.origin.y+imageView3.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [scrollView addSubview:label7];
    
    UIImageView *imageView4 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label7.frame.origin.y+label7.frame.size.height+5, 50, 70)];
    imageView4.image = [UIImage imageNamed:@"Action_exhale_right"];
    [scrollView addSubview:imageView4];
    
    label8.frame=CGRectMake(10, imageView4.frame.origin.y+imageView4.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label8.numberOfLines=0;
    [label8 sizeToFit];
    [scrollView addSubview:label8];
    
    FRHyperLabel *label = [FRHyperLabel new];
    label.frame=CGRectMake(10,label8.frame.origin.y+label8.frame.size.height , scrollView.frame.size.width-20, 200);
    label.numberOfLines=100;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]};
    
    
    label.attributedText = [[NSAttributedString alloc]initWithString:string attributes:attributes];
    label.font=[UIFont systemFontOfSize:16];
    
    [label setLinkForSubstring:@"www.7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.7pranayama.com/"]];
    }];
    [label setLinkForSubstring:@"info@7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://info@7pranayama.com/"]];
    }];
    
    [scrollView addSubview:label];
    
    scrollView.contentSize=CGSizeMake(scrollView.frame.size.width,label.frame.origin.y+label.frame.size.height+100);
    int scrollHeight=label.frame.origin.y+label.frame.size.height;
    [self ImageMethod:scrollHeight];
}


-(void)BhastrikaContent{
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    NSString *string;
    UILabel *label2 = [[UILabel alloc]init];
    UILabel *label3 = [[UILabel alloc]init];
    UILabel *label4 = [[UILabel alloc]init];
    UILabel *label5 = [[UILabel alloc]init];
    UILabel *label6 = [[UILabel alloc]init];
    UILabel *label7 = [[UILabel alloc]init];
    label1.textColor = UIColor.redColor;
    label4.textColor = UIColor.redColor;
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(14);
    label4.font=Ralewayfont(18);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);
    self.navigationItem.title = [MCLocalization stringForKey:@"help"];
    label1.text=[MCLocalization stringForKey:@"bhastrika_help1"];
    label2.text=[MCLocalization stringForKey:@"bhastrika_help2"];
    label3.text=[MCLocalization stringForKey:@"bhastrika_help3"];
    label4.text=[MCLocalization stringForKey:@"bhastrika_help4"];
    label5.text=[MCLocalization stringForKey:@"bhastrika_help5"];
    label6.text=[MCLocalization stringForKey:@"bhastrika_help6"];
    label7.text=[MCLocalization stringForKey:@"bhastrika_help7"];
    string=[MCLocalization stringForKey:@"bhastrika_link"];
    
    label1.numberOfLines=0;
    [label1 sizeToFit];
    [scrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [scrollView addSubview:label2];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label2.frame.origin.y+label2.frame.size.height+5, 60, 70)];
    imageView1.image = [UIImage imageNamed:@"kapal_postion"];
    [scrollView addSubview:imageView1];
    
    label3.frame=CGRectMake(10, imageView1.frame.origin.y+imageView1.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    [scrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [scrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [scrollView addSubview:label5];
    
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-25, label5.frame.origin.y+label5.frame.size.height+5, 50, 70)];
    imageView2.image = [UIImage imageNamed:@"action_inhale_both"];
    [scrollView addSubview:imageView2];
    
    label6.frame=CGRectMake(10, imageView2.frame.origin.y+imageView2.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [scrollView addSubview:label6];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-25, label6.frame.origin.y+label6.frame.size.height+5, 50, 70)];
    imageView3.image = [UIImage imageNamed:@"action_exhale_both"];
    [scrollView addSubview:imageView3];
    
    
    label7.frame=CGRectMake(10, imageView3.frame.origin.y+imageView3.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [scrollView addSubview:label7];
    
    
   FRHyperLabel *label = [FRHyperLabel new];
    label.frame=CGRectMake(10,label7.frame.origin.y+label7.frame.size.height, scrollView.frame.size.width-20, 200);
    label.numberOfLines=100;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]};
    
    
    label.attributedText = [[NSAttributedString alloc]initWithString:string attributes:attributes];
    label.font=[UIFont systemFontOfSize:16];
    
    [label setLinkForSubstring:@"www.7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.7pranayama.com/"]];
    }];
    [label setLinkForSubstring:@"info@7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://info@7pranayama.com/"]];
    }];
    
    [scrollView addSubview:label];
    
    scrollView.contentSize=CGSizeMake(scrollView.frame.size.width,label.frame.origin.y+label.frame.size.height+100);
    int scrollHeight=label.frame.origin.y+label.frame.size.height;
    [self ImageMethod:scrollHeight];
   }


-(void)SheetaliContent
{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc]init];
    UILabel *label3 = [[UILabel alloc]init];
    UILabel *label4 = [[UILabel alloc]init];
    UILabel *label5 = [[UILabel alloc]init];
    UILabel *label6 = [[UILabel alloc]init];
    UILabel *label7 = [[UILabel alloc]init];
    UILabel *label8 = [[UILabel alloc]init];
    NSString *string;
    label1.textColor = UIColor.redColor;
    label4.textColor = UIColor.redColor;
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(14);
    label4.font=Ralewayfont(18);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);
    label8.font=Ralewayfont(14);
    label1.text=[MCLocalization stringForKey:@"sheetali_help1"];
    label2.text=[MCLocalization stringForKey:@"sheetali_help2"];
    label3.text=[MCLocalization stringForKey:@"sheetali_help3"];
    label4.text=[MCLocalization stringForKey:@"sheetali_help4"];
    label5.text=[MCLocalization stringForKey:@"sheetali_help5"];
    label6.text=[MCLocalization stringForKey:@"sheetali_help6"];
    label7.text=[MCLocalization stringForKey:@"sheetali_help7"];
    label8.text=[MCLocalization stringForKey:@"sheetali_help8"];
    self.navigationItem.title = [MCLocalization stringForKey:@"help"];
    string=[MCLocalization stringForKey:@"sheetali_link"];
    
    label1.numberOfLines=0;
    [label1 sizeToFit];
    [scrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [scrollView addSubview:label2];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-35, label2.frame.origin.y+label2.frame.size.height+5, 60, 60)];
    imageView1.image = [UIImage imageNamed:@"tongue_pose"];
    [scrollView addSubview:imageView1];
    
    label3.frame=CGRectMake(10, imageView1.frame.origin.y+imageView1.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    [scrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [scrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [scrollView addSubview:label5];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label5.frame.origin.y+label5.frame.size.height+5, 50, 70)];
    imageView2.image = [UIImage imageNamed:@"sheetali_inhale"];
    [scrollView addSubview:imageView2];
    
    
    label6.frame=CGRectMake(10, imageView2.frame.origin.y+imageView2.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [scrollView addSubview:label6];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label6.frame.origin.y+label6.frame.size.height+5, 50, 70)];
    imageView3.image = [UIImage imageNamed:@"Action_no"];
    [scrollView addSubview:imageView3];
    
    label7.frame=CGRectMake(10, imageView3.frame.origin.y+imageView3.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [scrollView addSubview:label7];
    
    UIImageView *imageView4 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label7.frame.origin.y+label7.frame.size.height+5, 50, 70)];
    imageView4.image = [UIImage imageNamed:@"action_inhale_both"];
    [scrollView addSubview:imageView4];
    
    label8.frame=CGRectMake(10, imageView4.frame.origin.y+imageView4.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label8.numberOfLines=0;
    [label8 sizeToFit];
    [scrollView addSubview:label8];
    
    
    FRHyperLabel *label = [FRHyperLabel new];
    label.frame=CGRectMake(10,label8.frame.origin.y+label8.frame.size.height , scrollView.frame.size.width-20, 200);
    label.numberOfLines=100;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]};
    
    
    label.attributedText = [[NSAttributedString alloc]initWithString:string attributes:attributes];
    label.font=[UIFont systemFontOfSize:16];
    
    [label setLinkForSubstring:@"www.7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.7pranayama.com/"]];
    }];
    [label setLinkForSubstring:@"info@7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://info@7pranayama.com/"]];
    }];
    
    [scrollView addSubview:label];
    
    scrollView.contentSize=CGSizeMake(scrollView.frame.size.width,label.frame.origin.y+label.frame.size.height+100);
    int scrollHeight=label.frame.origin.y+label.frame.size.height;
    [self ImageMethod:scrollHeight];
}


-(void)UjjayiContent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc]init];
    UILabel *label3 = [[UILabel alloc]init];
    UILabel *label4 = [[UILabel alloc]init];
    UILabel *label5 = [[UILabel alloc]init];
    UILabel *label6 = [[UILabel alloc]init];
    UILabel *label7 = [[UILabel alloc]init];
     label1.textColor = UIColor.redColor;
     label3.textColor = UIColor.redColor;
    NSString *string;
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);
    self.navigationItem.title = [MCLocalization stringForKey:@"help"];
    label1.text=[MCLocalization stringForKey:@"ujjayi_help1"];
    label2.text=[MCLocalization stringForKey:@"ujjayi_help2"];
    label3.text=[MCLocalization stringForKey:@"ujjayi_help3"];
    label4.text=[MCLocalization stringForKey:@"ujjayi_help4"];
    label5.text=[MCLocalization stringForKey:@"ujjayi_help5"];
    label6.text=[MCLocalization stringForKey:@"ujjayi_help6"];
    label7.text=[MCLocalization stringForKey:@"ujjayi_help7"];
    string=[MCLocalization stringForKey:@"ujjayi_link"];
    
    label1.numberOfLines=0;
    [label1 sizeToFit];
    [scrollView addSubview:label1];
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [scrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    [scrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [scrollView addSubview:label4];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label4.frame.origin.y+label4.frame.size.height+5, 50, 60)];
    imageView1.image = [UIImage imageNamed:@"action_inhale_both"];
    [scrollView addSubview:imageView1];
    
    
    label5.frame=CGRectMake(10, imageView1.frame.origin.y+imageView1.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [scrollView addSubview:label5];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label5.frame.origin.y+label5.frame.size.height+5, 60, 60)];
    imageView2.image = [UIImage imageNamed:@"jalandrabandh"];
    [scrollView addSubview:imageView2];
    
    
    label6.frame=CGRectMake(10, imageView2.frame.origin.y+imageView2.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [scrollView addSubview:label6];
    ;
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label6.frame.origin.y+label6.frame.size.height+5, 50, 60)];
    imageView3.image = [UIImage imageNamed:@"Action_exhale_left"];
    [scrollView addSubview:imageView3];
    
    label7.frame=CGRectMake(10, imageView3.frame.origin.y+imageView3.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [scrollView addSubview:label7];
    
    FRHyperLabel *label = [FRHyperLabel new];
    label.frame=CGRectMake(10,label7.frame.origin.y+label7.frame.size.height , scrollView.frame.size.width-20, 200);
    label.numberOfLines=100;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]};
    
    
    label.attributedText = [[NSAttributedString alloc]initWithString:string attributes:attributes];
    label.font=[UIFont systemFontOfSize:16];
    
    [label setLinkForSubstring:@"www.7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.7pranayama.com/"]];
    }];
    [label setLinkForSubstring:@"info@7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://info@7pranayama.com/"]];
    }];
    
    [scrollView addSubview:label];
    
    scrollView.contentSize=CGSizeMake(scrollView.frame.size.width,label.frame.origin.y+label.frame.size.height+100);
    int scrollHeight=label.frame.origin.y+label.frame.size.height;
    [self ImageMethod:scrollHeight];
}


-(void)MeditativeContent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc]init];
    UILabel *label3 = [[UILabel alloc]init];
    UILabel *label4 = [[UILabel alloc]init];
    UILabel *label5 = [[UILabel alloc]init];
    UILabel *label6 = [[UILabel alloc]init];
    UILabel *label7 = [[UILabel alloc]init];
    UILabel *label8 = [[UILabel alloc]init];
    NSString *string;
    label1.font=Ralewayfont(16);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(16);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);
    label8.font=Ralewayfont(14);
    self.navigationItem.title = [MCLocalization stringForKey:@"help"];
    label1.textColor = UIColor.redColor;
    label3.textColor = UIColor.redColor;
    label1.text=[MCLocalization stringForKey:@"meditative_help1"];
    label2.text=[MCLocalization stringForKey:@"meditative_help2"];
    label3.text=[MCLocalization stringForKey:@"meditative_help3"];
    label4.text=[MCLocalization stringForKey:@"meditative_help4"];
    label5.text=[MCLocalization stringForKey:@"meditative_help5"];
    label6.text=[MCLocalization stringForKey:@"meditative_help6"];
    label7.text=[MCLocalization stringForKey:@"meditative_help7"];
    label8.text=[MCLocalization stringForKey:@"meditative_help8"];
    
    string=[MCLocalization stringForKey:@"meditative_link"];
    label1.numberOfLines=0;
    [label1 sizeToFit];
    [scrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [scrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    [scrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [scrollView addSubview:label4];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label4.frame.origin.y+label4.frame.size.height+5, 50, 60)];
    imageView1.image = [UIImage imageNamed:@"exhale_mouth"];
    [scrollView addSubview:imageView1];
    
    
    label5.frame=CGRectMake(10, imageView1.frame.origin.y+imageView1.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [scrollView addSubview:label5];
    
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label5.frame.origin.y+label5.frame.size.height+5, 50, 60)];
    imageView2.image = [UIImage imageNamed:@"action_inhale_both"];
    [scrollView addSubview:imageView2];
    
    label6.frame=CGRectMake(10, imageView2.frame.origin.y+imageView2.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [scrollView addSubview:label6];
    
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label6.frame.origin.y+label6.frame.size.height+5, 50, 60)];
    imageView3.image = [UIImage imageNamed:@"Action_no"];
    [scrollView addSubview:imageView3];
    
    label7.frame=CGRectMake(10, imageView3.frame.origin.y+imageView3.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [scrollView addSubview:label7];
    
    UIImageView *imageView4 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label7.frame.origin.y+label7.frame.size.height+5, 50, 60)];
    imageView4.image = [UIImage imageNamed:@"exhale_mouth"];
    [scrollView addSubview:imageView4];
    
    label8.frame=CGRectMake(10, imageView4.frame.origin.y+imageView4.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label8.numberOfLines=0;
    [label8 sizeToFit];
    [scrollView addSubview:label8];
    
    
    FRHyperLabel *label = [FRHyperLabel new];
    label.frame=CGRectMake(10,label8.frame.origin.y+label8.frame.size.height , scrollView.frame.size.width-20, 200);
    label.numberOfLines=100;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]};
    
    
    label.attributedText = [[NSAttributedString alloc]initWithString:string attributes:attributes];
    label.font=[UIFont systemFontOfSize:16];
    
    [label setLinkForSubstring:@"www.7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.7pranayama.com/"]];
    }];
    [label setLinkForSubstring:@"info@7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://info@7pranayama.com/"]];
    }];
    
    [scrollView addSubview:label];
    
    scrollView.contentSize=CGSizeMake(scrollView.frame.size.width,label.frame.origin.y+label.frame.size.height+100);
    int scrollHeight=label.frame.origin.y+label.frame.size.height;
    [self ImageMethod:scrollHeight];
}


-(void)ImageMethod:(int)scrollHeight{
    
    int a=self.view.frame.size.width/4;
    int b=a/2;
    UIButton*btn1=[[UIButton alloc] initWithFrame:CGRectMake(a/2-25, scrollHeight+20, 50, 50)];
    btn1.tag=1;
    btn1.layer.cornerRadius = btn1.bounds.size.width / 2.0;
    UIImage *btnImage1 = [UIImage imageNamed:@"facebook"];
    [btn1 setImage:btnImage1 forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(goToLink:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:btn1];
    
    UIButton*btn2=[[UIButton alloc] initWithFrame:CGRectMake(((2*a)-b)-25, scrollHeight+20, 50, 50)];
    btn2.tag=2;
    btn2.layer.cornerRadius = btn1.bounds.size.width / 2.0;
    UIImage *btnImage2 = [UIImage imageNamed:@"twitter"];
    [btn2 setImage:btnImage2 forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(goToLink:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:btn2];
    
    UIButton*btn3=[[UIButton alloc] initWithFrame:CGRectMake(((3*a)-b)-25, scrollHeight+20, 50, 50)];
    btn3.tag=3;
    btn3.layer.cornerRadius = btn1.bounds.size.width / 2.0;
    UIImage *btnImage3 = [UIImage imageNamed:@"pinterest"];
    [btn3 setImage:btnImage3 forState:UIControlStateNormal];
    [btn3 addTarget:self action:@selector(goToLink:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:btn3];
    
    UIButton*btn4=[[UIButton alloc] initWithFrame:CGRectMake((self.view.frame.size.width-b)-25,scrollHeight+20, 50, 50)];
    btn4.tag=4;
    btn4.layer.cornerRadius = btn1.bounds.size.width / 2.0;
    UIImage *btnImage4 = [UIImage imageNamed:@"google"];
    [btn4 setImage:btnImage4 forState:UIControlStateNormal];
    [btn4 addTarget:self action:@selector(goToLink:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:btn4];
    
}

-(void)udgeethContent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc]init];
    UILabel *label3 = [[UILabel alloc]init];
    UILabel *label4 = [[UILabel alloc]init];
    UILabel *label5 = [[UILabel alloc]init];
    UILabel *label6 = [[UILabel alloc]init];
    UILabel *label7 = [[UILabel alloc]init];
    label1.textColor = UIColor.redColor;
    label4.textColor = UIColor.redColor;
    NSString *string;
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(14);
    label4.font=Ralewayfont(18);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);
    self.navigationItem.title = [MCLocalization stringForKey:@"help"];
    label1.text=[MCLocalization stringForKey:@"udgeet_help1"];
    label2.text=[MCLocalization stringForKey:@"udgeet_help2"];
    label3.text=[MCLocalization stringForKey:@"udgeet_help3"];
    label4.text=[MCLocalization stringForKey:@"udgeet_help4"];
    label5.text=[MCLocalization stringForKey:@"udgeet_help5"];
    label6.text=[MCLocalization stringForKey:@"udgeet_help6"];
    label7.text=[MCLocalization stringForKey:@"udgeet_help7"];
    string=[MCLocalization stringForKey:@"udgeet_link"];
    label1.numberOfLines=0;
    [label1 sizeToFit];
    [scrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [scrollView addSubview:label2];

    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-80, label2.frame.origin.y+label2.frame.size.height+5, 160, 80)];
    imageView1.image = [UIImage imageNamed:@"padmasan"];
    [scrollView addSubview:imageView1];
    
   
    label3.frame=CGRectMake(10, imageView1.frame.origin.y+imageView1.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    [scrollView addSubview:label3];
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [scrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [scrollView addSubview:label5];
    
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label5.frame.origin.y+label5.frame.size.height+5, 60, 60)];
    imageView2.image = [UIImage imageNamed:@"action_inhale_both"];
    [scrollView addSubview:imageView2];
    
    label6.frame=CGRectMake(10, imageView2.frame.origin.y+imageView2.frame.size.height, scrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [scrollView addSubview:label6];
    
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-50, label6.frame.origin.y+label6.frame.size.height+5, 80, 80)];
    imageView3.image = [UIImage imageNamed:@"exhale_om"];
    [scrollView addSubview:imageView3];
    
    
    label7.frame=CGRectMake(10, imageView3.frame.origin.y+imageView3.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [scrollView addSubview:label7];
    
   
    FRHyperLabel *label = [FRHyperLabel new];
    label.frame=CGRectMake(10,label7.frame.origin.y+label7.frame.size.height , scrollView.frame.size.width-20, 200);
    label.numberOfLines=100;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]};
    
    
    label.attributedText = [[NSAttributedString alloc]initWithString:string attributes:attributes];
    label.font=[UIFont systemFontOfSize:16];
    
    [label setLinkForSubstring:@"www.7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.7pranayama.com/"]];
    }];
    [label setLinkForSubstring:@"info@7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://info@7pranayama.com/"]];
    }];
    
    [scrollView addSubview:label];
    
    scrollView.contentSize=CGSizeMake(scrollView.frame.size.width,label.frame.origin.y+label.frame.size.height+100);
    int scrollHeight=label.frame.origin.y+label.frame.size.height;
    [self ImageMethod:scrollHeight];
}



-(void)bahyaContent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc]init];
    UILabel *label3 = [[UILabel alloc]init];
    UILabel *label4 = [[UILabel alloc]init];
    UILabel *label5 = [[UILabel alloc]init];
    UILabel *label6 = [[UILabel alloc]init];
    UILabel *label7 = [[UILabel alloc]init];
    
    NSString *string;
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);
    self.navigationItem.title = [MCLocalization stringForKey:@"help"];
    
    label1.text=[MCLocalization stringForKey:@"bhya_help1"];
    label2.text=[MCLocalization stringForKey:@"bhya_help2"];
    label3.text=[MCLocalization stringForKey:@"bhya_help3"];
    label4.text=[MCLocalization stringForKey:@"bhya_help4"];
    label5.text=[MCLocalization stringForKey:@"bhya_help5"];
    label6.text=[MCLocalization stringForKey:@"bhya_help6"];
    label7.text=[MCLocalization stringForKey:@"bhya_help7"];
    label1.textColor = UIColor.redColor;
    label3.textColor = UIColor.redColor;
    
    string=[MCLocalization stringForKey:@"bahya_link"];
    label1.numberOfLines=0;
    [label1 sizeToFit];
    [scrollView addSubview:label1];

   label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [scrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    [scrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [scrollView addSubview:label4];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label4.frame.origin.y+label4.frame.size.height+5, 50, 60)];
    imageView1.image = [UIImage imageNamed:@"action_inhale_both"];
    [scrollView addSubview:imageView1];
    
    label5.frame=CGRectMake(10, imageView1.frame.origin.y+imageView1.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [scrollView addSubview:label5];
    
   UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label5.frame.origin.y+label5.frame.size.height+5, 60, 60)];
    imageView2.image = [UIImage imageNamed:@"action_exhale_both"];
    [scrollView addSubview:imageView2];
   
    label6.frame=CGRectMake(10, imageView2.frame.origin.y+imageView2.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [scrollView addSubview:label6];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-30, label6.frame.origin.y+label6.frame.size.height+5, 50, 60)];
    imageView3.image = [UIImage imageNamed:@"jalandhar_fullpose"];
    [scrollView addSubview:imageView3];
    
    label7.frame=CGRectMake(10, imageView3.frame.origin.y+imageView3.frame.size.height+5, scrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [scrollView addSubview:label7];
    
    
    FRHyperLabel *label = [FRHyperLabel new];
    label.frame=CGRectMake(10,label7.frame.origin.y+label7.frame.size.height , scrollView.frame.size.width-20, 200);
    label.numberOfLines=100;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]};
    
    
    label.attributedText = [[NSAttributedString alloc]initWithString:string attributes:attributes];
    label.font=[UIFont systemFontOfSize:16];
    
    [label setLinkForSubstring:@"www.7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.7pranayama.com/"]];
    }];
    [label setLinkForSubstring:@"info@7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://info@7pranayama.com/"]];
    }];
    
    [scrollView addSubview:label];
    
    scrollView.contentSize=CGSizeMake(scrollView.frame.size.width,label.frame.origin.y+label.frame.size.height+100);
    int scrollHeight=label.frame.origin.y+label.frame.size.height;
    [self ImageMethod:scrollHeight];
}

-(void)goToLink:(UIButton*)sender{
    if (sender.tag==1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.facebook.com/7pranayama-1742577192620397/"]];
    }
    else if (sender.tag==2){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://twitter.com/7pranayama"]];
        
    }
    else if (sender.tag==3){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://in.pinterest.com/7pranayama/"]];
        
    }
    else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://plus.google.com/111008006040750995181/posts"]];
    }
}
-(IBAction)backbutton:(id)sender
{
[[self navigationController] popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

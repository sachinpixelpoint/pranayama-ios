//
//  BhastrikaVC.h
//  Pranayama
//
//  Created by Manish Kumar on 11/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BhastrikaActionVC.h"

@interface BhastrikaVC : GAITrackedViewController <bhastrikadelegate>
{
    IBOutlet UIPickerView *inhalePicker;
    IBOutlet UIPickerView *exhalePicker;
    IBOutlet UIPickerView *roundPicker;
    int InhaleRow;
    int ExhaleRow;
    int RoundRow;
    NSMutableArray *InhaleData;
    NSMutableArray *ExhaleData;
    NSMutableArray *RoundData;
    
    IBOutlet UIView *topview;
    IBOutlet UIImageView *img;
    
    IBOutlet UILabel *inhalelbl;
    IBOutlet UILabel *exhalelbl;
    IBOutlet UILabel *roundlbl;
    
    IBOutlet UIButton *btnLeftInhel;
    IBOutlet UIButton *btnRightExhale;
    IBOutlet UIButton *StartButton;
    IBOutlet UIButton *reportButton;
    
    IBOutlet UILabel *lblHours;
    IBOutlet UILabel *lblminutes;
    IBOutlet UILabel *lblseconds;
    int totaltime;
    int seconds;
    int minutes;
    int hours;
    UIButton *button1,*button2,*button3;
    BOOL isMenuVisible;
    IBOutlet UIButton *Floatingbutton;
    UIView *introview;
    UIButton *gotItbutton;
    BOOL selectCondition;
    BOOL buttonCondition;
    NSTimer *timer1;
    NSString *TodayFitness,*Daystr,*Your_leve,*Fitness_regime_21,*Share_experience;
}
@end

//
//  Dincharya_ProgressVC.swift
//  Pranayama
//
//  Created by Manish Kumar on 20/10/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

import UIKit

class Dincharya_ProgressVC: GAITrackedViewController {
    
    let kAppDel = (UIApplication.shared.delegate! as! AppDelegate)
    
    var Topview : UIView!
    var MainView : UIView!
    
    var  TimeLbl : UILabel!
    
    @IBOutlet var scrollview : UIScrollView!
    
    var dincharya : Dincharya!
    var indexpath : Int = 0
    var maxswitch : Int = 0
    var switchValue : Int = 0
    
    
    var YogsanArr : NSMutableArray! = NSMutableArray()
    var PranayamaArr : NSMutableArray! = NSMutableArray()
    var pranaList : NSMutableArray! = NSMutableArray()
    var YogaList : NSMutableArray! = NSMutableArray()

    var reportArr : NSMutableArray! = NSMutableArray()
    
    var Font8 : CGFloat!
    var Font10 : CGFloat!
    var Font12 : CGFloat!
    var Font14 : CGFloat!
    var fontSize : CGFloat!
    var datestr : String!
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: (#selector(self.appDidBecomeActive(_:))), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        self.screenName = "Dincharya Progress"
        // Do any additional setup after loading the view.
        
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            Font8=14
            Font12=18
            Font10=16
            Font14=20
            fontSize=24
        }
        else{
            Font8=8
            Font12=12
            Font10=10
            Font14=14
            fontSize=18
        }

        
        UINavigationBar.appearance().titleTextAttributes=[NSFontAttributeName: Ralewayfont(fontSize!),NSForegroundColorAttributeName:UIColor.white]
        
        self.GetArrayFromdata()
        
        reportArr = NSMutableArray(array: kAppDel.fetchDincharyaReportFromDatabaseAcording(toName: dincharya.name))
        print(reportArr)
        switchValue = (Int(reportArr.count) / 7)
        indexpath = reportArr.count%7;
        if reportArr.count%7==0 && reportArr.count>0 {
            indexpath=self.switchValue*7;
            switchValue -= 1;
        }
        maxswitch=switchValue;
        
        self.GetText()
        self.TopviewMethod()
        self.CustumDesignView()
        
    }
    
    func appDidBecomeActive(_ notification:Notification) -> Void {
        
        CommonSounds.sharedInstance().appDidBecomeActive(notification, navigation: self.navigationController)
    }
    
    func GetArrayFromdata() -> Void {
        
        if dincharya == nil {
            let str = UserDefaults.standard.object(forKey: dincharyaName) as! String
            let ar = kAppDel.fetchDincharyaFromDatabaseAcording(toName: str) as NSArray
            if ar.count>0{
                dincharya = ar.object(at: 0) as! Dincharya
                let yogaAr = NSKeyedUnarchiver.unarchiveObject(with: dincharya.yogasana_arr!) as! NSArray
                let pranaAr = NSKeyedUnarchiver.unarchiveObject(with: dincharya.pranayama_arr!) as! NSArray
                
                YogsanArr = NSMutableArray(array: yogaAr)
                PranayamaArr = NSMutableArray(array: pranaAr)
                print(YogsanArr)
                print(PranayamaArr)
            }
        }
        else{
            let yogaAr = NSKeyedUnarchiver.unarchiveObject(with: dincharya.yogasana_arr!) as! NSArray
            let pranaAr = NSKeyedUnarchiver.unarchiveObject(with: dincharya.pranayama_arr!) as! NSArray
            
            YogsanArr = NSMutableArray(array: yogaAr)
            PranayamaArr = NSMutableArray(array: pranaAr)
            print(YogsanArr)
            print(PranayamaArr)
        }
    }
    
    
    func GetText() -> Void {
        
        self.navigationItem.title = MCLocalization.string(forKey: "progress")
        datestr = MCLocalization.string(forKey: "date")
        for i in 0..<PranayamaArr.count {
            if PranayamaArr.object(at: i) as! String == "AnulomVilom" {
                pranaList?.add(MCLocalization.string(forKey: "Anulom_Vilom"))
            }
            if PranayamaArr.object(at: i) as! String == "Kapalbhati" {
                pranaList?.add(MCLocalization.string(forKey: "kapalbhati"))
            }
            if PranayamaArr.object(at: i) as! String == "Bhramari" {
                pranaList?.add(MCLocalization.string(forKey: "bhramari"))
            }
            if PranayamaArr.object(at: i) as! String == "Surya Bhedana" {
                pranaList?.add(MCLocalization.string(forKey: "surya_bhedana"))
            }
            if PranayamaArr.object(at: i) as! String == "Chandra Bhedana" {
                pranaList?.add(MCLocalization.string(forKey: "chandra_bhedana"))
            }
            if PranayamaArr.object(at: i) as! String == "Bhastrika" {
                pranaList?.add(MCLocalization.string(forKey: "bhastrika"))
            }
            if PranayamaArr.object(at: i) as! String == "Sheetali" {
                pranaList?.add(MCLocalization.string(forKey: "sheetali"))
            }
            if PranayamaArr.object(at: i) as! String == "Ujjayi" {
                pranaList?.add(MCLocalization.string(forKey: "ujjayi"))
            }
            if PranayamaArr.object(at: i) as! String == "Meditative Breathing" {
                pranaList?.add(MCLocalization.string(forKey: "meditative"))
            }
            if PranayamaArr.object(at: i) as! String == "Udgeeth" {
                pranaList?.add(MCLocalization.string(forKey: "udgeeth"))
            }
            if PranayamaArr.object(at: i) as! String == "Bahya" {
                pranaList?.add(MCLocalization.string(forKey: "bahya"))
            }
        }
        for i in 0..<YogsanArr.count {
            if YogsanArr.object(at: i) as! String == "Sarvangasana" {
                YogaList?.add(MCLocalization.string(forKey: "Sarvangasana"))
            }
            if YogsanArr.object(at: i) as! String == "Halasana" {
                YogaList?.add(MCLocalization.string(forKey: "Halasana"))
            }
            if YogsanArr.object(at: i) as! String == "Vipritkarani" {
                YogaList?.add(MCLocalization.string(forKey: "Vipritkarani"))
            }
            if YogsanArr.object(at: i) as! String == "Paschimottanasana" {
                YogaList?.add(MCLocalization.string(forKey: "Paschimottanasana"))
            }
            if YogsanArr.object(at: i) as! String == "Dhanurasana" {
                YogaList?.add(MCLocalization.string(forKey: "Dhanurasana"))
            }
            if YogsanArr.object(at: i) as! String == "Balasana" {
                YogaList?.add(MCLocalization.string(forKey: "Balasana"))
            }
            if YogsanArr.object(at: i) as! String == "Hastapadasana" {
                YogaList?.add(MCLocalization.string(forKey: "Hastapadasana"))
            }
            if YogsanArr.object(at: i) as! String == "Marjariasana" {
                YogaList?.add(MCLocalization.string(forKey: "Marjariasana"))
            }
            if YogsanArr.object(at: i) as! String == "Uttanasana" {
                YogaList?.add(MCLocalization.string(forKey: "Uttanasana"))
            }
            if YogsanArr.object(at: i) as! String == "Setu Bandhasana" {
                YogaList?.add(MCLocalization.string(forKey: "Setu Bandhasana"))
            }
            if YogsanArr.object(at: i) as! String == "Virabhadrasana" {
                YogaList?.add(MCLocalization.string(forKey: "Virabhadrasana"))
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.disableSlidePanGestureForRightMenu()
    }
    
    func TopviewMethod() -> Void {
        
        Topview  = UIView(frame: CGRect(x: 5, y: 5, width: self.view.frame.size.width-10, height: self.view.frame.size.height/10))
        Topview.layer.borderWidth=1;
        Topview.layer.borderColor=UIColor.init(red: 200/255, green: 200/255, blue: 200/255, alpha: 1.0).cgColor
        scrollview.addSubview(Topview)
        
        var  Reportdate : NSDate!
        let index = indexpath
        if reportArr.count>0 && index>0 {
            let din = reportArr.object(at: index-1) as! DincharyaReport
            Reportdate = din.date as NSDate!
        }
        else{
            Reportdate = Date() as NSDate!
        }
        
        let dateformate = DateFormatter()
        dateformate.dateFormat="MMMM dd";
        let startdatestr = dateformate.string(from: Reportdate as Date)
        let enddate = NSDate.init(timeInterval: 6*24*60*60, since: Reportdate as Date)
        let enddatestr = dateformate.string(from: enddate as Date)
        
        TimeLbl = UILabel(frame: CGRect(x: 15, y: 0, width: Topview.frame.size.width-115, height: Topview.frame.size.height))
        TimeLbl.text = "\(startdatestr) - \(enddatestr)"
        TimeLbl.font=Ralewayfont(Font12)
        Topview.addSubview(TimeLbl)
   
        let leftBtn = UIButton(frame: CGRect(x: Topview.frame.size.width-100, y: 0, width: 50, height: Topview.frame.size.height))
        leftBtn.setImage(UIImage.init(named:"left_arrow-1"), for: .normal)
        leftBtn.imageEdgeInsets = UIEdgeInsetsMake(15, 15, 15, 15)
        leftBtn.addTarget(self, action: #selector(self.LeftRightBtnMethod(_:)), for: .touchUpInside)
        leftBtn.tag=1
        Topview.addSubview(leftBtn)
        
        let rightBtn = UIButton(frame: CGRect(x:Topview.frame.size.width-50 , y: 0, width: 50, height: Topview.frame.size.height))
        rightBtn.setImage(UIImage.init(named: "right_arrow-1"), for: .normal)
        rightBtn.imageEdgeInsets = UIEdgeInsetsMake(15, 15, 15, 15)
        rightBtn.addTarget(self, action: #selector(self.LeftRightBtnMethod(_:)), for: .touchUpInside)
        rightBtn.tag=2
        Topview.addSubview(rightBtn)
    }
    
    
    ////////////////// Left Right button Method //////////
    func LeftRightBtnMethod(_ sender : UIButton) -> Void {
        if sender.tag == 1 {
            if  switchValue == 0 {
                return;
            }
            else{
                indexpath=indexpath+7;
                switchValue -= 1;
                for view in Topview.subviews {
                    view.removeFromSuperview()
                }
                for view in MainView.subviews {
                    view.removeFromSuperview()
                }
                self.TopviewMethod()
                self.CustumDesignView()
            }
        }
        else{
            if switchValue == maxswitch {
                return;
            }
            else{
                indexpath=indexpath-7;
                switchValue += 1;
                for view in Topview.subviews {
                    view.removeFromSuperview()
                }
                for view in MainView.subviews {
                    view.removeFromSuperview()
                }
                self.TopviewMethod()
                self.CustumDesignView()
            }
            
        }
    }
    
    
    ///////////////////////////////////////////////////////////////////
    ////////// Main VIew Design Method//////////////
    
    func CustumDesignView() -> Void {
        let arCount = YogsanArr.count+PranayamaArr.count
        let count = CGFloat(arCount)
        
        ////////////  Main view ////////
        
        let comonGraycolor = UIColor.init(red: 225/255, green: 225/255, blue: 225/255, alpha: 1.0)
        
        let W = self.view.frame.size.width-10
        
        MainView = UIView(frame: CGRect(x: 5, y: Topview.frame.origin.y+Topview.frame.size.height+10, width: W, height: count*(W-6)/9+(W-6)/3))
        MainView.layer.borderWidth=1
        MainView.layer.borderColor=comonGraycolor.cgColor
        scrollview.addSubview(MainView)
 
        
        ////////////  Yogasana and Pranayama Titel view /////////////
        let pranaYogaArrVIew  =  UIView(frame: CGRect(x: 3, y:(MainView.frame.size.width-6)/9 , width:((MainView.frame.size.width-6)/9)*1.5, height:count*((MainView.frame.size.width-6)/9)))
        MainView.addSubview(pranaYogaArrVIew)
        pranaYogaArrVIew.layer.borderColor=comonGraycolor.cgColor
        pranaYogaArrVIew.layer.borderWidth=1

        for i in 0..<arCount {
            let YPlbl = UILabel(frame: CGRect(x: 0, y: CGFloat(i)*((MainView.frame.size.width-6)/9), width: pranaYogaArrVIew.frame.size.width, height: ((MainView.frame.size.width-6)/9)))
            if i<pranaList.count{
                YPlbl.text = pranaList.object(at: i) as? String
            }
            else{
                
                YPlbl.text = YogaList.object(at: i-pranaList.count) as? String
            }
            YPlbl.font = Ralewayfont(Font8)
            YPlbl.textAlignment = NSTextAlignment.center
            YPlbl.numberOfLines=0
            let line = UILabel(frame: CGRect(x: 0, y: YPlbl.frame.size.height-1, width: YPlbl.frame.size.width, height: 1))
            line.backgroundColor = comonGraycolor
            YPlbl.addSubview(line)
            
            pranaYogaArrVIew.addSubview(YPlbl)
        }
        
        //////////////// Day View ////////////////////
        
        let DayView = UIView(frame: CGRect(x: pranaYogaArrVIew.frame.origin.x+pranaYogaArrVIew.frame.size.width, y: 0, width: MainView.frame.size.width-pranaYogaArrVIew.frame.size.width-6, height: ((MainView.frame.size.width-6)/9)))
        MainView.addSubview(DayView)
        
        
        ////////////////   Start date for shedule interval ////////
        var  Reportdate : NSDate = NSDate()
        var index = indexpath
        if reportArr.count>0 && index>0 {
            let din = reportArr.object(at: index-1) as! DincharyaReport
            Reportdate = din.date as NSDate!
        }
        else{
            Reportdate = Date() as NSDate!
        }

        var date = Reportdate
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EE"
        for i in 0...6 {
            let datestr = dateFormatter.string(from: date as Date)
            let  dayLbl = UILabel(frame: CGRect(x: CGFloat(i)*(DayView.frame.size.width/7), y: 0, width: DayView.frame.size.width/7, height: DayView.frame.size.height))
            dayLbl.text = datestr
            dayLbl.font = Ralewayfont(Font10)
            dayLbl.textAlignment=NSTextAlignment.center
            DayView.addSubview(dayLbl)

            date = NSDate.init(timeInterval: 24*60*60, since: date as Date)
        }
        
        let ThumbMainView = UIView(frame: CGRect(x:DayView.frame.origin.x , y: DayView.frame.size.height, width: DayView.frame.size.width, height: pranaYogaArrVIew.frame.size.height))
        ThumbMainView.layer.borderColor=comonGraycolor.cgColor
        ThumbMainView.layer.borderWidth=1
        let thumbHeight = ThumbMainView.frame.size.height/CGFloat(arCount)
        MainView.addSubview(ThumbMainView)
        
        for i in 0...6 {
            var width : CGFloat = 2
            if i == 0 {
                width = 0
            }
            let varLine = UILabel(frame: CGRect(x: CGFloat(i)*ThumbMainView.frame.size.width/7, y:0 , width: width, height: ThumbMainView.frame.size.height))
            varLine.backgroundColor=comonGraycolor
            ThumbMainView.addSubview(varLine)
            
            if index>0 && reportArr.count>0 {
            let DincharyaRpt = reportArr.object(at: index-1) as! DincharyaReport
                
                for j in 0..<arCount {
                    let image = UIImageView(frame: CGRect(x: varLine.frame.origin.x, y: CGFloat(j)*thumbHeight, width: (ThumbMainView.frame.size.width/7), height:thumbHeight-1))
                    if j<PranayamaArr.count{
                        if self.CheckPranayama(Prana: (PranayamaArr.object(at: j) as? NSString)!, din: DincharyaRpt) {
                            image.image = UIImage.init(named: "thumbs_up")
                        }
                        else{
                            image.image = UIImage.init(named: "thumbs_down")
                        }
                    }
                    else{
                        if self.CheckYogasan(Yoga: (YogsanArr.object(at: j-PranayamaArr.count) as? NSString)!, din: DincharyaRpt) {
                            image.image = UIImage.init(named: "thumbs_up")
                        }
                        else{
                            image.image = UIImage.init(named: "thumbs_down")
                        }
                    }
                    ThumbMainView.addSubview(image)
                    let line = UILabel(frame: CGRect(x: image.frame.origin.x, y: image.frame.origin.y+image.frame.size.height, width: image.frame.size.width, height: 1))
                    line.backgroundColor=comonGraycolor
                    ThumbMainView.addSubview(line)
                }
                index -= 1
            }
        }
        
        
        //////////////////    Date view /////////////
        let DateView = UIView(frame: CGRect(x: DayView.frame.origin.x, y: ThumbMainView.frame.origin.y+ThumbMainView.frame.size.height, width: DayView.frame.size.width, height: DayView.frame.size.height))
        MainView.addSubview(DateView)
        
        /////////////////  start date of schedule interval //////////
        date = Reportdate
        dateFormatter.dateFormat = "dd"
        for i in 0...6 {
            let datestr = dateFormatter.string(from: date as Date)
            let  dateLbl = UILabel(frame: CGRect(x: CGFloat(i)*(DateView.frame.size.width/7), y: 0, width: DateView.frame.size.width/7, height: DateView.frame.size.height))
            dateLbl.text = datestr
            dateLbl.font = Ralewayfont(Font10)
            dateLbl.textAlignment=NSTextAlignment.center
            DateView.addSubview(dateLbl)
            
            date = NSDate.init(timeInterval: 24*60*60, since: date as Date)
        }
        
        //////////  
        let datelbl = UILabel(frame: CGRect(x: DateView.frame.origin.x, y: DateView.frame.origin.y+DateView.frame.size.height, width: DateView.frame.size.width, height: DateView.frame.size.height))
        datelbl.text=datestr
        datelbl.font=Ralewayfont(Font14)
        datelbl.textAlignment=NSTextAlignment.center
        MainView.addSubview(datelbl)
        
        scrollview.contentSize = CGSize(width:self.view.frame.size.width , height: MainView.frame.origin.y+MainView.frame.size.height+5)
        
    }
    
    func CheckYogasan(Yoga : NSString , din : DincharyaReport) -> Bool {
        var value = false
        if Yoga == "Sarvangasana" && din.sarvangasana == true{
            value = true
        }
        if Yoga == "Halasana" && din.halasana == true{
            value = true
        }
        
        if Yoga == "Vipritkarani" && din.vipritkarani == true{
            value = true
        }
        if Yoga == "Paschimottanasana" && din.paschimottanasana == true{
            value = true
        }
        if Yoga == "Dhanurasana" && din.dhanurasana == true{
            value = true
        }
        if Yoga == "Balasana" && din.balasana == true{
            value = true
        }
        if Yoga == "Hastapadasana" && din.hastapadasana == true{
            value = true
        }
        if Yoga == "Marjariasana" && din.marjariasana == true{
            value = true
        }
        if Yoga == "Uttanasana" && din.uttanasana == true{
            value = true
        }
        if Yoga == "Setu Bandhasana" && din.setu == true{
            value = true
        }
        if Yoga == "Virabhadrasana" && din.virabhadrasana == true{
            value = true
        }
        
        return value
    }
    
    func CheckPranayama(Prana : NSString , din : DincharyaReport) -> Bool {
        var value = false
        if Prana == "AnulomVilom" && din.anulomvilom == true{
            value = true
        }
        if Prana == "Kapalbhati" && din.kapalbhati == true{
            value = true
        }
        
        if Prana == "Bhramari" && din.bhramari == true{
            value = true
        }
        if Prana == "Surya Bhedana" && din.surya == true{
            value = true
        }
        if Prana == "Chandra Bhedana" && din.chandra == true{
            value = true
        }
        if Prana == "Bhastrika" && din.bhastrika == true{
            value = true
        }
        if Prana == "Sheetali" && din.sheetali == true{
            value = true
        }
        if Prana == "Ujjayi" && din.ujjayi == true{
            value = true
        }
        if Prana == "Meditative Breathing" && din.meditative == true{
            value = true
        }
        if Prana == "Udgeeth" && din.udgeeth == true{
            value = true
        }
        if Prana == "Bahya" && din.bahya == true{
            value = true
        }
        
        return value
    }
    
    
    
    @IBAction func backbutton(_ sender: AnyObject) {
        self.navigationController!.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  MCLocalizationOneJSONFilePerLanguageDataSource.h
//  Pranayama
//
//  Created by Manish Kumar on 02/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MCLocalizationDataSource.h"

@interface MCLocalizationOneJSONFilePerLanguageDataSource : NSObject <MCLocalizationDataSource>

- (id)initWithLanguageURLPairs:(NSDictionary *)languageURLPairs defaultLanguage:(NSString *)defaultLanguage;

@end

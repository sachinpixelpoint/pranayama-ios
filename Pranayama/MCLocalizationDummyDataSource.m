//
//  MCLocalizationDummyDataSource.m
//  Pranayama
//
//  Created by Manish Kumar on 02/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "MCLocalizationDummyDataSource.h"

@implementation MCLocalizationDummyDataSource

- (NSArray *)supportedLanguages
{
    return @[@"en"];
}

- (NSString *)defaultLanguage
{
    return @"en";
}

- (NSDictionary *)stringsForLanguage:(NSString *)language
{
    return @{};
}

@end

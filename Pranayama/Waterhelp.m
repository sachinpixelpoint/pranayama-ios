//
//  Waterhelp.m
//  Pranayama
//
//  Created by Manish Kumar on 8/29/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "Waterhelp.h"

@interface Waterhelp ()

@end

@implementation Waterhelp

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName=@"water_help";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    int fontSize;
    if (DEVICE==IPAD) {
        fontSize=24;
    }
    else{
        fontSize=18;
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self WaterContents];
   
    }


- (void)HomeMethod:(NSNotification *)notification {
    [[CommonSounds sharedInstance] appDidBecomeActive:notification navigation:self.navigationController];
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self disableSlidePanGestureForRightMenu];
}

-(UILabel *)WaterContents{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    
    self.navigationItem.title = [MCLocalization stringForKey:@"help"];
    label1.text=[MCLocalization stringForKey:@"water_help1"];
    label2.text=[MCLocalization stringForKey:@"water_help2"];
    label3.text=[MCLocalization stringForKey:@"water_help3"];
    label4.text=[MCLocalization stringForKey:@"water_help4"];
    label5.text=[MCLocalization stringForKey:@"water_help5"];
    label1.font=Ralewayfont(14);
    label2.font=Ralewayfont(18);
    label3.font=Ralewayfont(14);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(14);

    label1.numberOfLines=0;
    [label1 sizeToFit];
    [ScrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    label2.textColor = [UIColor redColor];
    [ScrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    [ScrollView addSubview:label3];
    
    label4.frame=CGRectMake(20, label3.frame.origin.y+label3.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [ScrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [ScrollView addSubview:label5];
    return label5;
}

-(IBAction)backbutton:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  MudraViewController.m
//  Pranayama
//
//  Created by Manish Kumar on 22/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "MudraViewController.h"

@interface MudraViewController ()

@end

@implementation MudraViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    self.screenName=@"Mudras";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:20]}];
    self.navigationItem.hidesBackButton=YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    // Do any additional setup after loading the view.
    [self content];
}


- (void)HomeMethod:(NSNotification *)notification {
    [[CommonSounds sharedInstance] appDidBecomeActive:notification navigation:self.navigationController];
}


-(void)content{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label8 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label9 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label10 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label11 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label12 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label13 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label14 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label15 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label16 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label17 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label18 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label19 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label20 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label21 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label22 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label23 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label24 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label25 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label26 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label27 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label28 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label29 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label30 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label31 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label32 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    UILabel *label33 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, scrollView.frame.size.width-20, 100)];
    label1.font=Ralewayfont(14);
    label2.font=Ralewayfont(18);
    label3.font=Ralewayfont(14);
    label4.font=Ralewayfont(18);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(18);
    label7.font=Ralewayfont(14);
    label8.font=Ralewayfont(18);
    label9.font=Ralewayfont(14);
    label10.font=Ralewayfont(18);
    label11.font=Ralewayfont(14);
    label12.font=Ralewayfont(18);
    label13.font=Ralewayfont(14);
    label14.font=Ralewayfont(18);
    label15.font=Ralewayfont(14);
    label16.font=Ralewayfont(18);
    label17.font=Ralewayfont(14);
    label18.font=Ralewayfont(18);
    label19.font=Ralewayfont(14);
    label20.font=Ralewayfont(18);
    label21.font=Ralewayfont(14);
    label22.font=Ralewayfont(18);
    label23.font=Ralewayfont(14);
    label24.font=Ralewayfont(18);
    label25.font=Ralewayfont(14);
    label26.font=Ralewayfont(18);
    label27.font=Ralewayfont(14);
    label28.font=Ralewayfont(18);
    label29.font=Ralewayfont(14);
    label30.font=Ralewayfont(18);
    label31.font=Ralewayfont(14);
    label32.font=Ralewayfont(18);
    label33.font=Ralewayfont(14);
    self.navigationItem.title = [MCLocalization stringForKey:@"mudras"];
    label1.text=[MCLocalization stringForKey:@"mudra_content"];
    label2.text=[MCLocalization stringForKey:@"mudra_content2"];
    label3.text=[MCLocalization stringForKey:@"mudra_content3"];
    label4.text=[MCLocalization stringForKey:@"benefit"];
    label5.text=[MCLocalization stringForKey:@"mudra_content4"];
    label6.text=[MCLocalization stringForKey:@"mudra_content5"];
    label7.text=[MCLocalization stringForKey:@"mudra_content6"];
    label8.text=[MCLocalization stringForKey:@"benefit"];
    label9.text=[MCLocalization stringForKey:@"mudra_content7"];
    label10.text=[MCLocalization stringForKey:@"mudra_content8"];
    label11.text=[MCLocalization stringForKey:@"mudra_content9"];
    label12.text=[MCLocalization stringForKey:@"benefit"];
    label13.text=[MCLocalization stringForKey:@"mudra_content10"];
    label14.text=[MCLocalization stringForKey:@"mudra_content11"];
    label15.text=[MCLocalization stringForKey:@"mudra_content12"];
    label16.text=[MCLocalization stringForKey:@"benefit"];
    label17.text=[MCLocalization stringForKey:@"mudra_content13"];
    label18.text=[MCLocalization stringForKey:@"mudra_content14"];
    label19.text=[MCLocalization stringForKey:@"mudra_content15"];
    label20.text=[MCLocalization stringForKey:@"benefit"];
    label21.text=[MCLocalization stringForKey:@"mudra_content16"];
    label22.text=[MCLocalization stringForKey:@"mudra_content17"];
    label23.text=[MCLocalization stringForKey:@"mudra_content18"];
    label24.text=[MCLocalization stringForKey:@"benefit"];
    label25.text=[MCLocalization stringForKey:@"mudra_content19"];
    label26.text=[MCLocalization stringForKey:@"mudra_content20"];
    label27.text=[MCLocalization stringForKey:@"mudra_content21"];
    label28.text=[MCLocalization stringForKey:@"benefit"];
    label29.text=[MCLocalization stringForKey:@"mudra_content22"];
    label30.text=[MCLocalization stringForKey:@"mudra_content23"];
    label31.text=[MCLocalization stringForKey:@"mudra_content24"];
    label32.text=[MCLocalization stringForKey:@"benefit"];
    label33.text=[MCLocalization stringForKey:@"mudra_content25"];
    
    NSString *string=[MCLocalization stringForKey:@"mudra_link"];

    label1.numberOfLines=0;
    [label1 sizeToFit];
    [scrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    label2.textColor=[UIColor redColor];
    [scrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    [scrollView addSubview:label3];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-40, label3.frame.origin.y+label3.frame.size.height+10, 80, 60)];
    imageView1.image = [UIImage imageNamed:@"gyanmudra"];
    [scrollView addSubview:imageView1];
    
    label4.frame=CGRectMake(10, imageView1.frame.origin.y+imageView1.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    label4.textColor=[UIColor redColor];
    [scrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [scrollView addSubview:label5];
    
    label6.frame=CGRectMake(10, label5.frame.origin.y+label5.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    label6.textColor=[UIColor redColor];
    [scrollView addSubview:label6];
    
    label7.frame=CGRectMake(10, label6.frame.origin.y+label6.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [scrollView addSubview:label7];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-40, label7.frame.origin.y+label7.frame.size.height+10, 80, 60)];
    imageView2.image = [UIImage imageNamed:@"vayumudra"];
    [scrollView addSubview:imageView2];
    
    label8.frame=CGRectMake(10, imageView2.frame.origin.y+imageView2.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label8.numberOfLines=0;
    [label8 sizeToFit];
    label8.textColor=[UIColor redColor];
    [scrollView addSubview:label8];
    
    label9.frame=CGRectMake(10, label8.frame.origin.y+label8.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label9.numberOfLines=0;
    [label9 sizeToFit];
    [scrollView addSubview:label9];
    
    label10.frame=CGRectMake(10, label9.frame.origin.y+label9.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label10.numberOfLines=0;
    [label10 sizeToFit];
    label10.textColor=[UIColor redColor];
    [scrollView addSubview:label10];
    
    label11.frame=CGRectMake(10, label10.frame.origin.y+label10.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label11.numberOfLines=0;
    [label11 sizeToFit];
    [scrollView addSubview:label11];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-40, label11.frame.origin.y+label11.frame.size.height+10, 80, 60)];
    imageView3.image = [UIImage imageNamed:@"akashmudra"];
    [scrollView addSubview:imageView3];
    
    label12.frame=CGRectMake(10, imageView3.frame.origin.y+imageView3.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label12.numberOfLines=0;
    [label12 sizeToFit];
    label12.textColor=[UIColor redColor];
    [scrollView addSubview:label12];
    
    label13.frame=CGRectMake(10, label12.frame.origin.y+label12.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label13.numberOfLines=0;
    [label13 sizeToFit];
    [scrollView addSubview:label13];
    
    label14.frame=CGRectMake(10, label13.frame.origin.y+label13.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label14.numberOfLines=0;
    [label14 sizeToFit];
    label14.textColor=[UIColor redColor];
    [scrollView addSubview:label14];
    
    label15.frame=CGRectMake(10, label14.frame.origin.y+label14.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label15.numberOfLines=0;
    [label15 sizeToFit];
    [scrollView addSubview:label15];
    
    UIImageView *imageView4 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-40, label15.frame.origin.y+label15.frame.size.height+10, 80, 60)];
    imageView4.image = [UIImage imageNamed:@"shunyamudra"];
    [scrollView addSubview:imageView4];
    
    label16.frame=CGRectMake(10, imageView4.frame.origin.y+imageView4.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label16.numberOfLines=0;
    [label16 sizeToFit];
    label16.textColor=[UIColor redColor];
    [scrollView addSubview:label16];
    
    label17.frame=CGRectMake(10, label16.frame.origin.y+label16.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label17.numberOfLines=0;
    [label17 sizeToFit];
    [scrollView addSubview:label17];
    
    label18.frame=CGRectMake(10, label17.frame.origin.y+label17.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label18.numberOfLines=0;
    [label18 sizeToFit];
    label18.textColor=[UIColor redColor];
    [scrollView addSubview:label18];
    
    label19.frame=CGRectMake(10, label18.frame.origin.y+label18.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label19.numberOfLines=0;
    [label19 sizeToFit];
    [scrollView addSubview:label19];
    
    UIImageView *imageView5 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-40, label19.frame.origin.y+label19.frame.size.height+10, 80, 60)];
    imageView5.image = [UIImage imageNamed:@"prithvimudra"];
    [scrollView addSubview:imageView5];
    
    label20.frame=CGRectMake(10, imageView5.frame.origin.y+imageView5.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label20.numberOfLines=0;
    [label20 sizeToFit];
    label20.textColor=[UIColor redColor];
    [scrollView addSubview:label20];
    
    label21.frame=CGRectMake(10, label20.frame.origin.y+label20.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label21.numberOfLines=0;
    [label21 sizeToFit];
    [scrollView addSubview:label21];
    
    label22.frame=CGRectMake(10, label21.frame.origin.y+label21.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label22.numberOfLines=0;
    [label22 sizeToFit];
    label22.textColor=[UIColor redColor];
    [scrollView addSubview:label22];
    
    label23.frame=CGRectMake(10, label22.frame.origin.y+label22.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label23.numberOfLines=0;
    [label23 sizeToFit];
    [scrollView addSubview:label23];
    
    UIImageView *imageView6 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-40, label23.frame.origin.y+label23.frame.size.height+10, 80, 60)];
    imageView6.image = [UIImage imageNamed:@"agnimudra"];
    [scrollView addSubview:imageView6];
    
    label24.frame=CGRectMake(10, imageView6.frame.origin.y+imageView6.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label24.numberOfLines=0;
    [label24 sizeToFit];
    label24.textColor=[UIColor redColor];
    [scrollView addSubview:label24];
    
    label25.frame=CGRectMake(10, label24.frame.origin.y+label24.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label25.numberOfLines=0;
    [label25 sizeToFit];
    [scrollView addSubview:label25];
    
    label26.frame=CGRectMake(10, label25.frame.origin.y+label25.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label26.numberOfLines=0;
    [label26 sizeToFit];
    label26.textColor=[UIColor redColor];
    [scrollView addSubview:label26];
    
    label27.frame=CGRectMake(10, label26.frame.origin.y+label26.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label27.numberOfLines=0;
    [label27 sizeToFit];
    [scrollView addSubview:label27];
    
    UIImageView *imageView7 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-40, label27.frame.origin.y+label27.frame.size.height+10, 80, 60)];
    imageView7.image = [UIImage imageNamed:@"varunamudra"];
    [scrollView addSubview:imageView7];
    
    label28.frame=CGRectMake(10, imageView7.frame.origin.y+imageView7.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label28.numberOfLines=0;
    [label28 sizeToFit];
    label28.textColor=[UIColor redColor];
    [scrollView addSubview:label28];
    
    label29.frame=CGRectMake(10, label28.frame.origin.y+label28.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label29.numberOfLines=0;
    [label29 sizeToFit];
    [scrollView addSubview:label29];
    
    label30.frame=CGRectMake(10, label29.frame.origin.y+label29.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label30.numberOfLines=0;
    [label30 sizeToFit];
    label30.textColor=[UIColor redColor];
    [scrollView addSubview:label30];
    
    label31.frame=CGRectMake(10, label30.frame.origin.y+label30.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label31.numberOfLines=0;
    [label31 sizeToFit];
    [scrollView addSubview:label31];
    
    UIImageView *imageView8 = [[UIImageView alloc] initWithFrame:CGRectMake((scrollView.frame.size.width/2)-40, label31.frame.origin.y+label31.frame.size.height+10, 80, 60)];
    imageView8.image = [UIImage imageNamed:@"pranmudra"];
    [scrollView addSubview:imageView8];
    
    label32.frame=CGRectMake(10, imageView8.frame.origin.y+imageView8.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label32.numberOfLines=0;
    [label32 sizeToFit];
    label32.textColor=[UIColor redColor];
    [scrollView addSubview:label32];
    
    label33.frame=CGRectMake(10, label32.frame.origin.y+label32.frame.size.height+10, scrollView.frame.size.width-20, 100);
    label33.numberOfLines=0;
    [label33 sizeToFit];
    [scrollView addSubview:label33];
    
    FRHyperLabel *label = [FRHyperLabel new];
    label.frame=CGRectMake(10,label33.frame.origin.y+label33.frame.size.height , scrollView.frame.size.width-20, 200);
    label.numberOfLines=100;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]};
    
    
    label.attributedText = [[NSAttributedString alloc]initWithString:string attributes:attributes];
    label.font=[UIFont systemFontOfSize:16];
    
    [label setLinkForSubstring:@"www.7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.7pranayama.com/"]];
    }];
    [label setLinkForSubstring:@"info@7pranayama.com" withLinkHandler:^(FRHyperLabel  *label, NSString  *substring){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://info@7pranayama.com/"]];
    }];
    
    [scrollView addSubview:label];

    scrollView.contentSize=CGSizeMake(scrollView.frame.size.width,label33.frame.origin.y+label33.frame.size.height+label.frame.size.height+100);
    
    int scrollHeight=label33.frame.origin.y+label33.frame.size.height+label.frame.size.height;
    int a=self.view.frame.size.width/4;
    int b=a/2;
    
    UIButton*btn1=[[UIButton alloc] initWithFrame:CGRectMake(a/2-25, scrollHeight+20, 50, 50)];
    btn1.tag=1;
    btn1.layer.cornerRadius = btn1.bounds.size.width / 2.0;
    UIImage *btnImage1 = [UIImage imageNamed:@"facebook"];
    [btn1 setImage:btnImage1 forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(goToLink:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:btn1];
    
    UIButton*btn2=[[UIButton alloc] initWithFrame:CGRectMake(((2*a)-b)-25, scrollHeight+20, 50, 50)];
    btn2.tag=2;
    btn2.layer.cornerRadius = btn1.bounds.size.width / 2.0;
    UIImage *btnImage2 = [UIImage imageNamed:@"twitter"];
    [btn2 setImage:btnImage2 forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(goToLink:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:btn2];
    
    UIButton*btn3=[[UIButton alloc] initWithFrame:CGRectMake(((3*a)-b)-25, scrollHeight+20, 50, 50)];
    btn3.tag=3;
    btn3.layer.cornerRadius = btn1.bounds.size.width / 2.0;
    UIImage *btnImage3 = [UIImage imageNamed:@"pinterest"];
    [btn3 setImage:btnImage3 forState:UIControlStateNormal];
    [btn3 addTarget:self action:@selector(goToLink:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:btn3];
    
    UIButton*btn4=[[UIButton alloc] initWithFrame:CGRectMake((self.view.frame.size.width-b)-25,scrollHeight+20, 50, 50)];
    btn4.tag=4;
    btn4.layer.cornerRadius = btn1.bounds.size.width / 2.0;
    UIImage *btnImage4 = [UIImage imageNamed:@"google"];
    [btn4 setImage:btnImage4 forState:UIControlStateNormal];
    [btn4 addTarget:self action:@selector(goToLink:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:btn4];
    
}

-(void)goToLink:(UIButton*)sender{
    if (sender.tag==1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.facebook.com/7pranayama-1742577192620397/"]];
    }
    else if (sender.tag==2){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://twitter.com/7pranayama"]];
        
    }
    else if (sender.tag==3){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://in.pinterest.com/7pranayama/"]];
        
    }
    else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://plus.google.com/111008006040750995181/posts"]];
    }
}

-(IBAction)backbutton:(id)sender{
    [[CommonSounds sharedInstance] NavigationMethod:self.navigationController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

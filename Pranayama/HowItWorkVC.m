//
//  HowItWorkVC.m
//  Pranayama
//
//  Created by Manish Kumar on 14/06/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "HowItWorkVC.h"

@interface HowItWorkVC ()

@end

@implementation HowItWorkVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
   
    self.screenName=@"How it Work";
    // Do any additional setup after loading the view.
     self.navigationController.navigationBar.tintColor =RGB(230, 123, 27);
    self.navigationItem.hidesBackButton=YES;
    [self setText];
    habbitType=[[NSUserDefaults standardUserDefaults] objectForKey:HabbitType];
    int fontSize;
    if (DEVICE==IPAD) {
        fontSize=24;
        inFontSize=23;
        OkfontSize=28;
        dotFontsize=40;
        if (self.view.frame.size.height==1366) {
            FbutonSize=80;
        }
        else{
            FbutonSize=60;
        }
    }
    else{
        FbutonSize=50;
        fontSize=18;
        inFontSize=10;
        OkfontSize=18;
        dotFontsize=24;
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    self.automaticallyAdjustsScrollViewInsets = NO;
    window = [UIApplication sharedApplication].keyWindow;
    [self setValuesFromKey];
    [self setVlueOfdata];
    [self setarrayForKapal];
    [self content];
    
    /////////////floatingbutton////////
    floatingButton=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width-FbutonSize-20, self.view.frame.size.height-FbutonSize-20, FbutonSize, FbutonSize)];
    floatingButton.layer.cornerRadius=floatingButton.bounds.size.width/2;
    floatingButton.backgroundColor=RGB(143, 0, 0);
    NSString  *habbitCondition=[[NSUserDefaults standardUserDefaults] objectForKey:HabbitCondition];
    if ([habbitType isEqualToString:habbitCondition]) {
        [floatingButton setImage:[UIImage imageNamed:@"end_habbit"] forState:UIControlStateNormal];
    }
    else {
        [floatingButton setImage:[UIImage imageNamed:@"start_habbit"] forState:UIControlStateNormal];
    }
    floatingButton.imageEdgeInsets= UIEdgeInsetsMake(7,7,7,7);
    [floatingButton addTarget:self action:@selector(floatingButtonMethod) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:floatingButton];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    singleFingerTap.delegate=self;
    [window addGestureRecognizer:singleFingerTap];
}

/////////tap gesture/////
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return  (touch.view == blackOverlay);
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    view1.hidden=YES;
}
////////

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self disableSlidePanGestureForRightMenu];
}


- (void)HomeMethod:(NSNotification *)notification {
    [[CommonSounds sharedInstance] appDidBecomeActive:notification navigation:self.navigationController];
}



-(void)setText{
    
    R_U_sure=[MCLocalization stringForKey:@"sure"];
    cancle_Habbit=[MCLocalization stringForKey:@"cancel_habbit"];
    yes=[MCLocalization stringForKey:@"ha"];
    no=[MCLocalization stringForKey:@"nahi"];
    warning=[MCLocalization stringForKey:@"Alert"];
    warning_text=[MCLocalization stringForKey:@"AlertDialog"];
    setRatio=[MCLocalization stringForKey:@"Ratio"];
    set=[MCLocalization stringForKey:@"set"];
    cancel=[MCLocalization stringForKey:@"Cancel"];
    inhalestr=[MCLocalization stringForKey:@"Inhale"];
    holdstr=[MCLocalization stringForKey:@"Hold"];
    exhalestr=[MCLocalization stringForKey:@"Exhale"];
    roundstr=[MCLocalization stringForKey:@"Rounds"];
    anulom= [MCLocalization stringForKey:@"Anulom_Vilom"];
    Ujjayi= [MCLocalization stringForKey:@"ujjayi"];
    surya= [MCLocalization stringForKey:@"surya_bhedana"];
    bhramari= [MCLocalization stringForKey:@"bhramari"];
    bhastrika= [MCLocalization stringForKey:@"bhastrika"];
    kapal= [MCLocalization stringForKey:@"kapalbhati"];
    udgeeth=[MCLocalization stringForKey:@"udgeeth"];
    bahya=[MCLocalization stringForKey:@"bahya"];
    timestr=[MCLocalization stringForKey:@"time(min.)"];
    NotificationAleart=[MCLocalization stringForKey:@"notifiAleart"];
    
}

-(void)setValuesFromKey{
    kapaltimeValue=1;
    kapalroundValue=60;
    oldTimeValue=kapaltimeValue;
    RoundForAction=kapalroundValue;
    kapalroundRow=RoundForAction;
    InhaleRow=1;
    holdRow=4;
    exhaleRow=2;
    afHoldRow=0;
    roundRow=1;
    bhramariRoundRow=4;
    bhramariInhaleRow=5;
    bhramariExhaleRow=10;
    UdgeethinhaleRow=4;
    UdgeethexhalORow=12;
    UdgeethexhaleMRow=4;
    bahyainhaleRow=1;
    bahyaexhaleRow=2;
    bahyaHoldRow=3;
}

-(void)setVlueOfdata{
    In_roundData = [[NSMutableArray alloc] init];
    exhaleData=[[NSMutableArray alloc] init];
    holdData=[[NSMutableArray alloc] init];
    afroundData=[[NSMutableArray alloc] init];
    bhramriInhlaeData=[[NSMutableArray alloc]init];
    bhramariExhaleData=[[NSMutableArray alloc]init];
    udgeethexhaleOData=[[NSMutableArray alloc]init];
    bahyaholdData=[[NSMutableArray alloc]init];
    for (int j=1; j<=50; j++){
        [In_roundData addObject:[NSString stringWithFormat:@"%d",j]];
    }
    for (int j=0; j<=200; j++){
        [holdData addObject:[NSString stringWithFormat:@"%d",j]];
    }
    for (int j=1; j<=100; j++){
        [exhaleData addObject:[NSString stringWithFormat:@"%d",j]];
    }
    for (int j=0; j<=10; j++){
        [afroundData addObject:[NSString stringWithFormat:@"%d",j]];
    }
    for (int j=1; j<=20; j++) {
        [bhramriInhlaeData addObject:[NSString stringWithFormat:@"%d",j]];
    }
    for (int j=1; j<=40; j++) {
        [bhramariExhaleData addObject:[NSString stringWithFormat:@"%d",j]];
    }
    for (int j=1; j<=60; j++) {
        [udgeethexhaleOData addObject:[NSString stringWithFormat:@"%d",j]];
    }
    for (int j=1; j<=150; j++) {
        [bahyaholdData addObject:[NSString stringWithFormat:@"%d",j]];
    }
}


-(void)floatingButtonMethod {
    
    NSString *habbitCondition=[[NSUserDefaults standardUserDefaults] objectForKey:HabbitCondition];
    if ([habbitType isEqualToString:habbitCondition]) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:R_U_sure
                                     message:cancle_Habbit
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:yes
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:HabbitCondition];
                                        [ScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
                                        [floatingButton setImage:[UIImage imageNamed:@"start_habbit"] forState:UIControlStateNormal];
                                        [self content];
                                        [self deleteProgressReport];
                                    }];
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:no
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle no, thanks button
                                   }];
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if(habbitCondition==nil){
        [self checkHabbitType];
    }
    else{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:warning
                                     message:warning_text
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:yes
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        [self checkHabbitType];
                                        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:HabbitCondition];
                                        [self deleteProgressReport];
                                    }];
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:no
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle no, thanks button
                                   }];
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)checkHabbitType{
    if ([habbitType isEqualToString:@"Insomnia"]) {
        [self SuryaRatiosViewMethod];
    }
    else {
        [self AnulomRatioViewMethod];
        
    }
}
///////delete progress report////
-(void)deleteProgressReport{
    NSManagedObjectContext *managedObjectContext=[kAppDele managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"Progress" inManagedObjectContext:managedObjectContext]];
    [request setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error = nil;
    NSArray *dataArray = [managedObjectContext executeFetchRequest:request error:&error];
    //error handling goes here
    for (NSManagedObject *object in dataArray) {
        [managedObjectContext deleteObject:object];
    }
    NSError *saveError = nil;
    [managedObjectContext save:&saveError];
}


-(void)setarrayForKapal{
    kapatimeData=[[NSMutableArray alloc]init];
    kapalroundData=[[NSMutableArray alloc]init];
    kapaltimeRow=kapaltimeValue;
    int i=kapaltimeRow;
    for (int j=10*i; j<=150*i; j++){
        [kapalroundData addObject:[NSString stringWithFormat:@"%d",j]];
    }
    for (int j=1; j<=20; j++){
        [kapatimeData addObject:[NSString stringWithFormat:@"%d",j]];
    }
}

////////////////content method////////
-(void)content{
    UILabel *label1 = [[UILabel alloc]init];
    if ([habbitType isEqualToString:@"Thyroid"]) {
        label1=[self ThyroidContents];
    }
    else if ([habbitType isEqualToString:@"Insomnia"]){
        label1=[self InsomniaContents];
    }
    else if ([habbitType isEqualToString:@"Diabetes"]){
        label1=[self DiabetesContents];
    }
    else if ([habbitType isEqualToString:@"Migraine"])
    {
        label1=[self MigraineContents];
    }
    else if ([habbitType isEqualToString:@"Weight Loss"]){
        label1=[self weightContents];
    }
    else if ([habbitType isEqualToString:@"Asthma"]){
        label1=[self asthmaContents];
    }
    else{
        label1=[self jointContents];
    }

    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, ScrollView.frame.size.width-20, 100)];
    
    NSString *contentCondition=[[NSUserDefaults standardUserDefaults] objectForKey:HabbitCondition];
    if (![habbitType isEqualToString:contentCondition]) {
        
        label2.text = [MCLocalization stringForKey:@"common_work"];
        label2.numberOfLines=0;
        [label2 sizeToFit];
        [ScrollView addSubview:label2];
        
        ScrollView.contentSize=CGSizeMake(ScrollView.frame.size.width,label2.frame.origin.y+label2.frame.size.height+20);
    }
    else{
        label2.text = [MCLocalization stringForKey:@"change_in_time"];
        label2.numberOfLines=0;
        [label2 sizeToFit];
        [ScrollView addSubview:label2];
        
        datepickerBtn=[[UIButton alloc]initWithFrame:CGRectMake(ScrollView.frame.size.width/2-40, label2.frame.origin.y+label2.frame.size.height+10, 80, 30)];
        [datepickerBtn addTarget:self action:@selector(TimePickerMethod) forControlEvents:UIControlEventTouchUpInside];
        NSDate *date=[[NSUserDefaults standardUserDefaults] objectForKey:kldate];
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"hh:mma"];
        NSString *datestr=[dateformate stringFromDate:date];
        [datepickerBtn setTitle:datestr forState:UIControlStateNormal];
        [datepickerBtn setTitleColor:RGB(230, 123, 27) forState:UIControlStateNormal];
        [ScrollView addSubview:datepickerBtn];
        
        ScrollView.contentSize=CGSizeMake(ScrollView.frame.size.width,label2.frame.origin.y+label2.frame.size.height+50);
    }
}
    



    /////////////////
    
-(UILabel *)ThyroidContents{
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    
    self.navigationItem.title = [MCLocalization stringForKey:@"work"];
    label1.text=[MCLocalization stringForKey:@"thyoride_work1"];
    label2.text=[MCLocalization stringForKey:@"thyoride_work2"];
    label3.text=[MCLocalization stringForKey:@"thyoride_work3"];
    label4.text=[MCLocalization stringForKey:@"thyoride_work4"];
    label5.text=[MCLocalization stringForKey:@"thyoride_work5"];
    label6.text=[MCLocalization stringForKey:@"thyoride_work6"];
    label7.text=[MCLocalization stringForKey:@"thyoride_work7"];
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [ScrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    label2.textColor = [UIColor redColor];
    [ScrollView addSubview:label2];
    
    label3.frame=CGRectMake(20, label2.frame.origin.y+label2.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    [ScrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [ScrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    label5.textColor = [UIColor redColor];
    [label5 sizeToFit];
    [ScrollView addSubview:label5];
    
    label6.frame=CGRectMake(20, label5.frame.origin.y+label5.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [ScrollView addSubview:label6];
    
    label7.frame=CGRectMake(10, label6.frame.origin.y+label6.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [ScrollView addSubview:label7];
    return label7;
    
}
    
-(UILabel *)InsomniaContents{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    self.navigationItem.title = [MCLocalization stringForKey:@"work"];
    label1.text=[MCLocalization stringForKey:@"insomnia_work1"];
    label2.text=[MCLocalization stringForKey:@"insomnia_work2"];
    label3.text=[MCLocalization stringForKey:@"insomnia_work3"];
    label4.text=[MCLocalization stringForKey:@"insomnia_work4"];
    label5.text=[MCLocalization stringForKey:@"insomnia_work5"];
    label6.text=[MCLocalization stringForKey:@"insomnia_work6"];
    label7.text=[MCLocalization stringForKey:@"insomnia_work7"];
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [ScrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    label2.textColor = [UIColor redColor];
    [ScrollView addSubview:label2];
    
    label3.frame=CGRectMake(20, label2.frame.origin.y+label2.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    [ScrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [ScrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    label5.textColor = [UIColor redColor];
    [label5 sizeToFit];
    [ScrollView addSubview:label5];
    
    label6.frame=CGRectMake(20, label5.frame.origin.y+label5.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [ScrollView addSubview:label6];
    
    label7.frame=CGRectMake(10, label6.frame.origin.y+label6.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [ScrollView addSubview:label7];
    return label7;
    
    
}
    
-(UILabel *)DiabetesContents{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    self.navigationItem.title = [MCLocalization stringForKey:@"work"];
    label1.text=[MCLocalization stringForKey:@"diabites_work1"];
    label2.text=[MCLocalization stringForKey:@"diabites_work2"];
    label3.text=[MCLocalization stringForKey:@"diabites_work3"];
    label4.text=[MCLocalization stringForKey:@"diabites_work4"];
    label5.text=[MCLocalization stringForKey:@"diabites_work5"];
    label6.text=[MCLocalization stringForKey:@"diabites_work6"];
    label7.text=[MCLocalization stringForKey:@"diabites_work7"];
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [ScrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    label2.textColor = [UIColor redColor];
    [ScrollView addSubview:label2];
    
    label3.frame=CGRectMake(20, label2.frame.origin.y+label2.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    [ScrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [ScrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    label5.textColor = [UIColor redColor];
    [label5 sizeToFit];
    [ScrollView addSubview:label5];
    
    label6.frame=CGRectMake(20, label5.frame.origin.y+label5.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [ScrollView addSubview:label6];
    
    label7.frame=CGRectMake(10, label6.frame.origin.y+label6.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [ScrollView addSubview:label7];
    return label7;
    
}
    
-(UILabel *)MigraineContents{
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    self.navigationItem.title = [MCLocalization stringForKey:@"work"];
    label1.text=[MCLocalization stringForKey:@"migrain_work1"];
    label2.text=[MCLocalization stringForKey:@"migrain_work2"];
    label3.text=[MCLocalization stringForKey:@"migrain_work3"];
    label4.text=[MCLocalization stringForKey:@"migrain_work4"];
    label5.text=[MCLocalization stringForKey:@"migrain_work5"];
    label6.text=[MCLocalization stringForKey:@"migrain_work6"];
    label7.text=[MCLocalization stringForKey:@"migrain_work7"];
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [ScrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    label2.textColor = [UIColor redColor];
    [ScrollView addSubview:label2];
    
    label3.frame=CGRectMake(20, label2.frame.origin.y+label2.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    [ScrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [ScrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    label5.textColor = [UIColor redColor];
    [label5 sizeToFit];
    [ScrollView addSubview:label5];
    
    label6.frame=CGRectMake(20, label5.frame.origin.y+label5.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [ScrollView addSubview:label6];
    
    label7.frame=CGRectMake(10, label6.frame.origin.y+label6.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [ScrollView addSubview:label7];
    return label7;

}

    
-(UILabel *)weightContents{
    self.navigationItem.title = [MCLocalization stringForKey:@"work"];
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    label1.text=[MCLocalization stringForKey:@"weight_work1"];
    label2.text=[MCLocalization stringForKey:@"weight_work2"];
    label3.text=[MCLocalization stringForKey:@"weight_work3"];
    label4.text=[MCLocalization stringForKey:@"weight_work4"];
    label5.text=[MCLocalization stringForKey:@"weight_work5"];
    label6.text=[MCLocalization stringForKey:@"weight_work6"];
    label7.text=[MCLocalization stringForKey:@"weight_work7"];
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [ScrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    label2.textColor = [UIColor redColor];
    [ScrollView addSubview:label2];
    
    label3.frame=CGRectMake(20, label2.frame.origin.y+label2.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    [ScrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [ScrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    label5.textColor = [UIColor redColor];
    [label5 sizeToFit];
    [ScrollView addSubview:label5];
    
    label6.frame=CGRectMake(20, label5.frame.origin.y+label5.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [ScrollView addSubview:label6];
    
    label7.frame=CGRectMake(10, label6.frame.origin.y+label6.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [ScrollView addSubview:label7];
    return label7;
    
    
}

-(UILabel *)asthmaContents{
    self.navigationItem.title = [MCLocalization stringForKey:@"work"];
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    label1.text=[MCLocalization stringForKey:@"asthma_work1"];
    label2.text=[MCLocalization stringForKey:@"asthma_work2"];
    label3.text=[MCLocalization stringForKey:@"asthma_work3"];
    label4.text=[MCLocalization stringForKey:@"asthma_work4"];
    label5.text=[MCLocalization stringForKey:@"asthma_work5"];
    label6.text=[MCLocalization stringForKey:@"asthma_work6"];
    label7.text=[MCLocalization stringForKey:@"asthma_work7"];
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [ScrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    label2.textColor = [UIColor redColor];
    [ScrollView addSubview:label2];
    
    label3.frame=CGRectMake(20, label2.frame.origin.y+label2.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    [ScrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [ScrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    label5.textColor = [UIColor redColor];
    [label5 sizeToFit];
    [ScrollView addSubview:label5];
    
    label6.frame=CGRectMake(20, label5.frame.origin.y+label5.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [ScrollView addSubview:label6];
    
    label7.frame=CGRectMake(10, label6.frame.origin.y+label6.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [ScrollView addSubview:label7];
    return label7;
}

-(UILabel *)jointContents{
    self.navigationItem.title = [MCLocalization stringForKey:@"work"];
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    label1.text=[MCLocalization stringForKey:@"joint_work1"];
    label2.text=[MCLocalization stringForKey:@"joint_work2"];
    label3.text=[MCLocalization stringForKey:@"joint_work3"];
    label4.text=[MCLocalization stringForKey:@"joint_work4"];
    label5.text=[MCLocalization stringForKey:@"joint_work5"];
    label6.text=[MCLocalization stringForKey:@"joint_work6"];
    label7.text=[MCLocalization stringForKey:@"joint_work7"];
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [ScrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    label2.textColor = [UIColor redColor];
    [ScrollView addSubview:label2];
    
    label3.frame=CGRectMake(20, label2.frame.origin.y+label2.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    [ScrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [ScrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    label5.textColor = [UIColor redColor];
    [label5 sizeToFit];
    [ScrollView addSubview:label5];
    
    label6.frame=CGRectMake(20, label5.frame.origin.y+label5.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [ScrollView addSubview:label6];
    
    label7.frame=CGRectMake(10, label6.frame.origin.y+label6.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [ScrollView addSubview:label7];
    return label7;
    
}

//////////////////picker Methods////////////////////
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    
    return 1;//Or return whatever as you intend
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    
    int i = 0;
    kapaltimeRow=kapaltimeValue;
    int j=kapaltimeRow;
    
    if([thePickerView isEqual: inhalePicker] || [thePickerView isEqual:roundPicker]){
        i=50;
    }
    else if([thePickerView isEqual: holdPicker]){
        i=201;
    }
    else if([thePickerView isEqual: exhalePicker] || [thePickerView isEqual:bahyaexhalepkr]) {
        i=100;
    }
    else if([thePickerView isEqual:afHoldPicker]){
        i=11;
    }
    else if ([thePickerView isEqual:kapalRoundPicker]){
        i=150*j-(j*10-1);
    }
    else if ([thePickerView isEqual:kapalTimePicker] || [thePickerView isEqual:bhramariInhalePicker] || [thePickerView isEqual:udgeethInhalepkr] || [thePickerView isEqual:udgeethexhaleMpkr]){
        i=20;
    }
    else if ([thePickerView isEqual:bhramariExhalepicker]){
        i=40;
    }
    else if ([thePickerView isEqual:udgeethexhaleOpkr]){
        i=60;
    }
    else if ([thePickerView isEqual:bahyaholdpkr]){
        i=150;
    }
    return i;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    int height;
    if (DEVICE==IPAD) {
        height=40;
    }
    else{
        height=30;
    }
    return height;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    
    if([pickerView isEqual: inhalePicker]){
        InhaleRow=[[In_roundData objectAtIndex:row] intValue];
        bahyainhaleRow = [[In_roundData objectAtIndex:row] intValue];
    }
    else if([pickerView isEqual: holdPicker]){
        holdRow=[[holdData objectAtIndex:row] intValue];
    }
    else if([pickerView isEqual: exhalePicker]) {
        exhaleRow=[[exhaleData objectAtIndex:row] intValue];
    }
    else if([pickerView isEqual:afHoldPicker]){
        afHoldRow=[[afroundData objectAtIndex:row] intValue];
    }
    else if([pickerView isEqual:roundPicker]){
        roundRow=[[In_roundData objectAtIndex:row] intValue];
        bhramariRoundRow=roundRow;
    }
    else if([pickerView isEqual: kapalRoundPicker]){
        kapalroundRow=[[kapalroundData objectAtIndex:row] intValue];
        RoundForAction=kapalroundRow;
        oldTimeValue=kapaltimeValue;
        kapaltimeValue=kapaltimeRow;
        kapalroundValue=RoundForAction;
    }
    else if([pickerView isEqual:kapalTimePicker]) {
        kapaltimeRow=[[kapatimeData objectAtIndex:row] intValue];
        kapaltimeValue=kapaltimeRow;
        kapalroundValue=RoundForAction;
        [self setarrayForKapal];
        [kapalRoundPicker reloadAllComponents];
        RoundForAction=(kapalroundRow/oldTimeValue)*kapaltimeRow;
        [kapalRoundPicker selectRow:RoundForAction-(kapaltimeRow*10) inComponent:0 animated:YES];
        kapaltimeValue=kapaltimeRow;
        kapalroundValue=RoundForAction;
    }
    else if ([pickerView isEqual:bhramariInhalePicker]){
        bhramariInhaleRow=[[bhramriInhlaeData objectAtIndex:row] intValue];
    }
    else if([pickerView isEqual:bhramariExhalepicker]){
        bhramariExhaleRow=[[bhramariExhaleData objectAtIndex:row] intValue];
    }
    else if ([pickerView isEqual:udgeethInhalepkr]){
        UdgeethinhaleRow=[[bhramriInhlaeData objectAtIndex:row] intValue];
    }
    else if ([pickerView isEqual:udgeethexhaleOpkr]){
        UdgeethexhalORow=[[udgeethexhaleOData objectAtIndex:row] intValue];
    }
    else if ([pickerView isEqual:udgeethexhaleMpkr]){
        UdgeethexhaleMRow=[[bhramriInhlaeData objectAtIndex:row] intValue];
    }
    else if ([pickerView isEqual:bahyaexhalepkr]){
        bahyaexhaleRow=[[exhaleData objectAtIndex:row] intValue];
    }
    else if ([pickerView isEqual:bahyaholdpkr]){
        holdRow=[[bahyaholdData objectAtIndex:row] intValue];
    }
    condition=YES;
    NSTimer *timer;
    timer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(yourMethodName) userInfo:nil repeats:NO];
}

-(void)pickerView1:(UIPickerView *)pickerView didSelectRow1: (NSInteger)row inComponent1:(NSInteger)component {
    if (condition==NO) {
    kapaltimeRow=[[kapatimeData objectAtIndex:row] intValue];
    kapaltimeValue=kapaltimeRow;
    kapalroundValue=RoundForAction;
    [self setarrayForKapal];
    [kapalRoundPicker reloadAllComponents];
    RoundForAction=(kapalroundRow/oldTimeValue)*kapaltimeRow;
    [kapalRoundPicker selectRow:RoundForAction-(kapaltimeRow*10) inComponent:0 animated:YES];
    kapaltimeValue=kapaltimeRow;
    kapalroundValue=RoundForAction;
    }
}

-(void)yourMethodName{
    condition=NO;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* tView = (UILabel*)view;
    if (!tView){
        tView = [[UILabel alloc] init];
                if (DEVICE==IPAD) {
                    [tView setFont:[UIFont systemFontOfSize:40]];
                }
                else{
                    [tView setFont:[UIFont systemFontOfSize:22]];
                }
        [tView setTextAlignment:NSTextAlignmentCenter];
        if([pickerView isEqual: inhalePicker] || [pickerView isEqual:roundPicker]){
            
            tView.text=[In_roundData objectAtIndex:row];
        }
        else if([pickerView isEqual: holdPicker]){
            tView.text=[holdData objectAtIndex:row];
        }
        else if([pickerView isEqual: exhalePicker] || [pickerView isEqual:bahyaexhalepkr]) {
            tView.text=[exhaleData objectAtIndex:row];
        }
        else if([pickerView isEqual:afHoldPicker]){
            tView.text=[afroundData objectAtIndex:row];
        }
        else if ([pickerView isEqual:kapalTimePicker]){
            tView.text=[kapatimeData objectAtIndex:row];
            
            [self pickerView1:pickerView didSelectRow1:row inComponent1:0];
        }
        else if ([pickerView isEqual:kapalRoundPicker]){
            tView.text=[kapalroundData objectAtIndex:row];
        }
        else if ([pickerView isEqual:bhramariInhalePicker] || [pickerView isEqual:udgeethInhalepkr] || [pickerView isEqual:udgeethexhaleMpkr]){
            tView.text=[bhramriInhlaeData objectAtIndex:row];
        }
        else if ([pickerView isEqual:bhramariExhalepicker]){
            tView.text=[bhramariExhaleData objectAtIndex:row];
        }
        else if ([pickerView isEqual:udgeethexhaleOpkr]){
            tView.text=[udgeethexhaleOData objectAtIndex:row];
        }
        else if ([pickerView isEqual:bahyaholdpkr]){
            tView.text=[bahyaholdData objectAtIndex:row];
        }
    }
    
    pickerView.subviews[1].backgroundColor = [UIColor whiteColor];
    pickerView.subviews[2].backgroundColor = [UIColor whiteColor];
    return tView;
}

/////////////////Anulom custum view methods///////////
-(void)createView1:(int)i pranayama:(NSString *)Type{
    int mainHeight,titlelblH,lineP,okP,okH,okW,topviewP,topviewH,pickerviewH,pickerviewP,pickerH,leftRightBtnH,space;
    if (DEVICE == IPAD) {
        mainHeight=400;
        titlelblH=50;
        lineP=70;
        topviewP=90;
        topviewH=50;
        pickerviewP=140;
        pickerviewH=170;
        leftRightBtnH=70;
        pickerH=70;
        okP=320;
        okH=70;
        okW=120;
        space=10;
    }
    else{
        mainHeight=200;
        titlelblH=25;
        lineP=35;
        topviewP=45;
        topviewH=25;
        pickerviewP=70;
        pickerviewH=85;
        leftRightBtnH=35;
        pickerH=50;
        okP=160;
        okH=35;
        okW=60;
        space=5;
    }
    
    view1=[[UIView alloc]initWithFrame:CGRectMake(0, 0,self.view.frame.size.width  , self.view.frame.size.height)];
    blackOverlay = [[UIView alloc] init];
    blackOverlay.frame=view1.frame;
    blackOverlay.layer.backgroundColor = [[UIColor blackColor] CGColor];
    blackOverlay.layer.opacity = 0.4f;
    [view1 addSubview: blackOverlay];

    [window addSubview:view1];
    PopupView=[[UIView alloc]initWithFrame:CGRectMake(10, view1.frame.size.height/2-mainHeight/2, ScrollView.frame.size.width-20, mainHeight)];
    PopupView.backgroundColor=RGB(239, 239, 239);
    PopupView.layer.cornerRadius=5;
    PopupView.alpha=0.0;
    [view1 addSubview:PopupView];
    [UIView animateWithDuration:0.5 animations:^{
        PopupView.alpha = 1.0; //to display loginview
    } completion:^(BOOL finished) {
    }];
    
    UILabel *titleLbl=[[UILabel alloc]initWithFrame:CGRectMake(10, space, PopupView.frame.size.width-10, titlelblH)];
    
    titleLbl.textColor=RGB(230, 123, 53);
    titleLbl.font=[UIFont systemFontOfSize:OkfontSize];
    [PopupView addSubview:titleLbl];
    UILabel *linelbl=[[UILabel alloc] initWithFrame:CGRectMake(0, lineP, PopupView.frame.size.width, 1)];
    linelbl.backgroundColor=RGB(230, 123, 53);
    [PopupView addSubview:linelbl];
    
    PopupView.layer.borderWidth=1;
    PopupView.layer.borderColor=[UIColor redColor].CGColor;
    
    UIView *toplblView=[[UIView alloc]initWithFrame:CGRectMake(10, topviewP, PopupView.frame.size.width-20, topviewH)];
    toplblView.backgroundColor=RGB(239, 239, 239);
    [PopupView addSubview:toplblView];
    
    int a=toplblView.frame.size.width/i;
    UILabel *inhaleLbl=[[UILabel alloc]initWithFrame:CGRectMake(a/2, 0, a, topviewH)];
    inhaleLbl.text= inhalestr;
    inhaleLbl.font=[UIFont systemFontOfSize:inFontSize];
    inhaleLbl.textAlignment=NSTextAlignmentCenter;
    [toplblView addSubview:inhaleLbl];
    UILabel *holdLbl=[[UILabel alloc]initWithFrame:CGRectMake(1.5*a, 0, a, topviewH)];
    holdLbl.text= holdstr;
    holdLbl.font=[UIFont systemFontOfSize:inFontSize];
    holdLbl.textAlignment=NSTextAlignmentCenter;
    [toplblView addSubview:holdLbl];
    UILabel *exhaleLbl=[[UILabel alloc]initWithFrame:CGRectMake(2.5*a, 0, a, topviewH)];
    exhaleLbl.text= exhalestr;
    exhaleLbl.font=[UIFont systemFontOfSize:inFontSize];
    exhaleLbl.textAlignment=NSTextAlignmentCenter;
    [toplblView addSubview:exhaleLbl];
    
    UIButton *Okbutton=[[UIButton alloc]initWithFrame:CGRectMake(PopupView.frame.size.width/2-okW/2, okP, okW,okH )];
    [Okbutton setTitle:@"OK" forState:UIControlStateNormal];
    Okbutton.titleLabel.font=[UIFont systemFontOfSize:OkfontSize];
    [Okbutton addTarget:self action:@selector(okbuttonMethod:) forControlEvents:UIControlEventTouchUpInside];
    [Okbutton setBackgroundColor:RGB(170, 170, 170)];
    [Okbutton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    Okbutton.layer.cornerRadius=3;
    [PopupView addSubview:Okbutton];
    
    if ([Type isEqualToString:@"Anulom"]) {
        UILabel *AfholdLbl=[[UILabel alloc]initWithFrame:CGRectMake(4*a, 0, a, topviewH)];
        AfholdLbl.text= holdstr;
        titleLbl.text=[NSString stringWithFormat:@"%@ : %@",setRatio,anulom];
        AfholdLbl.font=[UIFont systemFontOfSize:inFontSize];
        AfholdLbl.textAlignment=NSTextAlignmentCenter;
        [toplblView addSubview:AfholdLbl];
        UILabel *roundLbl=[[UILabel alloc]initWithFrame:CGRectMake(5*a, 0, a, topviewH)];
        roundLbl.text= roundstr;
        roundLbl.font=[UIFont systemFontOfSize:inFontSize];
        roundLbl.textAlignment=NSTextAlignmentCenter;
        [toplblView addSubview:roundLbl];
        
        Okbutton.tag=101;
    }
    else{
        NSString *str;
        if ([Type isEqualToString:@"Ujjayi"]) {
            Okbutton.tag=103;
            str=[NSString stringWithFormat:@"%@ : %@",setRatio,Ujjayi];
        }
        else if([Type isEqualToString:@"Surya"]){
            Okbutton.tag=104;
            str=[NSString stringWithFormat:@"%@ : %@",setRatio,surya];
        }
        else if([Type isEqualToString:@"Udgeeth"]){
            Okbutton.tag=107;
            str=[NSString stringWithFormat:@"%@ : %@",setRatio,udgeeth];
            holdLbl.text=[MCLocalization stringForKey:@"exhaleO"];
            exhaleLbl.text=[MCLocalization stringForKey:@"exhaleM"];
        }
        else{
            Okbutton.tag=108;
            str=[NSString stringWithFormat:@"%@ : %@",setRatio,bahya];
            holdLbl.text=exhalestr;
            exhaleLbl.text=holdstr;
        }
        
        titleLbl.text=str;
        UILabel *roundLbl=[[UILabel alloc]initWithFrame:CGRectMake(4*a, 0, a, topviewH)];
        roundLbl.text= roundstr;
        roundLbl.font=[UIFont systemFontOfSize:inFontSize];
        roundLbl.textAlignment=NSTextAlignmentCenter;
        [toplblView addSubview:roundLbl];
    }
    
    UIView *pickerview=[[UIView alloc]initWithFrame:CGRectMake(10, pickerviewP, PopupView.frame.size.width-20, pickerviewH)];
    pickerview.backgroundColor=[UIColor whiteColor];
    [PopupView addSubview:pickerview];
    
    UIButton *leftBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, pickerview.frame.size.height/2-leftRightBtnH/2, a/2, leftRightBtnH)];
    leftBtn.tag=1;
    
    
    [pickerview addSubview:leftBtn];
    UIButton *Rightbtn=[[UIButton alloc]initWithFrame:CGRectMake(3.5*a, pickerview.frame.size.height/2-leftRightBtnH/2, a/2, leftRightBtnH)];
    Rightbtn.tag=2;
    
    if (DEVICE==IPAD) {
        [leftBtn setImage:[UIImage imageNamed:@"left_arrow_i_pad"] forState:UIControlStateNormal];
        [Rightbtn setImage:[UIImage imageNamed:@"right_arrow_i_pad"] forState:UIControlStateNormal];
    }
    else{
        [leftBtn setImage:[UIImage imageNamed:@"left_arrow-1"] forState:UIControlStateNormal];
        [Rightbtn setImage:[UIImage imageNamed:@"right_arrow-1"] forState:UIControlStateNormal];
    }
    
    
    [pickerview addSubview:Rightbtn];
    ////////Check condition for different picker /////
    if ([Type isEqualToString:@"Udgeeth"]) {
        [leftBtn addTarget:self action:@selector(UdgeethLeftAndRightBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        [Rightbtn addTarget:self action:@selector(UdgeethLeftAndRightBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        udgeethInhalepkr=[[UIPickerView alloc]initWithFrame:CGRectMake(a/2, pickerview.frame.size.height/2-pickerH/2, a-5, pickerH)];
        udgeethInhalepkr.delegate=self;
        [pickerview addSubview:udgeethInhalepkr];
        
        udgeethexhaleOpkr=[[UIPickerView alloc]initWithFrame:CGRectMake(1.5*a, pickerview.frame.size.height/2-pickerH/2, a-5, pickerH)];
        udgeethexhaleOpkr.delegate=self;
        [pickerview addSubview:udgeethexhaleOpkr];
        
        udgeethexhaleMpkr=[[UIPickerView alloc]initWithFrame:CGRectMake(2.5*a, pickerview.frame.size.height/2-pickerH/2, a, pickerH)];
        udgeethexhaleMpkr.delegate=self;
        [pickerview addSubview:udgeethexhaleMpkr];
    }
    else if ([Type isEqualToString:@"Bahya"]){
        [leftBtn addTarget:self action:@selector(BahyaLeftAndRightBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        [Rightbtn addTarget:self action:@selector(BahyaLeftAndRightBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        inhalePicker=[[UIPickerView alloc]initWithFrame:CGRectMake(a/2, pickerview.frame.size.height/2-pickerH/2, a-5, pickerH)];
        inhalePicker.delegate=self;
        [pickerview addSubview:inhalePicker];
        
        bahyaexhalepkr=[[UIPickerView alloc]initWithFrame:CGRectMake(1.5*a, pickerview.frame.size.height/2-pickerH/2, a-5, pickerH)];
        bahyaexhalepkr.delegate=self;
        [pickerview addSubview:bahyaexhalepkr];
        
        bahyaholdpkr=[[UIPickerView alloc]initWithFrame:CGRectMake(2.5*a, pickerview.frame.size.height/2-pickerH/2, a, pickerH)];
        bahyaholdpkr.delegate=self;
        [pickerview addSubview:bahyaholdpkr];
    }
    else{
        [leftBtn addTarget:self action:@selector(LeftAndRightBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        [Rightbtn addTarget:self action:@selector(LeftAndRightBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        inhalePicker=[[UIPickerView alloc]initWithFrame:CGRectMake(a/2, pickerview.frame.size.height/2-pickerH/2, a-5, pickerH)];
        inhalePicker.delegate=self;
        [pickerview addSubview:inhalePicker];
        
        holdPicker=[[UIPickerView alloc]initWithFrame:CGRectMake(1.5*a, pickerview.frame.size.height/2-pickerH/2, a-5, pickerH)];
        holdPicker.delegate=self;
        [pickerview addSubview:holdPicker];
        
        exhalePicker=[[UIPickerView alloc]initWithFrame:CGRectMake(2.5*a, pickerview.frame.size.height/2-pickerH/2, a, pickerH)];
        exhalePicker.delegate=self;
        [pickerview addSubview:exhalePicker];
    }
    UILabel *dotlbl=[[UILabel alloc]initWithFrame:CGRectMake(1.5*a-5, pickerview.frame.size.height/2-pickerH/2-3, 10, pickerH)];
    dotlbl.text=@":";
    dotlbl.font=[UIFont systemFontOfSize:dotFontsize];
    dotlbl.textAlignment=NSTextAlignmentCenter;
    [pickerview addSubview:dotlbl];
    
    UILabel *dotlbl1=[[UILabel alloc]initWithFrame:CGRectMake(2.5*a-5, pickerview.frame.size.height/2-pickerH/2-3, 10, pickerH)];
    dotlbl1.text=@":";
    dotlbl1.font= [UIFont systemFontOfSize:dotFontsize];
    dotlbl1.textAlignment=NSTextAlignmentCenter;
    [pickerview addSubview:dotlbl1];
    
    
    
    if ([Type isEqualToString:@"Anulom"]) {
        afHoldPicker=[[UIPickerView alloc]initWithFrame:CGRectMake(4*a,pickerview.frame.size.height/2-pickerH/2, a, pickerH)];
        afHoldPicker.delegate=self;
        [pickerview addSubview:afHoldPicker];
        UILabel *dotlbl2=[[UILabel alloc]initWithFrame:CGRectMake(5*a-5, pickerview.frame.size.height/2-pickerH/2-3, 10, pickerH)];
        dotlbl2.text=@":";
        dotlbl2.font=[UIFont systemFontOfSize:dotFontsize];
        dotlbl2.textAlignment=NSTextAlignmentCenter;
        [pickerview addSubview:dotlbl2];
        
        roundPicker=[[UIPickerView alloc]initWithFrame:CGRectMake(5*a, pickerview.frame.size.height/2-pickerH/2, a, pickerH)];
        roundPicker.delegate=self;
        [pickerview addSubview:roundPicker];
        
    }
    else{
        roundPicker=[[UIPickerView alloc]initWithFrame:CGRectMake(4*a, pickerview.frame.size.height/2-pickerH/2, a, pickerH)];
        roundPicker.delegate=self;
        [pickerview addSubview:roundPicker];
    }
    [inhalePicker selectRow:InhaleRow-1 inComponent:0 animated:NO];
    [holdPicker selectRow:holdRow inComponent:0 animated:NO];
    [exhalePicker selectRow:exhaleRow-1 inComponent:0 animated:NO];
    [roundPicker selectRow:roundRow-1 inComponent:0 animated:NO];
    [afHoldPicker selectRow:afHoldRow-1 inComponent:0 animated:NO];
    [udgeethInhalepkr selectRow:UdgeethinhaleRow-1 inComponent:0 animated:NO];
    [udgeethexhaleOpkr selectRow:UdgeethexhalORow-1 inComponent:0 animated:NO];
    [udgeethexhaleMpkr selectRow:UdgeethexhaleMRow-1 inComponent:0 animated:NO];
    [bahyaexhalepkr selectRow:bahyaexhaleRow-1 inComponent:0 animated:NO];
    [bahyaholdpkr selectRow:bahyaHoldRow-1 inComponent:0 animated:NO];
}
//////////////////////

-(void)bhramari_BhastrikaViewMethod:(BOOL)Bhramari{
    int mainHeight,titlelblH,lineP,okP,okH,okW,topviewP,topviewH,pickerviewH,pickerviewP,pickerH,leftRightBtnH,space;
    if (DEVICE == IPAD) {
        mainHeight=400;
        titlelblH=50;
        lineP=70;
        topviewP=90;
        topviewH=50;
        pickerviewP=140;
        pickerviewH=170;
        leftRightBtnH=70;
        pickerH=70;
        okP=320;
        okH=70;
        okW=120;
        space=10;
    }
    else{
        mainHeight=200;
        titlelblH=25;
        lineP=35;
        topviewP=45;
        topviewH=25;
        pickerviewP=70;
        pickerviewH=85;
        leftRightBtnH=35;
        pickerH=50;
        okP=160;
        okH=35;
        okW=60;
        space=5;
    }

    view1=[[UIView alloc]initWithFrame:CGRectMake(0, 0,self.view.frame.size.width  , self.view.frame.size.height)];
    
    blackOverlay = [[UIView alloc] init];
    blackOverlay.frame=view1.frame;
    blackOverlay.layer.backgroundColor = [[UIColor blackColor] CGColor];
    blackOverlay.layer.opacity = 0.4f;
    [view1 addSubview: blackOverlay];
    
    [window addSubview:view1];
    PopupView=[[UIView alloc]initWithFrame:CGRectMake(10, view1.frame.size.height/2-mainHeight/2, ScrollView.frame.size.width-20, mainHeight)];
    PopupView.backgroundColor=RGB(239, 239, 239);
    PopupView.layer.cornerRadius=5;
    PopupView.alpha=0.0;
    [view1 addSubview:PopupView];
    [UIView animateWithDuration:0.5 animations:^{
        PopupView.alpha = 1.0; //to display loginview
    } completion:^(BOOL finished) {
    }];
    
    UILabel *titleLbl=[[UILabel alloc]initWithFrame:CGRectMake(10, space, PopupView.frame.size.width-10, titlelblH)];
    titleLbl.textColor=RGB(230, 123, 53);
    titleLbl.font=[UIFont systemFontOfSize:OkfontSize];
    [PopupView addSubview:titleLbl];
    UILabel *linelbl=[[UILabel alloc] initWithFrame:CGRectMake(0, lineP, PopupView.frame.size.width, 1)];
    linelbl.backgroundColor=RGB(230, 123, 53);
    [PopupView addSubview:linelbl];
    PopupView.layer.borderWidth=1;
    PopupView.layer.borderColor=[UIColor redColor].CGColor;;
    
    UIView *toplblView=[[UIView alloc]initWithFrame:CGRectMake(10, topviewP, PopupView.frame.size.width-20, topviewH)];
    toplblView.backgroundColor=RGB(239 , 239, 239);
    [PopupView addSubview:toplblView];
    
    int a=toplblView.frame.size.width/4;
    UILabel *inhaleLbl=[[UILabel alloc]initWithFrame:CGRectMake(a/2, 0, a, topviewH)];
    inhaleLbl.text= inhalestr;
    inhaleLbl.font=[UIFont systemFontOfSize:inFontSize];
    inhaleLbl.textAlignment=NSTextAlignmentCenter;
    [toplblView addSubview:inhaleLbl];
    UILabel *exhaleLbl=[[UILabel alloc]initWithFrame:CGRectMake(1.5*a, 0, a, topviewH)];
    exhaleLbl.text= exhalestr;
    exhaleLbl.font=[UIFont systemFontOfSize:inFontSize];
    exhaleLbl.textAlignment=NSTextAlignmentCenter;
    [toplblView addSubview:exhaleLbl];
    UILabel *roundLbl=[[UILabel alloc]initWithFrame:CGRectMake(3*a, 0, a, topviewH)];
    roundLbl.text= roundstr;
    roundLbl.font=[UIFont systemFontOfSize:inFontSize];
    roundLbl.textAlignment=NSTextAlignmentCenter;
    [toplblView addSubview:roundLbl];
    
    UIButton *Okbutton=[[UIButton alloc]initWithFrame:CGRectMake(PopupView.frame.size.width/2-okW/2, okP, okW,okH)];
    [Okbutton setTitle:@"OK" forState:UIControlStateNormal];
    Okbutton.titleLabel.font=[UIFont systemFontOfSize:OkfontSize];
    [Okbutton addTarget:self action:@selector(okbuttonMethod:) forControlEvents:UIControlEventTouchUpInside];
    [Okbutton setBackgroundColor:RGB(170, 170, 170)];
    [Okbutton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    Okbutton.layer.cornerRadius=3;
    [PopupView addSubview:Okbutton];
    
    if (Bhramari==YES) {
        titleLbl.text=[NSString stringWithFormat:@"%@ : %@",setRatio,bhramari];
        Okbutton.tag=105;
    }
    else{
        titleLbl.text=[NSString stringWithFormat:@"%@ : %@",setRatio,bhastrika];
        Okbutton.tag=106;
    }
    
    UIView *pickerview=[[UIView alloc]initWithFrame:CGRectMake(10, pickerviewP, PopupView.frame.size.width-20, pickerviewH)];
    pickerview.backgroundColor=[UIColor whiteColor];
    [PopupView addSubview:pickerview];
    
    UIButton *leftBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, pickerview.frame.size.height/2-leftRightBtnH/2, a/2, leftRightBtnH)];
    leftBtn.tag=1;
    [leftBtn setImage:[UIImage imageNamed:@"left_arrow-1"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(LeftAndRightBtn:) forControlEvents:UIControlEventTouchUpInside];
    [pickerview addSubview:leftBtn];
    
    UIButton *Rightbtn=[[UIButton alloc]initWithFrame:CGRectMake(2.5*a, pickerview.frame.size.height/2-leftRightBtnH/2, a/2, leftRightBtnH)];
    Rightbtn.tag=2;
    [Rightbtn setImage:[UIImage imageNamed:@"right_arrow-1"] forState:UIControlStateNormal];
    [Rightbtn addTarget:self action:@selector(LeftAndRightBtn:) forControlEvents:UIControlEventTouchUpInside];
    [pickerview addSubview:Rightbtn];
    
    if (DEVICE==IPAD) {
        [leftBtn setImage:[UIImage imageNamed:@"left_arrow_i_pad"] forState:UIControlStateNormal];
        [Rightbtn setImage:[UIImage imageNamed:@"right_arrow_i_pad"] forState:UIControlStateNormal];
    }
    else{
        [leftBtn setImage:[UIImage imageNamed:@"left_arrow-1"] forState:UIControlStateNormal];
        [Rightbtn setImage:[UIImage imageNamed:@"right_arrow-1"] forState:UIControlStateNormal];
    }

    bhramariInhalePicker=[[UIPickerView alloc]initWithFrame:CGRectMake(a/2, pickerview.frame.size.height/2-pickerH/2, a-5, pickerH)];
    bhramariInhalePicker.delegate=self;
    [pickerview addSubview:bhramariInhalePicker];
    UILabel *dotlbl=[[UILabel alloc]initWithFrame:CGRectMake(1.5*a-5, pickerview.frame.size.height/2-pickerH/2-3, 10, pickerH)];
    dotlbl.text=@":";
    dotlbl.font=[UIFont systemFontOfSize:dotFontsize];
    dotlbl.textAlignment=NSTextAlignmentCenter;
    [pickerview addSubview:dotlbl];
    
    bhramariExhalepicker=[[UIPickerView alloc]initWithFrame:CGRectMake(1.5*a, pickerview.frame.size.height/2-pickerH/2, a-5, pickerH)];
    bhramariExhalepicker.delegate=self;
    [pickerview addSubview:bhramariExhalepicker];
    
    roundPicker=[[UIPickerView alloc]initWithFrame:CGRectMake(3*a, pickerview.frame.size.height/2-pickerH/2, a, pickerH)];
    roundPicker.delegate=self;
    [pickerview addSubview:roundPicker];
    
    [bhramariInhalePicker selectRow:4 inComponent:0 animated:NO];
    [bhramariExhalepicker selectRow:9 inComponent:0 animated:NO];
    [roundPicker selectRow:3 inComponent:0 animated:NO];
}

-(void)LeftAndRightBtn:(UIButton *)sender{
    if (sender.tag==1) {
        
        if (InhaleRow==1 ) {
            InhaleRow=1;
            holdRow=InhaleRow*4;
            exhaleRow=InhaleRow*2;
            [inhalePicker selectRow:InhaleRow-1 inComponent:0 animated:YES];
            [holdPicker selectRow:holdRow inComponent:0 animated:YES];
            [exhalePicker selectRow:exhaleRow-1 inComponent:0 animated:YES];
        }
        else{
            InhaleRow=InhaleRow-1;
            holdRow=InhaleRow*4;
            exhaleRow=InhaleRow*2;
            [inhalePicker selectRow:InhaleRow-1 inComponent:0 animated:YES];
            [holdPicker selectRow:holdRow inComponent:0 animated:YES];
            [exhalePicker selectRow:exhaleRow-1 inComponent:0 animated:YES];
        }
        if ( bhramariInhaleRow==1) {
            bhramariInhaleRow=1;
            bhramariExhaleRow=bhramariInhaleRow*2;
            [bhramariInhalePicker selectRow:bhramariInhaleRow-1 inComponent:0 animated:YES];
            [bhramariExhalepicker selectRow:bhramariExhaleRow-1 inComponent:0 animated:YES];
        }
        else{
            bhramariInhaleRow=bhramariInhaleRow-1;
            bhramariExhaleRow=bhramariInhaleRow*2;
            [bhramariInhalePicker selectRow:bhramariInhaleRow-1 inComponent:0 animated:YES];
            [bhramariExhalepicker selectRow:bhramariExhaleRow-1 inComponent:0 animated:YES];
        }
        }
    else if (sender.tag==2){
        if (InhaleRow==50 ) {
            InhaleRow=50;
            holdRow=InhaleRow*4;
            exhaleRow=InhaleRow*2;
            [inhalePicker selectRow:InhaleRow-1 inComponent:0 animated:YES];
            [holdPicker selectRow:holdRow inComponent:0 animated:YES];
            [exhalePicker selectRow:exhaleRow-1 inComponent:0 animated:YES];
        }
        else{
            InhaleRow=InhaleRow+1;
            holdRow=InhaleRow*4;
            exhaleRow=InhaleRow*2;
            [inhalePicker selectRow:InhaleRow-1 inComponent:0 animated:YES];
            [holdPicker selectRow:holdRow inComponent:0 animated:YES];
            [exhalePicker selectRow:exhaleRow-1 inComponent:0 animated:YES];

        }
        if (bhramariInhaleRow==20) {
            bhramariInhaleRow=20;
            bhramariExhaleRow=bhramariInhaleRow*2;
            [bhramariInhalePicker selectRow:bhramariInhaleRow-1 inComponent:0 animated:YES];
            [bhramariExhalepicker selectRow:bhramariExhaleRow-1 inComponent:0 animated:YES];
        }
        else{
            bhramariInhaleRow=bhramariInhaleRow+1;
            bhramariExhaleRow=bhramariInhaleRow*2;
            [bhramariInhalePicker selectRow:bhramariInhaleRow-1 inComponent:0 animated:YES];
            [bhramariExhalepicker selectRow:bhramariExhaleRow-1 inComponent:0 animated:YES];
        }
    }
}

-(void)UdgeethLeftAndRightBtn:(UIButton*)sender{
    if (sender.tag==1) {
        if (UdgeethinhaleRow==1) {
            UdgeethinhaleRow=1;
            UdgeethexhalORow=UdgeethinhaleRow*3;
            UdgeethexhaleMRow=UdgeethinhaleRow;
            [udgeethInhalepkr selectRow:UdgeethinhaleRow-1 inComponent:0 animated:YES];
            [udgeethexhaleOpkr selectRow:UdgeethexhalORow-1 inComponent:0 animated:YES];
            [udgeethexhaleMpkr selectRow:UdgeethexhaleMRow-1 inComponent:0 animated:YES];
        }
        else{
            UdgeethinhaleRow=UdgeethinhaleRow-1;
            UdgeethexhalORow=UdgeethinhaleRow*3;
            UdgeethexhaleMRow=UdgeethinhaleRow;
            [udgeethInhalepkr selectRow:UdgeethinhaleRow-1 inComponent:0 animated:YES];
            [udgeethexhaleOpkr selectRow:UdgeethexhalORow-1 inComponent:0 animated:YES];
            [udgeethexhaleMpkr selectRow:UdgeethexhaleMRow-1 inComponent:0 animated:YES];
        }
    }
    else if (sender.tag==2){
        if (UdgeethinhaleRow==20) {
            UdgeethinhaleRow=20;
            UdgeethexhalORow=UdgeethinhaleRow*3;
            UdgeethexhaleMRow=UdgeethinhaleRow;
            [udgeethInhalepkr selectRow:UdgeethinhaleRow-1 inComponent:0 animated:YES];
            [udgeethexhaleOpkr selectRow:UdgeethexhalORow-1 inComponent:0 animated:YES];
            [udgeethexhaleMpkr selectRow:UdgeethexhaleMRow-1 inComponent:0 animated:YES];
        }
        else{
            UdgeethinhaleRow=UdgeethinhaleRow+1;
            UdgeethexhalORow=UdgeethinhaleRow*3;
            UdgeethexhaleMRow=UdgeethinhaleRow;
            [udgeethInhalepkr selectRow:UdgeethinhaleRow-1 inComponent:0 animated:YES];
            [udgeethexhaleOpkr selectRow:UdgeethexhalORow-1 inComponent:0 animated:YES];
            [udgeethexhaleMpkr selectRow:UdgeethexhaleMRow-1 inComponent:0 animated:YES];
        }

    }
}

-(void)BahyaLeftAndRightBtn:(UIButton*)sender{
    if (sender.tag==1) {
        if (bahyainhaleRow==1) {
            bahyainhaleRow=1;
            bahyaexhaleRow=bahyainhaleRow*2;
            bahyaHoldRow=bahyainhaleRow*3;
            [inhalePicker selectRow:bahyainhaleRow-1 inComponent:0 animated:YES];
            [bahyaexhalepkr selectRow:bahyaexhaleRow-1 inComponent:0 animated:YES];
            [bahyaholdpkr selectRow:bahyaHoldRow-1 inComponent:0 animated:YES];
        }
        else{
            bahyainhaleRow=bahyainhaleRow-1;
            bahyaexhaleRow=bahyainhaleRow*2;
            bahyaHoldRow=bahyainhaleRow*3;
            [inhalePicker selectRow:bahyainhaleRow-1 inComponent:0 animated:YES];
            [bahyaexhalepkr selectRow:bahyaexhaleRow-1 inComponent:0 animated:YES];
            [bahyaholdpkr selectRow:bahyaHoldRow-1 inComponent:0 animated:YES];
        }
    }
    else if (sender.tag==2){
        if (bahyainhaleRow==50) {
            bahyainhaleRow=50;
            bahyaexhaleRow=bahyainhaleRow*2;
            bahyaHoldRow=bahyainhaleRow*3;
            [inhalePicker selectRow:bahyainhaleRow-1 inComponent:0 animated:YES];
            [bahyaexhalepkr selectRow:bahyaexhaleRow-1 inComponent:0 animated:YES];
            [bahyaholdpkr selectRow:bahyaHoldRow-1 inComponent:0 animated:YES];
        }
        else{
            bahyainhaleRow=bahyainhaleRow+1;
            bahyaexhaleRow=bahyainhaleRow*2;
            bahyaHoldRow=bahyainhaleRow*3;
            [inhalePicker selectRow:bahyainhaleRow-1 inComponent:0 animated:YES];
            [bahyaexhalepkr selectRow:bahyaexhaleRow-1 inComponent:0 animated:YES];
            [bahyaholdpkr selectRow:bahyaHoldRow-1 inComponent:0 animated:YES];
        }
    }
}

-(void)AnulomRatioViewMethod{
    [self setValuesFromKey];
    [self createView1:6 pranayama:@"Anulom"];
}

-(void)UjjayiRatioViewMethod{
    [self setValuesFromKey];
    [self createView1:5 pranayama:@"Ujjayi"];
}

-(void)SuryaRatiosViewMethod{
    [self setValuesFromKey];
    [self createView1:5 pranayama:@"Surya"];
}


-(void)okbuttonMethod:(UIButton *)sender{
    if (sender.tag==101) {
        view1.hidden=YES;
        AnulomInhaleValue=InhaleRow;
        AnulomholdValue=holdRow;
        AnulomexhaleValue=exhaleRow;
        AnulomAfholdValue=afHoldRow;
        AnulomRoundValue=roundRow;
        [self setValuesFromKey];
        if ([habbitType isEqualToString:@"Joint Pain"]) {
            UdgeethinhaleRow=4;
            UdgeethexhalORow=12;
            UdgeethexhaleMRow=4;
            roundRow=6;
            [self createView1:5 pranayama:@"Udgeeth"];
        }
        else{
            if ([habbitType isEqualToString:@"Thyroid"] || [habbitType isEqualToString:@"Diabetes"] || [habbitType isEqualToString:@"Weight Loss"]) {
                [self KapalRatioViewMethod];
            }
            else if ([habbitType isEqualToString:@"Migraine"]){
                [self bhramari_BhastrikaViewMethod:YES];
            }
            else if([habbitType isEqualToString:@"Asthma"]){
                [self bhramari_BhastrikaViewMethod:NO];
            }
        }
    }
    else if(sender.tag==102){
        view1.hidden=YES;
        if ([habbitType isEqualToString:@"Thyroid"]) {
         [self UjjayiRatioViewMethod];
        }
        else if([habbitType isEqualToString:@"Diabetes"] || [habbitType isEqualToString:@"Weight Loss"]){
            [self bhramari_BhastrikaViewMethod:NO];
        }
    }
    else if (sender.tag==103){
        UjjayiInhaleValue=InhaleRow;
        UjjayiHoldValue=holdRow;
        UjjayiExhaleValue=exhaleRow;
        UjjayiRoundValue=roundRow;
        InhaleRow=1;
        holdRow=4;
        exhaleRow=2;
        afHoldRow=0;
        roundRow=1;
        view1.hidden=YES;
        [self TimePickerMethod];
    }
    else if(sender.tag==104){
         view1.hidden=YES;
        SuryaInhale=InhaleRow;
        SuryaHold=holdRow;
        SuryaExhale=exhaleRow;
        SuryaRound=roundRow;
        [self setValuesFromKey];
        [self bhramari_BhastrikaViewMethod:YES];
    }
    else if (sender.tag==105){
        view1.hidden=YES;
        BhramariInhale=bhramariInhaleRow;
        BhramariExhale=bhramariExhaleRow;
        BhramariRound=bhramariRoundRow;
        [self setValuesFromKey];
        if ([habbitType isEqualToString:@"Migraine"]) {
            [self bhramari_BhastrikaViewMethod:NO];
        }
        else{
        [self TimePickerMethod];
        }
    }
    else if (sender.tag==106){
        view1.hidden=YES;
        BhastrikaInhale=bhramariInhaleRow;
        BhastrikaExhale=bhramariExhaleRow;
        BhastrikaRound=bhramariRoundRow;
        [self setValuesFromKey];
        if ([habbitType isEqualToString:@"Asthma"]){
            [self UjjayiRatioViewMethod];
        }
        else{
            [self TimePickerMethod];
        }
    }
    else if (sender.tag==107){
        view1.hidden=YES;
        udgeethInhale=UdgeethinhaleRow;
        udgeethexhaleO=UdgeethexhalORow;
        udgeethexhaleM=UdgeethexhaleMRow;
        udgeethround=roundRow;
        [self setValuesFromKey];
        bahyainhaleRow=1;
        bahyaexhaleRow=2;
        bahyaHoldRow=3;
        [self createView1:5 pranayama:@"Bahya"];
    }
    else if (sender.tag==108){
        view1.hidden=YES;
        Bahyainhale=bahyainhaleRow;
        Bahyaexhale=bahyaexhaleRow;
        Bahyahold=bahyaHoldRow;
        [self TimePickerMethod];
    }
}

-(void)KapalRatioViewMethod{
    int mainHeight,titlelblH,lineP,okP,okH,okW,topviewP,topviewH,pickerviewH,pickerviewP,pickerH,space;
    if (DEVICE == IPAD) {
        mainHeight=400;
        titlelblH=50;
        lineP=70;
        topviewP=90;
        topviewH=50;
        pickerviewP=140;
        pickerviewH=170;
        pickerH=70;
        okP=320;
        okH=70;
        okW=120;
        space=10;
    }
    else{
        mainHeight=200;
        titlelblH=25;
        lineP=35;
        topviewP=45;
        topviewH=25;
        pickerviewP=70;
        pickerviewH=85;
        pickerH=50;
        okP=160;
        okH=35;
        okW=60;
        space=5;
    }

    
    view1=[[UIView alloc]initWithFrame:CGRectMake(0, 0,self.view.frame.size.width  , self.view.frame.size.height)];
    
    blackOverlay = [[UIView alloc] init];
    blackOverlay.frame=view1.frame;
    blackOverlay.layer.backgroundColor = [[UIColor blackColor] CGColor];
    blackOverlay.layer.opacity = 0.4f;
    [view1 addSubview: blackOverlay];
    
    [window addSubview:view1];
    PopupView=[[UIView alloc]initWithFrame:CGRectMake(10, view1.frame.size.height/2-mainHeight/2, view1.frame.size.width-20, mainHeight)];
    PopupView.backgroundColor=RGB(239, 239, 239);
    PopupView.layer.cornerRadius=5;
    PopupView.alpha=0.0;
    [view1 addSubview:PopupView];
    [UIView animateWithDuration:0.5 animations:^{
        PopupView.alpha = 1.0; //to display loginview
    } completion:^(BOOL finished) {
    }];
    
    UILabel *titleLbl=[[UILabel alloc]initWithFrame:CGRectMake(10, space, PopupView.frame.size.width-10, titlelblH)];
    titleLbl.text=[NSString stringWithFormat:@"%@ : %@",setRatio,kapal];
    titleLbl.font=[UIFont systemFontOfSize:OkfontSize];
    titleLbl.textColor=RGB(230, 123, 53);
    [PopupView addSubview:titleLbl];
    UILabel *linelbl=[[UILabel alloc] initWithFrame:CGRectMake(0, lineP, PopupView.frame.size.width, 1)];
    linelbl.backgroundColor=RGB(230, 123, 53);
    [PopupView addSubview:linelbl];
    PopupView.layer.borderWidth=1;
    PopupView.layer.borderColor=[UIColor redColor].CGColor;
    
    UIView *toplblView=[[UIView alloc]initWithFrame:CGRectMake(10, topviewP, PopupView.frame.size.width-20, topviewH)];
    toplblView.backgroundColor=RGB(239 , 239, 239);
    [PopupView addSubview:toplblView];
    
    int width=toplblView.frame.size.width/4;
    UILabel *time=[[UILabel alloc]initWithFrame:CGRectMake(width-60, 0, 120, topviewH)];
    time.text=timestr;
    time.font=[UIFont systemFontOfSize:inFontSize];
    time.textAlignment=NSTextAlignmentCenter;
    [toplblView addSubview:time];
    UILabel *round=[[UILabel alloc]initWithFrame:CGRectMake(3*width-60, 0, 120, topviewH)];
    round.text=roundstr;
    round.font=[UIFont systemFontOfSize:inFontSize];
    round.textAlignment=NSTextAlignmentCenter;
    [toplblView addSubview:round];
    
    UIButton *Okbutton=[[UIButton alloc]initWithFrame:CGRectMake(PopupView.frame.size.width/2-okW/2, okP, okW,okH )];
    [Okbutton setTitle:@"OK" forState:UIControlStateNormal];
    Okbutton.titleLabel.font=[UIFont systemFontOfSize:OkfontSize];
    [Okbutton addTarget:self action:@selector(okbuttonMethod:) forControlEvents:UIControlEventTouchUpInside];
    [Okbutton setBackgroundColor:RGB(170, 170, 170)];
    Okbutton.tag=102;
    [Okbutton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    Okbutton.layer.cornerRadius=3;
    [PopupView addSubview:Okbutton];
    UIView *pickerview=[[UIView alloc]initWithFrame:CGRectMake(10, pickerviewP, PopupView.frame.size.width-20, pickerviewH)];
    pickerview.backgroundColor=[UIColor whiteColor];
    [PopupView addSubview:pickerview];
    
    kapalTimePicker=[[UIPickerView alloc]initWithFrame:CGRectMake(width-20, pickerview.frame.size.height/2-pickerH/2, 60, pickerH)];
    kapalTimePicker.delegate=self;
    [pickerview addSubview:kapalTimePicker];
    kapalRoundPicker=[[UIPickerView alloc]initWithFrame:CGRectMake(3*width-60, pickerview.frame.size.height/2-pickerH/2, 120, pickerH)];
    kapalRoundPicker.delegate=self;
    [pickerview addSubview:kapalRoundPicker];
    
    [roundPicker selectRow:0 inComponent:0 animated:YES];
    [holdPicker selectRow:59 inComponent:0 animated:YES];
}

-(void)TimePickerMethod{
    int mainHeight,titlelblH,lineP,okP,okH,datepickerP,datepickerH,space;
    if (DEVICE == IPAD) {
        mainHeight=400;
        titlelblH=50;
        lineP=70;
        space=10;
        okP=320;
        okH=70;
        datepickerP=100;
        datepickerH=200;
    }
    else{
        mainHeight=200;
        titlelblH=25;
        space=5;
        lineP=35;
        okP=160;
        okH=35;
        datepickerP=50;
        datepickerH=100;
    }

    
    view1=[[UIView alloc]initWithFrame:CGRectMake(0, 0,self.view.frame.size.width  , self.view.frame.size.height)];
    
    blackOverlay = [[UIView alloc] init];
    blackOverlay.frame=view1.frame;
    blackOverlay.layer.backgroundColor = [[UIColor blackColor] CGColor];
    blackOverlay.layer.opacity = 0.4f;
    [view1 addSubview: blackOverlay];
    
    [window addSubview:view1];
    
    PopupView=[[UIView alloc]initWithFrame:CGRectMake(10, view1.frame.size.height/2-mainHeight/2, ScrollView.frame.size.width-20, mainHeight)];
    PopupView.backgroundColor=[UIColor whiteColor];
    PopupView.layer.cornerRadius=5;
    PopupView.alpha=0.0;
    [view1 addSubview:PopupView];
    [UIView animateWithDuration:0.5 animations:^{
        PopupView.alpha = 1.0; //to display loginview
    } completion:^(BOOL finished) {
    }];
    
    titleDateLbl=[[UILabel alloc]initWithFrame:CGRectMake(10, space, PopupView.frame.size.width/2, titlelblH)];
    titleDateLbl.tag=21;
    NSDate *date=[[NSUserDefaults standardUserDefaults] objectForKey:kldate];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"hh:mma"];
    NSString *datestr=[dateformate stringFromDate:date];
    titleDateLbl.text=datestr;
    titleDateLbl.font=[UIFont systemFontOfSize:OkfontSize];
    titleDateLbl.textColor=RGB(230, 123, 53);
    [PopupView addSubview:titleDateLbl];
    UILabel *linelbl=[[UILabel alloc] initWithFrame:CGRectMake(0, lineP, PopupView.frame.size.width, 1)];
    linelbl.backgroundColor=RGB(230, 123, 53);
    [PopupView addSubview:linelbl];
    
    
    NSDate *pickerdate=[[NSDate alloc]init];
    pickerdate=[dateformate dateFromString:datestr];
    datePicker=[[UIDatePicker alloc]initWithFrame:CGRectMake(20, datepickerP, PopupView.frame.size.width-40, datepickerH)];
    datePicker.datePickerMode=UIDatePickerModeTime;
    datePicker.date=pickerdate;
    [datePicker addTarget:self action:@selector(dateChange:) forControlEvents:UIControlEventValueChanged];
    [PopupView addSubview:datePicker];
    
    UILabel *linelbl1=[[UILabel alloc] initWithFrame:CGRectMake(0, okP, PopupView.frame.size.width, 1)];
    linelbl1.backgroundColor=RGB(239, 239, 239);
    [PopupView addSubview:linelbl1];
    UILabel *linelbl2=[[UILabel alloc] initWithFrame:CGRectMake(PopupView.frame.size.width/2, okP, 1, 40)];
    linelbl2.backgroundColor=RGB(239, 239, 239);
    [PopupView addSubview:linelbl2];
    PopupView.layer.borderWidth=1;
    PopupView.layer.borderColor=[UIColor redColor].CGColor;
    
    UIButton *Okbutton=[[UIButton alloc]initWithFrame:CGRectMake(PopupView.frame.size.width/2, okP, PopupView.frame.size.width/2,okH)];
    [Okbutton setTitle:set forState:UIControlStateNormal];
    [Okbutton addTarget:self action:@selector(dateOkbuttonMethod:) forControlEvents:UIControlEventTouchUpInside];
    [Okbutton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    Okbutton.titleLabel.font=[UIFont systemFontOfSize:OkfontSize];
    [PopupView addSubview:Okbutton];
    UIButton *Canclebutton=[[UIButton alloc]initWithFrame:CGRectMake(0, okP, PopupView.frame.size.width/2, okH)];
    [Canclebutton setTitle:cancel forState:UIControlStateNormal];
    [Canclebutton addTarget:self action:@selector(CanclebuttonMethod) forControlEvents:UIControlEventTouchUpInside];
    [Canclebutton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    Canclebutton.titleLabel.font=[UIFont systemFontOfSize:OkfontSize];
    [PopupView addSubview:Canclebutton];
}


-(void)dateChange:(id)sender{
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"hh:mma"];
    NSString *pickerTime=[outputFormatter stringFromDate:datePicker.date];
    titleDateLbl.text=pickerTime;
    
}

//////////////// select time ////////
-(void)dateOkbuttonMethod:(id)sender{
    view1.hidden=YES;
    [self setValueInKey];
    [floatingButton setImage:[UIImage imageNamed:@"end_habbit"] forState:UIControlStateNormal];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"hh:mma"];
    NSString *pickerTime=[outputFormatter stringFromDate:datePicker.date];
    NSDate *date=[outputFormatter dateFromString:pickerTime];
    
    [outputFormatter setDateFormat:@"HH"];
    int HH=[[outputFormatter stringFromDate:date] intValue];
    
    [outputFormatter setDateFormat:@"mm"];
    int mm=[[outputFormatter stringFromDate:date] intValue];
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components:unitFlags fromDate:[NSDate date]];
    comps.hour   = HH;
    comps.minute = mm;
    comps.second = 00;
    NSDate *newdate = [calendar dateFromComponents:comps];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:HabbitCondition]==nil) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:HabbitStartDate];
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:conditionalDate];
    }
    [[NSUserDefaults standardUserDefaults] setObject:newdate forKey:kldate];
    [[NSUserDefaults standardUserDefaults] setObject:habbitType forKey:HabbitCondition];
    
    [ScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self AleartMethod:pickerTime];
    [self content];
}

//////////// set balues in keys for further use///////////////
-(void)setValueInKey{
    [[NSUserDefaults standardUserDefaults] setInteger:AnulomInhaleValue forKey:anulomInhale];
    [[NSUserDefaults standardUserDefaults] setInteger:AnulomholdValue forKey:anulomHold];
    [[NSUserDefaults standardUserDefaults] setInteger:AnulomexhaleValue forKey:anulomExhale];
    [[NSUserDefaults standardUserDefaults] setInteger:AnulomAfholdValue forKey:anulomAfHold];
    [[NSUserDefaults standardUserDefaults] setInteger:AnulomRoundValue forKey:anulomRound];
    
    [[NSUserDefaults standardUserDefaults] setInteger: kapaltimeValue forKey:kapaltime];
    [[NSUserDefaults standardUserDefaults] setInteger:kapalroundValue forKey:kapalRound];
    
    [[NSUserDefaults standardUserDefaults] setInteger:UjjayiInhaleValue forKey:ProbUjjayiInhale];
    [[NSUserDefaults standardUserDefaults] setInteger:UjjayiHoldValue forKey:ProbUjjayiHold];
    [[NSUserDefaults standardUserDefaults] setInteger:UjjayiExhaleValue forKey:ProbUjjayiExhale];
    [[NSUserDefaults standardUserDefaults] setInteger:UjjayiRoundValue forKey:ProbUjjayiRound];
    
    [[NSUserDefaults standardUserDefaults] setInteger:SuryaInhale forKey:suryaInhale];
    [[NSUserDefaults standardUserDefaults] setInteger:SuryaHold forKey:suryaHold];
    [[NSUserDefaults standardUserDefaults] setInteger:SuryaExhale forKey:suryaExhale];
    [[NSUserDefaults standardUserDefaults] setInteger:SuryaRound forKey:suryaRound];
    
    [[NSUserDefaults standardUserDefaults] setInteger:BhramariInhale forKey:bhramariInhale];
    [[NSUserDefaults standardUserDefaults] setInteger:BhramariExhale forKey:bhramariExhale];
    [[NSUserDefaults standardUserDefaults] setInteger:BhramariRound forKey:bhramariRound];
    
    [[NSUserDefaults standardUserDefaults] setInteger:BhastrikaInhale forKey:bhastrikaInhale];
    [[NSUserDefaults standardUserDefaults] setInteger:BhastrikaExhale forKey:bhastrikaExhale];
    [[NSUserDefaults standardUserDefaults] setInteger:BhastrikaRound forKey:bhastrikaRound];
    
    [[NSUserDefaults standardUserDefaults] setInteger:udgeethInhale forKey:UdgeethInhale];
    [[NSUserDefaults standardUserDefaults] setInteger:udgeethexhaleO forKey:UdgeethExhale_O];
    [[NSUserDefaults standardUserDefaults] setInteger:udgeethexhaleM forKey:UdgeethExhale_M];
    [[NSUserDefaults standardUserDefaults] setInteger:udgeethround forKey:UdgeethRound];
    
    [[NSUserDefaults standardUserDefaults] setInteger:Bahyainhale forKey:bahyaInhale];
    [[NSUserDefaults standardUserDefaults] setInteger:Bahyaexhale forKey:bahyaExhale];
    [[NSUserDefaults standardUserDefaults] setInteger:bahyaround forKey:bahyaRound];
}

-(void)AleartMethod:(NSString *)time{
    CGRect rect=CGRectMake(0,self.view.frame.size.height-60 , self.view.frame.size.width, 60);
    aleartView=[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height+10, self.view.frame.size.width, 60)];
    UILabel *aleartLabel=[[UILabel alloc]initWithFrame:CGRectMake(20,10 , aleartView.frame.size.width-40, 40)];
    aleartView.backgroundColor=RGB(143, 0, 0);
    aleartLabel.backgroundColor=RGB(143, 0, 0);
    aleartLabel.textColor=[UIColor whiteColor];
    aleartLabel.font=[UIFont systemFontOfSize:15];
    aleartLabel.numberOfLines=5;
    aleartLabel.text=[NSString stringWithFormat:@"%@%@",NotificationAleart,time];
    [aleartView addSubview:aleartLabel];
    [self.view addSubview:aleartView];
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionCurlUp
                     animations:^(void) {
                         aleartView.frame=rect;
                     }
                     completion:NULL];
    
    [self performSelector:@selector(HideAleart) withObject:nil afterDelay:4.0];
}

-(void)HideAleart{
    CGRect rect=CGRectMake(0, self.view.frame.size.height+10, self.view.frame.size.width, 60);
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionCurlDown
                     animations:^(void) {
                         aleartView.frame=rect;
                     }
                     completion:NULL];
}

-(void)CanclebuttonMethod{
    view1.hidden=YES;
}

-(IBAction)backbutton:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

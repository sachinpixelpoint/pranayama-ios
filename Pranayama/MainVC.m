

#import "MainVC.h"
#import "UIColor+CreateMethods.h"

@interface MainVC ()

@end

@implementation MainVC

- (void)viewDidLoad
{
    [super viewDidLoad];
}

/*----------------------------------------------------*/
#pragma mark - Overriden Methods -
/*----------------------------------------------------*/

- (NSString *)segueIdentifierForIndexPathInLeftMenu:(NSIndexPath *)indexPath
{
    NSString *identifier = @"";
    switch (indexPath.row) {
        case 0:
            identifier = @"firstRow";
            break;
        case 1:
            identifier = @"secondRow";
            break;
    }
    
    return identifier;
}

- (NSString *)segueIdentifierForIndexPathInRightMenu:(NSIndexPath *)indexPath
{
    NSString *identifier = @"";
    if ( [[NSUserDefaults standardUserDefaults]boolForKey:HabbitMainCondition] && [[NSUserDefaults standardUserDefaults]boolForKey:delegateCondition] ) {
        identifier = @"secondRow";
    }
    else if([[NSUserDefaults standardUserDefaults]boolForKey:waterdelegate] && [[NSUserDefaults standardUserDefaults] boolForKey:waterSated]){
        identifier=@"thirdRow";
    }
    else if ([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaMainCon] && [[NSUserDefaults standardUserDefaults] boolForKey:DincharyaDelegate]){
        identifier=@"dincharyaRow";
    }
    else
    {
         identifier = @"firstRow";
    }
    return identifier;
}


- (CGFloat)leftMenuWidth
{
    return self.view.frame.size.width-80;
}

- (CGFloat)rightMenuWidth
{
    CGFloat returnValue;
    if (DEVICE==IPAD) {
        returnValue= 500;
    }
    else{
        returnValue=230;
    }
    return returnValue;
}

- (void)configureLeftMenuButton:(UIButton *)button
{
    button.frame = CGRectMake(0, 0, 23, 23);
    button.backgroundColor = [UIColor clearColor];
   [button setImage:[UIImage imageNamed:@"menu-icon.png"] forState:UIControlStateNormal];
}

- (void)configureRightMenuButton:(UIButton *)button
{
    button.frame = CGRectMake(0, 0, 23, 23);
    button.backgroundColor = [UIColor clearColor];
    [button setImage:[UIImage imageNamed:@"menu-icon.png"] forState:UIControlStateNormal];
}

- (void) configureSlideLayer:(CALayer *)layer
{
    layer.shadowColor = [UIColor blackColor].CGColor;
    layer.shadowOpacity = 1;
    layer.shadowOffset = CGSizeMake(0, 0);
    layer.shadowRadius = 0;
    layer.masksToBounds = NO;
    layer.shadowPath =[UIBezierPath bezierPathWithRect:self.view.layer.bounds].CGPath;
}

- (UIViewAnimationOptions) openAnimationCurve {
    return UIViewAnimationOptionCurveEaseOut;
}

- (UIViewAnimationOptions) closeAnimationCurve {
    return UIViewAnimationOptionCurveEaseOut;
}

- (AMPrimaryMenu)primaryMenu
{
    return AMPrimaryMenuLeft;
}


// Enabling Deepnes on left menu
- (BOOL)deepnessForLeftMenu
{
    return YES;
}

// Enabling Deepnes on left menu
- (BOOL)deepnessForRightMenu
{
    return YES;
}

// Enabling darkness while left menu is opening
- (CGFloat)maxDarknessWhileLeftMenu
{
    return 0;
}

// Enabling darkness while right menu is opening
- (CGFloat)maxDarknessWhileRightMenu
{
    return 0;
}

@end

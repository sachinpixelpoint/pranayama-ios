//
//  pranayamaTabelVC.m
//  Pranayama
//
//  Created by Manish Kumar on 01/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "pranayamaTabelVC.h"

@interface pranayamaTabelVC ()

@end

@implementation pranayamaTabelVC
@synthesize table1;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    table1.scrollEnabled = NO;
    self.navigationItem.title=@"Pranayam";
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    tabelList=[NSMutableArray arrayWithObjects:@"Anulom Vilom",@"Kapalbhati",@"Bharamari Pranayama",@"Surya Bhedana",@"Chandra Bhedana",@"Bhastrika",@"Sheetali", nil];
    // Do any additional setup after loading the view.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tabelList count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 72;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"system";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text=[tabelList objectAtIndex:indexPath.row];
    cell.textLabel.textColor=[UIColor whiteColor];
    int colors = indexPath.row%2;
    switch (colors) {
        case 0:
            cell.backgroundColor=[UIColor colorWithRed:143.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0f];
            break;
        case 1:
            cell.backgroundColor=[UIColor colorWithRed:150.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0f];
            break;
        default:
            break;
    }
    
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row==0) {
        [self performSegueWithIdentifier:@"anulom_vilom" sender:self];
    }
    else if (indexPath.row==1)
    {
        [self performSegueWithIdentifier:@"kapalbhati" sender:self];
    }
    else if (indexPath.row==2)
    {
        [self performSegueWithIdentifier:@"bhramari" sender:self];
    }
    else if (indexPath.row==3){
        [self performSegueWithIdentifier:@"surya_bhedna" sender:self];
    }
    else if (indexPath.row==4){
        [self performSegueWithIdentifier:@"report" sender:self];
    }
    else if (indexPath.row==5){
        [self performSegueWithIdentifier:@"report" sender:self];
    }
    else{
        [self performSegueWithIdentifier:@"report" sender:self];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

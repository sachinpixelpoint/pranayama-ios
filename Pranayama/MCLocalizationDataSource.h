//
//  MCLocalizationDataSource.h
//  Pranayama
//
//  Created by Manish Kumar on 02/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

@protocol MCLocalizationDataSource <NSObject>

- (NSArray *)supportedLanguages;
- (NSString *)defaultLanguage;
- (NSDictionary *)stringsForLanguage:(NSString *)language;

@end

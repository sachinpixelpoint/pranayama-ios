//
//  StepsViewController.m
//  Pranayama
//
//  Created by Manish Kumar on 01/07/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "StepsViewController.h"

@implementation StepsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.screenName=@"Steps";
       
    NSString *Yogasana=[[NSUserDefaults standardUserDefaults] objectForKey:YogasanCondition];
    if ([Yogasana isEqualToString:@"Sarvangasana"]) {
        [self Sarvangsanacontent];
    }
    else if ([Yogasana isEqualToString:@"Halasana"]){
        [self HalasanaContent];
    }
    else if ([Yogasana isEqualToString:@"Vipritkarani"]){
        [self VipritKarniContent];
    }
    else if ([Yogasana isEqualToString:@"Paschimottanasana"]){
        [self PashimothanasanaContent];
    }
    else if ([Yogasana isEqualToString:@"Dhanurasana"]){
        [self DhanurasaContent];
    }
    else if ([Yogasana isEqualToString:@"Balasana"]){
        [self BalasanaContent];
    }
    else if ([Yogasana isEqualToString:@"Hastapadasana"]){
        [self HastapadasanaContent];
    }
    else if ([Yogasana isEqualToString:@"Marjariasana"])
    {
        [self MarjariasanaContent];
    }
    else if ([Yogasana isEqualToString:@"Uttanasana"])
    {
        [self Uttanasanacontent];
    }
    else if ([Yogasana isEqualToString:@"Setu Bandhasana"]){
        [self Setucontent];
    }
    else{
        [self Virabhadrasanacontent];
    }
}

////////////////////////Sarvangsana content///////
-(void)Sarvangsanacontent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc]init];
    UILabel *label3 = [[UILabel alloc]init];
    UILabel *label4 = [[UILabel alloc]init];
    UILabel *label5 = [[UILabel alloc]init];
    UILabel *label6 = [[UILabel alloc]init];
    UILabel *label7 = [[UILabel alloc]init];
    UILabel *label8 = [[UILabel alloc]init];
    UILabel *label9 = [[UILabel alloc]init];
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);
    label8.font=Ralewayfont(14);
    label9.font=Ralewayfont(14);
    
    label1.text=[MCLocalization stringForKey:@"sarvagsana_step1"];
    label2.text=[MCLocalization stringForKey:@"sarvagsana_step2"];
    label3.text=[MCLocalization stringForKey:@"sarvagsana_step3"];
    label4.text=[MCLocalization stringForKey:@"sarvagsana_step4"];
    label5.text=[MCLocalization stringForKey:@"sarvagsana_step5"];
    label6.text=[MCLocalization stringForKey:@"sarvagsana_step6"];
    label7.text=[MCLocalization stringForKey:@"sarvagsana_step7"];
    label8.text=[MCLocalization stringForKey:@"sarvagsana_step8"];
    label9.text=[MCLocalization stringForKey:@"sarvagsana_step9"];
    
    
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [myScrollView addSubview:label1];
    
    label2.frame=CGRectMake(15, label1.frame.origin.y+label1.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [myScrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, myScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    label3.textColor = [UIColor redColor];
    [myScrollView addSubview:label3];
    
    label4.frame=CGRectMake(15, label3.frame.origin.y+label3.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [myScrollView addSubview:label4];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-120, label4.frame.origin.y+label4.frame.size.height+5, 260,80)];
    imageView1.image = [UIImage imageNamed:@"sarvangsana_step1"];
    [myScrollView addSubview:imageView1];
    
    label5.frame=CGRectMake(15, imageView1.frame.origin.y+imageView1.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [myScrollView addSubview:label5];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-80, label5.frame.origin.y+label5.frame.size.height+5, 160, 140)];
    imageView2.image = [UIImage imageNamed:@"sarvangsana_step2"];
    [myScrollView addSubview:imageView2];
    label6.frame=CGRectMake(15, imageView2.frame.origin.y+imageView2.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [myScrollView addSubview:label6];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-80, label6.frame.origin.y+label6.frame.size.height+5, 160, 140)];
    imageView3.image = [UIImage imageNamed:@"sarvangsana_step3"];
    [myScrollView addSubview:imageView3];
    label7.frame=CGRectMake(15, imageView3.frame.origin.y+imageView3.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [myScrollView addSubview:label7];
    
    UIImageView *imageView4 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-80, label7.frame.origin.y+label7.frame.size.height+5, 120, 200)];
    imageView4.image = [UIImage imageNamed:@"sarvangsana_step4"];
    [myScrollView addSubview:imageView4];
    label8.frame=CGRectMake(15, imageView4.frame.origin.y+imageView4.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label8.numberOfLines=0;
    [label8 sizeToFit];
    [myScrollView addSubview:label8];
    
    UIImageView *imageView5 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-80, label8.frame.origin.y+label8.frame.size.height+5, 120, 200)];
    imageView5.image = [UIImage imageNamed:@"sarvangsana_step5"];
    [myScrollView addSubview:imageView5];
    label9.frame=CGRectMake(15, imageView5.frame.origin.y+imageView5.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label9.numberOfLines=0;
    [label9 sizeToFit];
    [myScrollView addSubview:label9];

    myScrollView.contentSize=CGSizeMake(myScrollView.frame.size.width,label9.frame.origin.y+label9.frame.size.height+20);
}

/////////////////////////HalasanaContent////////////////

-(void)HalasanaContent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc]init];
    UILabel *label3 = [[UILabel alloc]init];
    UILabel *label4 = [[UILabel alloc]init];
    UILabel *label5 = [[UILabel alloc]init];
    UILabel *label6 = [[UILabel alloc]init];
    UILabel *label7 = [[UILabel alloc]init];
    UILabel *label8 = [[UILabel alloc]init];
    UILabel *label9 = [[UILabel alloc]init];
    UILabel *label10 = [[UILabel alloc]init];
    
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);
    label8.font=Ralewayfont(14);
    label9.font=Ralewayfont(14);
    label10.font=Ralewayfont(14);
    
    label1.text=[MCLocalization stringForKey:@"halasana_step1"];
    label2.text=[MCLocalization stringForKey:@"halasana_step2"];
    label3.text=[MCLocalization stringForKey:@"halasana_step3"];
    label4.text=[MCLocalization stringForKey:@"halasana_step4"];
    label5.text=[MCLocalization stringForKey:@"halasana_step5"];
    label6.text=[MCLocalization stringForKey:@"halasana_step6"];
    label7.text=[MCLocalization stringForKey:@"halasana_step7"];
    label8.text=[MCLocalization stringForKey:@"halasana_step8"];
    label9.text=[MCLocalization stringForKey:@"halasana_step9"];
    label10.text=[MCLocalization stringForKey:@"halasana_step10"];
    
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [myScrollView addSubview:label1];
    
    label2.frame=CGRectMake(15, label1.frame.origin.y+label1.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [myScrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, myScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    label3.textColor = [UIColor redColor];
    [myScrollView addSubview:label3];
    
    label4.frame=CGRectMake(15, label3.frame.origin.y+label3.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [myScrollView addSubview:label4];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-130, label4.frame.origin.y+label4.frame.size.height+5, 260,80)];
    imageView1.image = [UIImage imageNamed:@"sarvangsana_step1"];
    [myScrollView addSubview:imageView1];
    label5.frame=CGRectMake(15, imageView1.frame.origin.y+imageView1.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [myScrollView addSubview:label5];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-80, label5.frame.origin.y+label5.frame.size.height+5, 160, 140)];
    imageView2.image = [UIImage imageNamed:@"sarvangsana_step2"];
    [myScrollView addSubview:imageView2];
    label6.frame=CGRectMake(15, imageView2.frame.origin.y+imageView2.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [myScrollView addSubview:label6];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-90, label6.frame.origin.y+label6.frame.size.height+5, 180, 80)];
    imageView3.image = [UIImage imageNamed:@"halasana_step3"];
    [myScrollView addSubview:imageView3];
    label7.frame=CGRectMake(15, imageView3.frame.origin.y+imageView3.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [myScrollView addSubview:label7];
    
    UIImageView *imageView4 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-90, label7.frame.origin.y+label7.frame.size.height+5, 180, 80)];
    imageView4.image = [UIImage imageNamed:@"halasana_step4"];
    [myScrollView addSubview:imageView4];
    label8.frame=CGRectMake(15, imageView4.frame.origin.y+imageView4.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label8.numberOfLines=0;
    [label8 sizeToFit];
    [myScrollView addSubview:label8];
    
    UIImageView *imageView5 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-90, label8.frame.origin.y+label8.frame.size.height+5, 180, 80)];
    imageView5.image = [UIImage imageNamed:@"halasana_step5"];
    [myScrollView addSubview:imageView5];
    label9.frame=CGRectMake(15, imageView5.frame.origin.y+imageView5.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label9.numberOfLines=0;
    [label9 sizeToFit];
    [myScrollView addSubview:label9];
    
    UIImageView *imageView6 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-90, label9.frame.origin.y+label9.frame.size.height+5, 180, 80)];
    imageView6.image = [UIImage imageNamed:@"halasana_step6"];
    [myScrollView addSubview:imageView6];
    label10.frame=CGRectMake(15, imageView6.frame.origin.y+imageView6.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label10.numberOfLines=0;
    [label10 sizeToFit];
    [myScrollView addSubview:label10];
    
    myScrollView.contentSize=CGSizeMake(myScrollView.frame.size.width,label10.frame.origin.y+label10.frame.size.height+20);
}


/////////////////VIpritkarni///////////////
-(void)VipritKarniContent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc]init];
    UILabel *label3 = [[UILabel alloc]init];
    UILabel *label4 = [[UILabel alloc]init];
    UILabel *label5 = [[UILabel alloc]init];
    UILabel *label6 = [[UILabel alloc]init];
    UILabel *label7 = [[UILabel alloc]init];
    UILabel *label8 = [[UILabel alloc]init];
    
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);
    label8.font=Ralewayfont(14);
    
    label1.text=[MCLocalization stringForKey:@"vipritkarni_step1"];
    label2.text=[MCLocalization stringForKey:@"vipritkarni_step2"];
    label3.text=[MCLocalization stringForKey:@"vipritkarni_step3"];
    label4.text=[MCLocalization stringForKey:@"vipritkarni_step4"];
    label5.text=[MCLocalization stringForKey:@"vipritkarni_step5"];
    label6.text=[MCLocalization stringForKey:@"vipritkarni_step6"];
    label7.text=[MCLocalization stringForKey:@"vipritkarni_step7"];
    label8.text=[MCLocalization stringForKey:@"vipritkarni_step8"];

    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [myScrollView addSubview:label1];
    
    label2.frame=CGRectMake(15, label1.frame.origin.y+label1.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [myScrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, myScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    label3.textColor = [UIColor redColor];
    [myScrollView addSubview:label3];
    
    label4.frame=CGRectMake(15, label3.frame.origin.y+label3.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [myScrollView addSubview:label4];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-130, label4.frame.origin.y+label4.frame.size.height+5, 260,80)];
    imageView1.image = [UIImage imageNamed:@"sarvangsana_step1"];
    [myScrollView addSubview:imageView1];
    label5.frame=CGRectMake(15, imageView1.frame.origin.y+imageView1.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [myScrollView addSubview:label5];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-120, label5.frame.origin.y+label5.frame.size.height+5, 240, 100)];
    imageView2.image = [UIImage imageNamed:@"vitkar_step2"];
    [myScrollView addSubview:imageView2];
    label6.frame=CGRectMake(15, imageView2.frame.origin.y+imageView2.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [myScrollView addSubview:label6];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-90, label6.frame.origin.y+label6.frame.size.height+5, 180, 140)];
    imageView3.image = [UIImage imageNamed:@"sarvangsana_step2"];
    [myScrollView addSubview:imageView3];
    label7.frame=CGRectMake(15, imageView3.frame.origin.y+imageView3.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [myScrollView addSubview:label7];
    
    UIImageView *imageView4 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-50, label7.frame.origin.y+label7.frame.size.height+5, 100, 200)];
    imageView4.image = [UIImage imageNamed:@"vitkar_step4"];
    [myScrollView addSubview:imageView4];
    label8.frame=CGRectMake(15, imageView4.frame.origin.y+imageView4.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label8.numberOfLines=0;
    [label8 sizeToFit];
    [myScrollView addSubview:label8];
    
    myScrollView.contentSize=CGSizeMake(myScrollView.frame.size.width,label8.frame.origin.y+label8.frame.size.height+20);
    
}


/////////////////Pashimothanasana Content///////////////
-(void)PashimothanasanaContent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc]init];
    UILabel *label3 = [[UILabel alloc]init];
    UILabel *label4 = [[UILabel alloc]init];
    UILabel *label5 = [[UILabel alloc]init];
    UILabel *label6 = [[UILabel alloc]init];
    UILabel *label7 = [[UILabel alloc]init];
    
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);

    label1.text=[MCLocalization stringForKey:@"pas_step1"];
    label2.text=[MCLocalization stringForKey:@"pas_step2"];
    label3.text=[MCLocalization stringForKey:@"pas_step3"];
    label4.text=[MCLocalization stringForKey:@"pas_step4"];
    label5.text=[MCLocalization stringForKey:@"pas_step5"];
    label6.text=[MCLocalization stringForKey:@"pas_step6"];
    label7.text=[MCLocalization stringForKey:@"pas_step7"];

    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [myScrollView addSubview:label1];
    
    label2.frame=CGRectMake(15, label1.frame.origin.y+label1.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [myScrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, myScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    label3.textColor = [UIColor redColor];
    [myScrollView addSubview:label3];
    
    label4.frame=CGRectMake(15, label3.frame.origin.y+label3.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [myScrollView addSubview:label4];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-70, label4.frame.origin.y+label4.frame.size.height+5, 160,200)];
    imageView1.image = [UIImage imageNamed:@"seatedfor_step1"];
    [myScrollView addSubview:imageView1];
    label5.frame=CGRectMake(15, imageView1.frame.origin.y+imageView1.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [myScrollView addSubview:label5];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-80, label5.frame.origin.y+label5.frame.size.height+5, 160, 120)];
    imageView2.image = [UIImage imageNamed:@"seatedfor_step2"];
    [myScrollView addSubview:imageView2];
    label6.frame=CGRectMake(15, imageView2.frame.origin.y+imageView2.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [myScrollView addSubview:label6];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-90, label6.frame.origin.y+label6.frame.size.height+5, 160, 80)];
    imageView3.image = [UIImage imageNamed:@"seatedfor_step3"];
    [myScrollView addSubview:imageView3];
    label7.frame=CGRectMake(15, imageView3.frame.origin.y+imageView3.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [myScrollView addSubview:label7];

    myScrollView.contentSize=CGSizeMake(myScrollView.frame.size.width,label7.frame.origin.y+label7.frame.size.height+20);
}


///////////////Dhanurasana Content///////////
-(void)DhanurasaContent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc]init];
    UILabel *label3 = [[UILabel alloc]init];
    UILabel *label4 = [[UILabel alloc]init];
    UILabel *label5 = [[UILabel alloc]init];
    UILabel *label6 = [[UILabel alloc]init];
    UILabel *label7 = [[UILabel alloc]init];
    UILabel *label8 = [[UILabel alloc]init];
    
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);
    label8.font=Ralewayfont(14);
    
    label1.text=[MCLocalization stringForKey:@"dhanu_step1"];
    label2.text=[MCLocalization stringForKey:@"dhanu_step2"];
    label3.text=[MCLocalization stringForKey:@"dhanu_step3"];
    label4.text=[MCLocalization stringForKey:@"dhanu_step4"];
    label5.text=[MCLocalization stringForKey:@"dhanu_step5"];
    label6.text=[MCLocalization stringForKey:@"dhanu_step6"];
    label7.text=[MCLocalization stringForKey:@"dhanu_step7"];
    label8.text=[MCLocalization stringForKey:@"dhanu_step8"];
    
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [myScrollView addSubview:label1];
    
    label2.frame=CGRectMake(15, label1.frame.origin.y+label1.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [myScrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, myScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    label3.textColor = [UIColor redColor];
    [myScrollView addSubview:label3];
    
    label4.frame=CGRectMake(15, label3.frame.origin.y+label3.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [myScrollView addSubview:label4];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-130, label4.frame.origin.y+label4.frame.size.height+5, 260,60)];
    imageView1.image = [UIImage imageNamed:@"dhanurasanastep1"];
    [myScrollView addSubview:imageView1];
    label5.frame=CGRectMake(15, imageView1.frame.origin.y+imageView1.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [myScrollView addSubview:label5];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-80, label5.frame.origin.y+label5.frame.size.height+5, 160, 80)];
    imageView2.image = [UIImage imageNamed:@"dhanurasanastep2"];
    [myScrollView addSubview:imageView2];
    label6.frame=CGRectMake(15, imageView2.frame.origin.y+imageView2.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [myScrollView addSubview:label6];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-90, label6.frame.origin.y+label6.frame.size.height+5, 180, 80)];
    imageView3.image = [UIImage imageNamed:@"dhanurasanastep3"];
    [myScrollView addSubview:imageView3];
    label7.frame=CGRectMake(15, imageView3.frame.origin.y+imageView3.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [myScrollView addSubview:label7];
    
    UIImageView *imageView4 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-90, label7.frame.origin.y+label7.frame.size.height+5, 160, 80)];
    imageView4.image = [UIImage imageNamed:@"dhanurasanastep4"];
    [myScrollView addSubview:imageView4];
    label8.frame=CGRectMake(15, imageView4.frame.origin.y+imageView4.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label8.numberOfLines=0;
    [label8 sizeToFit];
    [myScrollView addSubview:label8];

    myScrollView.contentSize=CGSizeMake(myScrollView.frame.size.width,label8.frame.origin.y+label8.frame.size.height+20);
    
}


////////////////BALASANA content//////////////
-(void)BalasanaContent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc]init];
    UILabel *label3 = [[UILabel alloc]init];
    UILabel *label4 = [[UILabel alloc]init];
    UILabel *label5 = [[UILabel alloc]init];
    UILabel *label6 = [[UILabel alloc]init];
    UILabel *label7 = [[UILabel alloc]init];
    
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);
    
    label1.text=[MCLocalization stringForKey:@"bala_step1"];
    label2.text=[MCLocalization stringForKey:@"bala_step2"];
    label3.text=[MCLocalization stringForKey:@"bala_step3"];
    label4.text=[MCLocalization stringForKey:@"bala_step4"];
    label5.text=[MCLocalization stringForKey:@"bala_step5"];
    label6.text=[MCLocalization stringForKey:@"bala_step6"];
    label7.text=[MCLocalization stringForKey:@"bala_step7"];
    
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [myScrollView addSubview:label1];
    
    label2.frame=CGRectMake(15, label1.frame.origin.y+label1.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [myScrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, myScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    label3.textColor = [UIColor redColor];
    [myScrollView addSubview:label3];
    
    label4.frame=CGRectMake(15, label3.frame.origin.y+label3.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [myScrollView addSubview:label4];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-60, label4.frame.origin.y+label4.frame.size.height+5, 120,160)];
    imageView1.image = [UIImage imageNamed:@"childpose_step1"];
    [myScrollView addSubview:imageView1];
    label5.frame=CGRectMake(15, imageView1.frame.origin.y+imageView1.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [myScrollView addSubview:label5];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-80, label5.frame.origin.y+label5.frame.size.height+5, 200, 80)];
    imageView2.image = [UIImage imageNamed:@"childpose_step2"];
    [myScrollView addSubview:imageView2];
    label6.frame=CGRectMake(15, imageView2.frame.origin.y+imageView2.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [myScrollView addSubview:label6];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-90, label6.frame.origin.y+label6.frame.size.height+5, 200, 80)];
    imageView3.image = [UIImage imageNamed:@"childpose_step3"];
    [myScrollView addSubview:imageView3];
    label7.frame=CGRectMake(15, imageView3.frame.origin.y+imageView3.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [myScrollView addSubview:label7];

    myScrollView.contentSize=CGSizeMake(myScrollView.frame.size.width,label7.frame.origin.y+label7.frame.size.height+20);
}


/////////////////Hastapadasana Content/////////////
-(void)HastapadasanaContent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc]init];
    UILabel *label3 = [[UILabel alloc]init];
    UILabel *label4 = [[UILabel alloc]init];
    UILabel *label5 = [[UILabel alloc]init];
    UILabel *label6 = [[UILabel alloc]init];
    UILabel *label7 = [[UILabel alloc]init];
    UILabel *label8 = [[UILabel alloc]init];
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);
    label8.font=Ralewayfont(14);
    
    label1.text=[MCLocalization stringForKey:@"hast_step1"];
    label2.text=[MCLocalization stringForKey:@"hast_step2"];
    label3.text=[MCLocalization stringForKey:@"hast_step3"];
    label4.text=[MCLocalization stringForKey:@"hast_step4"];
    label5.text=[MCLocalization stringForKey:@"hast_step5"];
    label6.text=[MCLocalization stringForKey:@"hast_step6"];
    label7.text=[MCLocalization stringForKey:@"hast_step7"];
    label8.text=[MCLocalization stringForKey:@"hast_step8"];
    
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [myScrollView addSubview:label1];
    
    label2.frame=CGRectMake(15, label1.frame.origin.y+label1.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [myScrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, myScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    label3.textColor = [UIColor redColor];
    [myScrollView addSubview:label3];
    
    label4.frame=CGRectMake(15, label3.frame.origin.y+label3.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [myScrollView addSubview:label4];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-50, label4.frame.origin.y+label4.frame.size.height+5, 80,160)];
    imageView1.image = [UIImage imageNamed:@"standing_forward_step1"];
    [myScrollView addSubview:imageView1];
    label5.frame=CGRectMake(15, imageView1.frame.origin.y+imageView1.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [myScrollView addSubview:label5];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-50, label5.frame.origin.y+label5.frame.size.height+5, 100, 160)];
    imageView2.image = [UIImage imageNamed:@"standing_forward_step2"];
    [myScrollView addSubview:imageView2];
    label6.frame=CGRectMake(15, imageView2.frame.origin.y+imageView2.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [myScrollView addSubview:label6];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-50, label6.frame.origin.y+label6.frame.size.height+5, 100, 120)];
    imageView3.image = [UIImage imageNamed:@"standing_forward_step3"];
    [myScrollView addSubview:imageView3];
    label7.frame=CGRectMake(15, imageView3.frame.origin.y+imageView3.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [myScrollView addSubview:label7];
    
    UIImageView *imageView4 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-50, label7.frame.origin.y+label7.frame.size.height+5, 100, 160)];
    imageView4.image = [UIImage imageNamed:@"standing_forward_step4"];
    [myScrollView addSubview:imageView4];
    label8.frame=CGRectMake(15, imageView4.frame.origin.y+imageView4.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label8.numberOfLines=0;
    [label8 sizeToFit];
    [myScrollView addSubview:label8];

    myScrollView.contentSize=CGSizeMake(myScrollView.frame.size.width,label8.frame.origin.y+label8.frame.size.height+20);
    
}


///////////////////Marjariasana Content//////////
-(void)MarjariasanaContent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc]init];
    UILabel *label3 = [[UILabel alloc]init];
    UILabel *label4 = [[UILabel alloc]init];
    UILabel *label5 = [[UILabel alloc]init];
    UILabel *label6 = [[UILabel alloc]init];
    UILabel *label7 = [[UILabel alloc]init];
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);
    
    label1.text=[MCLocalization stringForKey:@"marj_step1"];
    label2.text=[MCLocalization stringForKey:@"marj_step2"];
    label3.text=[MCLocalization stringForKey:@"marj_step3"];
    label4.text=[MCLocalization stringForKey:@"marj_step4"];
    label5.text=[MCLocalization stringForKey:@"marj_step5"];
    label6.text=[MCLocalization stringForKey:@"marj_step6"];
    label7.text=[MCLocalization stringForKey:@"marj_step7"];
    
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [myScrollView addSubview:label1];
    
    label2.frame=CGRectMake(15, label1.frame.origin.y+label1.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [myScrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, myScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    label3.textColor = [UIColor redColor];
    [myScrollView addSubview:label3];
    
    label4.frame=CGRectMake(15, label3.frame.origin.y+label3.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [myScrollView addSubview:label4];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-100, label4.frame.origin.y+label4.frame.size.height+5, 200,80)];
    imageView1.image = [UIImage imageNamed:@"cat_step1"];
    [myScrollView addSubview:imageView1];
    label5.frame=CGRectMake(15, imageView1.frame.origin.y+imageView1.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [myScrollView addSubview:label5];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-100, label5.frame.origin.y+label5.frame.size.height+5, 200, 80)];
    imageView2.image = [UIImage imageNamed:@"cat_step2"];
    [myScrollView addSubview:imageView2];
    label6.frame=CGRectMake(15, imageView2.frame.origin.y+imageView2.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [myScrollView addSubview:label6];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-100, label6.frame.origin.y+label6.frame.size.height+5, 200, 80)];
    imageView3.image = [UIImage imageNamed:@"cat_step3"];
    [myScrollView addSubview:imageView3];
    label7.frame=CGRectMake(15, imageView3.frame.origin.y+imageView3.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [myScrollView addSubview:label7];

    myScrollView.contentSize=CGSizeMake(myScrollView.frame.size.width,label7.frame.origin.y+label7.frame.size.height+20);
}



-(void)Uttanasanacontent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc]init];
    UILabel *label3 = [[UILabel alloc]init];
    UILabel *label4 = [[UILabel alloc]init];
    UILabel *label5 = [[UILabel alloc]init];
    UILabel *label6 = [[UILabel alloc]init];
    UILabel *label7 = [[UILabel alloc]init];
    UILabel *label8 = [[UILabel alloc]init];
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);
    label8.font=Ralewayfont(14);
    
    label1.text=[MCLocalization stringForKey:@"Uttanasana_step1"];
    label2.text=[MCLocalization stringForKey:@"Uttanasana_step2"];
    label3.text=[MCLocalization stringForKey:@"Uttanasana_step3"];
    label4.text=[MCLocalization stringForKey:@"Uttanasana_step4"];
    label5.text=[MCLocalization stringForKey:@"Uttanasana_step5"];
    label6.text=[MCLocalization stringForKey:@"Uttanasana_step6"];
    label7.text=[MCLocalization stringForKey:@"Uttanasana_step7"];
    label8.text=[MCLocalization stringForKey:@"Uttanasana_step8"];
    
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [myScrollView addSubview:label1];
    
    label2.frame=CGRectMake(15, label1.frame.origin.y+label1.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [myScrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, myScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    label3.textColor = [UIColor redColor];
    [myScrollView addSubview:label3];
    
    label4.frame=CGRectMake(15, label3.frame.origin.y+label3.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [myScrollView addSubview:label4];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-50, label4.frame.origin.y+label4.frame.size.height+5, 50,130)];
    imageView1.image = [UIImage imageNamed:@"uttasana_step1"];
    [myScrollView addSubview:imageView1];
    label5.frame=CGRectMake(15, imageView1.frame.origin.y+imageView1.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [myScrollView addSubview:label5];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-60, label5.frame.origin.y+label5.frame.size.height+5, 100, 150)];
    imageView2.image = [UIImage imageNamed:@"uttasana_step2"];
    [myScrollView addSubview:imageView2];
    label6.frame=CGRectMake(15, imageView2.frame.origin.y+imageView2.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [myScrollView addSubview:label6];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-50, label6.frame.origin.y+label6.frame.size.height+5, 100, 150)];
    imageView3.image = [UIImage imageNamed:@"uttasana_step3"];
    [myScrollView addSubview:imageView3];
    label7.frame=CGRectMake(15, imageView3.frame.origin.y+imageView3.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [myScrollView addSubview:label7];
    
    UIImageView *imageView4 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-60, label7.frame.origin.y+label7.frame.size.height+5, 100, 150)];
    imageView4.image = [UIImage imageNamed:@"uttasana_step4"];
    [myScrollView addSubview:imageView4];
    label8.frame=CGRectMake(15, imageView4.frame.origin.y+imageView4.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label8.numberOfLines=0;
    [label8 sizeToFit];
    [myScrollView addSubview:label8];

    myScrollView.contentSize=CGSizeMake(myScrollView.frame.size.width,label8.frame.origin.y+label8.frame.size.height+20);
}



-(void)Setucontent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc]init];
    UILabel *label3 = [[UILabel alloc]init];
    UILabel *label4 = [[UILabel alloc]init];
    UILabel *label5 = [[UILabel alloc]init];
    UILabel *label6 = [[UILabel alloc]init];
    UILabel *label7 = [[UILabel alloc]init];
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);
    
    label1.text=[MCLocalization stringForKey:@"Setu_step1"];
    label2.text=[MCLocalization stringForKey:@"Setu_step2"];
    label3.text=[MCLocalization stringForKey:@"Setu_step3"];
    label4.text=[MCLocalization stringForKey:@"Setu_step4"];
    label5.text=[MCLocalization stringForKey:@"Setu_step5"];
    label6.text=[MCLocalization stringForKey:@"Setu_step6"];
    label7.text=[MCLocalization stringForKey:@"Setu_step7"];
    
    
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [myScrollView addSubview:label1];
    
    label2.frame=CGRectMake(15, label1.frame.origin.y+label1.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [myScrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, myScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    label3.textColor = [UIColor redColor];
    [myScrollView addSubview:label3];
    
    label4.frame=CGRectMake(15, label3.frame.origin.y+label3.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [myScrollView addSubview:label4];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-100, label4.frame.origin.y+label4.frame.size.height+5, 200,80)];
    imageView1.image = [UIImage imageNamed:@"bridge_step1"];
    [myScrollView addSubview:imageView1];
    label5.frame=CGRectMake(15, imageView1.frame.origin.y+imageView1.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [myScrollView addSubview:label5];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-100, label5.frame.origin.y+label5.frame.size.height+5, 200, 80)];
    imageView2.image = [UIImage imageNamed:@"bridge_step2"];
    [myScrollView addSubview:imageView2];
    label6.frame=CGRectMake(15, imageView2.frame.origin.y+imageView2.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [myScrollView addSubview:label6];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-100, label6.frame.origin.y+label6.frame.size.height+5, 200, 80)];
    imageView3.image = [UIImage imageNamed:@"bridge_step3"];
    [myScrollView addSubview:imageView3];
    label7.frame=CGRectMake(15, imageView3.frame.origin.y+imageView3.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [myScrollView addSubview:label7];
    
    myScrollView.contentSize=CGSizeMake(myScrollView.frame.size.width,label7.frame.origin.y+label7.frame.size.height+20);
}



-(void)Virabhadrasanacontent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, myScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc]init];
    UILabel *label3 = [[UILabel alloc]init];
    UILabel *label4 = [[UILabel alloc]init];
    UILabel *label5 = [[UILabel alloc]init];
    UILabel *label6 = [[UILabel alloc]init];
    UILabel *label7 = [[UILabel alloc]init];
    UILabel *label8 = [[UILabel alloc]init];
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(14);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(14);
    label8.font=Ralewayfont(14);
    
    label1.text=[MCLocalization stringForKey:@"Virabhadrasana_step1"];
    label2.text=[MCLocalization stringForKey:@"Virabhadrasana_step2"];
    label3.text=[MCLocalization stringForKey:@"Virabhadrasana_step3"];
    label4.text=[MCLocalization stringForKey:@"Virabhadrasana_step4"];
    label5.text=[MCLocalization stringForKey:@"Virabhadrasana_step5"];
    label6.text=[MCLocalization stringForKey:@"Virabhadrasana_step6"];
    label7.text=[MCLocalization stringForKey:@"Virabhadrasana_step7"];
    label8.text=[MCLocalization stringForKey:@"Virabhadrasana_step8"];
    
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [myScrollView addSubview:label1];
    
    label2.frame=CGRectMake(15, label1.frame.origin.y+label1.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [myScrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, myScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    [label3 sizeToFit];
    label3.textColor = [UIColor redColor];
    [myScrollView addSubview:label3];
    
    label4.frame=CGRectMake(15, label3.frame.origin.y+label3.frame.size.height+10, myScrollView.frame.size.width-25, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [myScrollView addSubview:label4];
    
    UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-40, label4.frame.origin.y+label4.frame.size.height+5, 70,150)];
    imageView1.image = [UIImage imageNamed:@"warrior_step1"];
    [myScrollView addSubview:imageView1];
    label5.frame=CGRectMake(15, imageView1.frame.origin.y+imageView1.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label5.numberOfLines=0;
    [label5 sizeToFit];
    [myScrollView addSubview:label5];
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-60, label5.frame.origin.y+label5.frame.size.height+5, 120, 150)];
    imageView2.image = [UIImage imageNamed:@"warrior_step2"];
    [myScrollView addSubview:imageView2];
    label6.frame=CGRectMake(15, imageView2.frame.origin.y+imageView2.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [myScrollView addSubview:label6];
    
    UIImageView *imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-60, label6.frame.origin.y+label6.frame.size.height+5, 120, 150)];
    imageView3.image = [UIImage imageNamed:@"warrior_step3"];
    [myScrollView addSubview:imageView3];
    label7.frame=CGRectMake(15, imageView3.frame.origin.y+imageView3.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label7.numberOfLines=0;
    [label7 sizeToFit];
    [myScrollView addSubview:label7];
    
    UIImageView *imageView4 = [[UIImageView alloc] initWithFrame:CGRectMake((myScrollView.frame.size.width/2)-60, label7.frame.origin.y+label7.frame.size.height+5, 120, 150)];
    imageView4.image = [UIImage imageNamed:@"warrior_step4"];
    [myScrollView addSubview:imageView4];
    label8.frame=CGRectMake(15, imageView4.frame.origin.y+imageView4.frame.size.height+5, myScrollView.frame.size.width-25, 100);
    label8.numberOfLines=0;
    [label8 sizeToFit];
    [myScrollView addSubview:label8];
    
    myScrollView.contentSize=CGSizeMake(myScrollView.frame.size.width,label8.frame.origin.y+label8.frame.size.height+20);
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end

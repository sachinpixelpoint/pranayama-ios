//
//  DincharyaReport+CoreDataProperties.h
//  Pranayama
//
//  Created by Manish Kumar on 28/10/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "DincharyaReport.h"

NS_ASSUME_NONNULL_BEGIN

@interface DincharyaReport (CoreDataProperties)
@property (nullable, nonatomic, retain) NSString *dincharya_name;
@property (nullable, nonatomic, retain) NSDate *date;
@property (nullable, nonatomic, retain) NSNumber *anulomvilom;
@property (nullable, nonatomic, retain) NSNumber *kapalbhati;
@property (nullable, nonatomic, retain) NSNumber *bhramari;
@property (nullable, nonatomic, retain) NSNumber *surya;
@property (nullable, nonatomic, retain) NSNumber *chandra;
@property (nullable, nonatomic, retain) NSNumber *bhastrika;
@property (nullable, nonatomic, retain) NSNumber *sheetali;
@property (nullable, nonatomic, retain) NSNumber *ujjayi;
@property (nullable, nonatomic, retain) NSNumber *meditative;
@property (nullable, nonatomic, retain) NSNumber *udgeeth;
@property (nullable, nonatomic, retain) NSNumber *bahya;

@property (nullable, nonatomic, retain) NSNumber *sarvangasana;
@property (nullable, nonatomic, retain) NSNumber *halasana;
@property (nullable, nonatomic, retain) NSNumber *vipritkarani;
@property (nullable, nonatomic, retain) NSNumber *paschimottanasana;
@property (nullable, nonatomic, retain) NSNumber *dhanurasana;
@property (nullable, nonatomic, retain) NSNumber *balasana;
@property (nullable, nonatomic, retain) NSNumber *hastapadasana;
@property (nullable, nonatomic, retain) NSNumber *marjariasana;
@property (nullable, nonatomic, retain) NSNumber *uttanasana;
@property (nullable, nonatomic, retain) NSNumber *setu;
@property (nullable, nonatomic, retain) NSNumber *virabhadrasana;
@end

NS_ASSUME_NONNULL_END

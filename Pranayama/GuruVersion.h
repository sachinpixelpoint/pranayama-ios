//
//  GuruVersion.h
//  Pranayama
//
//  Created by Manish Kumar on 16/09/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@interface GuruVersion : UIViewController<SKProductsRequestDelegate, SKPaymentTransactionObserver>
{
     UILabel*textlabel;
     UIImageView *image;
     UILabel*gurulbl,*fromLabel,*from,*from1,*from2,*from3;
    IBOutlet UIScrollView*scroller;
     int fontSize;
}

@end

//
//  RightMenuVC.m
//  AMSlideMenu
//


#import "RightMenuVC.h"
#import "AppDelegate.h"


@implementation RightMenuVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    if (DEVICE==IPAD) {
        fontSize=24;
        sectionHeight=70;
    }
    else{
        fontSize=16;
        sectionHeight=40;
    }
   
    [self ChangeText];
    indexPathArray=[[NSMutableArray alloc] init];
    [self.tableView setSeparatorColor:[UIColor clearColor]];
    arraycount=[[NSMutableArray alloc]initWithObjects:@"sittingdrower",@"mudradra",@"bandhdrower",@"setting_icon", nil];
    arrayimage=[[NSMutableArray alloc] initWithObjects:@"share",@"email",@"email",@"email", nil];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 && ![UIApplication sharedApplication].isStatusBarHidden)
    {
        self.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
    }
       [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTable:) name:@"RefreshTable" object:nil];
    float xPos=55;
    if (Device_Type==iPhone6) {
        xPos=80;
    }
    else if (Device_Type==iPhone6P){
        xPos=85;
    }
}
   
-(void)ChangeText{
    
    self.navigationItem.title = [MCLocalization stringForKey:@"home"];
    arrayTitle=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"how_to_sit"],[MCLocalization stringForKey:@"mudras"],[MCLocalization stringForKey:@"bandh"],[MCLocalization stringForKey:@"setting"], nil];
    arraysection=[[NSMutableArray alloc]initWithObjects:[MCLocalization stringForKey:@"share"],[MCLocalization stringForKey:@"email_support"],[MCLocalization stringForKey:@"report_a_bug"],[MCLocalization stringForKey:@"feature_request"], nil];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)refreshTable:(NSNotification*)userInfo{
    [self ChangeText];
    [_table1 reloadData];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0)
        return arrayTitle.count;
    if (section == 1)
        return arraysection.count;
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *categoryCellIdentifier = @"leftMenuCell";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:categoryCellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    UILabel *lblTitle1=(UILabel*)[cell viewWithTag:12];
     UILabel *colorlbl=(UILabel*)[cell viewWithTag:1];
     UIView *view1=(UIView*)[cell viewWithTag:5];
    if (indexPath.section == 0)
    {
        lblTitle1.textColor=[UIColor whiteColor];
        lblTitle1.text=[arrayTitle objectAtIndex:indexPath.row];
        lblTitle1.textColor=[UIColor whiteColor];
        lblTitle1.font=Ralewayfont(fontSize);
        UIImageView *image1=(UIImageView*)[cell viewWithTag:11];
        image1.image = [UIImage imageNamed:arraycount[indexPath.row]];
    }
    else if (indexPath.section == 1)
    {
        lblTitle1.textColor=[UIColor whiteColor];
        lblTitle1.text=[arraysection objectAtIndex:indexPath.row];
        lblTitle1.textColor=[UIColor whiteColor];
        lblTitle1.font=Ralewayfont(fontSize);
        UIImageView *image2=(UIImageView*)[cell viewWithTag:11];
        image2.image = [UIImage imageNamed:arrayimage[indexPath.row]];
        colorlbl.backgroundColor=[UIColor clearColor];
    }
    
    if ([indexPathArray containsObject:indexPath])
    {
        view1.backgroundColor=[UIColor colorWithRed:144/255.0 green:17/255.0 blue:19/255.0 alpha:0.5];
//        [self changeColor:cell];
        [self performSelector:@selector(changeColor:) withObject:cell afterDelay:1];
    } else {
        view1.backgroundColor = [UIColor clearColor];
        
    }
    
    return cell;
}
-(void)changeColor:(UITableViewCell*)cell{
   
        UIView *view1=(UIView*)[cell viewWithTag:5];
        view1.backgroundColor = [UIColor clearColor];
    
}

- (NSString* )tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0)
        return @"";
    if (section == 1)
        return @"  Communication";
    return @"undefined";
}
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    if ([view isMemberOfClass:[UITableViewHeaderFooterView class]]) {
        ((UITableViewHeaderFooterView *)view).backgroundView.backgroundColor = [UIColor clearColor];
    }
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView* )view;
    
    header.textLabel.textColor = [UIColor whiteColor];
    header.textLabel.font = Ralewayfont(fontSize);
    CGRect headerFrame = header.frame;
    header.textLabel.frame = headerFrame;
    header.textLabel.textAlignment = NSTextAlignmentLeft;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int a = 60,b = 70;
    if (DEVICE==IPAD) {
        a=100;
        b=150;
    }
    if (indexPath.section == 1) {
        return a;
    }
    return b;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    int a;
    if (section==0) {
        a= sectionHeight;
    }
    else{
        a=sectionHeight;
    }
    return a;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [indexPathArray removeAllObjects];
    [indexPathArray addObject:indexPath];
    [_table1 reloadData];
    
    if (indexPath.section==0) {
    if (indexPath.row==0) {
      
        [self performSegueWithIdentifier:@"next" sender:self];
    }
    else if (indexPath.row==1)
    {
        [self performSegueWithIdentifier:@"pushmudra" sender:self];
    }
    else if (indexPath.row==2)
    {
        [self performSegueWithIdentifier:@"pushbandha" sender:self];
    }
    else if (indexPath.row==3)
    {
        [self performSegueWithIdentifier:@"pushsetting" sender:self];
    }
    }
    else
    {
        
    if (indexPath.row==0)
    {
        NSString *str;
        NSString *appID=@"1127298201";
        float ver = [[[UIDevice currentDevice] systemVersion] floatValue];
        if (ver >= 7.0 && ver < 7.1) {
            str = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@",appID];
        } else if (ver >= 8.0) {
            str = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software",appID];
        } else {
            str = [NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@",appID];
        }
        
        NSURL *url1=[NSURL URLWithString:str];
        NSString *textToShare = @"Look at this awesome App for aspiring iOS Developers!";
        NSArray *objectsToShare = @[textToShare, url1];
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
        
        NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                       UIActivityTypePrint,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypeSaveToCameraRoll,
                                       UIActivityTypeAddToReadingList,
                                       UIActivityTypePostToFlickr,
                                       UIActivityTypePostToVimeo];
        
        activityVC.excludedActivityTypes = excludeActivities;
        if (DEVICE==Iphone) {
            [self presentViewController:activityVC animated:YES completion:nil];
        }
        else{
            UIPopoverController *popup=[[UIPopoverController alloc] initWithContentViewController:activityVC];
            [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
    }

    
    else if (indexPath.row==1)
    {
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
            mail.mailComposeDelegate = self;
            [mail setSubject:@""];
            [mail setMessageBody:@"Mail us for any query or Problem encountered." isHTML:NO];
            [mail setToRecipients:@[@"7pranayama@gmail.com"]];
            [self presentViewController:mail animated:YES completion:NULL];
            [tableView reloadData ];
        }
        else
        {
            NSLog(@"This device cannot send email");
        }
    }


else if (indexPath.row==2)
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        mail.mailComposeDelegate = self;
        [mail setSubject:@""];
        [mail setMessageBody:@"This app contains the bug as follows:" isHTML:NO];
        [mail setToRecipients:@[@"7pranayama@gmail.com"]];
        
        [self presentViewController:mail animated:YES completion:NULL];
        [tableView reloadData ];
    }
    else
    {
        NSLog(@"This device cannot send email");
    }
}

else if (indexPath.row==3)
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        mail.mailComposeDelegate = self;
        [mail setSubject:@""];
        [mail setMessageBody:@"Request to add following feature.." isHTML:NO];
        [mail setToRecipients:@[@"7pranayama@gmail.com"]];
        
        [self presentViewController:mail animated:YES completion:NULL];
        [tableView reloadData ];
    }
    else
    {
        NSLog(@"This device cannot send email");
    }
}
}
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    [self performSegueWithIdentifier:@"home" sender:self];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{

    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end

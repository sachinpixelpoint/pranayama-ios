//
//  MyProgressVC.m
//  Pranayama
//
//  Created by Manish Kumar on 17/06/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "MyProgressVC.h"

@interface MyProgressVC ()

@end

@implementation MyProgressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    // Do any additional setup after loading the view.
     self.navigationController.navigationBar.tintColor =RGB(230, 123, 27);
    self.screenName=@"My progress";
    [self ChangeText];
    self.navigationItem.hidesBackButton=YES;
    self.managedObjectContext=[kAppDele managedObjectContext];
    NSString *habbitType=[[NSUserDefaults standardUserDefaults] objectForKey:HabbitType];
    reportArray=[[kAppDele fetchReportFromProgressDatabseAccordingToType:habbitType]mutableCopy];
    Days=((int)reportArray.count/21);
    if ((int)reportArray.count%21==0 && (int)reportArray.count>0 ) {
        Days--;
    }
    Maxday=Days;
    index=21*Days;
    ProgressValue=0;
    duration=0;
    
    if (DEVICE==IPAD) {
        fontSize=24;
        fontSize2=18;
        fontSize3=16;
        fontsize4=14;
        view1H=100;
        view2H=70;
        view4H=700;
        dayH=30;
        excerciseH=60;
    }
    else{
        fontSize=18;
        fontSize2=14;
        fontSize3=10;
        fontsize4=7;
        view1H=70;
        view2H=50;
        view4H=470;
        dayH=20;
        excerciseH=40;
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self TopViewOfTimeAndDays];
    [self LevelView];
    [self StartEndDateVIew];
    [self ReportTableView];
    [self ProgressBarView];
}


- (void)HomeMethod:(NSNotification *)notification {
    [[CommonSounds sharedInstance] appDidBecomeActive:notification navigation:self.navigationController];
}


-(void )ChangeText {
    NSString *lan=[[NSUserDefaults standardUserDefaults] objectForKey:Languagevalue];
    if ([lan isEqualToString:Hindi]) {
        totalTimestr=[MCLocalization stringForKey:@"Total_Time"];
        totalDaysstr=[MCLocalization stringForKey:@"Total_Days"];
        levelstr=[MCLocalization stringForKey:@"level"];
        startTimestr=[MCLocalization stringForKey:@"start_date"];
        endTimestr=[MCLocalization stringForKey:@"end_date"];
        TotalprogressStr=[MCLocalization stringForKey:@"total_progress"];
        self.navigationItem.title=[MCLocalization stringForKey:@"progress"];
        NSString *habbitType=[[NSUserDefaults standardUserDefaults] objectForKey:HabbitType];
        if ([habbitType isEqualToString:@"Thyroid"]) {
            Excercise1=[MCLocalization stringForKey:@"Sarvangasana"];
            Excercise2=[MCLocalization stringForKey:@"Halasana"];
            Excercise3=[MCLocalization stringForKey:@"Anulom_Vilom"];
            Excercise4=[MCLocalization stringForKey:@"kapalbhati"];
            Excercise5=[MCLocalization stringForKey:@"ujjayi"];
        }
        else if ([habbitType isEqualToString:@"Insomnia"]){
            Excercise1=[MCLocalization stringForKey:@"Paschimottanasana"];
            Excercise2=[MCLocalization stringForKey:@"Vipritkarani"];
            Excercise3=[MCLocalization stringForKey:@"surya_bhedana"];
            Excercise4=[MCLocalization stringForKey:@"bhramari"];
            Excercise5=[MCLocalization stringForKey:@"meditative"];
        }
        else if ([habbitType isEqualToString:@"Diabetes"]){
            Excercise1=[MCLocalization stringForKey:@"Dhanurasana"];
            Excercise2=[MCLocalization stringForKey:@"Balasana"];
            Excercise3=[MCLocalization stringForKey:@"Anulom_Vilom"];
            Excercise4=[MCLocalization stringForKey:@"kapalbhati"];
            Excercise5=[MCLocalization stringForKey:@"bhastrika"];
        }
        else if ([habbitType isEqualToString:@"Migraine"])
        {
            Excercise1=[MCLocalization stringForKey:@"Hastapadasana"];
            Excercise2=[MCLocalization stringForKey:@"Marjariasana"];
            Excercise3=[MCLocalization stringForKey:@"Anulom_Vilom"];
            Excercise4=[MCLocalization stringForKey:@"bhramari"];
            Excercise5=[MCLocalization stringForKey:@"bhastrika"];
        }
        else if ([habbitType isEqualToString:@"Weight Loss"])
        {
            Excercise1=[MCLocalization stringForKey:@"Sarvangasana"];
            Excercise2=[MCLocalization stringForKey:@"Setu Bandhasana"];
            Excercise3=[MCLocalization stringForKey:@"Anulom_Vilom"];
            Excercise4=[MCLocalization stringForKey:@"kapalbhati"];
            Excercise5=[MCLocalization stringForKey:@"bhastrika"];
        }
        
        else if ([habbitType isEqualToString:@"Asthma"])
        {
            Excercise1=[MCLocalization stringForKey:@"Dhanurasana"];
            Excercise2=[MCLocalization stringForKey:@"Uttanasana"];
            Excercise3=[MCLocalization stringForKey:@"Anulom_Vilom"];
            Excercise4=[MCLocalization stringForKey:@"bhastrika"];
            Excercise5=[MCLocalization stringForKey:@"ujjayi"];
        }
        else
        {
            Excercise1=[MCLocalization stringForKey:@"Balasana"];
            Excercise2=[MCLocalization stringForKey:@"Virabhadrasana"];
            Excercise3=[MCLocalization stringForKey:@"Anulom_Vilom"];
            Excercise4=[MCLocalization stringForKey:@"udgeeth"];
            Excercise5=[MCLocalization stringForKey:@"bahya"];
        }

    }
    else if([lan isEqualToString:English]){
        totalTimestr=@"Total Time";
        totalDaysstr=@"Total Day";
        levelstr=@"Level";
        startTimestr=@"Start Date";
        endTimestr=@"End Date";
        TotalprogressStr=@"Total Progress";
        self.navigationItem.title=@"My Progress";
        NSString *habbitType=[[NSUserDefaults standardUserDefaults] objectForKey:HabbitType];
        if ([habbitType isEqualToString:@"Thyroid"]) {
            Excercise1=@"Sarvangasana";
            Excercise2=@"Halasana";
            Excercise3=@"AnulomVilom";
            Excercise4=@"Kapalbhati";
            Excercise5=@"Ujjayi";
        }
        else if ([habbitType isEqualToString:@"Insomnia"]){
            Excercise1=@"Paschimottanasana";
            Excercise2=@"Vipritkarani";
            Excercise3=@"Surya Bhedana";
            Excercise4=@"Bhamari";
            Excercise5=@"Meditative Breathing";
        }
        else if ([habbitType isEqualToString:@"Diabetes"]){
            Excercise1=@"Dhanurasana";
            Excercise2=@"Balasana";
            Excercise3=@"AnulomVilom";
            Excercise4=@"Kapalbhati";
            Excercise5=@"Bhastrika";
        }
        else if ([habbitType isEqualToString:@"Migraine"])
        {
            Excercise1=@"Hastapadasana";
            Excercise2=@"Marjariasana";
            Excercise3=@"AnulomVilom";
            Excercise4=@"Bhramari";
            Excercise5=@"Bhastrika";
        }
        else if ([habbitType isEqualToString:@"Weight Loss"])
        {
            Excercise1=@"Sarvangasana";
            Excercise2=@"Setu Bandhasana";
            Excercise3=@"AnulomVilom";
            Excercise4=@"Kapalbhati";
            Excercise5=@"Bhastrika";
        }
        else if ([habbitType isEqualToString:@"Asthma"])
        {
            Excercise1=@"Dhanurasana";
            Excercise2=@"Uttanasana";
            Excercise3=@"AnulomVilom";
            Excercise4=@"Bhastrika";
            Excercise5=@"Ujjayi";
        }
        else
        {
            Excercise1=@"Balasana";
            Excercise2=@"Virabhadrasana";
            Excercise3=@"AnulomVilom";
            Excercise4=@"Udgeeth";
            Excercise5=@"Bahya";
        }
    }
    else{
        totalTimestr=[MCLocalization stringForKey:@"Total_Time"];
        totalDaysstr=[MCLocalization stringForKey:@"Total_Days"];
        levelstr=[MCLocalization stringForKey:@"level"];
        startTimestr=[MCLocalization stringForKey:@"start_date"];
        endTimestr=[MCLocalization stringForKey:@"end_date"];
        TotalprogressStr=[MCLocalization stringForKey:@"total_progress"];
        self.navigationItem.title=[MCLocalization stringForKey:@"progress"];
        NSString *habbitType=[[NSUserDefaults standardUserDefaults] objectForKey:HabbitType];
        if ([habbitType isEqualToString:@"Thyroid"]) {
            Excercise1=[MCLocalization stringForKey:@"Sarvangasana"];
            Excercise2=[MCLocalization stringForKey:@"Halasana"];
            Excercise3=[MCLocalization stringForKey:@"Anulom_Vilom"];
            Excercise4=[MCLocalization stringForKey:@"kapalbhati"];
            Excercise5=[MCLocalization stringForKey:@"ujjayi"];
        }
        else if ([habbitType isEqualToString:@"Insomnia"]){
            Excercise1=[MCLocalization stringForKey:@"Paschimottanasana"];
            Excercise2=[MCLocalization stringForKey:@"Vipritkarani"];
            Excercise3=[MCLocalization stringForKey:@"surya_bhedana"];
            Excercise4=[MCLocalization stringForKey:@"bhramari"];
            Excercise5=[MCLocalization stringForKey:@"meditative"];
        }
        else if ([habbitType isEqualToString:@"Diabetes"]){
            Excercise1=[MCLocalization stringForKey:@"Dhanurasana"];
            Excercise2=[MCLocalization stringForKey:@"Balasana"];
            Excercise3=[MCLocalization stringForKey:@"Anulom_Vilom"];
            Excercise4=[MCLocalization stringForKey:@"kapalbhati"];
            Excercise5=[MCLocalization stringForKey:@"bhastrika"];
        }
        else if ([habbitType isEqualToString:@"Migraine"])
        {
            Excercise1=[MCLocalization stringForKey:@"Hastapadasana"];
            Excercise2=[MCLocalization stringForKey:@"Marjariasana"];
            Excercise3=[MCLocalization stringForKey:@"Anulom_Vilom"];
            Excercise4=[MCLocalization stringForKey:@"bhramari"];
            Excercise5=[MCLocalization stringForKey:@"bhastrika"];
        }
        else if ([habbitType isEqualToString:@"Weight Loss"])
        {
            Excercise1=[MCLocalization stringForKey:@"Sarvangasana"];
            Excercise2=[MCLocalization stringForKey:@"Setu Bandhasana"];
            Excercise3=[MCLocalization stringForKey:@"Anulom_Vilom"];
            Excercise4=[MCLocalization stringForKey:@"kapalbhati"];
            Excercise5=[MCLocalization stringForKey:@"bhastrika"];
        }
        
        else if ([habbitType isEqualToString:@"Asthma"])
        {
            Excercise1=[MCLocalization stringForKey:@"Dhanurasana"];
            Excercise2=[MCLocalization stringForKey:@"Uttanasana"];
            Excercise3=[MCLocalization stringForKey:@"Anulom_Vilom"];
            Excercise4=[MCLocalization stringForKey:@"bhastrika"];
            Excercise5=[MCLocalization stringForKey:@"ujjayi"];
        }
        else
        {
            Excercise1=[MCLocalization stringForKey:@"Balasana"];
            Excercise2=[MCLocalization stringForKey:@"Virabhadrasana"];
            Excercise3=[MCLocalization stringForKey:@"Anulom_Vilom"];
            Excercise4=[MCLocalization stringForKey:@"udgeeth"];
            Excercise5=[MCLocalization stringForKey:@"bahya"];
        }

    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self disableSlidePanGestureForRightMenu];
   }


-(void)TopViewOfTimeAndDays{
    View1=[[UIView alloc]initWithFrame:CGRectMake(5, 5, ScrollView.frame.size.width-10, view1H)];
    [ScrollView addSubview:View1];
    ///////borderlines//////
    [self Borderlinemethod:View1];
    
    UIView *totaltimeView=[[UIView alloc]initWithFrame:CGRectMake(10, 10, View1.frame.size.width/2-12.5, View1.frame.size.height-20)];
    totaltimeView.backgroundColor=RGB(239, 239, 239);
    [View1 addSubview:totaltimeView];
    UILabel *lbltime=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, totaltimeView.frame.size.width, totaltimeView.frame.size.height/2)];
    lbltime.text=totalTimestr;
    lbltime.font=[UIFont systemFontOfSize:fontSize];
    lbltime.textAlignment=NSTextAlignmentCenter;
    [totaltimeView addSubview:lbltime];
    totalTimeLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, totaltimeView.frame.size.height/2, totaltimeView.frame.size.width, totaltimeView.frame.size.height/2)];
    totalTimeLbl.text=@"00:00:00";
    totalTimeLbl.font=[UIFont systemFontOfSize:fontSize2];
    totalTimeLbl.textAlignment=NSTextAlignmentCenter;
    [totaltimeView addSubview:totalTimeLbl];
    
    UIView *totalDaysView=[[UIView alloc] initWithFrame:CGRectMake(totaltimeView.frame.size.width+15, 10, totaltimeView.frame.size.width, totaltimeView.frame.size.height)];
    totalDaysView.backgroundColor=RGB(239, 239, 239);
    [View1 addSubview:totalDaysView];
    UILabel *lbldays=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, totalDaysView.frame.size.width, totalDaysView.frame.size.height/2)];
    lbldays.text=totalDaysstr;
    lbldays.font=[UIFont systemFontOfSize:fontSize];
    lbldays.textAlignment=NSTextAlignmentCenter;
    [totalDaysView addSubview:lbldays];
    TotalDaysLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, totalDaysView.frame.size.height/2,totalDaysView.frame.size.width, totalDaysView.frame.size.height/2)];
    TotalDaysLbl.font=[UIFont systemFontOfSize:fontSize2];
    TotalDaysLbl.textAlignment=NSTextAlignmentCenter;
    [totalDaysView addSubview:TotalDaysLbl];
    
    NSInteger days=reportArray.count;
    days=days-21*Days;
    NSString *totaldays=[NSString stringWithFormat:@"%ld/21",(long)days];
    TotalDaysLbl.text=totaldays;
    
}

-(void)LevelView{
    
    ///////////view2///////////////
    View2=[[UIView alloc]initWithFrame:CGRectMake(5,View1.frame.origin.y+View1.frame.size.height+10, View1.frame.size.width , view2H)];
    View2.backgroundColor=RGB(239, 239, 239);
    [ScrollView addSubview:View2];
    [self Borderlinemethod:View2];
    
    UILabel *lblLevel=[[UILabel alloc]initWithFrame:CGRectMake(View2.frame.size.width/2-30, 0, 60, View2.frame.size.height/2)];
    lblLevel.text=levelstr;
    lblLevel.font=[UIFont systemFontOfSize:fontSize];
    lblLevel.textAlignment=NSTextAlignmentCenter;
    [View2 addSubview:lblLevel];
    LevelLbl=[[UILabel alloc]initWithFrame:CGRectMake(View2.frame.size.width/2-30, View2.frame.size.height/2, 60, View2.frame.size.height/2)];
    LevelLbl.textAlignment=NSTextAlignmentCenter;
    LevelLbl.font=[UIFont systemFontOfSize:fontSize2];
    LevelLbl.text=[NSString stringWithFormat:@"%d",Days+1];
    [View2 addSubview:LevelLbl];
}

-(void)StartEndDateVIew{
    ////////////view3//////////
    View3=[[UIView alloc]initWithFrame:CGRectMake(5, View2.frame.origin.y+View2.frame.size.height+10, View1.frame.size.width, view1H)];
    [ScrollView addSubview:View3];
    [self Borderlinemethod:View3];
    int a=View3.frame.size.width/3;
    UIButton *leftBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, view1H/2-view2H/2, a/2, view2H)];
    
    [leftBtn addTarget:self action:@selector(LeftRightBtnmethod:) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.tag=1;
    [View3 addSubview:leftBtn];
    
    UIView *startdateview=[[UIView alloc]initWithFrame:CGRectMake(a/2, 0, a, view1H)];
    [View3 addSubview:startdateview];
    UILabel *startDatelbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, a, view1H/2)];
    startDatelbl.text=startTimestr;
    startDatelbl.font=[UIFont systemFontOfSize:fontSize];
    startDatelbl.textAlignment=NSTextAlignmentCenter;
    [startdateview addSubview:startDatelbl];
    StartDate=[[UILabel alloc]initWithFrame:CGRectMake(0, view1H/2, a, view1H/2)];
    StartDate.font=[UIFont systemFontOfSize:fontSize2];
    StartDate.textAlignment=NSTextAlignmentCenter;
    [startdateview addSubview:StartDate];
    
    UIView *enddateview=[[UIView alloc]initWithFrame:CGRectMake(a+a/2, 0, a, view1H)];
    [View3 addSubview:enddateview];
    UILabel *EndDatelbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, a, view1H/2)];
    EndDatelbl.textAlignment=NSTextAlignmentCenter;
    EndDatelbl.text=endTimestr;
    EndDatelbl.font=[UIFont systemFontOfSize:fontSize];
    [enddateview addSubview:EndDatelbl];
    EndDate=[[UILabel alloc]initWithFrame:CGRectMake(0, view1H/2, a, view1H/2)];
    EndDate.font=[UIFont systemFontOfSize:fontSize2];
    EndDate.textAlignment=NSTextAlignmentCenter;
    [enddateview addSubview:EndDate];
    
    [self setContentofDateTime];
    
    UIButton *rightBtn=[[UIButton alloc]initWithFrame:CGRectMake(2*a+a/2, view1H/2-view2H/2, a/2, view2H)];
    
    [rightBtn addTarget:self action:@selector(LeftRightBtnmethod:) forControlEvents:UIControlEventTouchUpInside];
    rightBtn.tag=2;
    [View3 addSubview:rightBtn];
    if (DEVICE==IPAD) {
        [leftBtn setImage:[UIImage imageNamed:@"left_arrow_i_pad"] forState:UIControlStateNormal];
        [rightBtn setImage:[UIImage imageNamed:@"right_arrow_i_pad"] forState:UIControlStateNormal];
    }
    else{
        [leftBtn setImage:[UIImage imageNamed:@"left_arrow-1"] forState:UIControlStateNormal];
        [rightBtn setImage:[UIImage imageNamed:@"right_arrow-1"] forState:UIControlStateNormal];
    }
}


-(void)LeftRightBtnmethod:(UIButton *)sender{
    if (sender.tag==1) {
        if (Days<=0) {
            return;
        }
        else{
            Days--;
            TotalDaysLbl.text=@"21/21";
            [View4 removeFromSuperview];
            [self setContentofDateTime];
            [self ReportTableView];
        }
    }
    else{
        if (Days>=Maxday) {
            return;
        }
        else{
            Days++;
            NSInteger days=reportArray.count;
            days=days-21*Days;
            if (days>21) {
                days=21;
            }
            NSString *totaldays=[NSString stringWithFormat:@"%ld/21",(long)days];
            TotalDaysLbl.text=totaldays;
            [View4 removeFromSuperview];
            [self setContentofDateTime];
            [self ReportTableView];
        }
    }
}

-(void)setContentofDateTime{
    ProgressValue=0;
    duration=0;
    index=21*Days;
    LevelLbl.text=[NSString stringWithFormat:@"%d",Days+1];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd/MM/yyyy"];
    NSDate *date=[[[NSUserDefaults standardUserDefaults] objectForKey:HabbitStartDate]  dateByAddingTimeInterval:(21*Days+1)*24*60*60];
    NSString *startdatestr=[df stringFromDate:date];
    NSDate *enddate=[date dateByAddingTimeInterval:20*24*60*60];
    NSString *endDatestr=[df stringFromDate:enddate];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:HabbitCondition] isEqualToString:[[NSUserDefaults standardUserDefaults]objectForKey:HabbitType]]) {
        StartDate.text=startdatestr;
        EndDate.text=endDatestr;
    }
    else{
        StartDate.text=@"0";
        EndDate.text=@"0";
        
    }
}


///////////////view4///////////
-(void)ReportTableView{
    
    View4=[[UIView alloc]initWithFrame:CGRectMake(5, View3.frame.origin.y+View3.frame.size.height+10, View1.frame.size.width, view4H)];
    [ScrollView addSubview:View4];
    [self Borderlinemethod:View4];
    int startday=(Days+1)*21;
    int endDay=21*Days+1;
    int a=(View4.frame.size.width)/6;
    int j=5;
    for (int i=startday; i>=endDay; i--) {
        UILabel *daylbl=[[UILabel alloc]initWithFrame:CGRectMake(0, j, a, dayH)];
        daylbl.textAlignment=NSTextAlignmentCenter;
        NSString *str=[NSString stringWithFormat:@"Day%d",i];
        daylbl.text=str;
        daylbl.font=[UIFont systemFontOfSize:fontSize3];
        [View4 addSubview:daylbl];
        j=j+dayH;
    }
    ///////
    UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(a, 5, 5*a, view4H-excerciseH-10)];
    [img setImage:[UIImage imageNamed:@"celll21"]];
    [View4 addSubview:img];
    ////////
    UILabel *label1=[[UILabel alloc]initWithFrame:CGRectMake(a, img.frame.origin.y+view4H-excerciseH-10, a, excerciseH)];
    
    label1.textAlignment=NSTextAlignmentCenter;
    label1.font=[UIFont systemFontOfSize:fontsize4];
    label1.numberOfLines=5;
    [View4 addSubview:label1];
    UILabel *label2=[[UILabel alloc]initWithFrame:CGRectMake(2*a,img.frame.origin.y+view4H-excerciseH-10 , a, excerciseH)];
    
    label2.textAlignment=NSTextAlignmentCenter;
    label2.font=[UIFont systemFontOfSize:fontsize4];
    label2.numberOfLines=5;
    [View4 addSubview:label2];
    UILabel *label3=[[UILabel alloc]initWithFrame:CGRectMake(3*a,img.frame.origin.y+view4H-excerciseH-10 , a, excerciseH)];
    
    label3.textAlignment=NSTextAlignmentCenter;
    label3.font=[UIFont systemFontOfSize:fontsize4];
    label3.numberOfLines=5;
    [View4 addSubview:label3];
    UILabel *label4=[[UILabel alloc]initWithFrame:CGRectMake(4*a,img.frame.origin.y+view4H-excerciseH-10 , a, excerciseH)];
    
    label4.textAlignment=NSTextAlignmentCenter;
    label4.font=[UIFont systemFontOfSize:fontsize4];
    label4.numberOfLines=5;
    [View4 addSubview:label4];
    UILabel *label5=[[UILabel alloc]initWithFrame:CGRectMake(5*a,img.frame.origin.y+view4H-excerciseH-10 , a, excerciseH)];
    
    label5.textAlignment=NSTextAlignmentCenter;
    label5.font=[UIFont systemFontOfSize:fontsize4];
    label5.numberOfLines=5;
    [View4 addSubview:label5];
    
    label1.text=Excercise1;
    label2.text=Excercise2;
    label3.text=Excercise3;
    label4.text=Excercise4;
    label5.text=Excercise5;
    
    ////
    tableview=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, img.frame.size.width, img.frame.size.height) style:UITableViewStylePlain];
    tableview.delegate=self;
    tableview.dataSource=self;
    tableview.rowHeight=img.frame.size.height/21;
    tableview.transform=CGAffineTransformMakeRotation(M_PI);
    tableview.backgroundColor=[UIColor clearColor];
    tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    [img addSubview:tableview];
    
    lableHeight= img.frame.size.height/21;
    lablewidth=img.frame.size.width/5;
    
    if (reportArray.count==0) {
        UILabel *noRecordlbl=[[UILabel alloc]initWithFrame:CGRectMake(img.frame.size.width/2-75, img.frame.size.height/2-20, 150, 40)];
        noRecordlbl.text=@"No Record Found";
        noRecordlbl.font=[UIFont systemFontOfSize:fontSize2];
        noRecordlbl.textAlignment=NSTextAlignmentCenter;
        [img addSubview:noRecordlbl];
        
    }
}

//////////view5///////////
-(void)ProgressBarView{
   
    View5=[[UIView alloc]initWithFrame:CGRectMake(5, View4.frame.origin.y+View4.frame.size.height+10, View1.frame.size.width, view1H)];
    [ScrollView addSubview:View5];
    [self Borderlinemethod:View5];
    UILabel *totalProgrsLbl=[[UILabel alloc]initWithFrame:CGRectMake(View5.frame.size.width/2-100, 0, 200, view1H/2)];
    totalProgrsLbl.textAlignment=NSTextAlignmentCenter;
    totalProgrsLbl.text=TotalprogressStr;
    totalProgrsLbl.font=[UIFont systemFontOfSize:fontSize];
    [View5 addSubview:totalProgrsLbl];
    UILabel *minProgresslbl=[[UILabel alloc]initWithFrame:CGRectMake(0, view1H/2, View5.frame.size.width/6-5, view1H/2)];
    minProgresslbl.text=@"0%";
    minProgresslbl.font=[UIFont systemFontOfSize:fontSize2];
    minProgresslbl.textAlignment=NSTextAlignmentCenter;
    [View5 addSubview:minProgresslbl];
    maxProgresslbl=[[UILabel alloc]initWithFrame:CGRectMake((View5.frame.size.width/6)*5-5, view1H/2, View5.frame.size.width/6+5, view1H/2)];
    maxProgresslbl.textAlignment=NSTextAlignmentCenter;
    maxProgresslbl.text=@"0%";
    maxProgresslbl.font=[UIFont systemFontOfSize:fontSize2];
    [View5 addSubview:maxProgresslbl];
    progressVIew=[[UIProgressView alloc]initWithFrame:CGRectMake(View5.frame.size.width/6-5, (View5.frame.size.height/4)*3, (View5.frame.size.width/6)*4, 5)];
    [View5 addSubview:progressVIew];
    
    
    ScrollView.contentSize=CGSizeMake(ScrollView.frame.size.width,View5.frame.origin.y
                                      +View5.frame.size.height+10);
}

/////////////////Table view delegate method//////
- (NSInteger) numberOfSectionsInTableView: (UITableView*) tableView
{
    return 1;
}

- (NSInteger) tableView: (UITableView*) tableView numberOfRowsInSection: (NSInteger) section
{
    return reportArray.count-index;
}


- (UITableViewCell*) tableView: (UITableView*) tableView cellForRowAtIndexPath: (NSIndexPath*) indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    cell.backgroundColor=[UIColor clearColor];
    cell.transform=CGAffineTransformMakeRotation(M_PI);
    UILabel *asan1=[[UILabel alloc]initWithFrame:CGRectMake(0 , 0, lablewidth-1, lableHeight-1)];
    [cell addSubview:asan1];
    UILabel *asan2=[[UILabel alloc]initWithFrame:CGRectMake(lablewidth, 0, lablewidth-1, lableHeight-1)];
    [cell addSubview:asan2];
    UILabel *pranayama1=[[UILabel alloc]initWithFrame:CGRectMake(2*lablewidth, 0, lablewidth-1, lableHeight-1)];
    [cell addSubview:pranayama1];
    UILabel *pranayama2=[[UILabel alloc]initWithFrame:CGRectMake(3*lablewidth, 0, lablewidth-1, lableHeight-1)];
    [cell addSubview:pranayama2];
    UILabel *pranayama3=[[UILabel alloc]initWithFrame:CGRectMake(4*lablewidth, 0, lablewidth, lableHeight-1)];
    [cell addSubview:pranayama3];
    
    Progress *entity=[reportArray objectAtIndex:index];
    duration=duration+[entity.duration intValue];
    NSString *habbitType=[[NSUserDefaults standardUserDefaults] objectForKey:HabbitType];
    if ([habbitType isEqualToString:@"Thyroid"]) {
        if ([entity.sarvangasana boolValue]==YES) {
            asan1.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            asan1.backgroundColor=[UIColor redColor];
        }
        if ([entity.halasana boolValue]==YES) {
            asan2.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            asan2.backgroundColor=[UIColor redColor];
        }
        if ([entity.anulomVilom boolValue]==YES) {
            pranayama1.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama1.backgroundColor=[UIColor redColor];
        }
        if ([entity.kapalbhati boolValue]==YES) {
            pranayama2.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama2.backgroundColor=[UIColor redColor];
        }
        if ([entity.ujjayi boolValue]==YES) {
            pranayama3.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama3.backgroundColor=[UIColor redColor];
        }
    }
    else if ([habbitType isEqualToString:@"Insomnia"]){
        if ([entity.vipritkarani boolValue]==YES) {
            asan1.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            asan1.backgroundColor=[UIColor redColor];
        }
        if ([entity.pashimothanasana boolValue]==YES) {
            asan2.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            asan2.backgroundColor=[UIColor redColor];
        }
        if ([entity.bhramari boolValue]==YES) {
            pranayama1.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama1.backgroundColor=[UIColor redColor];
        }
        if ([entity.suryabhedana boolValue]==YES) {
            pranayama2.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama2.backgroundColor=[UIColor redColor];
        }
        if ([entity.medetativebreathing boolValue]==YES) {
            pranayama3.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama3.backgroundColor=[UIColor redColor];
        }
    }
    else if ([habbitType isEqualToString:@"Diabetes"]){
        if ([entity.dhanurasana boolValue]==YES) {
            asan1.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            asan1.backgroundColor=[UIColor redColor];
        }
        if ([entity.balasana boolValue]==YES) {
            asan2.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            asan2.backgroundColor=[UIColor redColor];
        }
        if ([entity.anulomVilom boolValue]==YES) {
            pranayama1.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama1.backgroundColor=[UIColor redColor];
        }
        if ([entity.kapalbhati boolValue]==YES) {
            pranayama2.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama2.backgroundColor=[UIColor redColor];
        }
        if ([entity.bhastrika boolValue]==YES) {
            pranayama3.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama3.backgroundColor=[UIColor redColor];
        }
    }
    else if ([habbitType isEqualToString:@"Migraine"])
    {
        if ([entity.hastapadasana boolValue]==YES) {
            asan1.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            asan1.backgroundColor=[UIColor redColor];
        }
        if ([entity.marjariasana boolValue]==YES) {
            asan2.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            asan2.backgroundColor=[UIColor redColor];
        }
        if ([entity.anulomVilom boolValue]==YES) {
            pranayama1.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama1.backgroundColor=[UIColor redColor];
        }
        if ([entity.bhramari boolValue]==YES) {
            pranayama2.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama2.backgroundColor=[UIColor redColor];
        }
        if ([entity.bhastrika boolValue]==YES) {
            pranayama3.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama3.backgroundColor=[UIColor redColor];
        }
    }
    else if ([habbitType isEqualToString:@"Weight Loss"])
    {
        if ([entity.sarvangasana boolValue]==YES) {
            asan1.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            asan1.backgroundColor=[UIColor redColor];
        }
        if ([entity.setubandhasana boolValue]==YES) {
            asan2.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            asan2.backgroundColor=[UIColor redColor];
        }
        if ([entity.anulomVilom boolValue]==YES) {
            pranayama1.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama1.backgroundColor=[UIColor redColor];
        }
        if ([entity.kapalbhati boolValue]==YES) {
            pranayama2.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama2.backgroundColor=[UIColor redColor];
        }
        if ([entity.bhastrika boolValue]==YES) {
            pranayama3.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama3.backgroundColor=[UIColor redColor];
        }
    }
    
    else if ([habbitType isEqualToString:@"Asthma"])
    {
        if ([entity.dhanurasana boolValue]==YES) {
            asan1.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            asan1.backgroundColor=[UIColor redColor];
        }
        if ([entity.uttanasana boolValue]==YES) {
            asan2.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            asan2.backgroundColor=[UIColor redColor];
        }
        if ([entity.anulomVilom boolValue]==YES) {
            pranayama1.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama1.backgroundColor=[UIColor redColor];
        }
        if ([entity.bhastrika boolValue]==YES) {
            pranayama2.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama2.backgroundColor=[UIColor redColor];
        }
        if ([entity.ujjayi boolValue]==YES) {
            pranayama3.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama3.backgroundColor=[UIColor redColor];
        }
    }
    
    else
    {
        if ([entity.balasana boolValue]==YES) {
            asan1.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            asan1.backgroundColor=[UIColor redColor];
        }
        if ([entity.virabhadrasana boolValue]==YES) {
            asan2.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            asan2.backgroundColor=[UIColor redColor];
        }
        if ([entity.anulomVilom boolValue]==YES) {
            pranayama1.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama1.backgroundColor=[UIColor redColor];
        }
        if ([entity.udgeeth boolValue]==YES) {
            pranayama2.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama2.backgroundColor=[UIColor redColor];
        }
        if ([entity.bahya boolValue]==YES) {
            pranayama3.backgroundColor=[UIColor greenColor];
            ProgressValue++;
        }
        else{
            pranayama3.backgroundColor=[UIColor redColor];
        }
        
    }
    float progress=((float)ProgressValue*20)/21;
    progressVIew.progress=progress/100;
    maxProgresslbl.text=[NSString stringWithFormat:@"%.02f%@",progress,@"%"];
    
    int seconds = duration % 60;
    int minutes = (duration / 60) % 60;
    int hours = duration / 3600;
    NSString *str=[NSString stringWithFormat:@"%02d:%02d:%02d",hours,minutes,seconds];
    totalTimeLbl.text=str;
    
    index++;
    return cell;
}


-(void)Borderlinemethod:(UIView *)view{
    UILabel *LeftBorderLine=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 1, view.frame.size.height)];
    LeftBorderLine.backgroundColor=RGB(170, 170, 170);
    [view addSubview:LeftBorderLine];
    UILabel *topBorderLine=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, view.frame.size.width, 1)];
    topBorderLine.backgroundColor=RGB(170, 170, 170);
    [view addSubview:topBorderLine];
    UILabel *bottomBorderLine=[[UILabel alloc]initWithFrame:CGRectMake(0,view.frame.size.height , view.frame.size.width, 1)];
    bottomBorderLine.backgroundColor=RGB(170, 170, 170);
    [view addSubview:bottomBorderLine];
    UILabel *rightBorderLine=[[UILabel alloc] initWithFrame:CGRectMake(view.frame.size.width, 0, 1, view.frame.size.height+1)];
    rightBorderLine.backgroundColor=RGB(170, 170, 170);
    [view addSubview:rightBorderLine];
}


-(IBAction)backbutton:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

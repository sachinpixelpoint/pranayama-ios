//
//  BhastrikaActionVC.h
//  Pranayama
//
//  Created by Manish Kumar on 11/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol bhastrikadelegate<NSObject>

-(void)playsound;

@end
@interface BhastrikaActionVC : GAITrackedViewController
{
    IBOutlet UIView *topview;
    
    IBOutlet UILabel *totaltimelbl;
    IBOutlet UILabel *actionlbl;
    
    IBOutlet UIButton *btnStop;
    IBOutlet UIButton *btnpuse;
    
    IBOutlet UILabel *lblHours;
    IBOutlet UILabel *lblminutes;
    IBOutlet UILabel *lblseconds;
    IBOutlet UILabel *lblTimer;
    IBOutlet UILabel *lblmode;
    IBOutlet UIImageView *imageview;
    
    NSTimer *startTimer;
    BOOL isPause;
    BOOL PlayStop;
    int inhale;
    int exhale;
    int seconds;
    int minutes;
    int hours;
    int roundforreport;
    int bhastrikaCount;
    int prepare;
}
@property (nonatomic,assign)id <bhastrikadelegate>delegate;
@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic) int rounds;
@property (nonatomic) int lblinhale;
@property (nonatomic) int lblexhale;
@property (nonatomic) int totalTime;
@end

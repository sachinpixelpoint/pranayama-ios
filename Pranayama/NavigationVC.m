//
//  NavigationVC.m
//  Pranayama
//
//  Created by Manish Kumar on 04/11/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "NavigationVC.h"
#import "Pranayama-Swift.h"
@interface NavigationVC ()

@end

@implementation NavigationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self disableSlidePanGestureForRightMenu];
    
    NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaName];
    NSArray *ar = [kAppDele FetchDincharyaFromDatabaseAcordingToName:str];
    Dincharya *dincharya = [ar objectAtIndex:0];
    
    NSArray *yogasanaList = [NSKeyedUnarchiver unarchiveObjectWithData:dincharya.yogasana_arr];
    NSArray *PranaList = [NSKeyedUnarchiver unarchiveObjectWithData:dincharya.pranayama_arr];
    [[NSUserDefaults standardUserDefaults] setObject: yogasanaList forKey:dincharyaYogaAr];
    [[NSUserDefaults standardUserDefaults] setObject:PranaList forKey:dincharyaPranaAr];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setInteger:[dincharya.autometicTime intValue] forKey:dincharyaDelayTime];
    NSMutableArray *PranaIdAr = [[NSMutableArray alloc]init];
    if (yogasanaList.count>0) {
        NSLog(@"%@",[yogasanaList objectAtIndex:0]);
        [[NSUserDefaults standardUserDefaults]setObject:[yogasanaList objectAtIndex:0] forKey:YogasanCondition];
        [[NSUserDefaults standardUserDefaults] setObject: yogasanaList forKey:dincharyaYogaAr];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    if (PranaList.count>0) {
        for (int i=0 ; i<PranaList.count; i++) {
            
            if ([[PranaList objectAtIndex:i] isEqualToString:@"AnulomVilom"]) {
                [PranaIdAr addObject:@"anulom"];
            }
            if ([[PranaList objectAtIndex:i] isEqualToString:@"Kapalbhati"]) {
                [PranaIdAr addObject:@"kapal"];
            }
            if ([[PranaList objectAtIndex:i] isEqualToString:@"Bhramari" ]){
                [PranaIdAr addObject:@"bhramari"];
            }
            if ([[PranaList objectAtIndex:i] isEqualToString:@"Surya Bhedana"]) {
                [PranaIdAr addObject:@"surya"];
            }
            if ([[PranaList objectAtIndex:i] isEqualToString:@"Chandra Bhedana"]) {
                [PranaIdAr addObject:@"chandra"];
            }
            if ([[PranaList objectAtIndex:i] isEqualToString:@"Bhastrika"]) {
                [PranaIdAr addObject:@"bhastrika"];
            }
            if ([[PranaList objectAtIndex:i] isEqualToString:@"Sheetali"]) {
                [PranaIdAr addObject:@"sheetali"];
            }
            if ([[PranaList objectAtIndex:i] isEqualToString:@"Ujjayi"]) {
                [PranaIdAr addObject:@"ujjayi"];
            }
            if ([[PranaList objectAtIndex:i] isEqualToString:@"Meditative Breathing"]) {
                [PranaIdAr addObject:@"meditative"];
            }
            if ([[PranaList objectAtIndex:i] isEqualToString:@"Udgeeth"]) {
                [PranaIdAr addObject:@"udgeeth"];
            }
            if ([[PranaList objectAtIndex:i] isEqualToString:@"Bahya"]) {
                [PranaIdAr addObject:@"bahya"];
            }
        }
        NSArray *Arr = [[NSArray alloc]init];
        Arr=[PranaIdAr copy];
        [[NSUserDefaults standardUserDefaults]setObject:Arr forKey:dincharyaPranaAr];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    
    ///////////  Move to Pranayama or Yogasana ////
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:commonClassCond];
    if (yogasanaList.count>0) {
        startViewController *start = [self.storyboard instantiateViewControllerWithIdentifier:@"nextcontroller"];
        [self.navigationController pushViewController:start animated:NO];
    }
    else{
        NSArray *ar = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaPranaAr];
        UIViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:[ar objectAtIndex:0]];
        [self.navigationController pushViewController:VC animated:NO];
    }
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

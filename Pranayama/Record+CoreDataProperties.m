//
//  Record+CoreDataProperties.m
//  Pranayama
//
//  Created by Manish Kumar on 02/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Record+CoreDataProperties.h"

@implementation Record (CoreDataProperties)

@dynamic anulomDate;
@dynamic anulomLevel;
@dynamic anulomRounds;
@dynamic anulomSerialNo;

@dynamic anulomduration;
@dynamic anulomtype;

@end

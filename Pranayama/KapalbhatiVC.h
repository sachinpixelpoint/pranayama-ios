//
//  KapalbhatiVC.h
//  Pranayama
//
//  Created by Manish Kumar on 02/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KapalbhatiActionVC.h"

@interface KapalbhatiVC : GAITrackedViewController <kapaldelegate>
{
    IBOutlet UIPickerView *RoundPicker;
    IBOutlet UIPickerView *TimePicker;
    int RoundRow;
    int TimeRow;
    NSMutableArray *RoundData;
    NSMutableArray *TimeData;
    
    IBOutlet UIView *topview;
    IBOutlet UIImageView *img;
    
    IBOutlet UILabel *roundlbl;
    IBOutlet UILabel *timelbl;
    
    IBOutlet UILabel *lblHours;
    IBOutlet UILabel *lblminutes;
    IBOutlet UILabel *lblseconds;
    int totaltime;
    int seconds;
    int minutes;
    int hours;
    int oldTimeValue;
    BOOL timevalue1;
    UIButton *button1,*button2,*button3;
    BOOL isMenuVisible;
    IBOutlet UIButton *Floatingbutton;
    IBOutlet UIButton *StartButton;
    IBOutlet UIButton *reportButton;
    UIView *introview;
    UIButton *gotItbutton;
    int RoundForAction;
    BOOL condition;
    NSTimer *Timer;
}
@end

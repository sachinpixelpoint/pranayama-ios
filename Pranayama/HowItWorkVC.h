//
//  HowItWorkVC.h
//  Pranayama
//
//  Created by Manish Kumar on 14/06/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HowItWorkVC : GAITrackedViewController <UIPickerViewDelegate,UIPickerViewDataSource,UIGestureRecognizerDelegate>
{
    IBOutlet UIScrollView*ScrollView;
    CGFloat inFontSize,OkfontSize,dotFontsize,FbutonSize;
    UIButton *datepickerBtn;
    UIView *PopupView,*view1,*aleartView;
    UIView *blackOverlay;
    UIPickerView *inhalePicker,*holdPicker,*exhalePicker,*afHoldPicker,*roundPicker,*kapalTimePicker,*kapalRoundPicker,*bhramariInhalePicker,*bhramariExhalepicker,*udgeethInhalepkr,*udgeethexhaleOpkr,*udgeethexhaleMpkr,*bahyaexhalepkr,*bahyaholdpkr;
    int InhaleRow,holdRow,exhaleRow,roundRow,afHoldRow,kapaltimeRow,kapalroundRow,bhramariInhaleRow,bhramariExhaleRow,bhramariRoundRow,UdgeethinhaleRow,UdgeethexhalORow,UdgeethexhaleMRow,bahyainhaleRow,bahyaexhaleRow,bahyaHoldRow;
    int AnulomInhaleValue,AnulomholdValue,AnulomexhaleValue,AnulomAfholdValue,AnulomRoundValue;
    int UjjayiInhaleValue,UjjayiHoldValue,UjjayiExhaleValue,UjjayiRoundValue;
    int SuryaInhale,SuryaExhale,SuryaHold,SuryaRound;
    int BhramariInhale,BhramariExhale,BhramariRound;
    int BhastrikaInhale,BhastrikaExhale,BhastrikaRound;
    int udgeethInhale,udgeethexhaleO,udgeethexhaleM,udgeethround;
    int Bahyainhale,Bahyaexhale,Bahyahold,bahyaround;
    NSMutableArray *In_roundData,*holdData,*exhaleData,*afroundData,*kapatimeData,*kapalroundData,*bhramriInhlaeData,*bhramariExhaleData,*udgeethexhaleOData,*bahyaholdData;
    UILabel *titleDateLbl;
    int oldTimeValue;
    int RoundForAction;
    int kapaltimeValue,kapalroundValue;
    UIDatePicker *datePicker;
    UIButton *floatingButton;
    UIWindow *window;
    NSString *habbitType;
    NSString *R_U_sure,*cancle_Habbit,*yes,*no,*setRatio,*inhalestr,*holdstr,*exhalestr,*roundstr,*set,*cancel,*timestr,*warning,*warning_text,*anulom,*kapal,*bhramari,*surya,*bhastrika,*Ujjayi,*udgeeth,*bahya,*NotificationAleart;
    BOOL condition;
}
@end

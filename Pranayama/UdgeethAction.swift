//
//  UdgeethAction.swift
//  Pranayama
//
//  Created by Manish Kumar on 06/09/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

import UIKit

class UdgeethAction: GAITrackedViewController {
    
    let kAppDel = (UIApplication.shared.delegate! as! AppDelegate)

    
    @IBOutlet var topview: UIView!
    @IBOutlet var totaltimelbl: UILabel!
    @IBOutlet var actionlbl: UILabel!
    @IBOutlet var btnStop: UIButton!
    @IBOutlet var btnpuse: UIButton!
    @IBOutlet var lblHours: UILabel!
    @IBOutlet var lblminutes: UILabel!
    @IBOutlet var lblseconds: UILabel!
    @IBOutlet var lblTimer: UILabel!
    @IBOutlet var lblmode: UILabel!
    @IBOutlet var imageview: UIImageView!
    var startTimer: Timer!
    var isPause = false
    var PlayStop = false
    var lblinhale1: Int = 0
    var lblexhaleO1 : Int = 0
    var lblexhalM1 : Int = 0
    var seconds : Int = 0
    var minutes : Int = 0
    var hours : Int = 0
    var roundForReport : Int = 0
    var UdgeethCount : Int = 0
    var prepare : Int = 0
    var managedObjectContext: NSManagedObjectContext!
    var rounds : Int = 0
    var lblinhale : Int = 0
    var lblexhaleO : Int = 0
    var lblexhaleM : Int = 0
    var totalTime : Int = 0
    
    var mySoundPlayer : AVAudioPlayer?

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: (#selector(self.appDidBecomeActive(_:))), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        self.screenName="Udgeeth"
        // Do any additional setup after loading the view.
        self.navigationItem.hidesBackButton = true
        var fontSize : CGFloat = 0
        var totaltimeFont : CGFloat = 0
        var lblmodeFont : CGFloat = 0
        if UI_USER_INTERFACE_IDIOM() == .pad {
            fontSize = 24
            totaltimeFont = 24
            lblmodeFont = 36
        }
        else {
            fontSize = 18
            totaltimeFont = 12
            lblmodeFont = 22
        }
        UINavigationBar.appearance().titleTextAttributes=[NSFontAttributeName: Ralewayfont(fontSize),NSForegroundColorAttributeName:UIColor.white]
        self.managedObjectContext = kAppDel.managedObjectContext!
        prepare = UserDefaults.standard.integer(forKey: preparationtime)
        
        let value = UserDefaults.standard.integer(forKey: dincharyaDelayTime)
        if UserDefaults.standard.bool(forKey: DincharyaMainCon) && value>0 {
            prepare = value
        }
        
        UserDefaults.standard.set(false, forKey: isPause1)
        PlayStop = false
        roundForReport = rounds
        totaltimelbl.font = Ralewayfont(totaltimeFont)
        actionlbl.font = Ralewayfont(lblmodeFont)
        lblmode.font = Ralewayfont(lblmodeFont)
        self.ChangeText()
        self.startButtonPreshed()
        self.setValueoflbl()
        if !(UI_USER_INTERFACE_IDIOM() == .pad) {
            topview.frame = CGRect(x: 8, y: 74, width: self.view.frame.size.width-16, height: 32)
        }
        
    }

    func appDidBecomeActive(_ notification:Notification) -> Void {
        
        CommonSounds.sharedInstance().appDidBecomeActive(notification, navigation: self.navigationController)
    }




    func ChangeText() {
        
        self.navigationItem.title = MCLocalization.string(forKey: "udgeet")!
        totaltimelbl.text = MCLocalization.string(forKey: "Total_Time")!
        actionlbl.text = MCLocalization.string(forKey: "Action")!
    }
    
    func setValueoflbl() {
        seconds = totalTime % 60
        minutes = (totalTime / 60) % 60
        hours = totalTime / 3600
        lblseconds.text = "\(seconds)"
        lblminutes.text = "\(minutes)"
        lblHours.text = "\(hours)"
        lblinhale1 = lblinhale
        lblexhaleO1 = lblexhaleO
        lblexhalM1 = lblexhaleM
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.disableSlidePanGestureForRightMenu()
    }
    
    @IBAction func pausebutton(_ sender: AnyObject) {
        UserDefaults.standard.set(false, forKey: isPause1)
        PlayStop = true
        btnStop.setImage(UIImage(named: "start_icon")!, for: UIControlState())
        startTimer.invalidate()
        mySoundPlayer?.stop()
        mySoundPlayer=nil
    }
    
    
    @IBAction func stopbutton(_ sender: AnyObject) {
        if PlayStop == true {
            startTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(onTick), userInfo: nil, repeats: true)
            btnStop.setImage(UIImage(named: "stop_icon")!, for: UIControlState())
            PlayStop = false
            if lblinhale1==0 && lblexhaleO1>0 && lblexhaleO1 != lblexhaleO && lblexhaleO1 != lblexhaleO-1{
                self.playExhaleOOOSound()
            }
            if lblinhale1 == 0 && lblexhaleO1 == 0 && lblexhalM1>0 {
                self.playExhaleMMMSound()
            }
        }
        else {
            startTimer.invalidate()
            if UdgeethCount > 0 {
                self.insertReportIntoDatabase()
                CommonSounds.sharedInstance().insertintoProgressReport(true, durationValue: Int32(UdgeethCount), isudgeeth: true)
                self.saveIndincharya(value: false)
            }
            self.navigationController!.popViewController(animated: false)
            UdgeethCount = 0
        }
    }
    
    func startButtonPreshed() {
        startTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(onTick), userInfo: nil, repeats: true)
    }
    
    func onTick(_ timer: Timer) {
        isPause = UserDefaults.standard.bool(forKey: isPause1)
        if isPause {
            self.pausebutton(isPause1 as AnyObject)
        }
        else {
            if prepare > 0 {
                lblmode.text = MCLocalization.string(forKey: "preparation_time")!
                lblTimer.text = "\(prepare)"
                CommonSounds.sharedInstance().playEverySound()
                prepare -= 1
            }
            else {
                if totalTime > 0 {
                    seconds = totalTime % 60
                    minutes = (totalTime / 60) % 60
                    hours = totalTime / 3600
                    lblseconds.text = "\(seconds)"
                    lblminutes.text = "\(minutes)"
                    lblHours.text = "\(hours)"
                    if rounds > 0 {
                        if lblinhale1 > 0 {
                            if lblinhale1 == lblinhale {
                                CommonSounds.sharedInstance().playInhaleSound()
                            }
                            else {
                                CommonSounds.sharedInstance().playEverySound()
                            }
                            imageview.image = UIImage(named: "action_inhale_both")!
                            lblmode.text = MCLocalization.string(forKey: "inhale_both")!
                            lblTimer.text = "\(lblinhale1)"
                            lblinhale1 = lblinhale1 - 1
                        }
                        else if lblexhaleO1 > 0 {
                            if lblexhaleO1 == lblexhaleO {
                                CommonSounds.sharedInstance().playExhaleSound()
                            }
                            else if lblexhaleO1 == lblexhaleO-1 {
                                self.playExhaleOOOSound()
                            }
                            imageview.image = UIImage(named: "exhale_om")!
                            lblmode.text = MCLocalization.string(forKey: "exhaleooo")!
                            lblTimer.text = "\(lblexhaleO1)"
                            lblexhaleO1 = lblexhaleO1 - 1
                        }
                        else if lblexhalM1 > 0 {
                            imageview.image = UIImage(named: "exhale_om")!
                            lblmode.text = MCLocalization.string(forKey: "exhalemmm")!
                            lblTimer.text = "\(lblexhalM1)"
                            if lblexhalM1 == lblexhaleM {
                                self.playExhaleMMMSound()
                            }
                            lblexhalM1 = lblexhalM1 - 1
                            if lblexhalM1 == 0 {
                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(0.7 * Double(NSEC_PER_SEC)) / Double(NSEC_PER_SEC), execute: {() -> Void in
                                    self.mySoundPlayer?.stop()
                                    self.mySoundPlayer=nil
                                })
                                rounds = rounds - 1
                                self.setValueoflbl()
                            }
                        }
                    }
                }
                else {
                    timer.invalidate()
                    UserDefaults.standard.set(true, forKey: "sound")
                    self.insertReportIntoDatabase()
                    CommonSounds.sharedInstance().insertintoProgressReport(false, durationValue: Int32(UdgeethCount), isudgeeth: true)
                    self.saveIndincharya(value: true)
                    self.navigationController!.popViewController(animated: false)
                }
                totalTime = totalTime - 1
                UdgeethCount += 1
            }
        }
    }
    
    
    func playExhaleOOOSound() {
        if UserDefaults.standard.bool(forKey: kISSound) {
            let soundurl : URL = Bundle.main.url(forResource: "ooo-1", withExtension: "mp3")!
            let volumeValue : Float = UserDefaults.standard.float(forKey: kVolume)
            do {
                self.mySoundPlayer = try AVAudioPlayer(contentsOf: soundurl)
            }
            catch _ {
            }
            self.mySoundPlayer?.volume = volumeValue
            //between 0 and 1
            self.mySoundPlayer?.prepareToPlay()
            self.mySoundPlayer?.numberOfLoops = 0
            //or more if needed
            self.mySoundPlayer?.play()
        }
    }
    
    func playExhaleMMMSound() {
        if UserDefaults.standard.bool(forKey: kISSound) {
            let soundurl : URL = Bundle.main.url(forResource: "bfinal40", withExtension: "mp3")!
            let volumeValue : Float = UserDefaults.standard.float(forKey: kVolume)
            do {
                self.mySoundPlayer = try AVAudioPlayer(contentsOf: soundurl)
            }
            catch _ {
            }
            self.mySoundPlayer?.volume = volumeValue
            //between 0 and 1
            self.mySoundPlayer?.prepareToPlay()
            self.mySoundPlayer?.numberOfLoops = 0
            //or more if needed
            self.mySoundPlayer?.play()
        }
    }
    
    func insertReportIntoDatabase() {
        var array = kAppDel.fetchReportFromDatabse(accordingToType: "Udgeeth")
        if (array?.count)! > 0 {
            let recordEntity = array?[0] as! Record
            let str : Date? = recordEntity.anulomDate
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let datestr = dateFormatter.string(from: str!)
            let date = dateFormatter.string(from: Date())
            if (datestr == date) {
                let du : String? = recordEntity.anulomduration
                let aaa : Int = Int(du!)!
                let duration : NSString? = "\(aaa+UdgeethCount)" as NSString?
                recordEntity.anulomduration = duration as String?
                do {
                    try self.managedObjectContext?.save()
                } catch {
                    print(error)
                }
            }
            else {
                let reportEntity = NSEntityDescription.insertNewObject(forEntityName: "Record", into: self.managedObjectContext) as! Record
                reportEntity.anulomLevel = "\(lblinhale):\(lblexhaleO + lblexhaleM)"
                reportEntity.anulomRounds = "\(roundForReport)"
                reportEntity.anulomDate = Date()
                reportEntity.anulomduration = "\(UdgeethCount)"
                reportEntity.anulomtype = "Udgeeth"

                do {
                    try self.managedObjectContext?.save()
                } catch {
                    print(error)
                }
            }
        }
        else {
            let reportEntity = NSEntityDescription.insertNewObject(forEntityName: "Record", into: self.managedObjectContext) as! Record
            
            reportEntity.anulomLevel = "\(lblinhale):\(lblexhaleO + lblexhaleM)"
            reportEntity.anulomRounds = "\(roundForReport)"
            reportEntity.anulomDate = Date()
            reportEntity.anulomduration = "\(UdgeethCount)"
            reportEntity.anulomtype = "Udgeeth"
            
            do {
                try self.managedObjectContext?.save()
            } catch {
                print(error)
            }
        }
        let reportEntity = NSEntityDescription.insertNewObject(forEntityName: "Report", into: self.managedObjectContext) as! Report
        
        reportEntity.reportLevel = "\(lblinhale):\(lblexhaleO + lblexhaleM)"
        reportEntity.reportRounds = "\(roundForReport)"
        reportEntity.reportDate = Date()
        reportEntity.reportduration = "\(UdgeethCount)"
        reportEntity.reporttype = "Udgeeth"
        
        do {
            try self.managedObjectContext?.save()
        } catch {
            print(error)
        }
    }
    
    
    func saveIndincharya(value : Bool) -> Void {
        if UserDefaults.standard.bool(forKey: DincharyaMainCon) {
            
            let str = UserDefaults.standard.object(forKey: dincharyaName) as! String
            let ar = kAppDel.fetchDincharyaFromDatabaseAcording(toName: str) as NSArray
            let dincharya = ar.object(at: 0) as! Dincharya
            let YogaAr = NSKeyedUnarchiver.unarchiveObject(with: dincharya.yogasana_arr!) as! NSArray
            let PranaMainList = NSKeyedUnarchiver.unarchiveObject(with: dincharya.pranayama_arr!) as! NSArray
            let pranaList = UserDefaults.standard.object(forKey: dincharyaPranaAr) as! NSArray
            
            UserDefaults.standard.set(true, forKey: viewwillCondition)
            
            if PranaMainList.count == pranaList.count && YogaAr.count == 0 {
                CommonSounds.sharedInstance().timecompare(value, managedObject: self.managedObjectContext, pranayama: "Udgeeth")
            }
            else{
                CommonSounds.sharedInstance().updateDincharya(intoDatabase: value, mangedObject: self.managedObjectContext, prana: "Udgeeth")
            }
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

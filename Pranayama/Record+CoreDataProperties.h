//
//  Record+CoreDataProperties.h
//  Pranayama
//
//  Created by Manish Kumar on 02/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Record.h"

NS_ASSUME_NONNULL_BEGIN

@interface Record (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *anulomDate;
@property (nullable, nonatomic, retain) NSString *anulomLevel;
@property (nullable, nonatomic, retain) NSString *anulomRounds;
@property (nullable, nonatomic, retain) NSString *anulomSerialNo;


@property (nullable, nonatomic, retain) NSString *anulomduration;
@property (nullable, nonatomic, retain) NSString*anulomtype;

@end

NS_ASSUME_NONNULL_END

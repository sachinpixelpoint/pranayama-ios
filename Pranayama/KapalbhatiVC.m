//
//  KapalbhatiVC.m
//  Pranayama
//
//  Created by Manish Kumar on 02/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "KapalbhatiVC.h"
#import "Pranayama-Swift.h"

@interface KapalbhatiVC ()

@end

@implementation KapalbhatiVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    // tracker
    self.screenName=@"Kapalbhati";
    // Do any additional setup after loading the view.
    [self setlblheight];
    Floatingbutton.layer.cornerRadius = Floatingbutton.bounds.size.width / 2.0;
    [self setupbutton];
    int fontSize;
    int lblfont;
    if (DEVICE==IPAD) {
        fontSize=24;
        lblfont=25;
    }
    else{
        fontSize=18;
        lblfont=12;
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    oldTimeValue=[[NSUserDefaults standardUserDefaults] floatForKey:kapaltime];
    RoundForAction=[[NSUserDefaults standardUserDefaults] floatForKey:kapalRound];
    [self setArrayNo];
    [self introduction];
    [self getvalue];
    roundlbl.font=Ralewayfont(lblfont);
    timelbl.font=Ralewayfont(lblfont);
    [self getTotalTime];
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaMainCon] && [[NSUserDefaults standardUserDefaults]boolForKey:DincharyaDelegate] && [[NSUserDefaults standardUserDefaults] boolForKey:commonClassCond]) {
        [self performSelector:@selector(navigationMove) withObject:nil afterDelay:0.5];
    }
    else{
        if ([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaMainCon]) {
            NSString *name = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaName];
            NSArray *ar = [kAppDele FetchDincharyaFromDatabaseAcordingToName:name];
            if (ar.count>0){
                Dincharya *Din = [ar objectAtIndex:0];
                if (Din.autometicTime != nil) {
                    [self performSelector:@selector(autometicMove) withObject:nil afterDelay:0.5];
                }
            }
        }
    }
}

-(void)navigationMove{
    [[CommonSounds sharedInstance]CheckStartController:self.navigationController Identifier:@"kapal"];
}

-(void)autometicMove{
    [self performSegueWithIdentifier:@"kapalbhatiAction" sender:self];
}


- (void)HomeMethod:(NSNotification *)notification {
    HomeTabelVC *homeObject = [[HomeTabelVC alloc]init];
    [homeObject appDidBecomeActive:notification];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self disableSlidePanGestureForRightMenu];
    [self ChangeText];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:HabbitMainCondition] && [[NSUserDefaults standardUserDefaults] boolForKey:viewwillCondition]) {
        [self performSelector:@selector(MovetoPranayama) withObject:nil afterDelay:0.2];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:viewwillCondition];
    }
    if ([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaMainCon] && [[NSUserDefaults standardUserDefaults]boolForKey:viewwillCondition]) {
        [self performSelector:@selector(Dinchariya_NextMethod) withObject:nil afterDelay:0.2];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:viewwillCondition];
    }
}

/////////////////    Move to next pranayama in Dincharya module/////////////
-(void)Dinchariya_NextMethod{
    NSArray *pranaAr = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaPranaAr];
    
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    arr = [pranaAr mutableCopy];
    [arr removeObject:@"kapal"];
    NSArray *Yarray = [arr copy];
    
    if (Yarray.count != 0) {
        [[NSUserDefaults standardUserDefaults]setObject:Yarray forKey:dincharyaPranaAr];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        HomeTabelVC *home=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
        DincharyaVC *dincharyavc=[self.storyboard instantiateViewControllerWithIdentifier:@"dincharya"];
        DincharyaNameVC *dinName=[self.storyboard instantiateViewControllerWithIdentifier:@"dincharya_name"];
        Dincharya_pranaVC *pranaVC = [self.storyboard instantiateViewControllerWithIdentifier:@"dincharya_prana"];
        UIViewController *prana = [self.storyboard instantiateViewControllerWithIdentifier:[Yarray objectAtIndex:0]];
        [self.navigationController setViewControllers:@[home,dincharyavc,dinName,pranaVC,prana] animated:YES];
    }
    else{
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:dincharyaPranaAr];
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:[MCLocalization stringForKey:@"session"]
                                     message:[MCLocalization stringForKey:@"Today_Fitness"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        [[CommonSounds sharedInstance] CommonAlertView:self.navigationController];
                                    }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];

    }
    
}


-(void)MovetoPranayama{
    UIStoryboard *storyBoard=[[UIStoryboard alloc]init];
    if (DEVICE==IPAD) {
        UIStoryboard *Board = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
        storyBoard=Board;
    }
    else{
        UIStoryboard * Board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        storyBoard=Board;
    }
    UIViewController *startView=[[UIViewController alloc]init];
    NSString *str=[[NSUserDefaults standardUserDefaults] objectForKey:HabbitCondition];
    if ([str isEqualToString:@"Thyroid"]) {
        UjjayiViewController *start = [storyBoard instantiateViewControllerWithIdentifier:@"ujjayi"];
        startView=start;
    }
    else{
        BhastrikaVC *start=[storyBoard instantiateViewControllerWithIdentifier:@"bhastrika"];
        startView=start;
    }
    
    HomeTabelVC *home=[storyBoard instantiateViewControllerWithIdentifier:@"home"];
    ProblemListViewController *ProblumList=[storyBoard instantiateViewControllerWithIdentifier:@"problumlist"];
    ProblemCatagoriVC *problumCatagori=[storyBoard instantiateViewControllerWithIdentifier:@"problumcatagori"];
    PranayamaTableVC *pranayamatbl=[storyBoard instantiateViewControllerWithIdentifier:@"pranayamatable"];
    [self.navigationController setViewControllers:@[home,ProblumList,problumCatagori,pranayamatbl,startView] animated:YES];
}


-(void)setArrayNo{
    RoundData = [[NSMutableArray alloc] init];
    TimeData=[[NSMutableArray alloc] init];
    TimeRow=[[NSUserDefaults standardUserDefaults] floatForKey:kapaltime];
    int i=TimeRow;
    for (int j=10*i; j<=150*i; j++){
        [RoundData addObject:[NSString stringWithFormat:@"%d",j]];
    }
    for (int j=1; j<=20; j++){
        [TimeData addObject:[NSString stringWithFormat:@"%d",j]];
    }
    
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    
    return 1;//Or return whatever as you intend
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    int height;
    if (DEVICE==IPAD) {
        height=40;
    }
    else{
        height=30;
    }
    return height;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    int i = 0;
    TimeRow=[[NSUserDefaults standardUserDefaults] floatForKey:kapaltime];
    int j=TimeRow;
    if([thePickerView isEqual: RoundPicker]){
        i=150*j-(j*10-1);
    }
    else {
        i=20;
    }
    return i;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    
    if([pickerView isEqual: RoundPicker]){
        RoundRow=[[RoundData objectAtIndex:row] intValue];
        RoundForAction=RoundRow;
        oldTimeValue=[[NSUserDefaults standardUserDefaults] floatForKey:kapaltime];
    }
    else {
        TimeRow=[[TimeData objectAtIndex:row] intValue];
        [self setvalueInkey];
        [self setArrayNo];
        [RoundPicker reloadAllComponents];
        RoundForAction=(RoundRow/oldTimeValue)*TimeRow;
        [RoundPicker selectRow:RoundForAction-(TimeRow*10) inComponent:0 animated:YES];
    }
    StartButton.userInteractionEnabled=YES;
    [self getTotalTime];
    [self setvalueInkey];
    condition=YES;
    NSTimer *timer;
    timer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(yourMethodName) userInfo:nil repeats:NO];
}

-(void)pickerView1:(UIPickerView *)pickerView didSelectRow1: (NSInteger)row inComponent1:(NSInteger)component {
    if (condition==NO) {
        TimeRow=[[TimeData objectAtIndex:row] intValue];
        [self setvalueInkey];
        [self setArrayNo];
        [RoundPicker reloadAllComponents];
        RoundForAction=(RoundRow/oldTimeValue)*TimeRow;
        [RoundPicker selectRow:RoundForAction-(TimeRow*10) inComponent:0 animated:YES];
        
        [self getTotalTime];
        [self setvalueInkey];
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* tView = (UILabel*)view;
    if (!tView){
        tView = [[UILabel alloc] init];
        if (DEVICE==IPAD) {
            [tView setFont:[UIFont systemFontOfSize:40]];
        }
        else{
            [tView setFont:[UIFont systemFontOfSize:22]];
        }
        [tView setTextAlignment:NSTextAlignmentCenter];
        if([pickerView isEqual: RoundPicker]){
            tView.text=[RoundData objectAtIndex:row];
        }
        else if([pickerView isEqual:TimePicker]) {
            tView.text=[TimeData objectAtIndex:row];
            [self pickerView1:pickerView didSelectRow1:row inComponent1:0];
        }
    }
    StartButton.userInteractionEnabled=NO;
    [Timer invalidate];
    Timer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(TouchEnable) userInfo:nil repeats:NO];
    pickerView.subviews[1].hidden = YES;
    pickerView.subviews[2].hidden = YES;
    return tView;
}
-(void)TouchEnable{
    StartButton.userInteractionEnabled=YES;
}

////////////// introduction /////////
-(void)introduction{
    bool intro=[[NSUserDefaults standardUserDefaults] boolForKey:Introduction];
    if (intro==YES) {
        [[CommonSounds sharedInstance] instructions:self.view];
    }
    else{
        NSTimer *timer;
        timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(yourMethodName) userInfo:nil repeats:NO];
    }
}
-(void)yourMethodName{
    StartButton.userInteractionEnabled=YES;
    condition=NO;
}
//////////////

-(void)setlblheight{
    int Device =Device_Type;
    switch (Device) {
        case iPhone4:
            img.frame=CGRectMake(68, 186, 186, 180);
            break;
        case iPhone5:
            img.frame=CGRectMake(68, 205, 186, 230);
            break;
        case iPhone6:
            img.frame=CGRectMake(63, 225, 250, 290);
            break;
        case iPhone6P:
            img.frame=CGRectMake(63, 240, 290, 330);
            break;
        default:
            break;
    }
    if (DEVICE == Iphone) {
        topview.frame = CGRectMake(8, 74, self.view.frame.size.width-16, 32);
    }
}
-(IBAction)reset:(id)sender{
    TimeRow=1;
    RoundRow=60;
    RoundForAction=60;
    [RoundPicker selectRow:RoundRow-10 inComponent:0 animated:YES];
    [TimePicker selectRow:TimeRow-1 inComponent:0 animated:YES];
    oldTimeValue=TimeRow;
    [self setvalueInkey];
    [self getTotalTime];
    [self setvalueInkey];
    [self setArrayNo];
    [RoundPicker reloadAllComponents];
    RoundForAction=(RoundRow/oldTimeValue)*TimeRow;
    [RoundPicker selectRow:RoundForAction-(TimeRow*10) inComponent:0 animated:YES];
    NSTimer *timer;
    timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(yourMethodName) userInfo:nil repeats:NO];
}

-(void)getvalue{
    TimeRow=[[NSUserDefaults standardUserDefaults] floatForKey:kapaltime];
    RoundRow=[[NSUserDefaults standardUserDefaults] floatForKey:kapalRound];
    
    [RoundPicker selectRow:RoundRow-(TimeRow*10) inComponent:0 animated:YES];
    [TimePicker selectRow:TimeRow-1 inComponent:0 animated:YES];
}
-(void)setvalueInkey{
    [[NSUserDefaults standardUserDefaults] setInteger:TimeRow forKey:kapaltime];
    [[NSUserDefaults standardUserDefaults] setInteger:RoundForAction forKey:kapalRound];
    
}

-(void)ChangeText{
    
    self.navigationItem.title = [MCLocalization stringForKey:@"kapalbhati"];
    roundlbl.text=[MCLocalization stringForKey:@"Rounds"];
    timelbl.text=[MCLocalization stringForKey:@"time"];
    
}

-(IBAction)backbutton:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}
-(void)getTotalTime{
    totaltime =(TimeRow*60);
    lblseconds.text=[NSString stringWithFormat:@"%d",totaltime];
    seconds = totaltime % 60;
    minutes = (totaltime / 60) % 60;
    hours = totaltime / 3600;
    lblseconds.text=[NSString stringWithFormat:@"%d",seconds];
    lblminutes.text=[NSString stringWithFormat:@"%d",minutes];
    lblHours.text=[NSString stringWithFormat:@"%d",hours];
}

-(void)playsound{
    [[CommonSounds sharedInstance] playFinishSound];
}


-(void)setupbutton{
    int a;
    if (DEVICE==IPAD) {
        if (self.view.frame.size.height==1366) {
            a=80;
        }
        else{
            a=60;
        }
    }
    else{
        a=35;
    }
    CGRect floatFrame = CGRectMake((Floatingbutton.frame.origin.x+5),(Floatingbutton.frame.origin.y+5), a, a);
    button1=[[UIButton alloc]initWithFrame:floatFrame];
    button1.backgroundColor=RGB(255, 154, 0);
    button1.layer.cornerRadius=(button1.bounds.size.width-2)/2;
    button1.tag=11;
    [button1 addTarget:self
                action:@selector(buttonmethod:) forControlEvents:UIControlEventTouchDown];
    button1.imageEdgeInsets= UIEdgeInsetsMake(7,7,7,7);
    [button1 setImage:[UIImage imageNamed:@"help"] forState:UIControlStateNormal];
    
    button2=[[UIButton alloc]initWithFrame:floatFrame];
    button2.backgroundColor=RGB(255, 154, 0);
    button2.layer.cornerRadius=(button2.bounds.size.width-2)/2;
    button2.tag=12;
    [button2 addTarget:self
                action:@selector(buttonmethod:) forControlEvents:UIControlEventTouchDown];
    button2.imageEdgeInsets= UIEdgeInsetsMake(7,7,7,7);
    [button2 setImage:[UIImage imageNamed:@"benefits_ic"] forState:UIControlStateNormal];
    
    button3=[[UIButton alloc]initWithFrame:floatFrame];
    button3.backgroundColor=RGB(255, 154, 0);
    button3.layer.cornerRadius=(button3.bounds.size.width-2)/2;
    button3.tag=13;
    [button3 addTarget:self
                action:@selector(buttonmethod:) forControlEvents:UIControlEventTouchDown];
    button3.imageEdgeInsets= UIEdgeInsetsMake(7,7,7,7);
    [button3 setImage:[UIImage imageNamed:@"about_ii"] forState:UIControlStateNormal];
    
}
-(IBAction)startbutton:(id)sender{
    [self dismissMenu];
}
-(IBAction)reportbutton:(id)sender{
    [self dismissMenu];
}
-(IBAction)floatingbutton:(id)sender
{
    if (isMenuVisible==YES)
    {
        [self dismissMenu];
    }
    else
    {
        [self showMenu];
    }
}

-(void)buttonmethod:(UIButton *)sender{
    if (sender.tag==11) {
        AboutViewController *help=[self.storyboard instantiateViewControllerWithIdentifier:@"help"];
        help.Type=@"Kapalbhati";
        [self.navigationController pushViewController:help animated:YES];
    }
    else if (sender.tag==12)
    {
        BenefitViewController *benefit=[self.storyboard instantiateViewControllerWithIdentifier:@"benefit"];
        benefit.Type=@"Kapalbhati";
        [self.navigationController pushViewController:benefit animated:YES];
    }
    else{
        HelpViewController *about=[self.storyboard instantiateViewControllerWithIdentifier:@"about"];
        about.Type=@"Kapalbhati";
        [self.navigationController pushViewController:about animated:YES];
    }
    button1.alpha=0;
    button2.alpha=0;
    button3.alpha=0;
    isMenuVisible=NO;
}

-(void) showMenu
{
    CGPoint Point1;
    CGPoint Point2;
    CGPoint Point3;
    if (DEVICE==IPAD) {
        Point1=CGPointMake(-8, -172);
        Point2=CGPointMake(-95, -115);
        Point3=CGPointMake(-150, -20);
    }
    else{
        Point1=CGPointMake(-10, -103);
        Point2=CGPointMake(-62, -73);
        Point3=CGPointMake(-90, -20);
    }
    [Floatingbutton setUserInteractionEnabled:NO];
    [UIButton animateWithDuration:0.5f
                       animations:^{
                           [UIButton animateWithDuration:2.0 delay:0.0 options:UIViewAnimationOptionRepeat | UIViewAnimationOptionCurveLinear animations:^{
                               CGAffineTransform transform = CGAffineTransformMakeRotation(M_PI);
                               button1.transform = transform;
                               button2.transform=transform;
                               button3.transform=transform;
                           } completion:NULL];
                           
                           [button1 setTransform:CGAffineTransformMakeTranslation(Point1.x,Point1.y)];
                           [button2 setTransform:CGAffineTransformMakeTranslation(Point2.x,Point2.y)];
                           [button3 setTransform:CGAffineTransformMakeTranslation(Point3.x,Point3.y)];
                           [self.view addSubview:button1];
                           [self.view addSubview:button2];
                           [self.view addSubview:button3];
                           button1.alpha=0.7;
                           button2.alpha=0.7;
                           button3.alpha=0.7;
                       }
                       completion:^(BOOL finished){
                           button1.alpha=1;
                           button2.alpha=1;
                           button3.alpha=1;
                           [Floatingbutton setUserInteractionEnabled:YES];
                           isMenuVisible=YES;
                       }];
    
}

-(void) dismissMenu
{
    [Floatingbutton setUserInteractionEnabled:NO];
    [UIButton animateWithDuration:0.5f
                       animations:^{
                           [UIButton animateWithDuration:2.0 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveLinear animations:^{
                               CGAffineTransform transform = CGAffineTransformMakeRotation(M_PI);
                               button1.transform = transform;
                               button2.transform=transform;
                               button3.transform=transform;
                           } completion:NULL];
                           
                           [button1 setTransform:CGAffineTransformMakeTranslation(0,0)];
                           [button2 setTransform:CGAffineTransformMakeTranslation(0,0)];
                           [button3 setTransform:CGAffineTransformMakeTranslation(0,0)];
                           button1.alpha=0.3;
                           button2.alpha=0.3;
                           button3.alpha=0.3;
                       }
                       completion:^(BOOL finished){
                           button1.alpha=0;
                           button2.alpha=0;
                           button3.alpha=0;
                           [Floatingbutton setUserInteractionEnabled:YES];
                           isMenuVisible=NO;
                       }];
    
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (isMenuVisible==YES) {
        [self dismissMenu];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"kapalbhatiAction"]) {
        KapalbhatiActionVC *controller = (KapalbhatiActionVC *)segue.destinationViewController;
        controller.totalTime=totaltime;
        controller.rounds=RoundForAction;
        controller.minutes=TimeRow;
        controller.delegate=self;
    }
}

@end

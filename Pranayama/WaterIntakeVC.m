//
//  WaterIntakeVC.m
//  Pranayama
//
//  Created by Manish Kumar on 24/08/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "WaterIntakeVC.h"

@interface WaterIntakeVC ()

@end

@implementation WaterIntakeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName=@"water_intake";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:waterdelegate] && [[NSUserDefaults standardUserDefaults]boolForKey:waterSated] && ![[NSUserDefaults standardUserDefaults]boolForKey:commonClassCond]) {
        
        HomeTabelVC *home=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
        ProblemListViewController *ProblumList=[self.storyboard instantiateViewControllerWithIdentifier:@"problumlist"];
        WaterIntakeVC *waterIntake=[self.storyboard instantiateViewControllerWithIdentifier:@"waterintake"];
        [self.navigationController setViewControllers:@[home,ProblumList,waterIntake]animated:YES];
    }
    
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:waterdelegate];
    
}


- (void)HomeMethod:(NSNotification *)notification {
    [[CommonSounds sharedInstance] appDidBecomeActive:notification navigation:self.navigationController];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self disableSlidePanGestureForRightMenu];
    [self activeMethod];
}


-(void)activeMethod{
    
    self.managedObjectContext = [kAppDele managedObjectContext];
    array=[[kAppDele fetchwaterReportFromDatabase]mutableCopy];
    self.automaticallyAdjustsScrollViewInsets=NO;
    if (DEVICE==IPAD) {
        fontSize=24;
        font1=25;
        font2=15;
    }
    else{
        fontSize=18;
        font1=14;
        font2=10;
    }
    [self addIndatabase];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    
    switchValue=((int)array.count/7);
    indexpath=array.count%7;
    if (array.count%7==0 && array.count>0) {
        indexpath=switchValue*7;
        switchValue--;
    }
    maxswitch=switchValue;
    
    
    [self topviewMethod];
    [self BottomviewMethod];
    [self changeText];
}


//////

-(void)changeText{
    NSString *quentitystrInml,*quentitystrInpound;
    NSString *lan=[[NSUserDefaults standardUserDefaults] objectForKey:Languagevalue];
    if ([lan isEqualToString:Hindi]) {
        self.navigationItem.title=[MCLocalization stringForKey:@"water"];
        quentitystrInml=[NSString stringWithFormat:@"%@ (%@)",[MCLocalization stringForKey:@"water"],[MCLocalization stringForKey:@"ml"]];
        quentitystrInpound=[NSString stringWithFormat:@"%@ (%@)",[MCLocalization stringForKey:@"water"],[MCLocalization stringForKey:@"oz"]];
        datelbl.text=[MCLocalization stringForKey:@"date"];
    }
    else if([lan isEqualToString:English]){
        self.navigationItem.title=@"Water Intake";
        quentitystrInml=@"Water in mL";
        quentitystrInpound=@"Water in oz";
        datelbl.text=@"Date";
    }
    else{
        self.navigationItem.title=[MCLocalization stringForKey:@"water"];
        quentitystrInml=[NSString stringWithFormat:@"%@ (%@)",[MCLocalization stringForKey:@"water"],[MCLocalization stringForKey:@"ml"]];
        quentitystrInpound=[NSString stringWithFormat:@"%@ (%@)",[MCLocalization stringForKey:@"water"],[MCLocalization stringForKey:@"oz"]];
        datelbl.text=[MCLocalization stringForKey:@"date"];
    }
    if ([[NSUserDefaults standardUserDefaults]boolForKey:isPounds]) {
        QuentityLbl.text=quentitystrInpound;
    }
    else{
        QuentityLbl.text=quentitystrInml;
    }
}

////////////////   Insert into Dataqbase////////
///////////////////////////////////////////////////////////

-(NSInteger)DayComparision:(NSString*)startdate enddate:(NSString*)Enddate{
    
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"dd-mm-yy"];
    NSDate *startDate = [f dateFromString:startdate];
    NSDate *endDate = [f dateFromString:Enddate];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    NSInteger numberOfDays=[components day];
    return numberOfDays;
}



-(void)addIndatabase{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:waterSated] && [[NSUserDefaults standardUserDefaults]boolForKey:waterMainCondition])
    {
        NSDateFormatter *dateFormate=[[NSDateFormatter alloc]init];
        dateFormate.dateFormat=@"dd-MM-yyyy";
        NSDate *today=[NSDate date];
        NSString *todaystr=[dateFormate stringFromDate:today];
        float waterQ=[[NSUserDefaults standardUserDefaults] floatForKey:notificationWater];
        if (array.count>0) {
            Water *waterReport=[array objectAtIndex:0];
            NSDate *date=waterReport.date;
            NSString *datestr=[dateFormate stringFromDate:date];
            NSInteger diff= [self DayComparision:datestr enddate:todaystr];
            if ([datestr isEqualToString:todaystr]) {
                float quentity=[waterReport.waterQuentity floatValue]+waterQ;
                waterReport.waterQuentity=[NSString stringWithFormat:@"%f",quentity];
                NSError *error;
                if (![self.managedObjectContext save:&error]) {
                    NSLog(@"%@",error);
                }
            }
            else{
                for (NSInteger a=1; a<=diff; a++) {
                    Water *report=[NSEntityDescription insertNewObjectForEntityForName:@"Water" inManagedObjectContext:self.managedObjectContext];
                    NSDate *date1=[NSDate dateWithTimeInterval:a*24*60*60 sinceDate:date];
                    report.date=date1;
                    if (a==diff) {
                        report.waterQuentity=[NSString stringWithFormat:@"%f",waterQ];
                    }
                    else{
                        report.waterQuentity=[NSString stringWithFormat:@"0"];
                    }
                    NSError *error;
                    if (![self.managedObjectContext save:&error]) {
                        NSLog(@"%@",error);
                    }
                }
            }
        }
        else{
            Water *report=[NSEntityDescription insertNewObjectForEntityForName:@"Water" inManagedObjectContext:self.managedObjectContext];
            report.date=[NSDate date];
            report.waterQuentity=[NSString stringWithFormat:@"%f",waterQ];
            NSError *error;
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"%@",error);
            }
        }
        array=[[kAppDele fetchwaterReportFromDatabase]mutableCopy];
        switchValue=((int)array.count/7);
        indexpath=array.count%7;
        if (array.count%7==0 && array.count>0) {
            indexpath=switchValue*7;
            switchValue--;
        }
        maxswitch=switchValue;
        
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:waterMainCondition];
    }
}


/////////// Custum design Method //////////

-(void)topviewMethod{
    
    topview=[[UIView alloc]initWithFrame:CGRectMake(5,5 , scrollview.frame.size.width-10,70)];
    topview.backgroundColor= RGB(247 ,247,247);
    topview.layer.borderColor=RGB(207,207,207).CGColor;
    topview.layer.borderWidth=1.0;
    [scrollview addSubview:topview];
    dayLbl=[[UILabel alloc]initWithFrame:CGRectMake(10, 1, topview.frame.size.width-110, topview.frame.size.height-2)];
//    dayLbl text seted in bottomviewmethod ////
    dayLbl.font=Ralewayfont(font1);
    [topview addSubview:dayLbl];
    UIButton *Leftbutton=[[UIButton alloc]initWithFrame:CGRectMake(dayLbl.frame.origin.x+dayLbl.frame.size.width, 1, 50, topview.frame.size.height-2)];
    [Leftbutton setImage:[UIImage imageNamed:@"left_arrow-1"] forState:UIControlStateNormal];
    Leftbutton.imageEdgeInsets = UIEdgeInsetsMake(15, 15, 15, 15);
    [Leftbutton addTarget:self action:@selector(LeftRightBtnmethod:) forControlEvents:UIControlEventTouchUpInside];
    Leftbutton.tag=1;
    [topview addSubview:Leftbutton];
    UIButton *rightButton=[[UIButton alloc]initWithFrame:CGRectMake(Leftbutton.frame.origin.x+50, 1, 50, topview.frame.size.height-2)];
     rightButton.imageEdgeInsets = UIEdgeInsetsMake(15, 15, 15, 15);
    [rightButton setImage:[UIImage imageNamed:@"right_arrow-1"] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(LeftRightBtnmethod:) forControlEvents:UIControlEventTouchUpInside];
    rightButton.tag=2;
    [topview addSubview:rightButton];
}


-(void)BottomviewMethod{
    int bottomviewH,labelhieght,view1hight,progressheight,daylablh,quentityW,quentityH,numberlblXY,progressCont;
    if (DEVICE==IPAD) {
        bottomviewH=1040;
        labelhieght=40;
        view1hight=70;
        progressheight=840;
        daylablh=70;
        quentityW=200;
        quentityH=40;
        numberlblXY=60;
        if (Device_Type==iPadAir) {
            progressCont=20;
        }
        else{
        progressCont=25;
        }
    }
    else{
        bottomviewH=520;
        labelhieght=20;
        view1hight=35;
        progressheight=420;
        daylablh=35;
        quentityW=100;
        quentityH=20;
        numberlblXY=30;
        progressCont=5;
    }
    bottomView.layer.sublayers=nil;
    bottomView=[[UIView alloc]initWithFrame:CGRectMake(5, topview.frame.origin.y+topview.frame.size.height+10,scrollview.frame.size.width-10 , bottomviewH)];
    bottomView.layer.borderWidth=1.0;
    bottomView.layer.borderColor=RGB(239,239,239).CGColor;
    [scrollview addSubview:bottomView];
    
    
    QuentityLbl=[[UILabel alloc]initWithFrame:CGRectMake(-((quentityW/2-quentityH/2)-5), bottomView.frame.size.height/2-quentityW/2, quentityW, quentityH)];
    QuentityLbl.font=Ralewayfont(font1);
    [QuentityLbl setTransform:CGAffineTransformMakeRotation(-M_PI / 2)];
    [bottomView addSubview:QuentityLbl];
    [self changeText];
    
    UIView *numberView=[[UIView alloc]initWithFrame:CGRectMake(QuentityLbl.frame.size.width+5,view1hight,daylablh , progressheight)];
    [bottomView addSubview:numberView];
    
    int k=0,l=0;
    float maxProgress=0.0;
    if ([[NSUserDefaults standardUserDefaults]boolForKey:isPounds]) {
        k=140;
        maxProgress=140;
        l=7;
    }
    else{
        k=4000;
        maxProgress=4000;
        l=200;
    }
    float j=0.0,heightday=0.0;
    heightday=numberView.frame.size.height/21;
    for (int i=21; i>0; i--) {
        UILabel *daylbl=[[UILabel alloc]initWithFrame:CGRectMake(0, j, numberlblXY, heightday)];
        daylbl.textAlignment=NSTextAlignmentCenter;
        NSString *str=[NSString stringWithFormat:@"%d",k];
        daylbl.text=str;
        daylbl.font=Ralewayfont(font2);
        [numberView addSubview:daylbl];
        j=j+heightday;
        k=k-l;
    }

    
    ////////
    NSDate *Reprtdate;
    NSInteger index=indexpath;
    if (array.count>0 && index>0) {
        Water *report=[array objectAtIndex:index-1];
        Reprtdate=report.date;
    }
    else{
        Reprtdate=[NSDate date];
    }
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    dateformate.dateFormat=@"EE";
    int x=0;
    NSDate *date=Reprtdate;
    
    view1=[[UIView alloc]initWithFrame:CGRectMake(numberView.frame.origin.x+numberView.frame.size.width+5, 0, bottomView.frame.size.width-(quentityH+daylablh+15), view1hight)];
    [bottomView addSubview:view1];
    float width=view1.frame.size.width/7;
    for (int i=7; i>0;i--) {
        NSString *daystr=[dateformate stringFromDate:date];
        daystr=[NSString stringWithFormat:@"%@",[daystr uppercaseString]];
        UILabel *day=[[UILabel alloc]initWithFrame:CGRectMake(x, 0,width ,view1hight)];
        day.font=Ralewayfont(font2);
        day.textAlignment=NSTextAlignmentCenter;
        day.text=daystr;
        [view1 addSubview:day];
        date=[NSDate dateWithTimeInterval:24*60*60 sinceDate:date];
        x=x+width;
    }
    

    
    ///// daylbl ////
    dateformate.dateFormat=@"MMMM dd";
    NSString *Startstr=[dateformate stringFromDate:Reprtdate];
    NSDate *Enddate=[NSDate dateWithTimeInterval:6*24*60*60 sinceDate:Reprtdate];
    NSString *Endstr=[dateformate stringFromDate:Enddate];
    dayLbl.text=[NSString stringWithFormat:@"%@ - %@",Startstr,Endstr];
    
    /////////////progress view ////////
    
    
    UIView *progressView=[[UIView alloc]initWithFrame:CGRectMake(view1.frame.origin.x, view1.frame.origin.y+view1.frame.size.height,view1.frame.size.width, progressheight)];
    progressView.layer.borderWidth=1;
    progressView.layer.borderColor=RGB(239,239,239).CGColor;
    [bottomView addSubview:progressView];
    
    int b=0;
    float width1=progressView.frame.size.width/7;
    for (int i=7; i>0; i--) {
        UIView *barview=[[UIView alloc]initWithFrame:CGRectMake(b-progressView.frame.size.height/2+width1/2-progressCont,progressView.frame.size.height/2-width1/2,progressView.frame.size.height-5,width1 )];
        [progressView addSubview:barview];
        
        
        LDProgressView *progress=[[LDProgressView alloc]initWithFrame:CGRectMake(4, width1/2-2, barview.frame.size.width-10, barview.frame.size.height/2+4)];
        
        float progressValue=0.0;
        
        if (index>0 && array.count>0) {
            Water *report1=[array objectAtIndex:index-1];
            progressValue=[report1.waterQuentity floatValue];
            index--;
        }
        else{
            progressValue=0.0;
        }
        progressValue=(progressValue/maxProgress);
        int number=progress.bounds.size.height/2;
        progress.borderRadius =[NSNumber numberWithInt:number];
        progress.layer.borderWidth=1;
        progress.layer.cornerRadius=progress.bounds.size.height/2;
        progress.layer.borderColor=RGB(180, 180, 180).CGColor;
        progress.showBackgroundInnerShadow = @YES;
        progress.progress =progressValue;
        progress.animate = @NO;
        progress.showText=@NO;
        progress.background=RGB(230, 230, 230);
        progress.color=RGB(135,206,250);
        
        [barview setTransform:CGAffineTransformMakeRotation(-M_PI / 2)];
        [barview addSubview:progress];
        b=b+width1;
    }
    
    dateView=[[UIView alloc]initWithFrame:CGRectMake(view1.frame.origin.x, progressView.frame.origin.y+progressView.frame.size.height, view1.frame.size.width, view1.frame.size.height)];
    [bottomView addSubview: dateView];
    
    date=Reprtdate;
    x=0;
    dateformate.dateFormat=@"dd";
    for (int i=7; i>0;i--) {
        NSString *daystr=[dateformate stringFromDate:date];
        UILabel *day=[[UILabel alloc]initWithFrame:CGRectMake(x, 0,width ,35)];
        day.font=Ralewayfont(font2);
        day.textAlignment=NSTextAlignmentCenter;
        day.text=daystr;
        [dateView addSubview:day];
        date=[NSDate dateWithTimeInterval:24*60*60 sinceDate:date];
        x=x+width;
    }

    datelbl=[[UILabel alloc]initWithFrame:CGRectMake(dateView.frame.origin.x, dateView.frame.origin.y+dateView.frame.size.height, dateView.frame.size.width, labelhieght)];
    datelbl.font=Ralewayfont(font1);
    datelbl.textAlignment=NSTextAlignmentCenter;
    [bottomView addSubview:datelbl];
    
    ////////
    scrollview.contentSize=CGSizeMake(scrollview.frame.size.width,bottomView.frame.origin.y+bottomView.frame.size.height+10);
    
}


-(void)LeftRightBtnmethod:(UIButton*)sender{
    if (sender.tag==1) {
        if (switchValue==0) {
            return;
        }
        else{
            indexpath=indexpath+7;
            switchValue--;
            [self BottomviewMethod];
        }
    }
    else{
        if (switchValue==maxswitch) {
            return;
        }
        else{
            indexpath=indexpath-7;
            switchValue++;
            [self BottomviewMethod];
        }
    }
}
-(IBAction)backbutton:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  Water.h
//  Pranayama
//
//  Created by Manish Kumar on 31/08/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Water : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Water+CoreDataProperties.h"

//
//  TotalReportVC.m
//  Pranayama
//
//  Created by Manish Kumar on 04/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "TotalReportVC.h"

@interface TotalReportVC ()

@end

@implementation TotalReportVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    // Do any additional setup after loading the view.
    
    self.screenName=@"Main Report";
    int fontSize;
    if (DEVICE==IPAD) {
        fontSize=24;
        entityFontSize=21;
    }
    else{
        fontSize=18;
        entityFontSize=12;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    datelbl.font=Ralewayfont(entityFontSize);
    durationlbl.font=Ralewayfont(entityFontSize);
    typelbl.font=Ralewayfont(entityFontSize);
    self.managedObjectContext=[kAppDele managedObjectContext];
    tblReport.allowsMultipleSelectionDuringEditing = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    reportArray=[[kAppDele fetchReportFromDatabse] mutableCopy];
    selectunselectarray=[[NSMutableArray alloc]init];
    for (int i=0; i<[reportArray count]; i++) {
        [selectunselectarray addObject:@"NO"];
    }
    [self setlblheight];
    [self ChangeText];
    [self Noresult];
    [self gesture];
}

- (void)HomeMethod:(NSNotification *)notification {
    [[CommonSounds sharedInstance] appDidBecomeActive:notification navigation:self.navigationController];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self disableSlidePanGestureForRightMenu];
    
}

-(void)setlblheight{
    int Device=Device_Type;
    switch (Device) {
        case iPhone4:
            topview.frame=CGRectMake(8, 75, 305, 32);
            tblReport.frame=CGRectMake(8, 118, 305, 358);
            break;
        case iPhone5:
            topview.frame=CGRectMake(8, 74, 305, 32);
            tblReport.frame=CGRectMake(8, 115, 305, 449);
            break;
        case iPhone6:
            topview.frame=CGRectMake(8, 74, 359, 32);
            tblReport.frame=CGRectMake(8, 115, 359, 548);
            break;
        case iPhone6P:
            topview.frame=CGRectMake(8, 74, 398, 32);
            tblReport.frame=CGRectMake(8, 115, 398, 617);
            break;
        case iPadPro:
            tblReport.frame=CGRectMake(8, 176, 1008, 1190);
        default:
            break;
    }
}

-(void)gesture{
    lpgr = [[UILongPressGestureRecognizer alloc]
            initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 0.5; //seconds
    lpgr.delegate = self;
    [tblReport addGestureRecognizer:lpgr];
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    
    p = [gestureRecognizer locationInView:tblReport];
    
    indexpath1 = [tblReport indexPathForRowAtPoint:p];
    UITableViewCell* theCell = [tblReport cellForRowAtIndexPath:indexpath1];
    if (indexpath1 == nil) {
        NSLog(@"long press on table view but not on a row");
    } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        NSLog(@"long press on table view at row %ld", (long)indexpath1.row);
        theCell.backgroundColor=RGB(19, 155, 231);
        cl++;
        [selectunselectarray replaceObjectAtIndex:indexpath1.row withObject:@"YES"];
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cellTapped:)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        tapGestureRecognizer.numberOfTouchesRequired = 1;
        tapGestureRecognizer.delegate = self;
        [tblReport addGestureRecognizer:tapGestureRecognizer];
    }
    else {
        NSLog(@"gestureRecognizer.state = %ld", (long)gestureRecognizer.state);
    }
}

-(void)cellTapped:(UITapGestureRecognizer*)tap1
{
    CGPoint q = [tap1 locationInView:tblReport];
    indexpath1 = [tblReport indexPathForRowAtPoint:q];
    if (indexpath1 != nil) {
        UITableViewCell* theCell = [tblReport cellForRowAtIndexPath:indexpath1];
        if (theCell.backgroundColor==[UIColor clearColor]) {
            
            theCell.backgroundColor=RGB(19, 155, 231);
            cl++;
            [selectunselectarray replaceObjectAtIndex:indexpath1.row withObject:@"YES"];
        }
        else{
            
            theCell.backgroundColor=[UIColor clearColor];
            cl--;
            [selectunselectarray replaceObjectAtIndex:indexpath1.row withObject:@"NO"];
        }
        if (cl==0) {
            tap1.enabled=NO;
            [self gesture];
        }
    }
}

-(IBAction)deletebutton:(id)sendar{
    for(int i=0; i<selectunselectarray.count; i++){
        NSIndexPath *a = [NSIndexPath indexPathForRow:i inSection:0];
        
        if ([[selectunselectarray objectAtIndex:i] isEqualToString:@"YES"]) {
            
            
            NSError *error;
            Record *repoEntity=[reportArray objectAtIndex:a.row];
            [self.managedObjectContext deleteObject:repoEntity];
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"unresolved Error: %@", error);
            }
            [reportArray removeObjectAtIndex:a.row];
            [selectunselectarray removeObjectAtIndex:a.row];
            [tblReport deleteRowsAtIndexPaths:@[a] withRowAnimation:UITableViewRowAnimationAutomatic];
            i=i-1;
        }
    }
    tap.enabled=NO;
    cl=0;
    [self gesture];
    [tblReport reloadData];
    [self Noresult];
}

-(void)ChangeText{
        
        self.navigationItem.title = [MCLocalization stringForKey:@"report"];
        datelbl.text=[MCLocalization stringForKey:@"date"];
        durationlbl.text=[MCLocalization stringForKey:@"duration"];
        typelbl.text=[MCLocalization stringForKey:@"type"];

}

-(IBAction)backbutton:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}
-(void)Noresult{
    if ([reportArray count]==0)
    {
        UILabel *noDataLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tblReport.bounds.size.width, tblReport.bounds.size.height)];
        noDataLabel.text = @"No Record Found";
        noDataLabel.textColor = [UIColor lightGrayColor];
        noDataLabel.textAlignment = NSTextAlignmentCenter;
        tblReport.backgroundView = noDataLabel;
    }
    else{
        tblReport.backgroundView = nil;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [reportArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *listingCellIdentifier = @"ReportCustomCell";
    cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:listingCellIdentifier];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    UILabel *lblDate=(UILabel*)[cell viewWithTag:1];
    UILabel *lblDuration=(UILabel *)[cell viewWithTag:2];
    UILabel *lblType=(UILabel *)[cell viewWithTag:3];
    lblDate.font=Ralewayfont(entityFontSize);
    lblType.font=Ralewayfont(entityFontSize);
    lblDuration.font=Ralewayfont(entityFontSize);
    cell.backgroundColor=[UIColor clearColor];
    
    Record *finalreportEntity=[reportArray objectAtIndex:indexPath.row];
    
    lblType.text=finalreportEntity.anulomtype;
    lblDuration.text=[self convertSecondToTime:[finalreportEntity.anulomduration intValue]];
    NSDate *date=finalreportEntity.anulomDate;
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    lblDate.text=[dateFormatter stringFromDate:date];
    if ([[selectunselectarray objectAtIndex:indexPath.row] isEqualToString:@"YES"]) {
        cell.backgroundColor=RGB(19, 155, 231);
    }
    
    return cell;
}

-(NSString*)convertSecondToTime:(NSUInteger)elapsedSeconds{
    NSUInteger h = elapsedSeconds / 3600;
    NSUInteger m = (elapsedSeconds / 60) % 60;
    NSUInteger s = elapsedSeconds % 60;
    NSString *formattedTime;
    if (h>0) {
        formattedTime = [NSString stringWithFormat:@"%lu hour %lu min %lu sec", (unsigned long)h, (unsigned long)m, (unsigned long)s];
    }
    else if (m>0 && h==0){
        formattedTime = [NSString stringWithFormat:@"%lu min %lu sec", (unsigned long)m, (unsigned long)s];
        
    }
    else if (h==0 && m==0){
        formattedTime = [NSString stringWithFormat:@"%lu sec",(unsigned long)s];
        
    }
    return formattedTime;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    if (cl>0) {
        return NO;
    }
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSError *error;
        Record *repoEntity=[reportArray objectAtIndex:indexPath.row];
        [self.managedObjectContext deleteObject:repoEntity];
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"unresolved Error: %@", error);
        }
        [reportArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        [tblReport reloadData];
        //add code here for when you hit delete
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

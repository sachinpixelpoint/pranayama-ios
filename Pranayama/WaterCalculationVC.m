//
//  WaterCalculationVC.m
//  Pranayama
//
//  Created by Manish Kumar on 24/08/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "WaterCalculationVC.h"

@interface WaterCalculationVC ()

@end

@implementation WaterCalculationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName=@"Water_Calculation";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    window = [UIApplication sharedApplication].keyWindow;
    if (DEVICE==IPAD) {
        fontSize=24;
    }
    else{
        fontSize=18;
    }

    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    [self getRIseandBedTime];
    [self changeTesxt];
    [self viewFrame];
    IsReadyToPlay=NO;
    BottomView.hidden=YES;
    WeightView.hidden=YES;
    fixView.hidden=YES;
    datePkrView.hidden=YES;
    stopbtn.hidden=YES;
    [submitButton setImage:[UIImage imageNamed:@"calculate"] forState:UIControlStateNormal];
    datePkrView.layer.cornerRadius=2;
    datepicker.datePickerMode=UIDatePickerModeTime;
    tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    singleFingerTap.delegate=self;
    [window addGestureRecognizer:singleFingerTap];
    [self textfeildLine:weight];
    [self textfeildLine:exercise];
    [self textfeildLine:water];
    [self checkWaterCondition];
    [self setFont];
   
}


- (void)HomeMethod:(NSNotification *)notification {
    [[CommonSounds sharedInstance] appDidBecomeActive:notification navigation:self.navigationController];
}



-(void)checkWaterCondition{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:waterSated]) {
        stopbtn.hidden=NO;
        weight.text=[NSString stringWithFormat:@"%ld",(long)[[NSUserDefaults standardUserDefaults]integerForKey:Userweight]];
        exercise.text=[NSString stringWithFormat:@"%ld",(long)[[NSUserDefaults standardUserDefaults]integerForKey:excerciseTime]];
        water.text=[NSString stringWithFormat:@"%ld",(long)[[NSUserDefaults standardUserDefaults]integerForKey:volumeAndTime]];
        [self checkValidation:NO];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self disableSlidePanGestureForRightMenu];
}

-(void)setFont{
    CGFloat fontsizes;
    if (DEVICE==IPAD) {
        fontsizes=24;
    }
    else{
        fontsizes=14;
    }
    weightlbl.font=Ralewayfont(fontsizes);
    RiseTimelbl.font=Ralewayfont(fontsizes);
    BedTimelbl.font=Ralewayfont(fontsizes);
    Excerciselbl.font=Ralewayfont(fontsizes);
    Selectonelbl.font=Ralewayfont(fontsizes);
     TotalTimeDaylbl.font=Ralewayfont(fontsizes);
     EveryIntervalWaterlbl.font=Ralewayfont(fontsizes);
     ReminderIntervallbl.font=Ralewayfont(fontsizes);
     Minlbl.font=Ralewayfont(fontsizes);
     Detaillbl.font=Ralewayfont(fontsizes);
     Resultlbl.font=Ralewayfont(fontsizes);
    volumeAndinterval.font=Ralewayfont(fontsizes);
     viewKgbtn.titleLabel.font=Ralewayfont(fontsizes);
     viewpoundbtn.titleLabel.font=Ralewayfont(fontsizes);
     fixWaterbtn.titleLabel.font=Ralewayfont(fontsizes);
     fixTimebtn.titleLabel.font=Ralewayfont(fontsizes);
     okBtn.titleLabel.font=Ralewayfont(fontsizes);
     cancelBtn.titleLabel.font=Ralewayfont(fontsizes);
    kgbutton.titleLabel.font=Ralewayfont(fontsizes);
    interval.titleLabel.font=Ralewayfont(fontsizes);
    ml.font=Ralewayfont(fontsizes);
  
    
}



-(void)changeTesxt{
    NSString *mlstr,*ozstr,*hour,*Notification_interval,*waterVolume;
    NSString *lan=[[NSUserDefaults standardUserDefaults] objectForKey:Languagevalue];
    if ([lan isEqualToString:Hindi]) {
        self.navigationItem.title = [MCLocalization stringForKey:@"water"];
        weightlbl.text=[NSString stringWithFormat:@"%@ :",[MCLocalization stringForKey:@"weight"]];
        RiseTimelbl.text=[MCLocalization stringForKey:@"riseTime"];
        BedTimelbl.text=[MCLocalization stringForKey:@"bedTime"];
        Excerciselbl.text=[MCLocalization stringForKey:@"excerciseTime"];
        Selectonelbl.text=[MCLocalization stringForKey:@"choseOne"];
        TotalTimeDaylbl.text=[MCLocalization stringForKey:@"per_day_water"];
        EveryIntervalWaterlbl.text=[MCLocalization stringForKey:@"ever_interval_water"];
        ReminderIntervallbl.text=[MCLocalization stringForKey:@"notification_interval"];
        Minlbl.text=[MCLocalization stringForKey:@"minat"];
        Detaillbl.text=[MCLocalization stringForKey:@"detail"];
        Resultlbl.text=[MCLocalization stringForKey:@"result"];
        [viewKgbtn setTitle:[MCLocalization stringForKey:@"kg"] forState:UIControlStateNormal];
        [viewpoundbtn setTitle:[MCLocalization stringForKey:@"pound"] forState:UIControlStateNormal];
        [fixWaterbtn setTitle:[MCLocalization stringForKey:@"fixwaterVolume"] forState:UIControlStateNormal];
        [fixTimebtn setTitle:[MCLocalization stringForKey:@"fixTime"] forState:UIControlStateNormal];
        mlstr=[MCLocalization stringForKey:@"ml"];
        ozstr=[MCLocalization stringForKey:@"oz"];
        hour=[MCLocalization stringForKey:@"hour"];
        Notification_interval=[MCLocalization stringForKey:@"interval_of_notification"];
        waterVolume=[MCLocalization stringForKey:@"waterVolume"];
        [okBtn setTitle:[MCLocalization stringForKey:@"set"] forState:UIControlStateNormal];
        [cancelBtn setTitle:[MCLocalization stringForKey:@"Cancel"] forState:UIControlStateNormal];
        Enter_weight=[MCLocalization stringForKey:@"weight_aleart"];
        Enter_excerciseTime=[MCLocalization stringForKey:@"excerciseTime_aleart"];
        Enter_waterVolume=[MCLocalization stringForKey:@"waterVolume_aleart"];
        Enter_interval=[MCLocalization stringForKey:@"interval_of_notification_aleart"];
        NotificationAleartstr=[MCLocalization stringForKey:@"water_notification_alert"];
        NotificationCancelStr=[MCLocalization stringForKey:@"notification_cancle_str"];
        str1=[MCLocalization stringForKey:@"weight"];
        str2=[MCLocalization stringForKey:@"above_weight"];
        str3=[MCLocalization stringForKey:@"at_a_time"];
        str4=[MCLocalization stringForKey:@"drink"];
        str5=[MCLocalization stringForKey:@"time"];
        str6=[MCLocalization stringForKey:@"down_time"];
    }
    else if([lan isEqualToString:English]){
        [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor],
           NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
        self.navigationItem.title=@"Water Intake";
        [viewKgbtn setTitle:@"Kg" forState:UIControlStateNormal];
        [viewpoundbtn setTitle:@"Pounds" forState:UIControlStateNormal];
        [fixTimebtn setTitle:@"Fix Interval of Time" forState:UIControlStateNormal];
        [fixWaterbtn setTitle:@"Fix Volume of Water" forState:UIControlStateNormal];
        [okBtn setTitle:@"OK" forState:UIControlStateNormal];
        [cancelBtn setTitle:@"Cancel" forState:UIControlStateNormal];
        Enter_weight=@"Enter the Weight";
        Enter_excerciseTime=@"Enter exercise time";
        Enter_waterVolume=@"Enter Water Volume";
        Enter_interval=@"Enter Interval";
        NotificationAleartstr=@"Your notifications are scheduled at the desired time. You will start getting notifications from now onwards.";
        NotificationCancelStr=@"Scheduled notifications cancelled. Now you will not receive notification.";
        str1=@"Enter the Weight Above";
        str2=@"";
        str3=@"One must drink atleast";
        str4=@"at a time";
        str5=@"Please Enter below";
        str6=@"";
        
        mlstr=@"mL";
        ozstr=@"OZ";
        hour=@"Hour";
        Notification_interval=@"Interval for Notification:";
        waterVolume=@"Volume of Water:";
    }
    else{
        self.navigationItem.title = [MCLocalization stringForKey:@"water"];
        weightlbl.text=[NSString stringWithFormat:@"%@ :",[MCLocalization stringForKey:@"weight"]];
        RiseTimelbl.text=[MCLocalization stringForKey:@"riseTime"];
        BedTimelbl.text=[MCLocalization stringForKey:@"bedTime"];
        Excerciselbl.text=[MCLocalization stringForKey:@"excerciseTime"];
        Selectonelbl.text=[MCLocalization stringForKey:@"choseOne"];
        TotalTimeDaylbl.text=[MCLocalization stringForKey:@"per_day_water"];
        EveryIntervalWaterlbl.text=[MCLocalization stringForKey:@"ever_interval_water"];
        ReminderIntervallbl.text=[MCLocalization stringForKey:@"notification_interval"];
        Minlbl.text=[MCLocalization stringForKey:@"minat"];
        Detaillbl.text=[MCLocalization stringForKey:@"detail"];
        Resultlbl.text=[MCLocalization stringForKey:@"result"];
        [viewKgbtn setTitle:[MCLocalization stringForKey:@"kg"] forState:UIControlStateNormal];
        [viewpoundbtn setTitle:[MCLocalization stringForKey:@"pound"] forState:UIControlStateNormal];
        [fixWaterbtn setTitle:[MCLocalization stringForKey:@"fixwaterVolume"] forState:UIControlStateNormal];
        [fixTimebtn setTitle:[MCLocalization stringForKey:@"fixTime"] forState:UIControlStateNormal];
        mlstr=[MCLocalization stringForKey:@"ml"];
        ozstr=[MCLocalization stringForKey:@"oz"];
        hour=[MCLocalization stringForKey:@"hour"];
        Notification_interval=[MCLocalization stringForKey:@"interval_of_notification"];
        waterVolume=[MCLocalization stringForKey:@"waterVolume"];
        [okBtn setTitle:[MCLocalization stringForKey:@"set"] forState:UIControlStateNormal];
        [cancelBtn setTitle:[MCLocalization stringForKey:@"Cancel"] forState:UIControlStateNormal];
        Enter_weight=[MCLocalization stringForKey:@"weight_aleart"];
        Enter_excerciseTime=[MCLocalization stringForKey:@"excerciseTime_aleart"];
        Enter_waterVolume=[MCLocalization stringForKey:@"waterVolume_aleart"];
        Enter_interval=[MCLocalization stringForKey:@"interval_of_notification_aleart"];
        NotificationAleartstr=[MCLocalization stringForKey:@"water_notification_alert"];
        NotificationCancelStr=[MCLocalization stringForKey:@"notification_cancle_str"];
        str1=[MCLocalization stringForKey:@"weight"];
        str2=[MCLocalization stringForKey:@"above_weight"];
        str3=[MCLocalization stringForKey:@"at_a_time"];
        str4=[MCLocalization stringForKey:@"drink"];
        str5=[MCLocalization stringForKey:@"time"];
        str6=[MCLocalization stringForKey:@"down_time"];
    }
    
    if (IspoundCon) {
        if (IsfixwaterCon) {
            ml.text=ozstr;
        }
        [kgbutton setTitle:viewpoundbtn.titleLabel.text forState:UIControlStateNormal];
    }
    else{
        if (IsfixwaterCon) {
             ml.text=mlstr;
        }
        [kgbutton setTitle:viewKgbtn.titleLabel.text forState:UIControlStateNormal];
    }
    if (IsfixwaterCon) {
        volumeAndinterval.text=waterVolume;
        [interval setTitle:fixWaterbtn.titleLabel.text forState:UIControlStateNormal];
    }
    else{
        volumeAndinterval.text=Notification_interval;
        ml.text=hour;
        [interval setTitle:fixTimebtn.titleLabel.text forState:UIControlStateNormal];
    }
    water.placeholder=ml.text;
}

-(void)viewFrame{
    CGFloat a=self.view.frame.size.height-64-submitView.frame.size.height-33;
    CGFloat b=(65*a)/100;
    Topview.frame=CGRectMake(8, 72, self.view.frame.size.width-16,b);
    DeviderLine.frame=CGRectMake(10, Topview.frame.origin.y+Topview.frame.size.height+8, Topview.frame.size.width-4, 1);
    BottomView.frame=CGRectMake(8, Topview.frame.origin.y+Topview.frame.size.height+17, Topview.frame.size.width, a-b);
    submitView.frame=CGRectMake(0, BottomView.frame.origin.y+BottomView.frame.size.height+8, submitView.frame.size.width, submitView.frame.size.height);
}

-(void)getRIseandBedTime{
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"hh:mm a"];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:riseTime] ==nil || [[NSUserDefaults standardUserDefaults]objectForKey:bedTime]==nil) {
        unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *comps = [calendar components:unitFlags fromDate:[NSDate date]];
        comps.hour   = 07;
        comps.minute = 00;
        comps.second = 00;
        NSDate *newdate = [calendar dateFromComponents:comps];
        NSString *sdate=[outputFormatter stringFromDate:newdate];
        NSDate *rise=[outputFormatter dateFromString:sdate];
        [[NSUserDefaults standardUserDefaults] setObject:rise forKey:riseTime];
        comps.hour   = 22;
        comps.minute = 00;
        comps.second = 00;
        NSDate *bedtime = [calendar dateFromComponents:comps];
        NSString *Edate=[outputFormatter stringFromDate:bedtime];
        NSDate *bed=[outputFormatter dateFromString:Edate];
        [[NSUserDefaults standardUserDefaults] setObject:bed forKey:bedTime];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:isfixWaterVolume];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:isPounds];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:waterSated];
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:Userweight];
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:excerciseTime];
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:volumeAndTime];
        [[NSUserDefaults standardUserDefaults] setFloat:0 forKey:notificationWater];
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:notificationTimeInterval];
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:notificationTimes];
    }
    
    NSDate *risetime=[[NSUserDefaults standardUserDefaults] objectForKey:riseTime];
    NSDate *bedtime=[[NSUserDefaults standardUserDefaults] objectForKey:bedTime];
    NSString *rise=[outputFormatter stringFromDate:risetime];
    NSString *bed=[outputFormatter stringFromDate:bedtime];
    [RiseTimebtn setTitle:rise forState:UIControlStateNormal];
    [bedTimebtn setTitle:bed forState:UIControlStateNormal];

    if ([[NSUserDefaults standardUserDefaults]boolForKey:isPounds]) {
        IspoundCon=YES;
    }
    else{
        IspoundCon=NO;
    }
    if ([[NSUserDefaults standardUserDefaults]boolForKey:isfixWaterVolume]) {
        IsfixwaterCon=YES;
    }
    else{
        IsfixwaterCon=NO;
    }
}

-(void)textfeildLine:(UITextField*)textfeild{
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, textfeild.frame.size.height - 1, textfeild.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor blackColor].CGColor;
    [textfeild.layer addSublayer:bottomBorder];
}

/////////tap gesture/////
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return  (touch.view == windowView);
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    windowView.hidden=YES;
    [self.view endEditing:YES];
}
////////
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    fixView.hidden=YES;
    WeightView.hidden=YES;
   
}


-(void)checkValidation:(BOOL)condition{
    
    int minWeight,minWater,totalwater = 0;
    NSString *waterstr;
    if (IspoundCon) {
        int a=0,b=0;
        a= ([weight.text intValue]*2)/3;
        b= ([exercise.text intValue]*12)/30;
        totalwater=a+b;
        minWeight=33;
        
        if (IsfixwaterCon) {
            minWater=3;
            waterstr=Enter_waterVolume;
        }
        else{
            waterstr=Enter_interval;
            minWater=1;
            totalwater=15;
        }
        
    }
    else{
        int a=0,b=0;
        a= ([weight.text intValue]*1000)/24;
        b= ([exercise.text intValue]*350)/30;
        totalwater=a+b;
        minWeight=15;
        
        if (IsfixwaterCon) {
            minWater=100;
            waterstr=Enter_waterVolume;
        }
        else{
            waterstr=Enter_interval;
            minWater=1;
            totalwater=15;
        }
        
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    if (weight.text.length==0 || [weight.text intValue] < minWeight) {
        if ([weight.text isEqualToString:@""]) {
            alert.message=Enter_weight;
            [alert show];
        }
        else{
            alert.message=[NSString stringWithFormat:@"%@ %d %@ %@",str1,minWeight,kgbutton.titleLabel.text,str2];
            [alert show];
        }
    }
    else if (exercise.text.length==0) {
        alert.message=Enter_excerciseTime;
        [alert show];
    }
    
    else if (water.text.length==0 || [water.text intValue] < minWater || [water.text intValue] > totalwater) {
        if (water.text.length==0 || [water.text intValue]==0) {
            alert.message=waterstr;
        }
        else if ([water.text intValue] < minWater){
            alert.message=[NSString stringWithFormat:@"%@ %d %@ %@",str3,minWater,ml.text,str4];
        }
        else{
            alert.message=[NSString stringWithFormat:@"%@ %d %@ %@",str5,totalwater,ml.text,str6];
        }
        
        [alert show];
    }
    
    else{
        [self calculationMethod:condition];
    }
}

-(void)calculationMethod:(BOOL)condition{
    if (IsReadyToPlay) {
        stopbtn.hidden=NO;
        [self AleartMethod:NotificationAleartstr];
        [self deleteWaterReport];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:waterSated];
        [[NSUserDefaults standardUserDefaults]setInteger:[weight.text intValue] forKey:Userweight];
        [[NSUserDefaults standardUserDefaults]setInteger:[exercise.text intValue] forKey:excerciseTime];
        [[NSUserDefaults standardUserDefaults]setInteger:[water.text intValue] forKey:volumeAndTime];
        [[NSUserDefaults standardUserDefaults]setBool:IspoundCon forKey:isPounds];
        [[NSUserDefaults standardUserDefaults]setBool:IsfixwaterCon forKey:isfixWaterVolume];
        [[NSUserDefaults standardUserDefaults]setFloat:waterQuentity forKey:notificationWater];
        
    }
    IsReadyToPlay=condition;
    if (IsReadyToPlay) {
        [submitButton setImage:[UIImage imageNamed:@"start_icon"] forState:UIControlStateNormal];
    }
    BottomView.hidden=NO;
    NSDate *risetime=[[NSUserDefaults standardUserDefaults] objectForKey:riseTime];
    NSDate *bedtime=[[NSUserDefaults standardUserDefaults] objectForKey:bedTime];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"hh:mm a"];
    NSString *rise=[outputFormatter stringFromDate:risetime];
    NSString *bed=[outputFormatter stringFromDate:bedtime];
    NSDate *Sdate=[outputFormatter dateFromString:rise];
    NSDate *Edate=[outputFormatter dateFromString:bed];
    NSTimeInterval distanceBetweenDates = [Edate timeIntervalSinceDate:Sdate];
    double secondInminute = 60;
    NSInteger minutesBetweenDates = distanceBetweenDates / secondInminute;
    if ([Sdate compare:Edate]==NSOrderedDescending) {
        minutesBetweenDates=minutesBetweenDates+1440;
    }

    ////calculation for pounds
    if (IspoundCon) {
        int a=0,b=0,c=0;
        a= ([weight.text intValue]*2)/3;
        b= ([exercise.text intValue]*12)/30;
        c=a+b;
        waterlabel.text=[NSString stringWithFormat:@"%d OZ",c];
        if (IsfixwaterCon) {
            minutesBetweenDates=minutesBetweenDates+60;
            waterQuentity=[water.text floatValue];
            volumelabel.text=[NSString stringWithFormat:@"%d OZ",[water.text intValue]];
            NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
            [outputFormatter setDateFormat:@"hh:mm a"];
            float d=(float)c/[water.text floatValue];
            float minutes=minutesBetweenDates/d;
            NSInteger hour = minutes / 60;
            NSInteger min = (int)minutes % 60;
            reminderlabel.text=[NSString stringWithFormat:@"%02ld:%02ld",(long)hour,(long)min];
            [[NSUserDefaults standardUserDefaults]setInteger:(NSInteger)minutes forKey:notificationTimeInterval];
            NSInteger times=(minutesBetweenDates/minutes)+1;
            [[NSUserDefaults standardUserDefaults]setInteger:times forKey:notificationTimes];
            
        }
        else{
            reminderlabel.text=[NSString stringWithFormat:@"%02d:00",[water.text intValue]];
            NSInteger value=minutesBetweenDates/([water.text integerValue]*60);
            NSInteger times=value+1;
            float a=(float)c/(float)times;
            volumelabel.text=[NSString stringWithFormat:@"%.02f OZ",a];
            waterQuentity=a;
            [[NSUserDefaults standardUserDefaults]setInteger:times forKey:notificationTimes];
            NSInteger timeinterval=[water.text integerValue]*60;
            [[NSUserDefaults standardUserDefaults]setInteger:timeinterval forKey:notificationTimeInterval];
            
        }
    }
    //////// calculation for kg
    else{
        int a=0,b=0,c=0;
        a= ([weight.text intValue]*1000)/24;
        b= ([exercise.text intValue]*350)/30;
        c=a+b;
        waterlabel.text=[NSString stringWithFormat:@"%d mL",c];
      
        if (IsfixwaterCon) {
            minutesBetweenDates=minutesBetweenDates+60;
            waterQuentity=[water.text floatValue];
            volumelabel.text=[NSString stringWithFormat:@"%d mL",[water.text intValue]];
            float d=(float)c/[water.text floatValue];
            float minutes=minutesBetweenDates/d;
            NSInteger hour = minutes / 60;
            NSInteger min = (int)minutes % 60;
            reminderlabel.text=[NSString stringWithFormat:@"%02ld:%02ld",(long)hour,(long)min];
            
            [[NSUserDefaults standardUserDefaults]setInteger:(NSInteger)minutes forKey:notificationTimeInterval];
            NSInteger times=((minutesBetweenDates-60)/minutes)+1;
            [[NSUserDefaults standardUserDefaults]setInteger:times forKey:notificationTimes];
        }
        else{
            reminderlabel.text=[NSString stringWithFormat:@"%02d:00",[water.text intValue]];

            NSInteger value=minutesBetweenDates/([water.text integerValue]*60);
            NSInteger times=value+1;
            float a=(float)c/(float)times;
            waterQuentity=a;
            volumelabel.text=[NSString stringWithFormat:@"%.02f mL",a];
            [[NSUserDefaults standardUserDefaults]setInteger:times forKey:notificationTimes];
            NSInteger timeinterval=[water.text integerValue]*60;
            [[NSUserDefaults standardUserDefaults]setInteger:timeinterval forKey:notificationTimeInterval];
            
        }
    }
}




- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
   
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if ([textField isEqual:water]) {
        return newLength <= 4;
    }
    return newLength <= 3;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    fixView.hidden=YES;
    WeightView.hidden=YES;
    [UIView animateWithDuration:0.25 animations:^
     {
         
         CGRect newFrame = [self.view frame];
         if ([textField isEqual:weight]) {
             newFrame.origin.y -= 30;
             [self.view setFrame:newFrame];
         }
         else if ([textField isEqual:exercise])
         {
             newFrame.origin.y -= 50;
             [self.view setFrame:newFrame];
 
         }
         else if ([textField isEqual:water])
         {
             newFrame.origin.y -= 70;
             [self.view setFrame:newFrame];

         }
         
         
         
     }completion:^(BOOL finished)
     {
         
     }];

    return YES;
}

- (BOOL) textFieldShouldEndEditing:(UITextField*)textField
{
    [UIView animateWithDuration:0.25 animations:^
     {
         CGRect newFrame = [self.view frame];
         if ([textField isEqual:weight]) {
             newFrame.origin.y += 30;
             [self.view setFrame:newFrame];
         }
         else if ([textField isEqual:exercise])
         {
             newFrame.origin.y += 50;
             [self.view setFrame:newFrame];
             
         }
         else if ([textField isEqual:water])
         {
             newFrame.origin.y += 70;
             [self.view setFrame:newFrame];
             
         }
         
     }completion:^(BOOL finished)
     {
         
     }];

    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)submitbutton:(id)sender{
    fixView.hidden=YES;
    WeightView.hidden=YES;
    [self checkValidation:YES];
    
}

-(IBAction)stopbuttonMethod:(id)sender{
    [self AleartMethod:NotificationCancelStr];
    IsReadyToPlay=NO;
    stopbtn.hidden=YES;
    BottomView.hidden=YES;
    [submitButton setImage:[UIImage imageNamed:@"calculate"] forState:UIControlStateNormal];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:waterSated];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

-(void)deleteWaterReport{
    NSArray *array=[kAppDele fetchwaterReportFromDatabase];
    
    if (array.count>0) {
        NSManagedObjectContext *managedObjectContext=[kAppDele managedObjectContext];
        NSDateFormatter *dateFormate=[[NSDateFormatter alloc]init];
        dateFormate.dateFormat=@"dd-MM-yyyy";
        NSDate *today=[NSDate date];
        NSString *todaystr=[dateFormate stringFromDate:today];
        Water *waterReport=[array objectAtIndex:0];
        NSDate *date=waterReport.date;
        NSString *datestr=[dateFormate stringFromDate:date];
        BOOL deleteCondition = NO;
        if ([todaystr isEqualToString:datestr]) {
            BOOL pound=[[NSUserDefaults standardUserDefaults]boolForKey:isPounds];
            if (pound!=IspoundCon) {
                float quentity=[waterReport.waterQuentity floatValue];
                if (pound==YES) {
                    quentity=quentity*30;
                }
                else{
                    quentity=quentity/30;
                }
                waterReport.waterQuentity=[NSString stringWithFormat:@"%f",quentity];
                NSError *error;
                if (![managedObjectContext save:&error]) {
                    NSLog(@"%@",error);
                }
            }
            
            deleteCondition=YES;
        }
        
        //error handling goes here
        for (NSManagedObject *object in array) {
            if (deleteCondition) {
                deleteCondition=NO;
            }
            else{
                [managedObjectContext deleteObject:object];
            }
        }
        NSError *saveError = nil;
        [managedObjectContext save:&saveError];
    }
}

-(IBAction)weightunit:(id)sender{
    fixView.hidden=YES;
    WeightView.hidden=NO;
    WeightView.layer.shadowColor = [UIColor grayColor].CGColor;
    WeightView.layer.shadowOffset = CGSizeMake(-5, 5);
    WeightView.layer.shadowOpacity = 1;
    WeightView.layer.shadowRadius = 2.0;
}
-(IBAction)kgunit:(id)sender{
    [kgbutton setTitle:viewKgbtn.titleLabel.text forState:UIControlStateNormal];
    WeightView.hidden=YES;
    IspoundCon=NO;
    [self changeTesxt];
}
-(IBAction)poundtunit:(id)sender{
    [kgbutton setTitle:viewpoundbtn.titleLabel.text forState:UIControlStateNormal];
     WeightView.hidden=YES;
    IspoundCon=YES;
    [self changeTesxt];
}



-(IBAction)interval:(id)sender{
    WeightView.hidden=YES;
    fixView.hidden=NO;
    fixView.layer.shadowColor = [UIColor grayColor].CGColor;
    fixView.layer.shadowOffset = CGSizeMake(-5, 5);
    fixView.layer.shadowOpacity = 1;
    fixView.layer.shadowRadius = 2.0;
    
}
-(IBAction)intervaloftime:(id)sender{
    [interval setTitle:fixTimebtn.titleLabel.text forState:UIControlStateNormal];
    fixView.hidden=YES;
    IsfixwaterCon=NO;
    [self changeTesxt];
}
-(IBAction)volumeofwater:(id)sender{
   [interval setTitle:fixWaterbtn.titleLabel.text forState:UIControlStateNormal];
    fixView.hidden=YES;
    IsfixwaterCon=YES;
    [self changeTesxt];
}


-(IBAction)datepicker1:(id)sender{
    [self datePickerMethod:100];
}
-(IBAction)datepicker2:(id)sender{
    [self datePickerMethod:101];
    
}

-(void)datePickerMethod:(int)tag{
    NSDate *risetime=[[NSUserDefaults standardUserDefaults] objectForKey:riseTime];
    NSDate *bedtime=[[NSUserDefaults standardUserDefaults] objectForKey:bedTime];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"hh:mm a"];
    NSString *rise=[outputFormatter stringFromDate:risetime];
    NSString *bed=[outputFormatter stringFromDate:bedtime];
    NSDate *pickerdate=[[NSDate alloc]init];
    if (tag==100) {
        pickerdate=[outputFormatter dateFromString:rise];
    }
    else{
        pickerdate=[outputFormatter dateFromString:bed];
    }
    datepicker.date=pickerdate;
    fixView.hidden=YES;
    WeightView.hidden=YES;
    windowView=[[UIView alloc]initWithFrame:CGRectMake(0, 0,self.view.frame.size.width  , self.view.frame.size.height)];
    windowView.backgroundColor=[[UIColor grayColor] colorWithAlphaComponent:0.4];
    [window addSubview:windowView];
    datePkrView.alpha=0;
    datePkrView.layer.borderWidth=1.0f;
    datePkrView.layer.borderColor=RGB(230, 123, 53).CGColor;
    [windowView addSubview:datePkrView];
    [UIView animateWithDuration:0.5 animations:^{
        datePkrView.alpha = 1.0; //to display loginview
    } completion:^(BOOL finished) {
    }];
    okBtn.tag=tag;
    datePkrView.hidden=NO;
    [datepicker addTarget:self action:@selector(dateChange:) forControlEvents:UIControlEventValueChanged];
    
    dateLbl.text=[self timeStr];

}
-(NSString *)timeStr{
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"hh:mm a"];
    NSString *pickerTime=[outputFormatter stringFromDate:datepicker.date];
    return pickerTime;
}
-(void)dateChange:(id*)sender{
    dateLbl.text=[self timeStr];
}

-(IBAction)Okbutton:(UIButton*)sender{
    if (sender.tag==100) {
        [RiseTimebtn setTitle:[self timeStr] forState:UIControlStateNormal];
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:@"hh:mm a"];
        NSDate *date=[outputFormatter dateFromString:[self timeStr]];
        [[NSUserDefaults standardUserDefaults] setObject:date forKey:riseTime];
    }
    else{
        [bedTimebtn setTitle:[self timeStr] forState:UIControlStateNormal];
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:@"hh:mm a"];
        NSDate *date=[outputFormatter dateFromString:[self timeStr]];
        [[NSUserDefaults standardUserDefaults] setObject:date forKey:bedTime];
    }
    windowView.hidden=YES;
}

-(IBAction)CancelButton:(id)sender{
    windowView.hidden=YES;
}


-(IBAction)backbutton:(id)sender{
        [[self navigationController] popViewControllerAnimated:YES];
}


-(void)AleartMethod:(NSString*)alertstr{
    CGRect rect=CGRectMake(0,self.view.frame.size.height-80 , self.view.frame.size.width, 80);
    aleartView=[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height+10, self.view.frame.size.width, 80)];
    UILabel *aleartLabel=[[UILabel alloc]initWithFrame:CGRectMake(20,10 , aleartView.frame.size.width-40, aleartView.frame.size.height-20)];
    aleartView.backgroundColor=RGB(143, 0, 0);
    aleartLabel.backgroundColor=RGB(143, 0, 0);
    aleartLabel.textColor=[UIColor whiteColor];
    aleartLabel.font=[UIFont systemFontOfSize:15];
    aleartLabel.numberOfLines=5;
    aleartLabel.text=alertstr;
    [aleartView addSubview:aleartLabel];
    [self.view addSubview:aleartView];
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionCurlUp
                     animations:^(void) {
                         aleartView.frame=rect;
                     }
                     completion:NULL];
    
    [self performSelector:@selector(HideAleart) withObject:nil afterDelay:4.0];
}

-(void)HideAleart{
    CGRect rect=CGRectMake(0, self.view.frame.size.height+10, self.view.frame.size.width, 80);
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionCurlDown
                     animations:^(void) {
                         aleartView.frame=rect;
                     }
                     completion:NULL];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

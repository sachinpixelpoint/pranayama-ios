//
//  Progress+CoreDataProperties.m
//  Pranayama
//
//  Created by Manish Kumar on 02/07/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Progress+CoreDataProperties.h"

@implementation Progress (CoreDataProperties)

@dynamic anulomVilom;
@dynamic days;
@dynamic halasana;
@dynamic kapalbhati;
@dynamic sarvangasana;
@dynamic ujjayi;
@dynamic duration;
@dynamic progressType;
@dynamic vipritkarani;
@dynamic pashimothanasana;
@dynamic dhanurasana;
@dynamic balasana;
@dynamic hastapadasana;
@dynamic marjariasana;
@dynamic bhramari;
@dynamic suryabhedana;
@dynamic medetativebreathing;
@dynamic bhastrika;
@dynamic udgeeth;
@dynamic bahya;
@dynamic setubandhasana;
@dynamic uttanasana;
@dynamic virabhadrasana;

@end

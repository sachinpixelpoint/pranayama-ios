//
//  SheetaliReportVC.m
//  Pranayama
//
//  Created by Manish Kumar on 11/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "SheetaliReportVC.h"

@interface SheetaliReportVC ()

@end

@implementation SheetaliReportVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    // tracker
    self.screenName=@"Sheetali_Report";
    // Do any additional setup after loading the view.
    [self ChangeText];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    int fontSize;
    int entityLblFont;
    int InExFont;
    if (DEVICE==IPAD) {
        fontSize=24;
        entityLblFont=21;
        InExFont=14;
        entityFontSize=21;
    }
    else{
        fontSize=18;
        entityLblFont=12;
        InExFont=8;
        entityFontSize=13;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    tblReport.allowsSelection = NO;
    self.managedObjectContext=[kAppDele managedObjectContext];
    tblReport.allowsMultipleSelection = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    sn.font=Ralewayfont(entityLblFont);
    levellbl.font=Ralewayfont(entityLblFont);
    roundlbl.font=Ralewayfont(entityLblFont);
    datelbl.font=Ralewayfont(entityLblFont);
    timelbl.font=Ralewayfont(entityLblFont);
    InHoldExlbl.font=Ralewayfont(InExFont);
    reportArray=[[kAppDele fetchAllReportFromDatabseAccordingToType:@"Sheetali"] mutableCopy];
    selectunselectarray=[[NSMutableArray alloc]init];
    for (int i=0; i<[reportArray count]; i++) {
        [selectunselectarray addObject:@"NO"];
    }
    [self Noresult];
    [self setlblheight];
    [self gesture];
}


- (void)HomeMethod:(NSNotification *)notification {
    HomeTabelVC *homeObject = [[HomeTabelVC alloc]init];
    [homeObject appDidBecomeActive:notification];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self disableSlidePanGestureForRightMenu];
}


-(void)setlblheight{
    int Device =Device_Type;
    switch (Device) {
        case iPhone4:
            topview.frame=CGRectMake(8, 75, 305, 40);
            tblReport.frame=CGRectMake(8, 126, 305, 358);
            break;
        case iPhone5:
            topview.frame=CGRectMake(8, 74, 305, 40);
            tblReport.frame=CGRectMake(8, 124, 305, 449);
            break;
        case iPhone6:
            topview.frame=CGRectMake(8, 74, 359, 40);
            tblReport.frame=CGRectMake(8, 124, 359, 548);
            break;
        case iPhone6P:
            topview.frame=CGRectMake(8, 74, 398, 40);
            tblReport.frame=CGRectMake(8, 124, 398, 617);
            break;
        case iPadPro:
            tblReport.frame=CGRectMake(8, 176, 1008, 1190);
        default:
            break;
    }
}

-(void)gesture{
    lpgr = [[UILongPressGestureRecognizer alloc]
            initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 0.5; //seconds
    lpgr.delegate = self;
    [tblReport addGestureRecognizer:lpgr];
}

-(void)ChangeText{
    
    self.navigationItem.title = [MCLocalization stringForKey:@"report"];
    sn.text=[MCLocalization stringForKey:@"No"];
    levellbl.text=[MCLocalization stringForKey:@"level"];
    roundlbl.text=[MCLocalization stringForKey:@"Rounds"];
    datelbl.text=[MCLocalization stringForKey:@"date"];
    timelbl.text=[MCLocalization stringForKey:@"time"];
    InHoldExlbl.text=[MCLocalization stringForKey:@"InHoldEx"];
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    
    p = [gestureRecognizer locationInView:tblReport];
    
    indexpath1 = [tblReport indexPathForRowAtPoint:p];
    UITableViewCell* theCell = [tblReport cellForRowAtIndexPath:indexpath1];
    if (indexpath1 == nil) {
        NSLog(@"long press on table view but not on a row");
    } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        NSLog(@"long press on table view at row %ld", (long)indexpath1.row);
        theCell.backgroundColor=RGB(19, 155, 231);
        cl++;
        [selectunselectarray replaceObjectAtIndex:indexpath1.row withObject:@"YES"];
        tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cellTapped:)];
        tap.numberOfTapsRequired = 1;
        tap.numberOfTouchesRequired = 1;
        tap.delegate = self;
        [tblReport addGestureRecognizer:tap];
    }
    else {
        NSLog(@"gestureRecognizer.state = %ld", (long)gestureRecognizer.state);
    }
}
-(void)cellTapped:(UITapGestureRecognizer*)tap1
{
    CGPoint q = [tap1 locationInView:tblReport];
    indexpath1 = [tblReport indexPathForRowAtPoint:q];
    if (indexpath1!=nil) {
    UITableViewCell* theCell = [tblReport cellForRowAtIndexPath:indexpath1];
    if (theCell.backgroundColor==[UIColor clearColor]) {
        theCell.backgroundColor=RGB(19, 155, 231);
        cl++;
        [selectunselectarray replaceObjectAtIndex:indexpath1.row withObject:@"YES"];
    }
    else{
        theCell.backgroundColor=[UIColor clearColor];
        cl--;
        [selectunselectarray replaceObjectAtIndex:indexpath1.row withObject:@"NO"];
    }
    if (cl==0) {
        tap1.enabled=NO;
        [self gesture];
    }
}
}

-(IBAction)deletebutton:(id)sendar{
    for(int i=0; i<selectunselectarray.count; i++){
        NSIndexPath *a = [NSIndexPath indexPathForRow:i inSection:0];
        
        if ([[selectunselectarray objectAtIndex:i] isEqualToString:@"YES"]) {
            
            
            NSError *error;
            Report *repoEntity=[reportArray objectAtIndex:a.row];
            [self.managedObjectContext deleteObject:repoEntity];
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"unresolved Error: %@", error);
            }
            [reportArray removeObjectAtIndex:a.row];
            [selectunselectarray removeObjectAtIndex:a.row];
            [tblReport deleteRowsAtIndexPaths:@[a] withRowAnimation:UITableViewRowAnimationAutomatic];
            i=i-1;
        }
    }
    tap.enabled=NO;
    cl=0;
    [self gesture];
    [tblReport reloadData];
    [self Noresult];
}

-(void)Noresult{
    if ([reportArray count]==0)
    {
        UILabel *noDataLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tblReport.bounds.size.width, tblReport.bounds.size.height)];
        noDataLabel.text = @"No Record Found";
        noDataLabel.textColor = [UIColor lightGrayColor];
        noDataLabel.textAlignment = NSTextAlignmentCenter;
        tblReport.backgroundView = noDataLabel;
    }
    else{
        tblReport.backgroundView = nil;
    }
}

-(IBAction)backbutton:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [reportArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *listingCellIdentifier = @"ReportCustomCell";
    cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:listingCellIdentifier];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.backgroundColor=[UIColor clearColor];
    UILabel *lblNo=(UILabel*)[cell viewWithTag:1];
    UILabel *lblLevel=(UILabel*)[cell viewWithTag:2];
    UILabel *lblRound=(UILabel*)[cell viewWithTag:3];
    UILabel *lblDate=(UILabel*)[cell viewWithTag:4];
    UILabel *lbltime=(UILabel *)[cell viewWithTag:5];
    
    lblNo.font=Ralewayfont(entityFontSize);
    lblLevel.font=Ralewayfont(entityFontSize);
    lblRound.font=Ralewayfont(entityFontSize);
    lblDate.font=Ralewayfont(entityFontSize);
    lbltime.font=Ralewayfont(entityFontSize);
    Report *reportEntity=[reportArray objectAtIndex:indexPath.row];
    reportEntity.reportSerialNo=[NSString stringWithFormat:@"%lu",(unsigned long)indexPath.row+1];
    lblNo.text=reportEntity.reportSerialNo;
    lblLevel.text=reportEntity.reportLevel;
    lblRound.text=reportEntity.reportRounds;
    NSDate *date=reportEntity.reportDate;
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yy"];
    lblDate.text=[dateFormatter stringFromDate:date];
    [dateFormatter setDateFormat:@"hh:mma"];
    lbltime.text=[dateFormatter stringFromDate:date];
    if ([[selectunselectarray objectAtIndex:indexPath.row] isEqualToString:@"YES"]) {
        cell.backgroundColor=RGB(19, 155, 231);
    }
    
    return cell;
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    if (cl>0) {
        return NO;
    }
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSError *error;
        Report *repoEntity=[reportArray objectAtIndex:indexPath.row];
        [self.managedObjectContext deleteObject:repoEntity];
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"unresolved Error: %@", error);
        }
        [reportArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        [tblReport reloadData];
        //add code here for when you hit delete
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

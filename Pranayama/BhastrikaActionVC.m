//
//  BhastrikaActionVC.m
//  Pranayama
//
//  Created by Manish Kumar on 11/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "BhastrikaActionVC.h"

@interface BhastrikaActionVC ()

@end

@implementation BhastrikaActionVC
@synthesize totalTime,lblexhale,lblinhale,rounds;
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    //google analytic
    NSString *name=@"Bhastrika ";
    NSString *ratio=[NSString stringWithFormat:@"%d:%d:%d",lblinhale,lblexhale,rounds];
    
    NSString *AppendStr=[name stringByAppendingString:ratio];
    self.screenName=AppendStr;
    //
    
    // Do any additional setup after loading the view.
    self.navigationItem.hidesBackButton=YES;
    int fontSize;
    int totaltimeFont;
    int lblmodeFont;
    if (DEVICE==IPAD) {
        fontSize=24;
        totaltimeFont=24;
        lblmodeFont=36;
    }
    else{
        fontSize=18;
        totaltimeFont=12;
        lblmodeFont=22;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback
                                     withOptions:AVAudioSessionCategoryOptionMixWithOthers
                                           error:nil];
    self.managedObjectContext=[kAppDele managedObjectContext];
    prepare = [[NSUserDefaults standardUserDefaults] floatForKey:preparationtime];
    NSInteger value = [[NSUserDefaults standardUserDefaults]integerForKey:dincharyaDelayTime];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaMainCon] && value>0) {
        prepare = (int) value ;
    }
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:isPause1];
    PlayStop=NO;
    totaltimelbl.font=Ralewayfont(totaltimeFont);
    actionlbl.font=Ralewayfont(lblmodeFont);
    lblmode.font=Ralewayfont(lblmodeFont);
    [self ChangeText];
    [self setValueoflbl];
    roundforreport=rounds;
    [self startButtonPreshed];
    
    //    [self setlblheight];
    if (DEVICE == Iphone) {
        topview.frame = CGRectMake(8, 74, self.view.frame.size.width-16, 32);
    }
}


- (void)HomeMethod:(NSNotification *)notification {
    HomeTabelVC *homeObject = [[HomeTabelVC alloc]init];
    [homeObject appDidBecomeActive:notification];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self disableSlidePanGestureForRightMenu];
}


//-(void)setlblheight{
//    if (Device_Type==iPhone4)
//        topview.frame=CGRectMake(8, 75, 305, 32);
//    else if(Device_Type==iPhone5)
//        topview.frame=CGRectMake(8, 74, 305, 32);
//    else if(Device_Type==iPhone6)
//        topview.frame=CGRectMake(8, 74, 359, 32);
//    else if(Device_Type==iPhone6P)
//        topview.frame=CGRectMake(8, 74, 397, 32);
//}

-(void)ChangeText{
    
    self.navigationItem.title = [MCLocalization stringForKey:@"bhastrika"];
    totaltimelbl.text=[MCLocalization stringForKey:@"Total_Time"];
    actionlbl.text=[MCLocalization stringForKey:@"Action"];
    
}

-(void)setValueoflbl
{
    seconds = totalTime % 60;
    minutes = (totalTime / 60) % 60;
    hours = totalTime / 3600;
    lblseconds.text=[NSString stringWithFormat:@"%d",seconds];
    lblminutes.text=[NSString stringWithFormat:@"%d",minutes];
    lblHours.text=[NSString stringWithFormat:@"%d",hours];
    inhale=lblinhale;
    exhale=lblexhale;
}
-(IBAction)pausebutton:(id)sender{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:isPause1];
    PlayStop=YES;
    [btnStop setImage:[UIImage imageNamed:@"start_icon"] forState:UIControlStateNormal];
    [startTimer invalidate];
}

-(IBAction)stopbutton:(id)sender{
    if (PlayStop==YES) {
        startTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                      target: self
                                                    selector:@selector(onTick:)
                                                    userInfo: nil repeats:YES];
        [btnStop setImage:[UIImage imageNamed:@"stop_icon"] forState:UIControlStateNormal];
        PlayStop=NO;
    }
    else{
        if (bhastrikaCount>0) {
            [self insertReportIntoDatabase];
            [self insertintoProgressReport:YES];
            [self saveInDincharya:NO];
        }
        [startTimer invalidate];
        [[self navigationController] popViewControllerAnimated:NO];
    }
}

-(void)startButtonPreshed
{
    startTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                  target: self
                                                selector:@selector(onTick:)
                                                userInfo: nil repeats:YES];
}
-(void)onTick:(NSTimer *)timer {
    isPause=[[NSUserDefaults standardUserDefaults] boolForKey:isPause1];
    if (isPause) {
        [self pausebutton:isPause1];
    }
    else{
        if (prepare>0) {
            lblmode.text=[MCLocalization stringForKey:@"preparation_time"];
            lblTimer.text=[NSString stringWithFormat:@"%d",prepare];
            [[CommonSounds sharedInstance] playEverySound];
            prepare--;
        }
        else{
            if (isPause) {
                return;
            }
            if (totalTime>0) {
                seconds = totalTime % 60;
                minutes = (totalTime / 60) % 60;
                hours = totalTime / 3600;
                lblseconds.text=[NSString stringWithFormat:@"%d",seconds];
                lblminutes.text=[NSString stringWithFormat:@"%d",minutes];
                lblHours.text=[NSString stringWithFormat:@"%d",hours];
                if (rounds>0) {
                    if (inhale>0) {
                        imageview.image=[UIImage imageNamed:@"action_inhale_both"];
                        lblmode.text=[MCLocalization stringForKey:@"inhale_force"];
                        if (inhale==lblinhale) {
                            [[CommonSounds sharedInstance] playInhaleSound];
                        }
                        else{
                            [[CommonSounds sharedInstance] playEverySound];
                        }
                        lblTimer.text=[NSString stringWithFormat:@"%d",inhale];
                        inhale=inhale-1;
                    }
                    else if (exhale>0){
                        if (exhale==lblexhale) {
                            [[CommonSounds sharedInstance] playExhaleSound];
                        }
                        else {
                            [[CommonSounds sharedInstance] playEverySound];
                        }
                        imageview.image=[UIImage imageNamed:@"action_exhale_both"];
                        lblmode.text=[MCLocalization stringForKey:@"exhale_force"];
                        lblTimer.text=[NSString stringWithFormat:@"%d",exhale];
                        exhale=exhale-1;
                        if (exhale==0) {
                            rounds=rounds-1;
                            [self setValueoflbl];
                        }
                    }
                    
                }
            }
            else{
                [self insertReportIntoDatabase];
                [self insertintoProgressReport:NO];
                [self saveInDincharya:YES];
                [timer invalidate];
                timer = nil;
                if ([self.delegate respondsToSelector:@selector(playsound)]) {
                    [self.delegate playsound];
                }
                [[self navigationController] popViewControllerAnimated:NO];
            }
            totalTime=totalTime-1;
            bhastrikaCount++;
        }
    }
}

-(void)insertReportIntoDatabase{
    NSArray *array=[kAppDele fetchReportFromDatabseAccordingToType:@"Bhastrika"];
    if (array.count>0) {
        Record *recordEntity=[array objectAtIndex:0];
        NSDate *str=recordEntity.anulomDate;
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        NSString *datestr=[dateFormatter stringFromDate:str];
        NSString *date=[dateFormatter stringFromDate:[NSDate date]];
        if ([datestr isEqualToString:date]) {
            NSString *duration=[NSString stringWithFormat:@"%d",[recordEntity.anulomduration intValue]+bhastrikaCount];
            recordEntity.anulomduration=duration;
            NSError *error;
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"%@",error);
            }
        }
        else{
            Record *reportEntity = [NSEntityDescription insertNewObjectForEntityForName:@"Record" inManagedObjectContext:self.managedObjectContext];
            reportEntity.anulomLevel=[NSString stringWithFormat:@"%d:%d",lblinhale,lblexhale];
            reportEntity.anulomRounds=[NSString stringWithFormat:@"%d",roundforreport];
            reportEntity.anulomDate=[NSDate date];
            reportEntity.anulomduration=[NSString stringWithFormat:@"%d",bhastrikaCount];
            reportEntity.anulomtype=@"Bhastrika";
            NSError *error;
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"%@",error);
            }
        }
    }
    else{
        Record *reportEntity = [NSEntityDescription insertNewObjectForEntityForName:@"Record" inManagedObjectContext:self.managedObjectContext];
        reportEntity.anulomLevel=[NSString stringWithFormat:@"%d:%d",lblinhale,lblexhale];
        reportEntity.anulomRounds=[NSString stringWithFormat:@"%d",roundforreport];
        reportEntity.anulomDate=[NSDate date];
        reportEntity.anulomduration=[NSString stringWithFormat:@"%d",bhastrikaCount];
        reportEntity.anulomtype=@"Bhastrika";
        NSError *error;
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"%@",error);
        }
        
    }
    Report *reportEntity = [NSEntityDescription insertNewObjectForEntityForName:@"Report" inManagedObjectContext:self.managedObjectContext];
    reportEntity.reportLevel=[NSString stringWithFormat:@"%d:%d",lblinhale,lblexhale];
    reportEntity.reportRounds=[NSString stringWithFormat:@"%d",roundforreport];
    reportEntity.reportDate=[NSDate date];
    reportEntity.reportduration=[NSString stringWithFormat:@"%d",bhastrikaCount];
    reportEntity.reporttype=@"Bhastrika";
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"%@",error);
    }
}

///////////insert Progress report///////

-(void)insertintoProgressReport:(BOOL)value{
    ////progress Report ///////////
    NSString *habbitconditon=[[NSUserDefaults standardUserDefaults]objectForKey:HabbitCondition];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:HabbitMainCondition]) {
        NSArray *Progressarray=[kAppDele fetchReportFromProgressDatabseAccordingToType:habbitconditon];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"Progress" inManagedObjectContext:self.managedObjectContext]];
        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"progressType==%@ AND days==%d",habbitconditon,Progressarray.count];
        [request setPredicate:predicate];
        
        NSError *error1=nil;
        NSArray *results=[self.managedObjectContext executeFetchRequest:request error:&error1];
        Progress *entity=[results objectAtIndex:0];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:viewwillCondition];

        if (value==YES) {
            NSNumber *duration=[NSNumber numberWithInt:[entity.duration intValue]+bhastrikaCount];
            entity.duration=duration;
        }
        else{
            entity.bhastrika=[NSNumber numberWithBool:YES];
            NSNumber *duration=[NSNumber numberWithInt:[entity.duration intValue]+bhastrikaCount];
            entity.duration=duration;
        }
        [self.managedObjectContext save:nil];
    }
}

-(void)saveInDincharya:(BOOL)Value{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaMainCon]) {
        
        NSString *str = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaName];
        NSArray *ar = [kAppDele FetchDincharyaFromDatabaseAcordingToName:str];
        Dincharya *dincharya = [ar objectAtIndex:0];
        NSArray *yogaAr = [NSKeyedUnarchiver unarchiveObjectWithData:dincharya.yogasana_arr];
        NSArray *pranaMainList = [NSKeyedUnarchiver unarchiveObjectWithData:dincharya.pranayama_arr];
        NSArray *pranaList = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaPranaAr];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:viewwillCondition];
        
        if (pranaMainList.count == pranaList.count  && yogaAr.count == 0) {
            [[CommonSounds sharedInstance]Timecompare:Value managedObject:self.managedObjectContext pranayama:@"Bhastrika"];
        }
        else{
            [[CommonSounds sharedInstance]UpdateDincharyaIntoDatabase:Value mangedObject:self.managedObjectContext Prana:@"Bhastrika"];
        }
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

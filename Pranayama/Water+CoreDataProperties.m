//
//  Water+CoreDataProperties.m
//  Pranayama
//
//  Created by Manish Kumar on 31/08/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Water+CoreDataProperties.h"

@implementation Water (CoreDataProperties)

@dynamic date;
@dynamic waterQuentity;

@end

//
//  ThyroidViewController.m
//  Pranayama
//
//  Created by Manish Kumar on 13/06/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "ProblemCatagoriVC.h"
#import "SettingsViewController.h"

@interface ProblemCatagoriVC ()

@end

@implementation ProblemCatagoriVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    self.screenName=@"Problem section";
    // Do any additional setup after loading the view.
    int fontSize;
    if (DEVICE==IPAD) {
        fontSize=24;
    }
    else{
        fontSize=18;
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    [self changeText];
}


- (void)HomeMethod:(NSNotification *)notification {
    [[CommonSounds sharedInstance] appDidBecomeActive:notification navigation:self.navigationController];
}




-(void)changeText{
        
        self.navigationItem.title=[MCLocalization stringForKey:[[NSUserDefaults standardUserDefaults] objectForKey:HabbitType]];
        [Basics setTitle:[MCLocalization stringForKey:@"basic"] forState:UIControlStateNormal];
        [How_it_work setTitle:[MCLocalization stringForKey:@"work"] forState:UIControlStateNormal];
        [Yogasana setTitle:[MCLocalization stringForKey:@"yogasan"] forState:UIControlStateNormal];
        [Pranayama setTitle:[MCLocalization stringForKey:@"Pranayam"] forState:UIControlStateNormal];
        [Pregress setTitle:[MCLocalization stringForKey:@"progress"] forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //If this vc can be poped , then
    if (self.navigationController.viewControllers.count > 1)
    {
        // Disabling pan gesture for left menu
        //        [self disableSlidePanGestureForLeftMenu];
    }
    
    AMSlideMenuMainViewController *mainVC = [AMSlideMenuMainViewController getInstanceForVC:self];
    if (mainVC.rightMenu)
    {
        // Adding right menu button to navigation bar
        [self addRightMenuButton];
    }
    [[NSUserDefaults standardUserDefaults] setObject:@"Habbitcatagori" forKey:controllerType];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)backbutton:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:HabbitMainCondition];
    [[self navigationController] popViewControllerAnimated:YES];
}

@end

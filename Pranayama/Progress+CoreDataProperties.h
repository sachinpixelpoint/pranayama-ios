//
//  Progress+CoreDataProperties.h
//  Pranayama
//
//  Created by Manish Kumar on 02/07/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Progress.h"

NS_ASSUME_NONNULL_BEGIN

@interface Progress (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *anulomVilom;
@property (nullable, nonatomic, retain) NSNumber *days;
@property (nullable, nonatomic, retain) NSNumber *halasana;
@property (nullable, nonatomic, retain) NSNumber *kapalbhati;
@property (nullable, nonatomic, retain) NSNumber *sarvangasana;
@property (nullable, nonatomic, retain) NSNumber *ujjayi;
@property (nullable, nonatomic, retain) NSNumber *duration;
@property (nullable, nonatomic, retain) NSString *progressType;
@property (nullable, nonatomic, retain) NSNumber *vipritkarani;
@property (nullable, nonatomic, retain) NSNumber *pashimothanasana;
@property (nullable, nonatomic, retain) NSNumber *dhanurasana;
@property (nullable, nonatomic, retain) NSNumber *balasana;
@property (nullable, nonatomic, retain) NSNumber *hastapadasana;
@property (nullable, nonatomic, retain) NSNumber *marjariasana;
@property (nullable, nonatomic, retain) NSNumber *bhramari;
@property (nullable, nonatomic, retain) NSNumber *suryabhedana;
@property (nullable, nonatomic, retain) NSNumber *medetativebreathing;
@property (nullable, nonatomic, retain) NSNumber *bhastrika;
@property (nullable, nonatomic, retain) NSNumber *uttanasana;
@property (nullable, nonatomic, retain) NSNumber *setubandhasana;
@property (nullable, nonatomic, retain) NSNumber *virabhadrasana;
@property (nullable, nonatomic, retain) NSNumber *udgeeth;
@property (nullable, nonatomic, retain) NSNumber *bahya;


@end

NS_ASSUME_NONNULL_END

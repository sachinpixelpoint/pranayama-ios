//  HomeTabelVC.m
//  Pranayama
//
//  Created by Manish Kumar on 30/03/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "HomeTabelVC.h"
#import "SettingsViewController.h"

#define DEGREES_IN_RADIANS(x) (M_PI * x / 180.0)
@interface HomeTabelVC ()

@end

@implementation HomeTabelVC
@synthesize table,shareButton;
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];


    /////////////////  new code/////////////////
    
    ///////
    ///////////////////////////////////////////////////////////////
    /////// Check The Pervious Transition//////
    //////////////////////////////////////////////////////////////

//    if (![[NSUserDefaults standardUserDefaults]boolForKey:Is_Perchaged]) {
//        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
//        [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
// }
    
    
    self.screenName = @"Home" ;
    view1.backgroundColor=[UIColor colorWithWhite:1 alpha:0.8];
    self.navigationController.navigationBar.tintColor =RGB(230, 123, 27);
    [self ChangeText];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.navigationItem.hidesBackButton=YES;
    if (DEVICE==IPAD) {
        fontSize=24;
    }
    else{
        
        fontSize=18;
     }
    
        [self.navigationController.navigationBar setTitleTextAttributes:
        @{NSForegroundColorAttributeName:[UIColor whiteColor],
         NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];

    view1.frame=CGRectMake(0, 64, self.view.frame.size.width,self.view.frame.size.height-table.frame.size.height-64);
    shareButton.frame=CGRectMake(shareButton.frame.origin.x, self.table.frame.origin.y-shareButton.frame.size.height-10, shareButton.frame.size.width, shareButton.frame.size.height);
    shareButton.layer.cornerRadius = shareButton.bounds.size.width / 2.0;
    shareButton.backgroundColor=RGB(138, 21, 0);
    
    outerLogo.frame=CGRectMake(self.view.frame.size.width/2-outerLogo.frame.size.width/2, (view1.frame.size.height/2-outerLogo.frame.size.height/2-nameLogo.frame.size.height/2), outerLogo.frame.size.width, outerLogo.frame.size.height);
    centerLogo.frame=CGRectMake(self.view.frame.size.width/2-centerLogo.frame.size.width/2, (view1.frame.size.height/2-centerLogo.frame.size.height/2-nameLogo.frame.size.height/2), centerLogo.frame.size.width, centerLogo.frame.size.height);
    nameLogo.frame=CGRectMake(self.view.frame.size.width/2-nameLogo.frame.size.width/2, outerLogo.frame.origin.y+outerLogo.frame.size.height, nameLogo.frame.size.width, nameLogo.frame.size.height);
}

//- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
//    
//    for (SKPaymentTransaction *transaction in transactions) {
//        
//        if (transaction.transactionState == SKPaymentTransactionStateRestored) {
//            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:Is_Perchaged];
//            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
//            [table reloadData];
//        }
//    }
//}

- (void)appDidBecomeActive:(NSNotification *)notification {
    [[CommonSounds sharedInstance] appDidBecomeActive:notification navigation:self.navigationController];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [table reloadData];
    [self runSpinAnimationOnView:outerLogo duration:1.5 rotations:360 repeat:0];
    AMSlideMenuMainViewController *mainVC = [AMSlideMenuMainViewController getInstanceForVC:self];
    if (mainVC.rightMenu)
    {
        // Adding right menu button to navigation bar
        [self addRightMenuButton];
    }
    [[NSUserDefaults standardUserDefaults] setObject:@"Home" forKey:controllerType];
    }

- (void) runSpinAnimationOnView:(UIImageView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
    {
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/  ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    }

-(void)ChangeText{
        
        self.navigationItem.title = [MCLocalization stringForKey:@"home"];
        homelist=[[NSMutableArray alloc] initWithObjects:[MCLocalization stringForKey:@"Pranayam"],[MCLocalization stringForKey:@"yogasan"],[MCLocalization stringForKey:@"habbit"],[MCLocalization stringForKey:@"dincharya"],[MCLocalization stringForKey:@"report"], nil];

}

- (IBAction)shareButtonPressed:(id)sender;
  {
    NSString *str;
    NSString *appID=@"1127298201";
    float ver = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (ver >= 7.0 && ver < 7.1) {
        str = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@",appID];
    } else if (ver >= 8.0) {
        str = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software",appID];
    } else {
        str = [NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@",appID];
    }
    
    NSURL *url1=[NSURL URLWithString:str];
    NSString *textToShare = @"Look at this awesome App for aspiring iOS Developers!";
    NSArray *objectsToShare = @[textToShare, url1];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    if (DEVICE==Iphone) {
    [self presentViewController:activityVC animated:YES completion:nil];
    }
    else{
        UIPopoverController *popup=[[UIPopoverController alloc] initWithContentViewController:activityVC];
        [popup presentPopoverFromRect:CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 0, 0)inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [homelist count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"homelistCell";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    UIImageView *lockImg = (UIImageView *)[cell viewWithTag:1];
    if (indexPath.row==3) {
        if ([[NSUserDefaults standardUserDefaults]boolForKey:Is_Perchaged]) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            lockImg.hidden=YES;
        }
        else{
            lockImg.hidden=NO;
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    else{
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        lockImg.hidden=YES;
    }

    cell.textLabel.text=[homelist objectAtIndex:indexPath.row];
    cell.textLabel.textColor=[UIColor whiteColor];
    cell.textLabel.backgroundColor=[UIColor clearColor];
    cell.textLabel.font=Ralewayfont(fontSize);
    int colors = indexPath.row%2;
    switch (colors) {
        case 0:
            cell.backgroundColor=[UIColor colorWithRed:187/255.0 green:20/255.0 blue:37/255.0 alpha:0.90];
            break;
        case 1:
            cell.backgroundColor=[UIColor colorWithRed:176/255.0 green:12/255.0 blue:28/255.0 alpha:0.90];
            break;
        default:
            break;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

   {
    if (indexPath.row==0) {
        [self performSegueWithIdentifier:@"nexttable" sender:self];
    }
    else if (indexPath.row==1)
    {
        [self performSegueWithIdentifier:@"yogasan" sender:self];
    }
    else if (indexPath.row==2)
    {
        [self performSegueWithIdentifier:@"7problems" sender:self];
    }
    else if (indexPath.row==3)
    {
//        if ([[NSUserDefaults standardUserDefaults]boolForKey:Is_Perchaged]) {
            [self performSegueWithIdentifier:@"dincharya" sender:self];
//        }
//        else{
//            [self performSegueWithIdentifier:@"guru" sender:self];
//        }
    }
    else if (indexPath.row==4)
    {
        [self performSegueWithIdentifier:@"report" sender:self];
    }
 }

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    if ([segue.identifier isEqualToString:@"setting"]) {
//        SettingsViewController *controller = (SettingsViewController *)segue.destinationViewController;
//        controller.delegate=self;
//    }
//}
@end

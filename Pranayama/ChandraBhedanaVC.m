//
//  ChandraBhedanaVC.m
//  Pranayama
//
//  Created by Manish Kumar on 11/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "ChandraBhedanaVC.h"
#import "Pranayama-Swift.h"

@interface ChandraBhedanaVC ()

@end

@implementation ChandraBhedanaVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    // tracker
    self.screenName=@"Chandra Bhedana";
    // Do any additional setup after loading the view.
    [self setlblheight];
    int fontSize;
    if (DEVICE==IPAD) {
        fontSize=24;
    }
    else{
        fontSize=18;
    }
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    Floatingbutton.layer.cornerRadius = Floatingbutton.bounds.size.width / 2.0;
    
    In_roundData = [[NSMutableArray alloc] init];
    exhaleData=[[NSMutableArray alloc] init];
    holdData=[[NSMutableArray alloc] init];
    for (int j=1; j<=50; j++){
        [In_roundData addObject:[NSString stringWithFormat:@"%d",j]];
    }
    for (int j=0; j<=200; j++){
        [holdData addObject:[NSString stringWithFormat:@"%d",j]];
    }
    for (int j=1; j<=100; j++){
        [exhaleData addObject:[NSString stringWithFormat:@"%d",j]];
    }
    
    [self introduction];
    [self setupbutton];
    [self getvalue];
    [self getTotalTime];
    [self setFont];
    // Do any additional setup after loading the view.

    if ([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaMainCon] && [[NSUserDefaults standardUserDefaults]boolForKey:DincharyaDelegate] && [[NSUserDefaults standardUserDefaults] boolForKey:commonClassCond]) {
        [self performSelector:@selector(navigationMove) withObject:nil afterDelay:0.5];
    }
    else{
        if ([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaMainCon]) {
            NSString *name = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaName];
            NSArray *ar = [kAppDele FetchDincharyaFromDatabaseAcordingToName:name];
            if (ar.count>0){
                Dincharya *Din = [ar objectAtIndex:0];
                if (Din.autometicTime != nil) {
                    [self performSelector:@selector(autometicMove) withObject:nil afterDelay:0.5];
                }
            }
        }
    }
}

-(void)navigationMove{
    [[CommonSounds sharedInstance]CheckStartController:self.navigationController Identifier:@"chandra"];
}


-(void)autometicMove{
    [self performSegueWithIdentifier:@"chandra_action" sender:self];
}


- (void)HomeMethod:(NSNotification *)notification {
    HomeTabelVC *homeObject = [[HomeTabelVC alloc]init];
    [homeObject appDidBecomeActive:notification];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self disableSlidePanGestureForRightMenu];
    [self ChangeText];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:DincharyaMainCon] && [[NSUserDefaults standardUserDefaults]boolForKey:viewwillCondition]) {
        [self performSelector:@selector(Dinchariya_NextMethod) withObject:nil afterDelay:0.2];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:viewwillCondition];
    }

}


/////////////////    Move to next pranayama in Dincharya module/////////////
-(void)Dinchariya_NextMethod{
    NSArray *pranaAr = [[NSUserDefaults standardUserDefaults]objectForKey:dincharyaPranaAr];
    
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    arr = [pranaAr mutableCopy];
    [arr removeObject:@"chandra"];
    NSArray *Yarray = [arr copy];
    if (Yarray.count != 0) {
        [[NSUserDefaults standardUserDefaults]setObject:Yarray forKey:dincharyaPranaAr];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        HomeTabelVC *home=[self.storyboard instantiateViewControllerWithIdentifier:@"home"];
        DincharyaVC *dincharyavc=[self.storyboard instantiateViewControllerWithIdentifier:@"dincharya"];
        DincharyaNameVC *dinName=[self.storyboard instantiateViewControllerWithIdentifier:@"dincharya_name"];
        Dincharya_pranaVC *pranaVC = [self.storyboard instantiateViewControllerWithIdentifier:@"dincharya_prana"];
        UIViewController *prana = [self.storyboard instantiateViewControllerWithIdentifier:[Yarray objectAtIndex:0]];
        [self.navigationController setViewControllers:@[home,dincharyavc,dinName,pranaVC,prana] animated:YES];
    }
    else{
        [[NSUserDefaults standardUserDefaults]setObject:nil forKey:dincharyaPranaAr];
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:[MCLocalization stringForKey:@"session"]
                                     message:[MCLocalization stringForKey:@"Today_Fitness"]
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        [[CommonSounds sharedInstance] CommonAlertView:self.navigationController];
                                    }];
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    
    return 1;//Or return whatever as you intend
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    int height;
    if (DEVICE==IPAD) {
        height=40;
    }
    else{
        height=30;
    }
    return height;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    
    int i = 0;
    if([thePickerView isEqual: inhalePicker] || [thePickerView isEqual:roundPicker]){
        i=50;
    }
    else if([thePickerView isEqual: holdPicker]){
        i=201;
    }
    else if([thePickerView isEqual: exhalePicker]) {
        i=100;
    }
    return i;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    
    if([pickerView isEqual: inhalePicker]){
        InhaleRow=[[In_roundData objectAtIndex:row] intValue];
    }
    else if([pickerView isEqual: holdPicker]){
        HoldRow=[[holdData objectAtIndex:row] intValue];
    }
    else if([pickerView isEqual: exhalePicker]) {
        ExhaleRow=[[exhaleData objectAtIndex:row] intValue];
    }
    else{
        RoundRow=[[In_roundData objectAtIndex:row] intValue];
    }
    StartButton.userInteractionEnabled=YES;
    selectCondition=NO;
    [self getTotalTime];
    [self setvalueInkey];
}

-(void)pickerView1:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    
    if([pickerView isEqual: inhalePicker]){
        InhaleRow=[[In_roundData objectAtIndex:row] intValue];
    }
    else if([pickerView isEqual: holdPicker]){
        HoldRow=[[holdData objectAtIndex:row] intValue];
    }
    else if([pickerView isEqual: exhalePicker]) {
        ExhaleRow=[[exhaleData objectAtIndex:row] intValue];
    }
    else{
        RoundRow=[[In_roundData objectAtIndex:row] intValue];
    }
    [timer1 invalidate];
    timer1 = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(yourMethodName) userInfo:nil repeats:NO];
    [self getTotalTime];
    [self setvalueInkey];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* tView = (UILabel*)view;
    if (!tView){
        tView = [[UILabel alloc] init];
        if (DEVICE==IPAD) {
            [tView setFont:[UIFont systemFontOfSize:40]];
        }
        else{
            [tView setFont:[UIFont systemFontOfSize:22]];
        }
        
        [tView setTextAlignment:NSTextAlignmentCenter];
        if([pickerView isEqual: inhalePicker] || [pickerView isEqual:roundPicker]){
            
            tView.text=[In_roundData objectAtIndex:row];
        }
        else if([pickerView isEqual: holdPicker]){
            tView.text=[holdData objectAtIndex:row];
        }
        else {
            tView.text=[exhaleData objectAtIndex:row];
        }
    }
    if (buttonCondition==YES) {
    }
    else if (selectCondition==NO) {
        [self pickerView1:pickerView didSelectRow:row inComponent:component];
    }
    else{
        NSTimer *timer;
        timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(MethodName) userInfo:nil repeats:NO];
    }
    StartButton.userInteractionEnabled=NO;
    
    pickerView.subviews[1].hidden = YES;
    pickerView.subviews[2].hidden = YES;
    return tView;
}
-(void)MethodName{
    selectCondition=NO;
}

////////////// introduction /////////
-(void)introduction{
    bool intro=[[NSUserDefaults standardUserDefaults] boolForKey:Introduction];
    if (intro==YES) {
        [[CommonSounds sharedInstance] instructions:self.view];
    }
    else{
        NSTimer *timer;
        timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(yourMethodName) userInfo:nil repeats:NO];
    }
}
-(void)yourMethodName{
    StartButton.userInteractionEnabled=YES;
}
//////////////

-(void)setlblheight{
    int Device =Device_Type;
    switch (Device) {
        case iPhone4:
            img.frame=CGRectMake((self.view.frame.size.width/2)-90, 160, 180, 240);
            break;
        case iPhone5:
            img.frame=CGRectMake((self.view.frame.size.width/2)-95, 180, 190, 290);
            break;
        case iPhone6:
            img.frame=CGRectMake((self.view.frame.size.width/2)-125, 190, 250, 360);
            break;
        case iPhone6P:
            img.frame=CGRectMake((self.view.frame.size.width/2)-135, 200, 270, 410);
            break;
        default:
            break;
    }
    if (DEVICE == Iphone) {
        topview.frame = CGRectMake(8, 74, self.view.frame.size.width-16, 32);
    }
}

-(IBAction)reset:(id)sender{
    if (InhaleRow==1&&HoldRow==4&&ExhaleRow==2&&RoundRow==1) {
        selectCondition=NO;
    }
    else{
        selectCondition=YES;
    }
    InhaleRow=1;
    HoldRow=4;
    ExhaleRow=2;
    RoundRow=1;
    [inhalePicker selectRow:InhaleRow-1 inComponent:0 animated:YES];
    [holdPicker selectRow:HoldRow inComponent:0 animated:YES];
    [exhalePicker selectRow:ExhaleRow-1 inComponent:0 animated:YES];
    [roundPicker selectRow:RoundRow-1 inComponent:0 animated:YES];
    [self setvalueInkey];
    [self getTotalTime];
    NSTimer *timer;
    timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(yourMethodName) userInfo:nil repeats:NO];
}

-(void)getvalue{
    InhaleRow=[[NSUserDefaults standardUserDefaults] floatForKey:chandraInhale];
    HoldRow=[[NSUserDefaults standardUserDefaults] floatForKey:chandraHold];
    ExhaleRow =[[NSUserDefaults standardUserDefaults] floatForKey:chandraExhale];
    RoundRow=[[NSUserDefaults standardUserDefaults] floatForKey:chandraRound];
    
    [inhalePicker selectRow:InhaleRow-1 inComponent:0 animated:YES];
    [holdPicker selectRow:HoldRow inComponent:0 animated:YES];
    [exhalePicker selectRow:ExhaleRow-1 inComponent:0 animated:YES];
    [roundPicker selectRow:RoundRow-1 inComponent:0 animated:YES];
    
    [self getTotalTime];
}
-(void)setvalueInkey{
    [[NSUserDefaults standardUserDefaults] setInteger:InhaleRow forKey:chandraInhale];
    [[NSUserDefaults standardUserDefaults] setInteger:HoldRow forKey:chandraHold];
    [[NSUserDefaults standardUserDefaults] setInteger:ExhaleRow forKey:chandraExhale];
    [[NSUserDefaults standardUserDefaults] setInteger:RoundRow forKey:chandraRound];
    
}

-(void)setFont{
    int fontSize;
    if (DEVICE==IPAD) {
        fontSize=25;
    }
    else{
        fontSize=12;
    }
    inhalelbl.font=Ralewayfont(fontSize);
    holdlbl.font=Ralewayfont(fontSize);
    exhalelbl.font=Ralewayfont(fontSize);
    roundlbl.font=Ralewayfont(fontSize);
    
}

-(void)ChangeText{
    
    self.navigationItem.title = [MCLocalization stringForKey:@"chandra_bhedana"];
    inhalelbl.text=[MCLocalization stringForKey:@"Inhale"];
    exhalelbl.text=[MCLocalization stringForKey:@"Exhale"];
    holdlbl.text=[MCLocalization stringForKey:@"Hold"];
    roundlbl.text=[MCLocalization stringForKey:@"Rounds"];
    
}

-(IBAction)backbutton:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}
//Play finish sound
-(void)playsound{
    [[CommonSounds sharedInstance] playFinishSound];
}


-(IBAction)inhaleButtonPressed:(UIButton*)sender{
    buttonCondition=YES;
    if (sender.tag==1) {
        
        if (InhaleRow==1) {
            InhaleRow=1;
            HoldRow=InhaleRow*4;
            ExhaleRow=InhaleRow*2;
            [inhalePicker selectRow:InhaleRow-1 inComponent:0 animated:YES];
            [holdPicker selectRow:HoldRow inComponent:0 animated:YES];
            [exhalePicker selectRow:ExhaleRow-1 inComponent:0 animated:YES];
        }
        else{
            InhaleRow=InhaleRow-1;
            HoldRow=InhaleRow*4;
            ExhaleRow=InhaleRow*2;
            [inhalePicker selectRow:InhaleRow-1 inComponent:0 animated:YES];
            [holdPicker selectRow:HoldRow inComponent:0 animated:YES];
            [exhalePicker selectRow:ExhaleRow-1 inComponent:0 animated:YES];
        }
    }
    else if (sender.tag==2){
        if (InhaleRow==50) {
            InhaleRow=50;
            HoldRow=InhaleRow*4;
            ExhaleRow=InhaleRow*2;
            [inhalePicker selectRow:InhaleRow-1 inComponent:0 animated:YES];
            [holdPicker selectRow:HoldRow inComponent:0 animated:YES];
            [exhalePicker selectRow:ExhaleRow-1 inComponent:0 animated:YES];
        }
        else{
            InhaleRow=InhaleRow+1;
            HoldRow=InhaleRow*4;
            ExhaleRow=InhaleRow*2;
            [inhalePicker selectRow:InhaleRow-1 inComponent:0 animated:YES];
            [holdPicker selectRow:HoldRow inComponent:0 animated:YES];
            [exhalePicker selectRow:ExhaleRow-1 inComponent:0 animated:YES];
        }
    }
    [self getTotalTime];
    [self setvalueInkey];
    [timer1 invalidate];
    timer1 = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(yourMethodName1) userInfo:nil repeats:NO];
}
-(void)yourMethodName1{
    buttonCondition=NO;
    StartButton.userInteractionEnabled=YES;
}
-(void)getTotalTime{
    totaltime=((InhaleRow+HoldRow+ExhaleRow)*RoundRow);
    lblseconds.text=[NSString stringWithFormat:@"%d",totaltime];
    seconds = totaltime % 60;
    minutes = (totaltime / 60) % 60;
    hours = totaltime / 3600;
    lblseconds.text=[NSString stringWithFormat:@"%d",seconds];
    lblminutes.text=[NSString stringWithFormat:@"%d",minutes];
    lblHours.text=[NSString stringWithFormat:@"%d",hours];
}

-(void)setupbutton{
    int a;
    if (DEVICE==IPAD) {
        if (self.view.frame.size.height==1366) {
            a=80;
        }
        else{
            a=60;
        }
    }
    else{
        a=35;
    }
    CGRect floatFrame = CGRectMake((Floatingbutton.frame.origin.x+5),(Floatingbutton.frame.origin.y+5), a, a);
    button1=[[UIButton alloc]initWithFrame:floatFrame];
    button1.backgroundColor=RGB(255, 154, 0);
    button1.layer.cornerRadius=(button1.bounds.size.width-2)/2;
    button1.tag=11;
    [button1 addTarget:self
                action:@selector(buttonmethod:) forControlEvents:UIControlEventTouchDown];
    button1.imageEdgeInsets= UIEdgeInsetsMake(7,7,7,7);
    [button1 setImage:[UIImage imageNamed:@"help"] forState:UIControlStateNormal];
    
    button2=[[UIButton alloc]initWithFrame:floatFrame];
    button2.backgroundColor=RGB(255, 154, 0);
    button2.layer.cornerRadius=(button2.bounds.size.width-2)/2;
    button2.tag=12;
    [button2 addTarget:self
                action:@selector(buttonmethod:) forControlEvents:UIControlEventTouchDown];
    button2.imageEdgeInsets= UIEdgeInsetsMake(7,7,7,7);
    [button2 setImage:[UIImage imageNamed:@"benefits_ic"] forState:UIControlStateNormal];
    
    button3=[[UIButton alloc]initWithFrame:floatFrame];
    button3.backgroundColor=RGB(255, 154, 0);
    button3.layer.cornerRadius=(button3.bounds.size.width-2)/2;
    button3.tag=13;
    [button3 addTarget:self
                action:@selector(buttonmethod:) forControlEvents:UIControlEventTouchDown];
    button3.imageEdgeInsets= UIEdgeInsetsMake(7,7,7,7);
    [button3 setImage:[UIImage imageNamed:@"about_ii"] forState:UIControlStateNormal];
    
}
-(IBAction)startbutton:(id)sender{
    [self dismissMenu];
}
-(IBAction)reportbutton:(id)sender{
    [self dismissMenu];
}
-(IBAction)floatingbutton:(id)sender
{
    if (isMenuVisible==YES)
    {
        [self dismissMenu];
    }
    else
    {
        [self showMenu];
    }
}

-(void)buttonmethod:(UIButton *)sender{
    if (sender.tag==11) {
        [self performSegueWithIdentifier:@"help"sender:self];
    }
    else if (sender.tag==12)
    {
        [self performSegueWithIdentifier:@"benefit"sender:self];
    }
    else{
        [self performSegueWithIdentifier:@"about"sender:self];
    }
    button1.alpha=0;
    button2.alpha=0;
    button3.alpha=0;
    isMenuVisible=NO;
}

-(void) showMenu
{
    CGPoint Point1;
    CGPoint Point2;
    CGPoint Point3;
    if (DEVICE==IPAD) {
        Point1=CGPointMake(-8, -172);
        Point2=CGPointMake(-95, -115);
        Point3=CGPointMake(-150, -20);
    }
    else{
        Point1=CGPointMake(-10, -103);
        Point2=CGPointMake(-62, -73);
        Point3=CGPointMake(-90, -20);
    }
    [Floatingbutton setUserInteractionEnabled:NO];
    [UIButton animateWithDuration:0.5f
                       animations:^{
                           [UIButton animateWithDuration:2.0 delay:0.0 options:UIViewAnimationOptionRepeat | UIViewAnimationOptionCurveLinear animations:^{
                               CGAffineTransform transform = CGAffineTransformMakeRotation(M_PI);
                               button1.transform = transform;
                               button2.transform=transform;
                               button3.transform=transform;
                           } completion:NULL];
                           
                           [button1 setTransform:CGAffineTransformMakeTranslation(Point1.x,Point1.y)];
                           [button2 setTransform:CGAffineTransformMakeTranslation(Point2.x,Point2.y)];
                           [button3 setTransform:CGAffineTransformMakeTranslation(Point3.x,Point3.y)];
                           [self.view addSubview:button1];
                           [self.view addSubview:button2];
                           [self.view addSubview:button3];
                           button1.alpha=0.7;
                           button2.alpha=0.7;
                           button3.alpha=0.7;
                       }
                       completion:^(BOOL finished){
                           button1.alpha=1;
                           button2.alpha=1;
                           button3.alpha=1;
                           [Floatingbutton setUserInteractionEnabled:YES];
                           isMenuVisible=YES;
                       }];
    
}

-(void) dismissMenu
{
    [Floatingbutton setUserInteractionEnabled:NO];
    [UIButton animateWithDuration:0.5f
                       animations:^{
                           [UIButton animateWithDuration:2.0 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveLinear animations:^{
                               CGAffineTransform transform = CGAffineTransformMakeRotation(M_PI);
                               button1.transform = transform;
                               button2.transform=transform;
                               button3.transform=transform;
                           } completion:NULL];
                           
                           [button1 setTransform:CGAffineTransformMakeTranslation(0,0)];
                           [button2 setTransform:CGAffineTransformMakeTranslation(0,0)];
                           [button3 setTransform:CGAffineTransformMakeTranslation(0,0)];
                           button1.alpha=0.3;
                           button2.alpha=0.3;
                           button3.alpha=0.3;
                       }
                       completion:^(BOOL finished){
                           button1.alpha=0;
                           button2.alpha=0;
                           button3.alpha=0;
                           [Floatingbutton setUserInteractionEnabled:YES];
                           isMenuVisible=NO;
                       }];
    
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (isMenuVisible==YES) {
        [self dismissMenu];
        isMenuVisible=NO;
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"chandra_action"]) {
        ChandraBhedanaActionVC *controller = (ChandraBhedanaActionVC *)segue.destinationViewController;
        controller.totalTime=totaltime;
        controller.lblinhale=InhaleRow;
        controller.lblexhale=ExhaleRow;
        controller.lblhold=HoldRow;
        controller.rounds=RoundRow;
        controller.delegate=self;
    }
    if ([segue.identifier isEqualToString:@"about"]) {
        AboutViewController *about=(AboutViewController *)segue.destinationViewController;
        about.Type=@"Chandra";
    }
    if ([segue.identifier isEqualToString:@"benefit"]) {
        BenefitViewController *about=(BenefitViewController *)segue.destinationViewController;
        about.Type=@"Chandra";
    }
    if ([segue.identifier isEqualToString:@"help"]) {
        HelpViewController *about=(HelpViewController *)segue.destinationViewController;
        about.Type=@"Chandra";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

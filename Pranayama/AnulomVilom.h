//
//  AnulomVilom.h
//  Pranayama
//
//  Created by Manish Kumar on 30/03/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnulomVilomActionVC.h"

@interface AnulomVilom : GAITrackedViewController <anulomdelegate>
{
    IBOutlet UIPickerView *inhalePicker;
    IBOutlet UIPickerView *holdPicker;
    IBOutlet UIPickerView *exhalePicker;
    IBOutlet UIPickerView *roundPicker;
    IBOutlet UIPickerView *afterHoldPicker;
    int InhaleRow;
    int HoldRow;
    int ExhaleRow;
    int RoundRow;
    int afterHoldRow;
    NSMutableArray *In_roundData;
    NSMutableArray *holdData;
    NSMutableArray *exhaleData;
    NSMutableArray *afroundData;
    
    IBOutlet UIView *topview;
    IBOutlet UIImageView *img;
    
    IBOutlet UILabel *inhalelbl;
    IBOutlet UILabel *holdlbl;
    IBOutlet UILabel *exhalelbl;
    IBOutlet UILabel *roundlbl;
    IBOutlet UILabel *AfHoldlbl;
    
    IBOutlet UIButton *btnLeftInhel;
    IBOutlet UIButton *btnRightExhale;
    IBOutlet UIButton *StartButton;
    IBOutlet UIButton *reportButton;
    
    IBOutlet UILabel *lblHours;
    IBOutlet UILabel *lblminutes;
    IBOutlet UILabel *lblseconds;
    int totaltime;
    int seconds;
    int minutes;
    int hours;
    UIButton *button1,*button2,*button3;
    BOOL isMenuVisible;
    IBOutlet UIButton *Floatingbutton;
    UIView *introview;
    UIButton *gotItbutton;
    BOOL selectCondition;
    BOOL buttonCondition;
    NSTimer *timer1;
    }
@end

//
//  HomeTabelVC.h
//  Pranayama
//
//  Created by Manish Kumar on 30/03/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsViewController.h"
#import <StoreKit/StoreKit.h>
@interface HomeTabelVC : GAITrackedViewController <SKPaymentTransactionObserver>
{
    NSMutableArray *homelist;
    IBOutlet UIImageView *centerLogo;
    IBOutlet UIImageView *outerLogo;
    IBOutlet UIImageView *nameLogo;
   
    int fontSize;
    IBOutlet UIView*view1;
}
@property(nonatomic,strong) IBOutlet UITableView *table;
@property (nonatomic, strong) IBOutlet UIButton *shareButton;
- (void)appDidBecomeActive:(NSNotification *)notification;
@end

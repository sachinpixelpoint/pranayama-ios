//
//  Dincharya_yogaVC.swift
//  Pranayama
//
//  Created by Manish Kumar on 20/10/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

import UIKit

class Dincharya_yogaVC: GAITrackedViewController,UITableViewDelegate,UITableViewDataSource {
    
    let kAppDel = (UIApplication.shared.delegate! as! AppDelegate)
    
    var yogasanList : NSMutableArray! = NSMutableArray()
    var ListArray : NSMutableArray! = NSMutableArray()
    var ListArray2 : NSMutableArray! = NSMutableArray()
    
    @IBOutlet var tabelview : UITableView!
    
    var Fontsize : CGFloat!
    var Fontsize2 : CGFloat!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: (#selector(self.appDidBecomeActive(_:))), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        self.screenName="Dincharya Yogasana list"
        // Do any additional setup after loading the view.
        
        self.automaticallyAdjustsScrollViewInsets=false
       
        
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            Fontsize=24
            Fontsize2=16
        }
        else{
            Fontsize=18
            Fontsize2=10
        }
        
        UINavigationBar.appearance().titleTextAttributes=[NSFontAttributeName: Ralewayfont(Fontsize!),NSForegroundColorAttributeName:UIColor.white]
        
        tabelview.delegate=self
        tabelview.dataSource=self
        
        
    }
    
    func appDidBecomeActive(_ notification:Notification) -> Void {
        
        CommonSounds.sharedInstance().appDidBecomeActive(notification, navigation: self.navigationController)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let mainVC = AMSlideMenuMainViewController.getInstanceForVC(self) as AMSlideMenuMainViewController
        if (mainVC.rightMenu != nil) {
            self.addRightMenuButton()
        }
        UserDefaults.standard.set("Dincharya Yoga", forKey: controllerType)
        
        if yogasanList.count==0 {
            let str = UserDefaults.standard.object(forKey: dincharyaName) as! String
            let ar = kAppDel.fetchDincharyaFromDatabaseAcording(toName: str) as NSArray
            if ar.count>0{
                let dincharya = ar.object(at: 0) as! Dincharya
                let pranaAr = NSKeyedUnarchiver.unarchiveObject(with: dincharya.yogasana_arr!) as! NSArray
                yogasanList = NSMutableArray(array: pranaAr)
            }
        }
        
        self.getList()
    }
    
    func getList() -> Void {
        
        self.navigationItem.title=MCLocalization.string(forKey: "yogasan")
        for i in 0..<yogasanList.count {
            if yogasanList.object(at: i) as! String == "Sarvangasana" {
                ListArray?.add(MCLocalization.string(forKey: "Sarvangasana"))
                ListArray2.add(MCLocalization.string(forKey: "Shoulder_Stand"))
            }
            if yogasanList.object(at: i) as! String == "Halasana" {
                ListArray?.add(MCLocalization.string(forKey: "Halasana"))
                ListArray2.add(MCLocalization.string(forKey: "Plow_Pose"))
            }
            if yogasanList.object(at: i) as! String == "Vipritkarani" {
                ListArray?.add(MCLocalization.string(forKey: "Vipritkarani"))
                ListArray2.add(MCLocalization.string(forKey: "Legs_up_the_Wall"))
            }
            if yogasanList.object(at: i) as! String == "Paschimottanasana" {
                ListArray?.add(MCLocalization.string(forKey: "Paschimottanasana"))
                ListArray2.add(MCLocalization.string(forKey: "Seated_Forward_Bend"))
            }
            if yogasanList.object(at: i) as! String == "Dhanurasana" {
                ListArray?.add(MCLocalization.string(forKey: "Dhanurasana"))
                ListArray2.add(MCLocalization.string(forKey: "Bow_Pose"))
            }
            if yogasanList.object(at: i) as! String == "Balasana" {
                ListArray?.add(MCLocalization.string(forKey: "Balasana"))
                ListArray2.add(MCLocalization.string(forKey: "Child_Pose"))
            }
            if yogasanList.object(at: i) as! String == "Hastapadasana" {
                ListArray?.add(MCLocalization.string(forKey: "Hastapadasana"))
                ListArray2.add(MCLocalization.string(forKey: "Standing_Forward_Bend"))
            }
            if yogasanList.object(at: i) as! String == "Marjariasana" {
                ListArray?.add(MCLocalization.string(forKey: "Marjariasana"))
                ListArray2.add(MCLocalization.string(forKey: "Cat_Stretch"))
            }
            if yogasanList.object(at: i) as! String == "Uttanasana" {
                ListArray?.add(MCLocalization.string(forKey: "Uttanasana"))
                ListArray2.add(MCLocalization.string(forKey: "Standing_Forward_Fold"))
            }
            if yogasanList.object(at: i) as! String == "Setu Bandhasana" {
                ListArray?.add(MCLocalization.string(forKey: "Setu Bandhasana"))
                ListArray2.add(MCLocalization.string(forKey: "Bridge_Pose"))
            }
            if yogasanList.object(at: i) as! String == "Virabhadrasana" {
                ListArray?.add(MCLocalization.string(forKey: "Virabhadrasana"))
                ListArray2.add(MCLocalization.string(forKey: "Warrior_Pose"))
            }
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return (ListArray?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellidentifier") as UITableViewCell!
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        cell.accessoryType = .disclosureIndicator
        let lan = UserDefaults.standard.object(forKey: Languagevalue) as! NSString
        if lan.isEqual(to: Hindi) {
             cell.textLabel?.text=ListArray?[(indexPath as NSIndexPath).row] as! String?
            cell.textLabel?.font = Ralewayfont(Fontsize)
        }
        else {
            let str1 = ListArray.object(at: indexPath.row) as! String
            let str2 = ListArray2.object(at: indexPath.row) as! String
            let string = "\(str1) \(str2)"
            let attributedStr = NSMutableAttributedString(string: string)
            attributedStr.addAttribute(NSFontAttributeName, value: Ralewayfont(Fontsize), range: NSRange(location: 0, length: str1.characters.count))
            attributedStr.addAttribute(NSFontAttributeName, value: Ralewayfont(Fontsize2), range: NSRange(location: str1.characters.count + 1, length: str2.characters.count))
            cell.textLabel?.attributedText = attributedStr
        }
        cell.textLabel?.textColor=UIColor.white
        cell.textLabel?.numberOfLines=0
        
        let colors = (indexPath as NSIndexPath).row%2
        switch (colors) {
        case 0:
            cell.backgroundColor=UIColor(red:187/255.0 , green: 20/255.0, blue: 37/255.0, alpha: 0.5)
            break
        case 1:
            cell.backgroundColor=UIColor(red:176/255.0 , green: 12/255.0, blue: 28/255.0, alpha: 0.5)
            break
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UserDefaults.standard.set(yogasanList?[(indexPath as NSIndexPath).row] as! String?, forKey: YogasanCondition)
        let  VC = self.storyboard?.instantiateViewController(withIdentifier: "nextcontroller") as! startViewController
        self.navigationController?.pushViewController(VC, animated: true)
        
    }

    
    
    @IBAction func backbutton(_ sender: AnyObject) {
        self.navigationController!.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

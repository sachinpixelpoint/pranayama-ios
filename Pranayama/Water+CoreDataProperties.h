//
//  Water+CoreDataProperties.h
//  Pranayama
//
//  Created by Manish Kumar on 31/08/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Water.h"

NS_ASSUME_NONNULL_BEGIN

@interface Water (CoreDataProperties)

@property (nullable, nonatomic, retain) NSDate *date;
@property (nullable, nonatomic, retain) NSString *waterQuentity;

@end

NS_ASSUME_NONNULL_END

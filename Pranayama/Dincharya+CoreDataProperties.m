//
//  Progress+CoreDataProperties.m
//  Pranayama
//
//  Created by Manish Kumar on 02/07/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Dincharya+CoreDataProperties.h"

@implementation Dincharya (CoreDataProperties)
@dynamic name,pranayama_arr,yogasana_arr,notification_time,autometicTime;
@end

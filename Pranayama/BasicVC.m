//
//  BasicThyroidVC.m
//  Pranayama
//
//  Created by Manish Kumar on 13/06/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "BasicVC.h"

@interface BasicVC ()
    
    @end

@implementation BasicVC
    
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    self.screenName=@"Basic";
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.tintColor =RGB(230, 123, 27);
    self.navigationItem.hidesBackButton=YES;
    int fontSize;
    if (DEVICE==IPAD) {
        fontSize=24;
    }
    else{
        fontSize=18;
    }
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    self.automaticallyAdjustsScrollViewInsets = NO;
    NSString *habbitType=[[NSUserDefaults standardUserDefaults] objectForKey:HabbitType];
    self.navigationItem.title = [MCLocalization stringForKey:@"basic"];
    if ([habbitType isEqualToString:@"Thyroid"]) {
        [self ThyroidContent];
    }
    else if ([habbitType isEqualToString:@"Insomnia"]){
        [self InsomniaContent];
    }
    else if ([habbitType isEqualToString:@"Diabetes"]){
        [self DiabetesContent];
    }
    else if ([habbitType isEqualToString:@"Migraine"]){
        [self MigraineContent];
    }
    else if ([habbitType isEqualToString:@"Weight Loss"]){
        [self Weightcontent];
    }
    else if ([habbitType isEqualToString:@"Asthma"])
    {
        [self asthmacontent];
    }
    else{
        [self jointpaincontent];
    }
}
    
    
- (void)HomeMethod:(NSNotification *)notification {
    [[CommonSounds sharedInstance] appDidBecomeActive:notification navigation:self.navigationController];
}
    
    
- (void)viewWillAppear:(BOOL)animated
    {
        [super viewWillAppear:animated];
        [self disableSlidePanGestureForRightMenu];
    }
    
    
    
    
-(void)ThyroidContent{
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label8 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label9 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label10 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label11 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label12 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(18);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(18);
    label8.font=Ralewayfont(14);
    label9.font=Ralewayfont(18);
    label10.font=Ralewayfont(14);
    label11.font=Ralewayfont(18);
    label12.font=Ralewayfont(14);
    
    self.navigationItem.title = [MCLocalization stringForKey:@"basic"];
    label1.text=[MCLocalization stringForKey:@"thyoride1"];
    label2.text=[MCLocalization stringForKey:@"thyoride2"];
    label3.text=[MCLocalization stringForKey:@"thyoride3"];
    label4.text=[MCLocalization stringForKey:@"thyoride4"];
    label5.text=[MCLocalization stringForKey:@"thyoride5"];
    label6.text=[MCLocalization stringForKey:@"thyoride6"];
    label7.text=[MCLocalization stringForKey:@"thyoride7"];
    label8.text=[MCLocalization stringForKey:@"thyoride8"];
    label9.text=[MCLocalization stringForKey:@"thyoride9"];
    label10.text=[MCLocalization stringForKey:@"thyoride10"];
    label11.text=[MCLocalization stringForKey:@"thyoride11"];
    label12.text=[MCLocalization stringForKey:@"thyoride12"];
    
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [ScrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [ScrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    label3.textColor = [UIColor redColor];
    [label3 sizeToFit];
    [ScrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [ScrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    label5.textColor = [UIColor redColor];
    [label5 sizeToFit];
    [ScrollView addSubview:label5];
    
    label6.frame=CGRectMake(10, label5.frame.origin.y+label5.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [ScrollView addSubview:label6];
    
    label7.frame=CGRectMake(10, label6.frame.origin.y+label6.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    label7.textColor = [UIColor redColor];
    [label7 sizeToFit];
    [ScrollView addSubview:label7];
    
    label8.frame=CGRectMake(10, label7.frame.origin.y+label7.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label8.numberOfLines=0;
    [label8 sizeToFit];
    [ScrollView addSubview:label8];
    
    label9.frame=CGRectMake(10, label8.frame.origin.y+label8.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label9.numberOfLines=0;
    [label9 sizeToFit];
    label9.textColor = [UIColor redColor];
    [ScrollView addSubview:label9];
    
    label10.frame=CGRectMake(10, label9.frame.origin.y+label9.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label10.numberOfLines=0;
    [label10 sizeToFit];
    [ScrollView addSubview:label10];
    
    label11.frame=CGRectMake(10, label10.frame.origin.y+label10.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label11.numberOfLines=0;
    label11.textColor = [UIColor redColor];
    [label11 sizeToFit];
    [ScrollView addSubview:label11];
    
    label12.frame=CGRectMake(10, label11.frame.origin.y+label11.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label12.numberOfLines=0;
    [label12 sizeToFit];
    [ScrollView addSubview:label12];
    
    ScrollView.contentSize=CGSizeMake(ScrollView.frame.size.width,label12.frame.origin.y+label12.frame.size.height+20);
}
    
    
    
-(void)InsomniaContent{
    self.navigationItem.title = [MCLocalization stringForKey:@"basic"];
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label8 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label9 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label10 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label11 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label12 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label13 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label14 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(18);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(18);
    label8.font=Ralewayfont(14);
    label9.font=Ralewayfont(18);
    label10.font=Ralewayfont(14);
    label11.font=Ralewayfont(18);
    label12.font=Ralewayfont(14);
    label13.font=Ralewayfont(18);
    label14.font=Ralewayfont(14);
    self.navigationItem.title = [MCLocalization stringForKey:@"basic"];
    label1.text=[MCLocalization stringForKey:@"insomaiya1"];
    label2.text=[MCLocalization stringForKey:@"insomaiya2"];
    label3.text=[MCLocalization stringForKey:@"insomaiya3"];
    label4.text=[MCLocalization stringForKey:@"insomaiya4"];
    label5.text=[MCLocalization stringForKey:@"insomaiya5"];
    label6.text=[MCLocalization stringForKey:@"insomaiya6"];
    label7.text=[MCLocalization stringForKey:@"insomaiya7"];
    label8.text=[MCLocalization stringForKey:@"insomaiya8"];
    label9.text=[MCLocalization stringForKey:@"insomaiya9"];
    label10.text=[MCLocalization stringForKey:@"insomaiya10"];
    label11.text=[MCLocalization stringForKey:@"insomaiya11"];
    label12.text=[MCLocalization stringForKey:@"insomaiya12"];
    label13.text=[MCLocalization stringForKey:@"insomaiya13"];
    label14.text=[MCLocalization stringForKey:@"insomaiya14"];
    
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [ScrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [ScrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    label3.textColor = [UIColor redColor];
    [label3 sizeToFit];
    [ScrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [ScrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    label5.textColor = [UIColor redColor];
    [label5 sizeToFit];
    [ScrollView addSubview:label5];
    
    label6.frame=CGRectMake(10, label5.frame.origin.y+label5.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [ScrollView addSubview:label6];
    
    label7.frame=CGRectMake(10, label6.frame.origin.y+label6.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    label7.textColor = [UIColor redColor];
    [label7 sizeToFit];
    [ScrollView addSubview:label7];
    
    label8.frame=CGRectMake(10, label7.frame.origin.y+label7.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label8.numberOfLines=0;
    [label8 sizeToFit];
    [ScrollView addSubview:label8];
    
    label9.frame=CGRectMake(10, label8.frame.origin.y+label8.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label9.numberOfLines=0;
    [label9 sizeToFit];
    label9.textColor = [UIColor redColor];
    [ScrollView addSubview:label9];
    
    label10.frame=CGRectMake(10, label9.frame.origin.y+label9.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label10.numberOfLines=0;
    [label10 sizeToFit];
    [ScrollView addSubview:label10];
    
    label11.frame=CGRectMake(10, label10.frame.origin.y+label10.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label11.numberOfLines=0;
    label11.textColor = [UIColor redColor];
    [label11 sizeToFit];
    [ScrollView addSubview:label11];
    
    label12.frame=CGRectMake(10, label11.frame.origin.y+label11.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label12.numberOfLines=0;
    [label12 sizeToFit];
    [ScrollView addSubview:label12];
    
    label13.frame=CGRectMake(10, label12.frame.origin.y+label12.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label13.numberOfLines=0;
    label13.textColor = [UIColor redColor];
    [label13 sizeToFit];
    [ScrollView addSubview:label13];
    
    label14.frame=CGRectMake(10, label13.frame.origin.y+label13.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label14.numberOfLines=0;
    [label14 sizeToFit];
    [ScrollView addSubview:label14];
    
    
    
    
    ScrollView.contentSize=CGSizeMake(ScrollView.frame.size.width,label14.frame.origin.y+label14.frame.size.height+20);
}
    
-(void)DiabetesContent{
    self.navigationItem.title = [MCLocalization stringForKey:@"basic"];
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label8 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label9 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label10 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];        label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(18);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(18);
    label8.font=Ralewayfont(14);
    label9.font=Ralewayfont(18);
    label10.font=Ralewayfont(14);
    
    self.navigationItem.title = [MCLocalization stringForKey:@"basic"];
    label1.text=[MCLocalization stringForKey:@"diabites1"];
    label2.text=[MCLocalization stringForKey:@"diabites2"];
    label3.text=[MCLocalization stringForKey:@"diabites3"];
    label4.text=[MCLocalization stringForKey:@"diabites4"];
    label5.text=[MCLocalization stringForKey:@"diabites5"];
    label6.text=[MCLocalization stringForKey:@"diabites6"];
    label7.text=[MCLocalization stringForKey:@"diabites7"];
    label8.text=[MCLocalization stringForKey:@"diabites8"];
    label9.text=[MCLocalization stringForKey:@"diabites9"];
    label10.text=[MCLocalization stringForKey:@"diabites10"];
    
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [ScrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [ScrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    label3.textColor = [UIColor redColor];
    [label3 sizeToFit];
    [ScrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [ScrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    label5.textColor = [UIColor redColor];
    [label5 sizeToFit];
    [ScrollView addSubview:label5];
    
    label6.frame=CGRectMake(10, label5.frame.origin.y+label5.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [ScrollView addSubview:label6];
    
    label7.frame=CGRectMake(10, label6.frame.origin.y+label6.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    label7.textColor = [UIColor redColor];
    [label7 sizeToFit];
    [ScrollView addSubview:label7];
    
    label8.frame=CGRectMake(10, label7.frame.origin.y+label7.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label8.numberOfLines=0;
    [label8 sizeToFit];
    [ScrollView addSubview:label8];
    
    label9.frame=CGRectMake(10, label8.frame.origin.y+label8.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label9.numberOfLines=0;
    [label9 sizeToFit];
    label9.textColor = [UIColor redColor];
    [ScrollView addSubview:label9];
    
    label10.frame=CGRectMake(10, label9.frame.origin.y+label9.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label10.numberOfLines=0;
    [label10 sizeToFit];
    [ScrollView addSubview:label10];
    
    ScrollView.contentSize=CGSizeMake(ScrollView.frame.size.width,label10.frame.origin.y+label10.frame.size.height+20);
    
}
    
-(void)MigraineContent{
    self.navigationItem.title = [MCLocalization stringForKey:@"basic"];
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label8 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label9 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label10 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label11 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label12 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label13 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label14 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label15 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label16 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label17 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label18 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label19 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label20 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(18);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(18);
    label8.font=Ralewayfont(14);
    label9.font=Ralewayfont(18);
    label10.font=Ralewayfont(14);
    label11.font=Ralewayfont(18);
    label12.font=Ralewayfont(14);
    label13.font=Ralewayfont(18);
    label14.font=Ralewayfont(14);
    label15.font=Ralewayfont(18);
    label16.font=Ralewayfont(14);
    label17.font=Ralewayfont(18);
    label18.font=Ralewayfont(14);
    label19.font=Ralewayfont(18);
    label20.font=Ralewayfont(14);
    
    self.navigationItem.title = [MCLocalization stringForKey:@"basic"];
    label1.text=[MCLocalization stringForKey:@"migrain1"];
    label2.text=[MCLocalization stringForKey:@"migrain2"];
    label3.text=[MCLocalization stringForKey:@"migrain3"];
    label4.text=[MCLocalization stringForKey:@"migrain4"];
    label5.text=[MCLocalization stringForKey:@"migrain5"];
    label6.text=[MCLocalization stringForKey:@"migrain6"];
    label7.text=[MCLocalization stringForKey:@"migrain7"];
    label8.text=[MCLocalization stringForKey:@"migrain8"];
    label9.text=[MCLocalization stringForKey:@"migrain9"];
    label10.text=[MCLocalization stringForKey:@"migrain10"];
    label11.text=[MCLocalization stringForKey:@"migrain11"];
    label12.text=[MCLocalization stringForKey:@"migrain12"];
    label13.text=[MCLocalization stringForKey:@"migrain13"];
    label14.text=[MCLocalization stringForKey:@"migrain14"];
    label15.text=[MCLocalization stringForKey:@"migrain15"];
    label16.text=[MCLocalization stringForKey:@"migrain16"];
    label17.text=[MCLocalization stringForKey:@"migrain17"];
    label18.text=[MCLocalization stringForKey:@"migrain18"];
    label19.text=[MCLocalization stringForKey:@"migrain19"];
    label20.text=[MCLocalization stringForKey:@"migrain20"];
    
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [ScrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [ScrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    label3.textColor = [UIColor redColor];
    [label3 sizeToFit];
    [ScrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [ScrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    label5.textColor = [UIColor redColor];
    [label5 sizeToFit];
    [ScrollView addSubview:label5];
    
    label6.frame=CGRectMake(10, label5.frame.origin.y+label5.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [ScrollView addSubview:label6];
    
    label7.frame=CGRectMake(10, label6.frame.origin.y+label6.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    label7.textColor = [UIColor redColor];
    [label7 sizeToFit];
    [ScrollView addSubview:label7];
    
    label8.frame=CGRectMake(10, label7.frame.origin.y+label7.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label8.numberOfLines=0;
    [label8 sizeToFit];
    [ScrollView addSubview:label8];
    
    label9.frame=CGRectMake(10, label8.frame.origin.y+label8.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label9.numberOfLines=0;
    [label9 sizeToFit];
    label9.textColor = [UIColor redColor];
    [ScrollView addSubview:label9];
    
    label10.frame=CGRectMake(10, label9.frame.origin.y+label9.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label10.numberOfLines=0;
    [label10 sizeToFit];
    [ScrollView addSubview:label10];
    
    label11.frame=CGRectMake(10, label10.frame.origin.y+label10.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label11.numberOfLines=0;
    label11.textColor = [UIColor redColor];
    [label11 sizeToFit];
    [ScrollView addSubview:label11];
    
    label12.frame=CGRectMake(10, label11.frame.origin.y+label11.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label12.numberOfLines=0;
    [label12 sizeToFit];
    [ScrollView addSubview:label12];
    
    label13.frame=CGRectMake(10, label12.frame.origin.y+label12.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label13.numberOfLines=0;
    label13.textColor = [UIColor redColor];
    [label13 sizeToFit];
    [ScrollView addSubview:label13];
    
    label14.frame=CGRectMake(10, label13.frame.origin.y+label13.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label14.numberOfLines=0;
    [label14 sizeToFit];
    [ScrollView addSubview:label14];
    
    
    label15.frame=CGRectMake(10, label14.frame.origin.y+label14.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label15.numberOfLines=0;
    label15.textColor = [UIColor redColor];
    [label15 sizeToFit];
    [ScrollView addSubview:label15];
    
    label16.frame=CGRectMake(10, label15.frame.origin.y+label15.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label16.numberOfLines=0;
    [label16 sizeToFit];
    [ScrollView addSubview:label16];
    
    label17.frame=CGRectMake(10, label16.frame.origin.y+label16.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label17.numberOfLines=0;
    label17.textColor = [UIColor redColor];
    [label17 sizeToFit];
    [ScrollView addSubview:label17];
    
    label18.frame=CGRectMake(10, label17.frame.origin.y+label17.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label18.numberOfLines=0;
    [label18 sizeToFit];
    [ScrollView addSubview:label18];
    
    
    label19.frame=CGRectMake(10, label18.frame.origin.y+label18.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label19.numberOfLines=0;
    [label19 sizeToFit];
    label19.textColor = [UIColor redColor];
    [ScrollView addSubview:label19];
    
    label20.frame=CGRectMake(10, label19.frame.origin.y+label19.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label20.numberOfLines=0;
    [label20 sizeToFit];
    [ScrollView addSubview:label20];
    
    
    
    
    
    ScrollView.contentSize=CGSizeMake(ScrollView.frame.size.width,label20.frame.origin.y+label20.frame.size.height+20);
    
    
    
    
}
    
    
-(void)Weightcontent{
    
    self.navigationItem.title = [MCLocalization stringForKey:@"basic"];
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label8 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label9 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label10 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label11 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label12 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(18);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(18);
    label8.font=Ralewayfont(14);
    label9.font=Ralewayfont(18);
    label10.font=Ralewayfont(14);
    label11.font=Ralewayfont(18);
    label12.font=Ralewayfont(14);
    self.navigationItem.title = [MCLocalization stringForKey:@"basic"];
    label1.text=[MCLocalization stringForKey:@"Weight1"];
    label2.text=[MCLocalization stringForKey:@"Weight2"];
    label3.text=[MCLocalization stringForKey:@"Weight3"];
    label4.text=[MCLocalization stringForKey:@"Weight4"];
    label5.text=[MCLocalization stringForKey:@"Weight5"];
    label6.text=[MCLocalization stringForKey:@"Weight6"];
    label7.text=[MCLocalization stringForKey:@"Weight7"];
    label8.text=[MCLocalization stringForKey:@"Weight8"];
    label9.text=[MCLocalization stringForKey:@"Weight9"];
    label10.text=[MCLocalization stringForKey:@"Weight10"];
    label11.text=[MCLocalization stringForKey:@"Weight11"];
    label12.text=[MCLocalization stringForKey:@"Weight12"];
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [ScrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [ScrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    label3.textColor = [UIColor redColor];
    [label3 sizeToFit];
    [ScrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [ScrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    label5.textColor = [UIColor redColor];
    [label5 sizeToFit];
    [ScrollView addSubview:label5];
    
    label6.frame=CGRectMake(10, label5.frame.origin.y+label5.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [ScrollView addSubview:label6];
    
    label7.frame=CGRectMake(10, label6.frame.origin.y+label6.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    label7.textColor = [UIColor redColor];
    [label7 sizeToFit];
    [ScrollView addSubview:label7];
    
    label8.frame=CGRectMake(10, label7.frame.origin.y+label7.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label8.numberOfLines=0;
    [label8 sizeToFit];
    [ScrollView addSubview:label8];
    
    label9.frame=CGRectMake(10, label8.frame.origin.y+label8.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label9.numberOfLines=0;
    [label9 sizeToFit];
    label9.textColor = [UIColor redColor];
    [ScrollView addSubview:label9];
    
    label10.frame=CGRectMake(10, label9.frame.origin.y+label9.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label10.numberOfLines=0;
    [label10 sizeToFit];
    [ScrollView addSubview:label10];
    
    label11.frame=CGRectMake(10, label10.frame.origin.y+label10.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label11.numberOfLines=0;
    label11.textColor = [UIColor redColor];
    [label11 sizeToFit];
    [ScrollView addSubview:label11];
    
    label12.frame=CGRectMake(10, label11.frame.origin.y+label11.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label12.numberOfLines=0;
    [label12 sizeToFit];
    [ScrollView addSubview:label12];
    ScrollView.contentSize=CGSizeMake(ScrollView.frame.size.width,label12.frame.origin.y+label12.frame.size.height+20);
    
}
    
    
-(void)asthmacontent{
    self.navigationItem.title = [MCLocalization stringForKey:@"basic"];
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label7 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    UILabel *label8 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
    
    label1.font=Ralewayfont(18);
    label2.font=Ralewayfont(14);
    label3.font=Ralewayfont(18);
    label4.font=Ralewayfont(14);
    label5.font=Ralewayfont(18);
    label6.font=Ralewayfont(14);
    label7.font=Ralewayfont(18);
    label8.font=Ralewayfont(14);
    
    self.navigationItem.title = [MCLocalization stringForKey:@"basic"];
    label1.text=[MCLocalization stringForKey:@"Asthm1"];
    label2.text=[MCLocalization stringForKey:@"Asthm2"];
    label3.text=[MCLocalization stringForKey:@"Asthm3"];
    label4.text=[MCLocalization stringForKey:@"Asthm4"];
    label5.text=[MCLocalization stringForKey:@"Asthm5"];
    label6.text=[MCLocalization stringForKey:@"Asthm6"];
    label7.text=[MCLocalization stringForKey:@"Asthm7"];
    label8.text=[MCLocalization stringForKey:@"Asthm8"];
    label1.numberOfLines=0;
    [label1 sizeToFit];
    label1.textColor = [UIColor redColor];
    [ScrollView addSubview:label1];
    
    label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label2.numberOfLines=0;
    [label2 sizeToFit];
    [ScrollView addSubview:label2];
    
    label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label3.numberOfLines=0;
    label3.textColor = [UIColor redColor];
    [label3 sizeToFit];
    [ScrollView addSubview:label3];
    
    label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label4.numberOfLines=0;
    [label4 sizeToFit];
    [ScrollView addSubview:label4];
    
    label5.frame=CGRectMake(10, label4.frame.origin.y+label4.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label5.numberOfLines=0;
    label5.textColor = [UIColor redColor];
    [label5 sizeToFit];
    [ScrollView addSubview:label5];
    
    label6.frame=CGRectMake(10, label5.frame.origin.y+label5.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label6.numberOfLines=0;
    [label6 sizeToFit];
    [ScrollView addSubview:label6];
    
    label7.frame=CGRectMake(10, label6.frame.origin.y+label6.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label7.numberOfLines=0;
    label7.textColor = [UIColor redColor];
    [label7 sizeToFit];
    [ScrollView addSubview:label7];
    
    label8.frame=CGRectMake(10, label7.frame.origin.y+label7.frame.size.height+10, ScrollView.frame.size.width-20, 100);
    label8.numberOfLines=0;
    [label8 sizeToFit];
    [ScrollView addSubview:label8];
    ScrollView.contentSize=CGSizeMake(ScrollView.frame.size.width,label8.frame.origin.y+label8.frame.size.height+20);
    
}
    
    -(void)jointpaincontent {
        self.navigationItem.title = [MCLocalization stringForKey:@"basic"];
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
        UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
        UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ScrollView.frame.size.width-20, 100)];
        label1.font=Ralewayfont(18);
        label2.font=Ralewayfont(14);
        label3.font=Ralewayfont(18);
        label4.font=Ralewayfont(14);
        self.navigationItem.title = [MCLocalization stringForKey:@"basic"];
        label1.text=[MCLocalization stringForKey:@"Joint1"];
        label2.text=[MCLocalization stringForKey:@"Joint2"];
        label3.text=[MCLocalization stringForKey:@"Joint3"];
        label4.text=[MCLocalization stringForKey:@"Joint4"];
        label1.numberOfLines=0;
        [label1 sizeToFit];
        label1.textColor = [UIColor redColor];
        [ScrollView addSubview:label1];
        
        label2.frame=CGRectMake(10, label1.frame.origin.y+label1.frame.size.height+10, ScrollView.frame.size.width-20, 100);
        label2.numberOfLines=0;
        [label2 sizeToFit];
        [ScrollView addSubview:label2];
        
        label3.frame=CGRectMake(10, label2.frame.origin.y+label2.frame.size.height+10, ScrollView.frame.size.width-20, 100);
        label3.numberOfLines=0;
        label3.textColor = [UIColor redColor];
        [label3 sizeToFit];
        [ScrollView addSubview:label3];
        
        label4.frame=CGRectMake(10, label3.frame.origin.y+label3.frame.size.height+10, ScrollView.frame.size.width-20, 100);
        label4.numberOfLines=0;
        [label4 sizeToFit];
        [ScrollView addSubview:label4];
        
        ScrollView.contentSize=CGSizeMake(ScrollView.frame.size.width,label4.frame.origin.y+label4.frame.size.height+20);
        
    }
    
-(IBAction)backbutton:(id)sender
    {
        [[self navigationController] popViewControllerAnimated:YES];
    }
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
    /*
     #pragma mark - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
    
    @end

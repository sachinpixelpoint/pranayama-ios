//
//  DincharyaReport+CoreDataProperties.m
//  Pranayama
//
//  Created by Manish Kumar on 28/10/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "DincharyaReport+CoreDataProperties.h"

@implementation DincharyaReport (CoreDataProperties)
@dynamic anulomvilom,kapalbhati,bhramari,surya,chandra,bhastrika,sheetali,ujjayi,meditative,udgeeth,bahya;
@dynamic sarvangasana,halasana,vipritkarani,paschimottanasana,dhanurasana,balasana,hastapadasana,marjariasana,uttanasana,setu,virabhadrasana;
@dynamic dincharya_name,date;
@end

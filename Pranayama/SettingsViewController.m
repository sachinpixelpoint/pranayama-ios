//  SettingsViewController.m
//  AnulomaViloma
//
//  Created by Hirendra Sharma on 1/14/16.
//  Copyright © 2016 Hirendra Sharma. All rights reserved.
//

#import "SettingsViewController.h"
@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HomeMethod:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    self.screenName=@"Setting";
    int fontSize;
    int spaceHeight;
    if (DEVICE==IPAD) {
        fontSize=24;
        spaceHeight=16;
    }
    else{
        fontSize=18;
        spaceHeight=8;
        if (self.view.frame.size.height>568) {
            spaceHeight=10;
        }
    }
    
     ////// new
    CGFloat width =languageView.frame.size.width;
    CGFloat height = (languageView.frame.size.height-3)/4;
    englishButton.frame=CGRectMake(0, 0,width,height);
    hindiButton.frame=CGRectMake(0, height+1, width, height);
    frenchButton.frame=CGRectMake(0, 2*height+2, width, height);
    russionButton.frame=CGRectMake(0, 3*height+3, width, height);
    
   
    CGFloat a=self.view.frame.size.height-88;
    CGFloat b=(55*a)/100;
    TopView.frame=CGRectMake(8, 72, self.view.frame.size.width-16, b);
    bottomView.frame=CGRectMake(8,TopView.frame.origin.y+TopView.frame.size.height+spaceHeight , self.view.frame.size.width-16, a-b);
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"Raleway-Medium" size:fontSize]}];
    [self change];
    btn.layer.cornerRadius=4.0f;
    picker.datePickerMode = UIDatePickerModeTime;
    dateview.hidden=YES;
    viewvoice.hidden=YES;
    
    [picker addTarget:self action:@selector(getSelection:) forControlEvents:UIControlEventValueChanged];
    int pre=[[NSUserDefaults standardUserDefaults] floatForKey:preparationtime];
    preprationTime.text=[NSString stringWithFormat:@"%d",pre];
    preprationTime.delegate=self;
    [self setFont];
    
    BOOL isInAppSound=[[NSUserDefaults standardUserDefaults] boolForKey:kISSound];
    BOOL isInAppSoundEverSec=[[NSUserDefaults standardUserDefaults] boolForKey:kISSoundEver];
    BOOL isInAppVibrate=[[NSUserDefaults standardUserDefaults] boolForKey:kISVibrator];
    BOOL isInAppKeepDisplay=[[NSUserDefaults standardUserDefaults] boolForKey:kISKeepDisplay];
    
    NSDate *date=[[NSUserDefaults standardUserDefaults] objectForKey:kldate];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"hh:mm a"];
    NSString *str=[outputFormatter stringFromDate:date];
    [dateButton setTitle:str forState:UIControlStateNormal];
    NSDate *pickerdate=[[NSDate alloc]init];
    pickerdate=[outputFormatter dateFromString:str];
    picker.date=pickerdate;
    
    languageView.hidden=YES;
    
    [englishButton setTitle:@"English" forState:UIControlStateNormal];
    [hindiButton setTitle:@"हिंदी" forState:UIControlStateNormal];
    [russionButton setTitle:@"русский" forState:UIControlStateNormal];
    [frenchButton setTitle:@"français" forState:UIControlStateNormal];
    
    if (!isInAppSound) {
        btnSound.on=NO;
    }
    if (!isInAppSoundEverSec) {
        btnSoundEverSec.on=NO;
    }
    if (!isInAppVibrate) {
        btnVibrator.on=NO;
    }
    if (!isInAppKeepDisplay) {
        btnKeepDisplay.on=NO;
    }
    
    float value = [[NSUserDefaults standardUserDefaults] floatForKey:kVolume];
    volumeSlider.value=value;
    // Do any additional setup after loading the view.
}


- (void)HomeMethod:(NSNotification *)notification {
    [[CommonSounds sharedInstance] appDidBecomeActive:notification navigation:self.navigationController];

}

-(void)getSelection:(id)sender{
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"hh:mm a"];
    
    NSString *pickerTime=[outputFormatter stringFromDate:picker.date];
    NSDate *pickerDate1=[outputFormatter dateFromString:pickerTime];
    [dateButton setTitle:pickerTime forState:UIControlStateNormal];
    
    [outputFormatter setDateFormat:@"HH"];
    int HH=[[outputFormatter stringFromDate:pickerDate1] intValue];
    
    [outputFormatter setDateFormat:@"mm"];
    int mm=[[outputFormatter stringFromDate:pickerDate1] intValue];
    
    NSDate *oldDate =NSDate.date; // Or however you get it.
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components:unitFlags fromDate:oldDate];
    comps.hour   = HH;
    comps.minute = mm;
    comps.second = 00;
    NSDate *newdate = [calendar dateFromComponents:comps];
    
    [[NSUserDefaults standardUserDefaults] setObject:newdate forKey:kldate];
    
}


-(IBAction)languagebutton:(id)sender
{
    languageView.hidden=NO;
    dateview.hidden=YES;
    languageView.layer.shadowColor = [UIColor grayColor].CGColor;
    languageView.layer.shadowOffset = CGSizeMake(-5, 5);
    languageView.layer.shadowOpacity = 1;
    languageView.layer.shadowRadius = 2.0;
    [preprationTime resignFirstResponder];
}


-(IBAction)englishbutton:(id)sender{
    [language setTitle:@"English" forState:UIControlStateNormal];
    languageView.hidden=YES;
    [MCLocalization sharedInstance].language = @"en";
    [[NSUserDefaults standardUserDefaults] setObject:@"English" forKey:Languagevalue];
    [self change];
    }


-(IBAction)hidibutton:(id)sender{
    [language setTitle:@"हिंदी" forState:UIControlStateNormal];
    languageView.hidden=YES;
    [[NSUserDefaults standardUserDefaults] setObject:@"Hindi" forKey:Languagevalue];
    [MCLocalization sharedInstance].language = @"hi";
    [self change];
    
}

-(IBAction)russionbutton:(id)sender{
    [language setTitle:@"русский" forState:UIControlStateNormal];
    languageView.hidden=YES;
    [[NSUserDefaults standardUserDefaults]setObject:@"Russion" forKey:Languagevalue];
    [MCLocalization sharedInstance].language = @"rs";
    [self change];
    }


-(IBAction)frenchbutton:(id)sender{
    
   [language setTitle:@"français" forState:UIControlStateNormal];
     languageView.hidden=YES;
    [[NSUserDefaults standardUserDefaults] setObject:@"français" forKey:Languagevalue];
    [MCLocalization sharedInstance].language = @"fr";
    [self change];
 }

-(IBAction)soundTypeButton:(id)sender{
    viewvoice.hidden=NO;
    viewvoice.layer.shadowColor = [UIColor grayColor].CGColor;
    viewvoice.layer.shadowOffset = CGSizeMake(-5, 5);
    viewvoice.layer.shadowOpacity = 1;
    viewvoice.layer.shadowRadius = 2.0;
}

-(IBAction)voiceButton:(id)sender{
    [soundTypeBtn setTitle:voiceBtn.titleLabel.text forState:UIControlStateNormal];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:soundType];
    viewvoice.hidden=YES;
}

-(IBAction)TuneButton:(id)sender{
    [soundTypeBtn setTitle:tuneBtn.titleLabel.text forState:UIControlStateNormal];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:soundType];
    viewvoice.hidden=YES;
  }



//////////lable font///////

-(void)setFont{
    int fontSize;
    if (DEVICE==IPAD) {
        fontSize=25;
    }
    else{
        fontSize=14;
    }
    lblSound.font=Ralewayfont(fontSize);
    lblSoundEverSec.font=Ralewayfont(fontSize);
    lblVibrator.font=Ralewayfont(fontSize);
    lblVolume.font=Ralewayfont(fontSize);
    lblKeepDisplay.font=Ralewayfont(fontSize);
    lblLanguage.font=Ralewayfont(fontSize);
     reminder.font=Ralewayfont(fontSize);
     setPreprationTime.font=Ralewayfont(fontSize);
     language. titleLabel. font=Ralewayfont(fontSize);
     soundTypelbl.font=Ralewayfont(fontSize);
     voiceBtn.titleLabel. font=Ralewayfont(fontSize);
     tuneBtn. titleLabel. font=Ralewayfont(fontSize);
     topsetSoundLbl.font=Ralewayfont(fontSize);
     topOthersettongLbl.font=Ralewayfont(fontSize);
    soundTypeBtn.titleLabel.font=Ralewayfont(fontSize);
    
}


-(void)change{
    
    BOOL soundtype=[[NSUserDefaults standardUserDefaults]boolForKey:soundType];
    
    self.navigationItem.title=[MCLocalization stringForKey:@"setting"];
    lblSound.text=[MCLocalization stringForKey:@"sound"];
    lblSoundEverSec.text=[MCLocalization stringForKey:@"sound_sec"];
    lblVibrator.text=[MCLocalization stringForKey:@"vibration"];
    lblVolume.text=[MCLocalization stringForKey:@"volume"];
    lblKeepDisplay.text=[MCLocalization stringForKey:@"display"];
    lblLanguage.text=[MCLocalization stringForKey:@"language"];
    reminder.text=[MCLocalization stringForKey:@"Reminder"];
    setPreprationTime.text=[MCLocalization stringForKey:@"set_prepration_time"];
    [language setTitle:[MCLocalization stringForKey:@"lan"] forState:UIControlStateNormal];
    soundTypelbl.text=[MCLocalization stringForKey:@"soundtype"];
    [voiceBtn setTitle:[MCLocalization stringForKey:@"sound"] forState:UIControlStateNormal];
    [tuneBtn setTitle:[MCLocalization stringForKey:@"tone"] forState:UIControlStateNormal];
    topsetSoundLbl.text=[MCLocalization stringForKey:@"set_sound"];
    topOthersettongLbl.text=[MCLocalization stringForKey:@"other_setting"];
    if (soundtype) {
        [soundTypeBtn setTitle:[MCLocalization stringForKey:@"sound"] forState:UIControlStateNormal];
    }
    else{
        [soundTypeBtn setTitle:[MCLocalization stringForKey:@"tone"] forState:UIControlStateNormal];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshTable" object:nil];
    
}
-(IBAction)backbutton:(id)sender{
    [[CommonSounds sharedInstance] NavigationMethod:self.navigationController];
    
}

-(IBAction)changeVolumeSlider:(UISlider*)sender
{
    float sliderValue= (float)sender.value;
    NSLog(@"%f",sliderValue);
    [[NSUserDefaults standardUserDefaults] setFloat:sliderValue forKey:kVolume];
}

- (IBAction)soundSwitch:(id)sender{
    if([sender isOn]){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kISSound];
        NSLog(@"Switch is ON");
    } else{
        NSLog(@"Switch is OFF");
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kISSound];
        [btnSoundEverSec setOn:NO animated:YES];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kISSoundEver];
        languageView.hidden=YES;
        dateview.hidden=YES;
     }

}

- (IBAction)soundEverSecSwitch:(id)sender{
    if([sender isOn]){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kISSoundEver];
        NSLog(@"Switch is ON");
    } else{
        NSLog(@"Switch is OFF");
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kISSoundEver];
        languageView.hidden=YES;
        dateview.hidden=YES;
    }}

- (IBAction)vibratorSwitch:(id)sender{
    if([sender isOn]){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kISVibrator];
        NSLog(@"Switch is ON");
    } else{
        NSLog(@"Switch is OFF");
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kISVibrator];
        languageView.hidden=YES;
        dateview.hidden=YES;
     }
 }

- (IBAction)keepDisplayOnSwitch:(id)sender{
    if([sender isOn]){
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kISKeepDisplay];
        NSLog(@"Switch is ON");
    } else{
        NSLog(@"Switch is OFF");
        [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kISKeepDisplay];
        languageView.hidden=YES;
        dateview.hidden=YES;
    }
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    languageView.hidden=YES;
    dateview.hidden=YES;
    viewvoice.hidden=YES;
    int prepare=[preprationTime.text intValue];
    [[NSUserDefaults standardUserDefaults] setInteger:prepare forKey:preparationtime];
    [preprationTime resignFirstResponder];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //first, check if the new string is numeric only. If not, return NO;
    NSCharacterSet *characterSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    if ([newString rangeOfCharacterFromSet:characterSet].location != NSNotFound)
    {
        return NO;
    }
    return [newString doubleValue] <= 10;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.25 animations:^
     {
         
             CGRect newFrame = [self.view frame];
             newFrame.origin.y -= 170;
             [self.view setFrame:newFrame];
         }
         
     completion:^(BOOL finished)
     {
         
     }];
    
    return YES;
}

- (BOOL) textFieldShouldEndEditing:(UITextField*)textField
{
    [UIView animateWithDuration:0.25 animations:^
     {
         CGRect newFrame = [self.view frame];
         newFrame.origin.y += 170;
         [self.view setFrame:newFrame];
         
     }completion:^(BOOL finished)
     {
         
     }];
    
    return YES;
}



- (void)changeSwitch:(id)sender{
    
    if([sender isOn]){
        dateButton.hidden=NO;
        NSLog(@"Switch is ON");
        [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:klReminder];
    } else{
        
        NSLog(@"Switch is OFF");
        [[NSUserDefaults standardUserDefaults] setBool:NO  forKey:klReminder];
        dateview.hidden=YES;
        dateButton.hidden=YES;
     }
}
-(IBAction)datebutton:(id)sender{
    dateview.hidden=NO;
    languageView.hidden=YES;
}
-(IBAction)showdatepicker:(id)sender
{
    [self changeSwitch:sender];
    [preprationTime resignFirstResponder];
}

-(IBAction)donebutton:(id)sender{
    dateview.hidden=YES;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

//
//  GuruVersionVC.swift
//  Pranayama
//
//  Created by Manish Kumar on 09/11/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

import UIKit
import StoreKit

class GuruVersionVC: UIViewController ,SKProductsRequestDelegate,SKPaymentTransactionObserver {
    
    @IBOutlet var scrollview : UIScrollView!
    @IBOutlet var appICon : UIImageView!
    @IBOutlet var GuruLbl : UILabel!
    @IBOutlet var UnlockLbl : UILabel!
    @IBOutlet var pranaLbl : UILabel!
    @IBOutlet var pranaImg : UIImageView!
    @IBOutlet var yogaLbl : UILabel!
    @IBOutlet var yogaImg : UIImageView!
    @IBOutlet var habitLbl : UILabel!
    @IBOutlet var habitImg : UIImageView!
    @IBOutlet var dincharyaLbl : UILabel!
    @IBOutlet var dincharyaImg : UIImageView!
    
    @IBOutlet var BottomVIew : UIView!
    
    let IAP_Product_Id = "com.pixelpranayama.app.dincharya"
    
    @IBOutlet var perchageBtn : UIButton!
    var productsArray: Array<SKProduct?> = []

    override func viewDidLoad() {
        super.viewDidLoad()
        SKPaymentQueue.default().add(self)
        // Do any additional setup after loading the view.
        self.disableSlidePanGestureForRightMenu()
        self.setText()
        self.custumDesigns()
        
        let networkReachability = Reachability.forInternetConnection() as Reachability
        let  networkstatus = networkReachability.currentReachabilityStatus()
        if networkstatus == .NotReachable{
            if #available(iOS 8.0, *) {
                let actionSheetController = UIAlertController(title: "7 Pranayama", message: "There is no internet connection", preferredStyle: UIAlertControllerStyle.alert)
                let OkBtn = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel) { (action) -> Void in
                                    }
                actionSheetController.addAction(OkBtn)
                
                perchageBtn.isUserInteractionEnabled=false;
                self.present(actionSheetController, animated: true, completion: nil)
            }
            
        }
        else{
            if SKPaymentQueue.canMakePayments() {
                let set : Set<String> = [self.IAP_Product_Id]
                let productsRequest = SKProductsRequest(productIdentifiers: set)
                productsRequest.delegate = self;
                productsRequest.start();
                self.loadingWindow()
            }
            else {
                print("Cannot perform In App Purchases.")
            }
        }
    }
    
    func setText() -> Void {
        let lan = UserDefaults.standard.object(forKey: Languagevalue) as! NSString
        if lan.isEqual(to: Hindi) {
            UnlockLbl.text=MCLocalization.string(forKey: "unlock")
            pranaLbl.text=MCLocalization.string(forKey: "unlock_prana")
            yogaLbl.text=MCLocalization.string(forKey: "unlock_yoga")
            habitLbl.text=MCLocalization.string(forKey: "unlock_habit")
            dincharyaLbl.text=MCLocalization.string(forKey: "unlock_dincharya")
        }
        else if lan.isEqual(to: English){
            
            UnlockLbl.text="Unlock advanced features and help us to improve the app by supporting us."
            pranaLbl.text="Get access to more pranayamas."
            yogaLbl.text="Get access to more yogasanas."
            habitLbl.text="Get access to more habits : Asthma, Joint Pain and Weight Loss."
            dincharyaLbl.text="You can make your own package of your schedule by selection pranayamas and asanas you wish."
        }
        else{
            UnlockLbl.text=MCLocalization.string(forKey: "unlock")
            pranaLbl.text=MCLocalization.string(forKey: "unlock_prana")
            yogaLbl.text=MCLocalization.string(forKey: "unlock_yoga")
            habitLbl.text=MCLocalization.string(forKey: "unlock_habit")
            dincharyaLbl.text=MCLocalization.string(forKey: "unlock_dincharya")
        }
        GuruLbl.text="Guru Version"
        
        var fontsize : CGFloat!
        var fontsize2 : CGFloat!
        
        if (UI_USER_INTERFACE_IDIOM() == .pad) {
            fontsize = 24
            fontsize2 = 20
        }
        else{
            fontsize = 18
            fontsize2 = 14
        }
        
        GuruLbl.font=Ralewayfont(fontsize)
        pranaLbl.font=Ralewayfont(fontsize2)
        yogaLbl.font=Ralewayfont(fontsize2)
        habitLbl.font=Ralewayfont(fontsize2)
        dincharyaLbl.font=Ralewayfont(fontsize2)
        self.navigationItem.title="Guru Version"
        UINavigationBar.appearance().titleTextAttributes=[NSFontAttributeName: Ralewayfont(fontsize),NSForegroundColorAttributeName:UIColor.white]
    }
    
    func custumDesigns() -> Void {
        
        appICon.frame = CGRect(x: self.view.frame.size.width/2-appICon.frame.size.width/2, y: 84, width: appICon.frame.size.width, height: appICon.frame.size.height)
        GuruLbl.frame = CGRect(x: self.view.frame.size.width/2-GuruLbl.frame.size.width/2, y: appICon.frame.origin.y+appICon.frame.size.height+5, width: GuruLbl.frame.size.width, height: GuruLbl.frame.size.height)
        scrollview.frame = CGRect(x: 0, y: GuruLbl.frame.origin.y+GuruLbl.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height-(GuruLbl.frame.origin.y+GuruLbl.frame.size.height+BottomVIew.frame.size.height))
        BottomVIew.frame = CGRect(x: 0, y: self.view.frame.size.height-BottomVIew.frame.size.height, width: BottomVIew.frame.size.width, height: BottomVIew.frame.size.height)
        
        UnlockLbl.frame = CGRect(x: 10, y: UnlockLbl.frame.origin.y, width: scrollview.frame.size.width-20, height: UnlockLbl.frame.size.height)
        pranaLbl.frame = CGRect(x:pranaImg.frame.size.width+20, y: pranaLbl.frame.origin.y, width:scrollview.frame.size.width-(pranaImg.frame.size.width+30) , height: pranaLbl.frame.size.height)
        yogaLbl.frame = CGRect(x:yogaImg.frame.size.width+20, y: yogaLbl.frame.origin.y, width:scrollview.frame.size.width-(yogaImg.frame.size.width+30) , height: yogaLbl.frame.size.height)
        habitLbl.frame = CGRect(x:habitImg.frame.size.width+20, y: habitLbl.frame.origin.y, width:scrollview.frame.size.width-(habitImg.frame.size.width+30) , height: habitLbl.frame.size.height)
        dincharyaLbl.frame = CGRect(x:dincharyaImg.frame.size.width+20, y: dincharyaLbl.frame.origin.y, width:scrollview.frame.size.width-(dincharyaImg.frame.size.width+30) , height: dincharyaLbl.frame.size.height)
        
    }
    
    
    @IBAction func PerchageBtn_Method (_ sendar: AnyObject){
        
        let payment = SKPayment(product: self.productsArray[0]! as SKProduct)
        SKPaymentQueue.default().add(payment)
        SKPaymentQueue.default().restoreCompletedTransactions()
        self.loadingWindow()

    }
    
    // MARK: SKProductsRequestDelegate method implementation
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        dismiss(animated: true, completion: nil)
        print(response.products)
        if response.products.count != 0 {
            for product in response.products {
                productsArray.append(product )
            }
        }
        else {
            print("There are no products.")
        }
        
        if response.invalidProductIdentifiers.count != 0 {
            print(response.invalidProductIdentifiers.description)
        }
    }
    
    
    // MARK: SKPaymentTransactionObserver method implementation
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        dismiss(animated: true, completion: nil)
        for transaction in transactions {
            switch transaction.transactionState {
            case SKPaymentTransactionState.purchased:
                print("Transaction completed successfully.")
                SKPaymentQueue.default().finishTransaction(transaction)
                UserDefaults.standard.set(true, forKey: Is_Perchaged)
                self.navigationController!.popViewController(animated: true)
                
            case SKPaymentTransactionState.failed:
                print("Transaction Failed");
                SKPaymentQueue.default().finishTransaction(transaction)
                //                transactionInProgress = false
            default:
                print(transaction.transactionState.rawValue)
            }
        }
    }
    
    @IBAction func backbutton(_ sender: AnyObject) {
        self.navigationController!.popViewController(animated: true)
    }
    
//    
//    func paymentQueue(_ queue: SKPaymentQueue, updatedDownloads downloads: [SKDownload]) {
//        
//    }
//    
    
//    func paymentQueue(queue: SKPaymentQueue,
//                      updatedTransactions transactions: [SKPaymentTransaction]){
//        
//    }
    
    
    
     func loadingWindow() -> Void {
        if #available(iOS 8.0, *) {
            let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
            alert.view.tintColor = UIColor.black
            let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50)) as UIActivityIndicatorView
            loadingIndicator.hidesWhenStopped = true
            loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
            loadingIndicator.startAnimating();
            
            alert.view.addSubview(loadingIndicator)
            present(alert, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
        }
    }

    

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

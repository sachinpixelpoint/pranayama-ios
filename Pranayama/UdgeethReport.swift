//
//  UdgeethReport.swift
//  Pranayama
//
//  Created by Manish Kumar on 09/09/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

import UIKit

class UdgeethReport: GAITrackedViewController,UIGestureRecognizerDelegate {
    
    let kAppDel = (UIApplication.shared.delegate! as! AppDelegate)
    
    @IBOutlet var topview: UIView!
    @IBOutlet var sn: UILabel!
    @IBOutlet var levellbl: UILabel!
    @IBOutlet var roundlbl: UILabel!
    @IBOutlet var datelbl: UILabel!
    @IBOutlet var timelbl: UILabel!
    @IBOutlet var InHoldExlbl: UILabel!
    var reportArray : NSMutableArray! = NSMutableArray()
    @IBOutlet var tblReport: UITableView!
    var cell: UITableViewCell!
    var p = CGPoint.zero
    var indexpath1: IndexPath!
    var lpgr: UILongPressGestureRecognizer!
    var tap: UITapGestureRecognizer!
    var cl = 0
    var selectunselectarray : NSMutableArray! = NSMutableArray()
    var entityFontSize : CGFloat = 0
    var managedObjectContext: NSManagedObjectContext!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: (#selector(self.appDidBecomeActive(_:))), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        self.screenName="Udgeeth Report"
        // Do any additional setup after loading the view.
        
//        cell.selectionStyle = .None
        var fontSize : CGFloat = 0
        var entityLblFont : CGFloat = 0
        var InExFont : CGFloat = 0
        if UI_USER_INTERFACE_IDIOM() == .pad {
            fontSize = 24
            entityLblFont = 18
            InExFont = 14
            entityFontSize = 19
        }
        else {
            fontSize = 18
            entityLblFont = 12
            InExFont = 8
            entityFontSize = 13
        }
        UINavigationBar.appearance().titleTextAttributes=[NSFontAttributeName: Ralewayfont(fontSize),NSForegroundColorAttributeName:UIColor.white]
        tblReport.allowsSelection = false
        self.managedObjectContext = kAppDel.managedObjectContext!
        tblReport.allowsMultipleSelection = true
        self.automaticallyAdjustsScrollViewInsets = false
        sn.font = Ralewayfont(entityLblFont)
        levellbl.font = Ralewayfont(entityLblFont)
        roundlbl.font = Ralewayfont(entityLblFont)
        datelbl.font = Ralewayfont(entityLblFont)
        timelbl.font = Ralewayfont(entityLblFont)
        InHoldExlbl.font = Ralewayfont(InExFont)
        reportArray = NSMutableArray(array: kAppDel.fetchAllReportFromDatabse(accordingToType: "Udgeeth"))
        selectunselectarray = NSMutableArray()
        for _ in 0..<reportArray.count {
            let st = "NO" as String
            selectunselectarray.add(st)
        }
        print(selectunselectarray)
        self.Noresult()
        self.setlblheight()
        self.gesture()
        self.ChangeText()

    }
    
    func appDidBecomeActive(_ notification:Notification) -> Void {
        
        CommonSounds.sharedInstance().appDidBecomeActive(notification, navigation: self.navigationController)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.disableSlidePanGestureForRightMenu()
    }
    
    func setlblheight() {
         let Device : Int32 = Int32(UIScreen.main.bounds.size.height)
        switch Device {
        case iPhone4:
            topview.frame = CGRect(x: 8, y: 75, width: 305, height: 40)
            tblReport.frame = CGRect(x: 8, y: 126, width: 305, height: 358)
        case iPhone5:
            topview.frame = CGRect(x: 8, y: 74, width: 305, height: 40)
            tblReport.frame = CGRect(x: 8, y: 124, width: 305, height: 449)
        case iPhone6:
            topview.frame = CGRect(x: 8, y: 74, width: 359, height: 40)
            tblReport.frame = CGRect(x: 8, y: 124, width: 359, height: 548)
        case iPhone6P:
            topview.frame = CGRect(x: 8, y: 74, width: 398, height: 40)
            tblReport.frame = CGRect(x: 8, y: 124, width: 398, height: 617)
        case iPadPro:
            tblReport.frame = CGRect(x: 8, y: 176, width: 1008, height: 1190)
        default:
            break
        }
    }

    
    func gesture() {
        
        lpgr = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress))
        lpgr.minimumPressDuration = 0.5
        //seconds
        lpgr.delegate = self
        tblReport.addGestureRecognizer(lpgr)
    }
    
    func ChangeText() {
        
        self.navigationItem.title = MCLocalization.string(forKey: "report")!
        sn.text = MCLocalization.string(forKey: "No")!
        levellbl.text = MCLocalization.string(forKey: "level")!
        roundlbl.text = MCLocalization.string(forKey: "Rounds")!
        datelbl.text = MCLocalization.string(forKey: "date")!
        timelbl.text = MCLocalization.string(forKey: "time")!
        InHoldExlbl.text = MCLocalization.string(forKey: "InExhale")!
    }
    
    func handleLongPress(_ gestureRecognizer: UILongPressGestureRecognizer) {
        p = gestureRecognizer.location(in: tblReport)
        indexpath1 = tblReport.indexPathForRow(at: p)
        
        if indexpath1 == nil {
            print("long press on table view but not on a row")
        }
        else if gestureRecognizer.state == .began {
            let theCell = tblReport.cellForRow(at: indexpath1)
            print("long press on table view at row \(Int(indexpath1.row))")
            theCell?.backgroundColor = UIColor.init(red: 19/255, green: 155/255, blue: 231/255, alpha: 1)
            cl += 1
            selectunselectarray[indexpath1.row] = "YES" as String
            tap = UITapGestureRecognizer(target: self, action: #selector(self.cellTapped))
            tap.numberOfTapsRequired = 1
            tap.numberOfTouchesRequired = 1
            tap.delegate = self
            tblReport.addGestureRecognizer(tap)
        }
        else {
            print("gestureRecognizer.state = \(gestureRecognizer.state)")
        }
    }
    
    func cellTapped(_ tap1: UITapGestureRecognizer) {
        let q = tap1.location(in: tblReport)
        indexpath1 = tblReport.indexPathForRow(at: q)
        if indexpath1 != nil {
            let theCell = tblReport.cellForRow(at: indexpath1)
            if theCell?.backgroundColor == UIColor.clear {
                theCell?.backgroundColor = UIColor.init(red: 19/255, green: 155/255, blue: 231/255, alpha: 1)
                cl += 1
                selectunselectarray[indexpath1.row] = "YES" as String
            }
            else {
                theCell?.backgroundColor = UIColor.clear
                cl -= 1
                selectunselectarray[indexpath1.row] = "NO" as String
            }
            if cl == 0 {
                tap1.isEnabled = false
                self.gesture()
            }
        }
    }
    
    @IBAction func deletebutton(_ sendar: AnyObject) {
        var j = 0
        for var i  in 0..<selectunselectarray.count  {
            i = i-j
            let a = IndexPath(row: i, section: 0)
            if (selectunselectarray[i] as! String == "YES") {
                let repoEntity = reportArray[(a as NSIndexPath).row] as! Report
                self.managedObjectContext.delete(repoEntity as NSManagedObject)
                do {
                    try self.managedObjectContext?.save()
                } catch {
                    print(error)
                }
                reportArray.removeObject(at: (a as NSIndexPath).row)
                selectunselectarray.removeObject(at: (a as NSIndexPath).row)
                tblReport.deleteRows(at: [a], with: .automatic)
                j = j+1
            }
        }
        tap?.isEnabled = false
        cl = 0
        self.gesture()
        tblReport.reloadData()
        self.Noresult()
    }
    
    func Noresult() {
        if reportArray.count == 0 {
            let noDataLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tblReport.bounds.size.width, height: tblReport.bounds.size.height))
            noDataLabel.text = "No Record Found"
            noDataLabel.textColor = UIColor.lightGray
            noDataLabel.textAlignment = .center
            tblReport.backgroundView = noDataLabel
        }
        else {
            tblReport.backgroundView = nil
        }
    }
    
    @IBAction func backbutton(_ sender: AnyObject) {
        self.navigationController!.popViewController(animated: true)
    }
    
//////////// Table view data source
    
     func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reportArray.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        let listingCellIdentifier = "ReportCustomCell"
        cell = (tableView.dequeueReusableCell(withIdentifier: listingCellIdentifier)! )
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        let lblNo = (cell.viewWithTag(1)! as! UILabel)
        let lblLevel = (cell.viewWithTag(2)! as! UILabel)
        let lblRound = (cell.viewWithTag(3)! as! UILabel)
        let lblDate = (cell.viewWithTag(4)! as! UILabel)
        let lbltime = (cell.viewWithTag(5)! as! UILabel)
        lblNo.font = Ralewayfont(entityFontSize)
        lblLevel.font = Ralewayfont(entityFontSize)
        lblRound.font = Ralewayfont(entityFontSize)
        lblDate.font = Ralewayfont(entityFontSize)
        lbltime.font = Ralewayfont(entityFontSize)
        let reportEntity = reportArray[(indexPath as NSIndexPath).row] as! Report
        reportEntity.setValue("\(UInt((indexPath as NSIndexPath).row) + 1)", forKey: "reportSerialNo")
        lblNo.text = reportEntity.reportSerialNo
        lblLevel.text = reportEntity.reportLevel
        lblRound.text = reportEntity.reportRounds
        let date : Date? = reportEntity.reportDate
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yy"
        lblDate.text = dateFormatter.string(from: date!)
        dateFormatter.dateFormat = "hh:mma"
        lbltime.text = dateFormatter.string(from: date!)
        if (selectunselectarray[(indexPath as NSIndexPath).row] as! String == "YES") {
            cell.backgroundColor = UIColor.init(red: 19/255, green: 155/255, blue: 231/255, alpha: 1)
        }
        return cell
    }
    
     func tableView(_ tableView: UITableView, canEditRowAtIndexPath indexPath: IndexPath) -> Bool {
        // Return YES if you want the specified item to be editable.
        if cl > 0 {
            return false
        }
        return true
    }
    // Override to support editing the table view.
    
     func tableView(_ tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: IndexPath) {
        if editingStyle == .delete {
            let repoEntity = reportArray[(indexPath as NSIndexPath).row] as! Report
            self.managedObjectContext.delete(repoEntity as NSManagedObject)
            do {
                try self.managedObjectContext?.save()
            } catch {
                print(error)
            }
            reportArray.removeObject(at: (indexPath as NSIndexPath).row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tblReport.reloadData()
            //add code here for when you hit delete
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

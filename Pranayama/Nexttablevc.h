//
//  Nexttablevc.h
//  Pranayama
//
//  Created by Manish Kumar on 09/04/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Nexttablevc : GAITrackedViewController<UITableViewDelegate,UITableViewDataSource>

{
    NSMutableArray *homelist,*homeList2,*noArray;
    NSMutableArray *nextList,*nextList2;
    CGFloat fontSize;
    CGFloat fontSize2;
    UIButton*btn;
    NSTimer *timer;
    NSArray *movearray;
 }

@property(nonatomic,strong) IBOutlet UITableView *table;
@end

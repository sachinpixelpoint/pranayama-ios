//
//  UjjayiActionVC.h
//  Pranayama
//
//  Created by Manish Kumar on 17/06/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol Ujjayidelegate <NSObject>

-(void)playsound;

@end

@interface UjjayiActionVC : GAITrackedViewController
{
    IBOutlet UIView *topview;
    
    IBOutlet UILabel *totaltimelbl;
    IBOutlet UILabel *actionlbl;
    
    IBOutlet UIButton *btnStop;
    IBOutlet UIButton *btnpuse;
    
    IBOutlet UILabel *lblHours;
    IBOutlet UILabel *lblminutes;
    IBOutlet UILabel *lblseconds;
    IBOutlet UILabel *lblTimer;
    IBOutlet UILabel *lblmode;
    IBOutlet UIImageView *imageview;
    
    NSTimer *startTimer;
    BOOL isPause;
    BOOL PlayStop;
    int lblinhale1;
    int lblhold1;
    int lblexhale1;
    int seconds;
    int minutes;
    int hours;
    int roundForReport;
    int ujjayiCount;
    int prepare;
}
@property (nonatomic,assign)id <Ujjayidelegate>delegate;
@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic) int rounds;
@property (nonatomic) int lblinhale;
@property (nonatomic) int lblexhale;
@property (nonatomic) int lblhold;
@property (nonatomic) int totalTime;
@end

//
//  AppDelegate.m
//  Pranayama
//
//  Created by Manish Kumar on 30/03/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import "AppDelegate.h"
#import "GAI.h"
#import "Appirater.h"



static NSString *const kTrackingId = @"UA-76568359-2";
static NSString *const kAllowTracking = @"allowTracking";

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSDictionary *appDefaults = @{kAllowTracking: @(YES)};
    [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
    [GAI sharedInstance].optOut =
    ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
    [GAI sharedInstance].dispatchInterval = 10;
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    self.tracker = [[GAI sharedInstance] trackerWithName:@"Pranayama"
                                              trackingId:kTrackingId];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:@"firstLaunch"])
    {
        unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *comps = [calendar components:unitFlags fromDate:[NSDate date]];
        comps.hour   = 07;
        comps.minute = 00;
        comps.second = 00;
        NSDate *newdate = [calendar dateFromComponents:comps];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:Is_Perchaged];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:tableIntro];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:Introduction];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kISKeepDisplay];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kISSound];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kISSoundEver];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kISVibrator];
        [[NSUserDefaults standardUserDefaults] setObject:English forKey:Languagevalue];
        [[NSUserDefaults standardUserDefaults] setFloat:1 forKey:kVolume];
        [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:klReminder];
        [[NSUserDefaults standardUserDefaults] setObject:newdate forKey:kldate];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:HabbitCondition];
        [[NSUserDefaults standardUserDefaults] setInteger:3 forKey:preparationtime];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:isPause1];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:soundType];
        
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:anulomInhale];
        [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:anulomHold];
        [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:anulomExhale];
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:anulomRound];
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:anulomAfHold];
        
        [[NSUserDefaults standardUserDefaults] setInteger:60 forKey:kapalRound];
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:kapaltime];
        
        [[NSUserDefaults standardUserDefaults] setInteger:5 forKey:bhramariInhale];
        [[NSUserDefaults standardUserDefaults] setInteger:10 forKey:bhramariExhale];
        [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:bhramariRound];
        
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:suryaInhale];
        [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:suryaHold];
        [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:suryaExhale];
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:suryaRound];
        
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:chandraInhale];
        [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:chandraHold];
        [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:chandraExhale];
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:chandraRound];
        
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:bhastrikaInhale];
        [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:bhastrikaExhale];
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:bhastrikaRound];
        
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:sheetaliInhale];
        [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:sheetaliHold];
        [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:sheetaliExhale];
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:sheetaliRound];
        
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:ProbUjjayiInhale];
        [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:ProbUjjayiHold];
        [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:ProbUjjayiExhale];
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:ProbUjjayiRound];
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:meditativeRound];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstLaunch"];
    }
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:HabbitType];
    
    
    BOOL isKeepDisplay=[[NSUserDefaults standardUserDefaults] boolForKey:kISKeepDisplay];
    if (isKeepDisplay) {
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    }
    else{
        [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    }
    [[UIApplication sharedApplication] cancelLocalNotification:notifications];
    [[UIApplication sharedApplication] cancelLocalNotification:dincharyaNotification];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [application registerUserNotificationSettings:[UIUserNotificationSettings
                                                   settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|
                                                   UIUserNotificationTypeSound categories:nil]];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    
        
    
    NSDictionary * languageURLPairs = @{
                                        @"en":[[NSBundle mainBundle] URLForResource:@"en.json" withExtension:nil],
                                        @"hi":[[NSBundle mainBundle] URLForResource:@"hi.json" withExtension:nil],
                                        @"rs":[[NSBundle mainBundle] URLForResource:@"rs.json" withExtension:nil],
                                        @"fr":[[NSBundle mainBundle] URLForResource:@"fr.json" withExtension:nil],
                                        };
   
    [MCLocalization loadFromLanguageURLPairs:languageURLPairs defaultLanguage:@"en"];
     
    [MCLocalization sharedInstance].noKeyPlaceholder = @"[No '{key}' in '{language}']";
    
    [Appirater setAppId:@"1127298201"];
    [Appirater setDaysUntilPrompt:3];
    [Appirater setUsesUntilPrompt:3];
    [Appirater appLaunched:YES];
    
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:HabbitMainCondition];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:commonClassCond];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:waterMainCondition];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:DincharyaMainCon];
        
    UILocalNotification *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (notification)
    { 
        [self application:application didReceiveLocalNotification:notification];
    }
    
    
    
    return YES;
    
}


- (void)applicationWillResignActive:(UIApplication *)application {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:isPause1];
    [application setApplicationIconBadgeNumber:0];
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    [[UIApplication sharedApplication] cancelLocalNotification:notifications];
    BOOL isNotifie=[[NSUserDefaults standardUserDefaults] boolForKey:klReminder];
    NSDate *fDate=[[NSUserDefaults standardUserDefaults] objectForKey:kldate];
    NSString *habbitCondition= [[NSUserDefaults standardUserDefaults] objectForKey:HabbitCondition];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"hh:mm a"];
    
    ////Water notifications//////
    if ([[NSUserDefaults standardUserDefaults] boolForKey:waterSated]) {
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        float water=[[NSUserDefaults standardUserDefaults]floatForKey:notificationWater];
        NSInteger times=[[NSUserDefaults standardUserDefaults]integerForKey:notificationTimes];
        NSInteger interval=[[NSUserDefaults standardUserDefaults]integerForKey:notificationTimeInterval];
        NSDate *startdate=[[NSUserDefaults standardUserDefaults]objectForKey:riseTime];
        NSString *datestr=[outputFormatter stringFromDate:startdate];
        NSDate *date=[outputFormatter dateFromString:datestr];
        for (NSInteger i=0; i<times; i++) {
            
            waterNotification=[[UILocalNotification alloc] init];
            waterNotification.fireDate=[NSDate dateWithTimeInterval:i*interval*60 sinceDate:date];
            waterNotification.soundName = @"waterintake.mp3";
            waterNotification.timeZone=[NSTimeZone defaultTimeZone];
            waterNotification.alertBody=[NSString stringWithFormat:@"You have to drink %.02f ml of water",water];
            waterNotification.alertAction=@"go back";
            notifications.userInfo= @{@"information": [NSString stringWithFormat:@"Some information"]};
            waterNotification.repeatInterval=NSCalendarUnitDay;
            [[UIApplication sharedApplication] scheduleLocalNotification:waterNotification];
            
        }
    }
    
    NSString *fdatestr=[outputFormatter stringFromDate:fDate];
    NSDate *date2=[outputFormatter dateFromString:fdatestr];
    if (isNotifie==YES || habbitCondition!=nil) {
        notifications = [[UILocalNotification alloc] init];
        notifications.fireDate = [NSDate dateWithTimeInterval:24*60*60 sinceDate:date2];
        notifications.timeZone = [NSTimeZone defaultTimeZone];
        notifications.alertBody = [NSString stringWithFormat: @"Hi, Time to do Pranayama !"];
        notifications.alertAction = @"go back";
        notifications.userInfo= @{@"information": [NSString stringWithFormat:@"Some information"]};
        notifications.repeatInterval= NSCalendarUnitDay;
        notifications.soundName = UILocalNotificationDefaultSoundName;
        notifications.applicationIconBadgeNumber = 1;
        [[UIApplication sharedApplication] scheduleLocalNotification:notifications];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    }
    
    NSArray *dincharyaAr = self.FetchDincharyaFromDatabase;
    [[UIApplication sharedApplication] cancelLocalNotification:dincharyaNotification];
    if (dincharyaAr.count>0) {
        for (int i=0; i<dincharyaAr.count; i++) {
            Dincharya *dincharya = [dincharyaAr objectAtIndex:i];
            if (dincharya.notification_time != nil) {
                
                NSString *datestr = [outputFormatter stringFromDate:dincharya.notification_time];
                NSDate *DFdate = [outputFormatter dateFromString:datestr];
                dincharyaNotification = [[UILocalNotification alloc]init];
                dincharyaNotification.fireDate = DFdate;
                dincharyaNotification.soundName=UILocalNotificationDefaultSoundName;
                dincharyaNotification.timeZone=[NSTimeZone defaultTimeZone];
                dincharyaNotification.alertBody=[NSString stringWithFormat:@"Hi, Time to start %@ schedule!",dincharya.name];
                dincharyaNotification.alertAction=@"go back";
                dincharyaNotification.userInfo= @{@"information": [NSString stringWithFormat:@"Some information"]};
                dincharyaNotification.repeatInterval= NSCalendarUnitDay;
                dincharyaNotification.applicationIconBadgeNumber = 1;
                [[UIApplication sharedApplication] scheduleLocalNotification:dincharyaNotification];
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
                
            }
        }
    }
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    //[GAI sharedInstance].optOut =
    // ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:waterMainCondition];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:HabbitMainCondition];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:isPause1];
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.hirendra.AnulomaViloma" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Anulom" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Anulom.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Getting Report
-(NSArray*)fetchAllReportFromDatabseAccordingToType:(NSString*)type{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Report" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"reporttype = %@",type]];
    NSSortDescriptor *serial = [NSSortDescriptor sortDescriptorWithKey:@"reportDate" ascending:NO];
    fetchRequest.sortDescriptors = @[serial];
    NSError *error = nil;
    NSArray *items = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    return items;
}

#pragma mark - Getting Report
-(NSArray*)fetchReportFromDatabseAccordingToType:(NSString*)type{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Record" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"anulomtype = %@",type]];
    NSSortDescriptor *serial = [NSSortDescriptor sortDescriptorWithKey:@"anulomDate" ascending:NO];
    fetchRequest.sortDescriptors = @[serial];
    NSError *error = nil;
    NSArray *items = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    return items;
}

#pragma mark - Getting Report
-(NSArray*)fetchReportFromDatabse{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Record" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSSortDescriptor *serial = [NSSortDescriptor sortDescriptorWithKey:@"anulomDate" ascending:NO];
    fetchRequest.sortDescriptors = @[serial];
    NSError *error = nil;
    NSArray *items = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    return items;
}
/////For progress report///
-(NSArray*)fetchReportFromProgressDatabseAccordingToType:(NSString*)type{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Progress" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"progressType = %@",type]];
    NSSortDescriptor *serial = [NSSortDescriptor sortDescriptorWithKey:@"days" ascending:YES];
    fetchRequest.sortDescriptors = @[serial];
    NSError *error = nil;
    NSArray *items = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    return items;
}

///////Water report/////
-(NSArray*)fetchwaterReportFromDatabase{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Water" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSSortDescriptor *serial = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO];
    fetchRequest.sortDescriptors = @[serial];
    NSError *error = nil;
    NSArray *items = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    return items;
}

-(NSArray*)FetchDincharyaFromDatabase{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Dincharya" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSSortDescriptor *serial = [NSSortDescriptor sortDescriptorWithKey:@"notification_time" ascending:NO];
    fetchRequest.sortDescriptors = @[serial];
    NSError *error = nil;
    NSArray *items = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    return items;
}

-(NSArray*)FetchDincharyaFromDatabaseAcordingToName:(NSString*)Name{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Dincharya" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"name = %@",Name]];
    NSSortDescriptor *serial = [NSSortDescriptor sortDescriptorWithKey:@"notification_time" ascending:NO];
    fetchRequest.sortDescriptors = @[serial];
    NSError *error = nil;
    NSArray *items = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    return items;
}

-(NSArray*)FetchDincharyaReportFromDatabaseAcordingToName:(NSString*)Name{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"DincharyaReport" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"dincharya_name = %@",Name]];
    NSSortDescriptor *serial = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO];
    fetchRequest.sortDescriptors = @[serial];
    NSError *error = nil;
    NSArray *items = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    return items;
}


-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    
    
    if ([notification.alertBody containsString:@"water"]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:waterdelegate];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:waterMainCondition];
    }
    else if ([notification.alertBody containsString:@"schedule"]){
        [[UIApplication sharedApplication] cancelLocalNotification:dincharyaNotification];
        NSArray *array = [notification.alertBody componentsSeparatedByString:@" "];
        NSString *str=[array objectAtIndex:4];
        [[NSUserDefaults standardUserDefaults]setObject:str forKey:dincharyaName];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:DincharyaMainCon];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:DincharyaDelegate];
    }
    else{
        
        [[UIApplication sharedApplication] cancelLocalNotification:notifications];
        NSString *habbitCondition= [[NSUserDefaults standardUserDefaults] objectForKey:HabbitCondition];
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:@"dd-MM-YY"];
        NSString *date1=[outputFormatter stringFromDate:[[NSUserDefaults standardUserDefaults] objectForKey:kldate]];
        NSString *currentDate=[outputFormatter stringFromDate:[NSDate date]];
        if (![date1 isEqualToString:currentDate] && habbitCondition!=nil){
            if ([habbitCondition isEqualToString:@"Thyroid"] || [habbitCondition isEqualToString:@"Weight Loss"]) {
                [[NSUserDefaults standardUserDefaults]setObject:@"Sarvangasana" forKey:YogasanCondition];
            }
            else if ([habbitCondition isEqualToString:@"Insomnia"]){
                [[NSUserDefaults standardUserDefaults] setObject:@"Vipritkarani" forKey:YogasanCondition];
            }
            else if ([habbitCondition isEqualToString:@"Diabetes"] || [habbitCondition isEqualToString:@"Asthma"]){
                [[NSUserDefaults standardUserDefaults] setObject:@"Dhanurasana" forKey:YogasanCondition];
            }
            else if([habbitCondition isEqualToString:@"Migraine"]){
                [[NSUserDefaults standardUserDefaults]setObject:@"Hastapadasana" forKey:YogasanCondition];
            }
            else {
                [[NSUserDefaults standardUserDefaults] setObject:@"Balasana" forKey:YogasanCondition];
            }
            [[NSUserDefaults standardUserDefaults] setObject:habbitCondition forKey:HabbitType];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:HabbitMainCondition];
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:delegateCondition];

        }
    }
}



@end


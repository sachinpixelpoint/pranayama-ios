//
//  MeditativeActionVC.h
//  Pranayama
//
//  Created by Manish Kumar on 05/07/16.
//  Copyright © 2016 Manish Kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol meditativedelegate <NSObject>

-(void)playsound;

@end

@interface MeditativeActionVC : GAITrackedViewController
{
    IBOutlet UIView *topview;
    
    IBOutlet UILabel *totaltimelbl;
    IBOutlet UILabel *actionlbl;
    
    IBOutlet UIButton *btnStop;
    IBOutlet UIButton *btnpuse;
    
    IBOutlet UILabel *lblHours;
    IBOutlet UILabel *lblminutes;
    IBOutlet UILabel *lblseconds;
    IBOutlet UILabel *lblTimer;
    IBOutlet UILabel *lblmode;
    IBOutlet UIImageView *imageview;
    
    NSTimer *startTimer;
    BOOL isPause;
    BOOL PlayStop;
    int lblinhale,lblhold,lblexhale;
    int seconds,minutes,hours,roundForReport,meditativeCount,prepare,totalTime;

}
@property (nonatomic,assign)id <meditativedelegate>delegate;
@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic) int rounds;


@end
